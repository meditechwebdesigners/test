<?php 

// Error reporting.
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Automatically load classes when needed.
function apps_autoload($class){
	
	if (file_exists('classes\\' . $class . '.php')) {
	
		// File exists, so load it.
		require_once 'classes\\' . $class . '.php';
		
	} else {
	
		// File does not exist, so show error.
		throw new Exception('The ' . $class . ' class does not exist.');
	
	}
	
}
 
// Register the autoload function.
spl_autoload_register('apps_autoload');

?>