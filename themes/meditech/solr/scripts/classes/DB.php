<?php 

/**
 * Define database connection variables based on DEV, TEST, or LIVE url.
 */
$host = $_SERVER['HTTP_HOST'];

if (strpos($host,':8010') !== false || strpos($host,'staffdev') !== false || strpos($host,':8020') !== false || strpos($host,'stafftest') !== false) {
	define("DBHOST","atdmariadbtest.meditech.com");
} else {
	define("DBHOST","atdmariadblive.meditech.com");	
}
define("DBUSER","web");
define("DBPASS","meditech");
define("DBNAME","atwebapps");

/**
 * Database class.
 */
class DB {

	/**
	 * Define variables.
	 */
	private $host = DBHOST;
	private $user = DBUSER;
	private $pass = DBPASS;
	private $dbname = DBNAME;
	private $dbh;
	private $error;
	private $stmt;

	/**
	 * Connect to the database when an object is created.
	 */
	public function __construct() {
		$dsn = 'mysql:host='.$this->host.';dbname='.$this->dbname;
		$options = array(
            PDO::ATTR_PERSISTENT    => TRUE,
			PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION
		);
		try {
			$this->dbh = new PDO($dsn,$this->user,$this->pass,$options);
		} catch(PDOException $e) {
			$this->error = $e->getMessage();
		}
	}
	
	/**
	 * Disconnect from the database when an object is destroyed.
	 */
	public function __destruct() {
		$this->dbh = null;
	}
	
	/** 
	 * Prepare the query.
	 */
	public function query($query) {
		$this->stmt = $this->dbh->prepare($query);
	}
	
	/**
	 * Bind values passed into query. 
	 */
	public function bind($param,$value,$type=null) {
		if(is_null($type)) {
			switch(true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
		$this->stmt->bindValue($param,$value,$type);
	}
	
	/**
	 * Execute the prepared statement.
	 */
	public function execute() {
		return $this->stmt->execute();
	}
	
	/**
	 * Return all rows as an associative array.
	 */
	public function resultSet() {
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	
	/**
	 * Return id as property and name as value.
	 */
	public function resultObject() {
		$this->execute();
		$result = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
		$object = new stdClass();
		for ($i=0; $i<count($result); $i++) {
			$entry = $result[$i];
			$id = $entry["id"];
			$name = $entry["name"];
			$object->$id = $name;
		}
		return $object;
	}
	
	/**
	 * Return array of just ids.
	 */
	public function resultArray() {
		$this->execute();
		$result = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
		$array = array();
		for ($i=0; $i<count($result); $i++) {
			$entry = $result[$i];
			$id = $entry["id"];
			array_push($array,$id);
		}
		return $array;
	}	
	
	/** 
	 * Return single row as an associative array.
	 */
	public function single() {
		$this->execute();
		return $this->stmt->fetch(PDO::FETCH_ASSOC);
	}
	
	/**
	 * Return row count.
	 */
	public function rowCount() {
		return $this->stmt->rowCount();
	}
	
	/** 
	 * Return last insert id.
	 */
	public function lastInsertId() {
		return $this->dbh->lastInsertId();
	}
	
	/**
	 * Begin a transaction.
	 */
	public function begin() {
		return $this->dbh->beginTransaction();
	}
	
	/**
	 * End a transaction.
	 */
	public function end() {
		return $this->dbh->commit();
	}	
	
	/**
	 * Cancel a transaction and roll back changes.
	 */
	public function cancel() {
		return $this->dbh->rollBack();
	}	
	
	/**
	 * Dumps information that was contained within the prepared statement.
	 */
	public function debugDumpParams() {
		return $this->stmt->debugDumpParams();
	}	

	/**
	 * Used when trying to output an object as a string.
	 */
	public function __toString() {  
		return 'Sorry, you cannot output an object as a string.<br />';
	}
 
}
?>