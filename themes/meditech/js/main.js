// make sure not to conflict with Drupal's jQuery...
var $jq = jQuery.noConflict();

$jq(document).ready(function(){
  
  
  // NAV Manipulation *******************************************
  // add/remove shrink class to/from header on scroll...
  $jq(window).scroll(function () { 
    if($jq(window).scrollTop() >= 20) {
      $jq('header').addClass('shrink');
    } else {
      $jq('header').removeClass('shrink');
    }    
  });
  $jq(window).scroll();
  // END of NAV Manipulation ******************************************
  
  

  // LOAD SLIDEBARS ***************************************
  $jq.slidebars();
  

  
  // ACCORDION DROPDOWN ***************************************
  $jq('.accordion__link').bind('click', function(e){
    $jq(this).parent().find('.accordion__dropdown').slideToggle('fast');  // apply the toggle to the ul
    $jq(this).parent().toggleClass('is-expanded');
    e.preventDefault();
  });
  

  // ACCORDION TOGGLE ***************************************
  $jq('.js__accordion__toggle').click(function(){
    var topoflist = $jq(this).closest('.accordion__list__item').position().top;
    $jq(this).parent().slideUp(300);
    $jq("body,html").animate({scrollTop: Math.round(topoflist)},800);
    $jq(this).closest('.is-expanded').toggleClass('is-expanded');
    return false;
  });
  

  // IMAGE OVERLAY ON VIDEO v2(for mutiple videos) **********************************************
  // Idea started from:
  // http://codepen.io/laviperchik/pen/jHeGC
  // **********************************************
  $jq(".js__video").each(function(index, el){
    var video_src = "//player.vimeo.com/video/"+$jq(el).data("video-id")+"?&autoplay=1";
    $jq(el).find(".video__container").html('');
    $jq(el).find(".video__play-btn").click(function(e){
      $jq(el).find(".video__container").html('<iframe frameborder="0" allowfullscreen id="player" class="active" height="390" width="820" src="'+video_src+'"></iframe>');
      $jq(el).find(".video__overlay, .video__play-btn").addClass("display-none");
      e.preventDefault();
    });
  });


  // Adds image swap to old video structure *******************************
  // This can be deleted once all videos have been swapped over to the new html structure
  function addImageSwappingToOldVideos(){
    $jq(".js-video").each(function(index, el){
      var video_src = "//player.vimeo.com/video/"+$jq(el).data("video-id")+"?&autoplay=1";
      $jq(el).find("#videoContainer").html('');
      $jq(el).find(".btnPlay").click(function(e){
        $jq(el).find("#videoContainer").html('<iframe frameborder="0" allowfullscreen id="player" class="active" height="390" width="820" src="'+video_src+'"></iframe>');
        $jq(el).find(".video__overlay, .btnPlay").addClass("displayNone");
        e.preventDefault();
      });
    });
  }
  

  // This adds the .js-video class to old videos then runs the old video function ******************
  // This can be deleted once all videos have been swapped over to the new html structure
  if($jq('#video').length > 0){
    $jq(".content__callout__media").each(function(index, el){
      $jq(el).find("#video").addClass('js-video');
      addImageSwappingToOldVideos();
    });
  }

  
  // STYLE SELECT FORM FIELDS ***************************************
  // Add class 'focus' when selected
  $jq('body').addClass('js');

  $jq('select').each(function(){
    var $element = $jq(this);
    // Bind event handlers to <select>
    $element.on({
      'focus': function () {
        $element.addClass('focus');
      }
    });
    // Trigger the change event so the value is current
    $element.trigger('change');
  });
  

  // dropdown menu ***************************************
  // http://refills.bourbon.io/unstyled/
  $jq(".dropdown-container").click(function() {
    $jq(".dropdown-menu").toggleClass("show-menu");
    $jq(".dropdown-menu > li").click(function(){
      $jq(".dropdown-menu").removeClass("show-menu");
  });
  $jq(".dropdown-menu.dropdown-select > li").click(function() {
     $jq(".dropdown-container").html($jq(this).html());
    });
  });
  
  
  // remove instances of <p>&nbsp;</p> HTML ***************************************
  $jq('p').each(function(){
    $jq(this).html($jq(this).html().replace(/&nbsp;/gi,' '));
  });
  
  
  
  // Modal Pop-up Box ***************************************
  $jq('.open-modal').on('click', function(){
    var opener = $jq(this);
    var modal = '#' + opener.attr('data-target');
    if( $jq(modal + ' div.modal-content').children().is('iframe') ){
      var iframe_src = $jq('div.modal-content--vid iframe').attr('src');
      var new_iframe_src = iframe_src + '?autoplay=1';
      $jq('div.modal-content--vid iframe').attr( 'src', new_iframe_src );
    }
    $jq('#mask,' + modal).fadeIn(200);
  });
  
  $jq('.close-modal').on('click', function(){
    var closer = $jq(this);
    var modal = '#' + closer.parent().attr('id');
    if( closer.next().children().is('iframe') ){
      var source = $jq(modal + ' div iframe, ' + modal.selector + ' div img').attr('src');
      var new_source = source.replace('?autoplay=1', '');
      $jq(modal + ' iframe').attr( 'src', '' );
      $jq(modal + ' iframe').attr( 'src', new_source );
      $jq('#mask,' + modal).fadeOut(200);
    }
    else{
      $jq('#mask,' + modal).fadeOut(200);
    }
  });

});




/*
VANILLA JS code...
*/

// create a "document is ready" function...
function ready(callbackFunc){
  if(document.readyState !== "loading"){ // Document is already ready, call the callback directly
    callbackFunc();
  } else if(document.addEventListener){ // All modern browsers to register DOMContentLoaded
    document.addEventListener("DOMContentLoaded", callbackFunc);
  } else{ // Old IE browsers
    document.attachEvent("onreadystatechange", function(){
      if(document.readyState === "complete"){
        callbackFunc();
      }
    });
  }
}


// workaround for using non-arrays with foreach...
var forEach = function(arr, callback){
  Array.prototype.forEach.call(arr, callback);
};


// perform these scripts when document is ready...
ready(function(){
  
  // add target="_blank" to any link that is not internal and is missing the target="_blank"...
  var links = document.querySelectorAll("a[href]");
  forEach(links, function(item){
    var url = item.getAttribute('href');
    // using indexOf instead of startsWith in order to support IE11...
    // if URL does not contain meditech.com or vimeo.com and it does have https:// or http://, add target="_blank" (if it's missing)...
    // test page (https://ehr.meditech.com/news/meditech-to-integrate-nccn-chemotherapy-order-regimens-into-web-ehr)
    if( url.indexOf('meditech.com') < 0 && url.indexOf('vimeo.com') < 0 && ( url.indexOf('http://') > -1 || url.indexOf('https://') > -1 ) ){
      if(item.target == ''){
        item.target = "_blank";
      }
    }
    // if URL does contain meditech.com and has target="_blank", remove it...
    if( url.indexOf('meditech.com') > -1 ){
      if(item.target == '_blank'){
        item.target = "";
      }
    }
  }); 
  
  // add noreferrer noopener to new window links to prevent URLs being pointed at from accessing your window DOM object (improves performance and security)...
  var tLinks = document.querySelectorAll("a[target=\"_blank\"]");
  forEach(tLinks, function(item){
    if(item.rel == ""){
      item.rel = "noreferrer noopener";
    }
    else{
      item.rel += " noreferrer noopener";
    }
  });
  
});


