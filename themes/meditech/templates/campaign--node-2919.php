<!-- START campaign--node-2919.php template -->

<?php // **** This template is set up to control the display of the HOME PAGE content type
$url = $GLOBALS['base_url']; // grabs the site url
?>

<!-- Announcements/Alerts
<div class="container--page-title" style="background-color:#087E68; color:white; padding:.5em 0;">
  <div class="container__centered">
    <p style="margin:0; font-size:.85em;">On <strong>July 14 at 11 PM EDT</strong>, MEDITECH will update ehr.meditech.com for a period no longer than 15 minutes. There may be temporary slowness during this time frame.</p>
  </div>
</div>
-->

<div class="js__seo-tool__body-content">

    <style>
        /* **** Contact Sales Button ************************************** */

        #contact-sales-btn {
            position: fixed;
            right: 20px;
            bottom: 20px;
            width: 190px;
            border: 6px solid #00BC6F;
            box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.3);
            z-index: 10;
        }

        .contact-title {
            background: #00BC6F;
            padding: 5px;
            color: #fff;
            font-size: 21px;
            font-weight: bold;
            cursor: pointer;
        }

        .contact-title a {
            color: #fff;
        }

        .contact-title a:hover {
            color: #fff;
        }

        .info-window {
            background-color: #fff;
            padding: 10px;
            font-size: 18px;
            display: none;
            text-align: center;
        }

        .float--x-container {
            float: right;
            z-index: 1001;
            padding-right: 3px;
        }

        .float--x {
            color: #fff;
        }

        /* **** VIDEO ************************************** */

        .video--loop-container {
            min-height: 600px;
            max-height: 650px;
        }

        @media (max-width: 800px) {
            .video--loop-container {
                min-height: 330px;
            }
        }

        @media (max-width: 440px) {
            .video--loop-container {
                min-height: 700px;
            }
        }

        .video--loop-container video {
            /* Make video to at least 100% wide and tall */
            min-width: 100%;
            min-height: 100%;
            /* Setting width & height to auto prevents the browser from stretching or squishing the video */
            width: auto;
            height: auto;
            /* Center the video */
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .video--title {
            max-width: 1152px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 1em;
            padding-right: 2em;
            position: absolute;
            top: 20%;
            bottom: 0;
            left: 6%;
            text-align: left;
            color: #fff;
            z-index: 3;
        }

        @media (max-width: 1000px) {
            .video--title {
                position: absolute;
                top: 5%;
            }
        }

        @media all and (max-width: 50em) {
            #contact-sales-btn {
                display: none;
            }
        }

        .container__one-fifth {
            width: 18%;
            margin-right: 2%;
            padding: 1em 1%;
            float: left;
        }

        .container__one-fifth:last-child {
            margin-right: 0;
        }

        @media (max-width: 1000px) {
            .container__one-fifth {
                width: 100%;
                margin-right: 0;
                margin-top: 1em;
                padding: 1em 5%;
                float: left;
            }
        }

        .bg-overlay--white {
            background-color: none;
        }

        @media (max-width: 900px) {
            .bg-overlay--white {
                background-color: rgba(255, 255, 255, 0.8);
            }
        }

        .box {
            width: 400px;
            height: 250px;
        }

        .teal {
            background-image: linear-gradient(to right, rgba(16, 186, 179, .3), rgba(16, 186, 179, .3));
            position: absolute;
            border-radius: 15px;
            left: -40px;
            top: -30px;
            z-index: 2;
        }

        .blue {
            background-image: linear-gradient(to right, rgba(1, 88, 119, .3), rgba(1, 88, 119, .3));
            position: absolute;
            border-radius: 15px;
            left: 184px;
            top: 39px;
            z-index: 2;

        }


        #grad1 {
            height: 800px;
            background-color: white;
            /* For browsers that do not support gradients */
            background-image: linear-gradient(to bottom right, rgba(128, 128, 128, 0), rgba(128, 128, 128, 0));
            /* Standard syntax (must be last) */
            z-index: 0;
        }


        .hero-image-container {
            position: relative;
        }

        .hero-image-container img {
            border-radius: 0.5em;
            margin-top: 2em;
            max-width: 100%;
            z-index: 1;

        }

        .background-rectangle--right {
            content: "";
            position: absolute;
            border-radius: 0.5em;
            height: 108%;
            width: 95%;
            z-index: -1;
            background-color: #fbfdd7;
            top: 0;
            bottom: 0;
            left: 5em;
            right: 0;
        }


        .z-five {
            z-index: 5;
        }

    </style>


    <!-- Contact Sales Button -->
    <div id="contact-sales-btn">
        <div class="float--x-container"><a href="#!" class="hide_sales_gae" onclick="hide('contact-sales-btn')"><i class="fas fa-times fa-lg float--x"></i></a></div>
        <div class="contact-title">
            <a href="#!" class="sales_button_gae"><i class="fa fa-phone fa-md fa-flip-horizontal" style="margin-right:5px;"></i> Get Started</a>
        </div>
        <div class="info-window">
            <p style="margin-bottom:0px;">Learn more about Expanse:<br> <span style="font-weight:bold;">781-774-7700</span></p>
        </div>
    </div>

    <script>
        // make sure not to conflict with Drupal's jQuery...
        var $jq = jQuery.noConflict();
        $jq(document).ready(function() {
            $jq(".contact-title").click(function() {
                $jq(".info-window").toggle("fast");
            });
        });

        function hide(target) {
            document.getElementById(target).style.display = "none";
        }

    </script>
    <!-- End Contact Sales Button -->


    <h1 style="display:none;">MEDITECH EHR Software Company</h1>



    <!-- Expanse Block -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/starry-sky-house-on-winter-hills.jpg);">
        <div class="container__centered">
            <div class="container">
                <img style="padding-left:2em; padding-right:2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-expanse-logo--green-white.png" alt="MEDITECH Expanse Logo">
            </div>
            <div class="container transparent-overlay center no-pad--top text--white text-shadow--black" style="background-color: rgba(21, 19, 74, 0.5); width: 70%; margin: 0 auto; padding: 1em 2em;">
                <p class="text--large">Our fully interoperable web-based platform navigates the care continuum with unparalleled confidence, so you can see the full picture and treat the whole patient &mdash; no matter where you are.</p>
                <p class="text--large" style="margin-bottom:0;">One EHR, no limits. Welcome to the new vision of healthcare.</p>
            </div>

            <div class="center" style="padding-top: 4em;">
                <a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Explore MEDITECH Expanse</a>
            </div>

        </div>
    </div>
    <!-- End Expanse Block -->


    <div id="grad1" style="text-align:center;margin:auto;color:black;">


        <div class="container__centered" style="padding-top:4em; padding-bottom: 6em;">
            <div class="container__one-half">
                <figure>
                    <img src="https://ehr-test.meditech.com/drupal/sites/all/themes/meditech/images/home/innovation--test.jpg" alt="medical pres" style="width:100%;">
                </figure>
            </div>
            <div class="container__one-half">
                <div>
                    <h2>Innovation, Realized</h2>
                    <p>Innovation is more than a promise for the future â€” itâ€™s right here, ready to be unlocked in your EHR. Read our <a href="https://ehr.meditech.com/case-studies">case studies</a> to see how our customers are improving outcomes with real results.</p>
                    <div class="center">
                        <a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Download The Innovators Booklet</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="container__centered">
            <div class="container__one-half">
                <div>
                    <h2>Labor and Delivery</h2>
                    <p>The solution nurses need to welcome their newest patients. Give nurses the full perinatal story to guide safe, informed care for new mothers and babies.</p>
                    <div class="center">
                        <a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Learn More About Expanse Labor And Delivery</a>
                    </div>
                </div>
            </div>
            <div class="container__one-half">
                <figure>
                    <img src="https://ehr-test.meditech.com/drupal/sites/all/themes/meditech/images/home/laboranddelivery--test.jpg" alt="medical pres" style="width:100%;">
                </figure>
            </div>
        </div>
    </div>

<!-- start quote block -->

    <div class="container background--cover" style="background-image: url(<?php print $url; ?> https://ehr-test.meditech.com/drupal/sites/all/themes/meditech/images/home/quote_bg.jpg);">
        <div class="container__centered">
            <div class="container">
                <img style="padding-left:2em; padding-right:2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-expanse-logo--green-white.png" alt="MEDITECH Expanse Logo">
            </div>
            <div class="container transparent-overlay center no-pad--top text--white text-shadow--black" style="background-color: rgba(21, 19, 74, 0.5); width: 70%; margin: 0 auto; padding: 1em 2em;">
                <p class="text--large">Our fully interoperable web-based platform navigates the care continuum with unparalleled confidence, so you can see the full picture and treat the whole patient &mdash; no matter where you are.</p>
                <p class="text--large" style="margin-bottom:0;">One EHR, no limits. Welcome to the new vision of healthcare.</p>
            </div>

            <div class="center" style="padding-top: 4em;">
                <a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Explore MEDITECH Expanse</a>
            </div>

        </div>
    </div>

<!-- End quote block -->

    <!-- Begin Numbers-->
    <div class="container background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/home/grey-purple-blurry-gradient.jpg); padding-bottom: 0;">
        <div class="container__centered">
            <h2 style="font-size:3em; margin-bottom:.15em; margin-top:0;">Exploring new horizons</h2>
            <p>As healthcare's digital transformation continued in 2018, MEDITECH and <a href="https://ehr.meditech.com/news/more-providers-sign-on-for-meditech-expanse-in-2018">our customers</a> went beyond pushing the envelope: we arrived at our destination, delivering the full web experience to both physicians and nurses. This is more than an evolution â€” it's a revolution:</p>

            <div style="margin-top: 3em;">
                <div class="container__one-fifth center" style="margin-bottom:1em;">
                    <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">65</span><br> <strong>hospitals went LIVE with Expanse</strong></p>
                </div>
                <div class="container__one-fifth center" style="margin-bottom:1em;">
                    <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">15<span style="font-size:.5em; position:relative; top:-.6em">%</span></span><br> <strong>increase in new Expanse customers</strong></p>
                </div>
                <div class="container__one-fifth center" style="margin-bottom:1em;">
                    <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">83<span style="font-size:.5em; position:relative; top:-.6em">%</span></span><br> <strong>increase in Magic-to-Expanse upgrades</strong></p>
                </div>
                <div class="container__one-fifth center" style="margin-bottom:1em;">
                    <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">54<span style="font-size:.5em; position:relative; top:-.6em">%</span></span><br> <strong>of 2018 signed business was net new customers, a 15% increase</strong></p>
                </div>
                <div class="container__one-fifth center" style="margin-bottom:1em;">
                    <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">75<span style="font-size:.5em; position:relative; top:-.6em">%</span></span><br> <strong>of Expanse signings &amp; migrations are implementing Expanse Ambulatory</strong></p>
                </div>
            </div>
        </div>

        <div class="container__centered center" style="margin-bottom:3em;">
            <div class="gl-container" style="background-color: rgba(62, 69, 69, 0.5);">

                <div class="container__one-half bg--black-coconut">
                    <h3 style="margin-top:1.5em;">Connecting care across the continuum to give our customers the complete and total view:</h3>
                    <p class="text--small" style="line-height:1.25em;">
                        Ambulatory <span style="margin:0 .15em;">&bull;</span> Home Care <span style="margin:0 .15em;">&bull;</span> Behavioral Health <span style="margin:0 .15em;">&bull;</span> IDNs <span style="margin:0 .15em;">&bull;</span> Critical Access Hospitals <span style="margin:0 .15em;">&bull;</span> Surgical Hospitals <span style="margin:0 .15em;">&bull;</span> Community Hospitals <span style="margin:0 .15em;">&bull;</span> Specialty Hospitals
                    </p>
                </div>

                <div class="container__one-half">
                    <div class="container__one-half">
                        <h2 style="font-size:2em;">Top of the KLAS</h2>
                        <div class="center" style="margin-top:2em;">
                            <?php hubspot_button('08fe9426-f548-4533-a158-b98661f7c455', "Read About Our KLAS Win"); ?>
                        </div>
                    </div>
                    <div class="container__one-half">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/2019-best-in-klas--MEDITECH--acute-care-emr--canada.png" alt="2019 Best in KLAS -- MEDITECH -- acute care EMR -- Canada award logo" style="width:200px;">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End Numbers-->

    <div id="grad1" style="text-align:center;margin:auto;color:black;">
        <div class="container__centered" style="padding-top:4em; padding-bottom: 4em;">

            <div class="container__one-half" style="padding-right: 2em; position: relative;">
                <div class="box teal">
                </div>
                <figure class="z-five" style="position: relative;">
                    <img src="https://ehr-test.meditech.com/drupal/sites/all/themes/meditech/images/home/innovation--test.jpg" alt="medical pres" style="width:100%; border-radius: 15px;">
                </figure>

            </div>
            <div class="container__one-half">
                <div>
                    <h2>Test Test Test Innovation, Realized</h2>
                    <p>Innovation is more than a promise for the future â€” itâ€™s right here, ready to be unlocked in your EHR. Read our <a href="https://ehr.meditech.com/case-studies">case studies</a> to see how our customers are improving outcomes
                        with real results.</p>
                    <div class="center">
                        <a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Download The Innovators Booklet</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container__centered">
            <div class="container__one-half">
                <div>
                    <h2>Labor and Delivery</h2>
                    <p>The solution nurses need to welcome their newest patients. Give nurses the full perinatal story to guide safe, informed care for new mothers and babies.</p>
                    <div class="center">
                        <a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Learn More About Expanse Labor And Delivery</a>
                    </div>
                </div>
            </div>


            <div class="container__one-half" style="padding-left: 2em; position: relative;">
                <div class="box blue">
                </div>
                <figure class="z-five" style="position: relative;">
                    <img src="https://ehr-test.meditech.com/drupal/sites/all/themes/meditech/images/home/laboranddelivery--test.jpg" alt="medical pres" style="width:100%; border-radius: 15px;">
                </figure>

            </div>
        </div>
    </div>

    <!--
    <div class="container">
        <div class="container__centered">
            <div class="container__one-half" style="padding-top: 2em;">
                <h1>
                    2019 Physician and CIO Forum
                </h1>
                <h2>
                    Foxborough, MA
                    <br>
                    September 18th - 19th
                </h2>
                <p>
                    Join MEDITECH and home care leaders at the 2019 National
                    Association for Home Care &amp; Hospice Conference and Expo.
                </p>
                <div class="center" tabindex="0"><a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Register Today!">Register Today!</a>
                </div>
            </div>
            <div class="container__one-half">
                <div class="hero-image-container">
                    <div class="background-rectangle--right"></div>
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/events-hero-test-image--1.jpg" alt="Events Hero Image">
                </div>
            </div>

        </div>
    </div>
-->

    <!-- Nurse Forum -->
    <div class="content__callout">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="327293262">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--MEDITECH-Nurse-Forum-2019-promo.jpg" alt="MEDITECH Nurse Forum 2019 - Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/327293262?"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>Experience <em>Innovative Care Coordination</em> at the 2019 Nurse Forum</h2>
                    <p>Nurses, this is your year. We invite you to discover what it means to administer care when traditional barriers have been torn down at our <a href="https://ehr.meditech.com/events/2019-nurse-forum">2019 Nurse Forum: Innovative Care Coordination</a>, taking place June 12-14 in Foxborough, MA.</p>
                    <div class="center" style="margin-top:2em;">
                        <?php hubspot_button('0018b681-9273-4061-a25b-1d573c911d2f', "Register For the 2019 Nurse Forum"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Nurse Forum -->



    <!-- MaaS Campaign -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/home/MaaS-Campaign--2019.jpg); padding:6em 0; background-position: top left;">
        <div class="container__centered">
            <div class="container__one-half">
                &nbsp;
            </div>
            <div class="container__one-half">
                <div class="transparent-overlay--xp text--white">
                    <h2>Reach new heights with <i>MaaS</i></h2>
                    <p>MEDITECH as a Service (MaaS) is a cost-effective monthly subscription to Expanse that will get your organization up and running with <a href="https://ehr.meditech.com/expanse">an innovative, web-based EHR</a>. MaaS handles the technology so that you can focus on what you do best: caring for your community. </p>
                    <div class="center">
                        <?php hubspot_button('7e89aa4b-8ba6-4c87-8752-30fda7980c6e', "Get More with MaaS"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End MaaS Campaign -->

  

</div>


<?php // SEO tool for internal use...
if( user_is_logged_in() ){
  print '<!-- SEO Tool is added to this div -->';
  print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
}
?>

<!-- end node--home-page.tpl.php template -->
