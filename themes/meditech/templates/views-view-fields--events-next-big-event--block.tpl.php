<?php  // This template is for each row of the Views block: EVENTS - NEXT BIG EVENT ....................... 

  $url = $GLOBALS['base_url']; // grabs the site url
  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);
?>
<style>


</style>
<!-- start views-view-fields--events-next-big-event--block.tpl.php template -->
<div class="container">
    <div class="container__centered flex-order--container">
        <div class="container__one-half content--pad-right">
            <h2 class="header-one">
                <?php print $fields['title']->content; ?>
            </h2>
            <h3 class="header-two">
                <?php 
          print $fields['field_location_city']->content;
          if($fields['field_location_state']->content){
            $stateTerms = field_view_field('node', $node, 'field_location_state'); 
            if(!empty($stateTerms)){
              foreach($stateTerms["#items"] as $sTerm){
                $eventState = ', '.$sTerm["taxonomy_term"]->description;
              }
            }
            else{
              $eventState = '';
            }
            print $eventState; 
          }
          if($fields['field_location_country']->content){
            if($fields['field_location_country']->content != 'United States' && $fields['field_location_country']->content != 'Canada'){
              print ', '.$fields['field_location_country']->content;
            }
          }
        ?> <br>

                <?php 
        $eventDate = field_get_items('node', $node, 'field_event_date');

        // get first date data...
        $date1 = date_create($eventDate[0]['value']);
        $day1 = date_format($date1, 'jS');
        $month1 = date_format($date1, 'F');

        // get second date data...
        $date2 = date_create($eventDate[0]['value2']);
        $day2 = date_format($date2, 'jS');
        $month2 = date_format($date2, 'F');

        print $month1.' '.$day1;
        // if start date and end date are not in the same month...
        if($month1 != $month2){
            print " - ".$month2.' '.$day2;
        }
        // else if dates are in the same month...
        elseif($month1 == $month2 && $day1 != $day2){
            print " - ".$day2;
        }
        // else one day event...
        else{
        }
      ?>
            </h3>
            <?php print $fields['field_summary']->content; ?>

            <div tabindex="0">
                <?php 
      // for annual HIMSS page...
      // create regular event page with image, date, location, etc.
      // change button URL to main HIMSS page...
      /*
      if($nid == 2242){
        $url_path = 'https://ehr.meditech.com/events/meditech-at-himss18';
      }
      else{
        $url_path = $fields['path']->content;
      }
      */
      $url_path = $fields['path']->content;
      ?>
                <a class="btn--orange-gradient event_reg_gae" href="<?php print $url_path; ?>" title="Register Today!">Register Today!</a>
            </div>
            <?php // add Edit Video link...
          if( user_is_logged_in() ){ 
            print '<div style="display:block; margin:1em 0;"><span style="font-size:12px;">'; print l( t('Edit Hero Text'),'node/'. $fields['nid']->content .'/edit', array('attributes' => array('style' => array('color:#3e4545') ) ) ); print "</span>";
            print ' | <span style="font-size:12px;"><a href="https://app.hubspot.com/cta/2897117" target="_blank" style="color:#3e4545;">Edit Hero Button in Hubspot</a></span></div>'; 
          } 
        ?>
        </div>

        <div class="container__one-half">
            <div class="bg-pattern--container">
                <?php 
    if( !empty($fields['field_event_image']->content) ){      
      $imageData = field_get_items('node', $node, 'field_event_image');
      $imageURI = $imageData[0]['uri'];
      $imageFileURL = explode('/', $imageURI);
      $imageFileName = $imageFileURL[4];
    }
  ?>

                <img src="<?php print $url.'/sites/default/files/images/events/'.$imageFileName; ?>" alt="Events Hero Image">
                <div class="bg-pattern--green-gradient bg-pattern--right bg-pattern--full-height"></div>
            </div>
        </div>

    </div>
</div>
<!-- end views-view-fields--events-next-big-event--block.tpl.php template -->
