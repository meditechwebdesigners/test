<!-- START inc--taxonomy-page-wrapper.php template -->
<section class="container__centered">

  <div class="container__two-thirds">

      <h1 class="page__title" style="margin-bottom:1.5em;"><?php 
        // get taxonomy term to see if the word "News" should come after it...
        $term_name = $term_heading['term']['#term']->name;
        print $term_name;
        // if the term is any of the following, then don't add " News" after them...
        $terms_array = array('Expanse Updates', 'Industry Awards', 'MEDITECH in the Press', 'Podcasts', 'Press Releases', 'Quality Outcomes', 'Signings', 'Solutions', 'Success Stories', 'Videos');
        if( !in_array($term_name, $terms_array) ){
          print ' News'; 
        }
      ?></h1>

      <div class="taxonomy-results">
        <?php print render($page['content']); // this content is usually controlled at the node template level (node--news-article.tpl.php) ?>
      </div>

      <?php // add pagination links (convert to Load More button with Jquery help)...
      print $pager;        
      // Get the total number of node results...
      $totalResults = $GLOBALS['pager_total_items'][0];
      if($totalResults > 10){ // don't show Load More if less than 11 results
      ?>
        <div>
          <ul class="pager-load-more">
            <li><button class="load-more-ajax">Load more</button></li>
          </ul>
        </div>
      <?php 
      } 
      ?>
      <script type="text/javascript">
        (function($) {
          Drupal.behaviors.loadMoreAjax = {
            attach: function(context, settings) {
              $('.load-more-ajax', context).click(function() {
                var nextPage = $('.pager .pager-next a').attr('href');
                var lastPage = $('.pager .pager-last a').attr('href');
                $.get(nextPage, function(data) {
                  $(data).find('.taxonomy-results').insertBefore($('.item-list'));
                  $('.item-list .pager').remove();
                  if (nextPage == lastPage) {
                    $('.load-more-ajax').remove();
                  } else {
                    $(data).find('.item-list .pager').appendTo($('.item-list'));
                    Drupal.attachBehaviors($('.item-list'));
                  }
                });
              });
              $('.item-list .pager').hide();
            }
          };
        })(jQuery);
      </script>

    </div>

  <?php include('inc-news-sidebar.php'); ?>

</section>
<!-- END inc--taxonomy-page-wrapper.php template -->