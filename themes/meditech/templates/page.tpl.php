<?php $currentURL = $_SERVER[REQUEST_URI]; // get current page URL info (minus domain)... ?>
<!-- start page.tpl.php template -->
<?php $websiteURL = $GLOBALS['base_url']; // grabs the site url ?>


<?php include('inc--header.php'); ?>


<!-- START Button Panel Theme Adjuster -->
<?php 
if( user_is_logged_in() === true ){
  global $user;
  $userID = $user->uid;
  if( $userID == 105 || $userID == 109 || $userID == 108 || $userID == 3473 ){
?>
<style>
	.fa-cog:before {
		content: "\f013";
	}

	.btn-container {
		left: -200px;
		position: fixed;
		top: 15%;
		width: 200px;
		z-index: 999;
		transition: all 0.3s ease;
	}

	.btn-container.push {
		left: 0;
		transition: all 0.3s ease;
	}

	.btn-panel input[type="radio"] {
		display: none;
	}

	.btn-panel label {
		color: #ffffff;
		display: block;
		width: 100px;
		height: 30px;
		float: left;
		cursor: pointer;
		margin: 5px 13px;
		border-radius: 4px;
	}

	.btn-panel {
		text-align: center;
		background-color: #222222;
		padding: 0em 2em 2em 2em;
		clear: both;
		overflow: hidden;
	}

	.panel-tab {
		cursor: pointer;
		height: 50px;
		position: absolute;
		right: -48px;
		top: 0;
		width: 50px;
		background-color: #222222;
	}

	label[for="theme-1"],
	.theme-1 {
		background: #5041AA !important;
		border: none !important;
	}

	label[for="theme-2"],
	.theme-2 {
		background: #612BF7 !important;
		border: none !important;
	}

	label[for="theme-3"],
	.theme-3 {
		background: #EFC99D !important;
		color: #3e4545 !important;
		border: none !important;
	}

	label[for="theme-4"] {
		background-color: #087E68 !important;
		border: 3px solid #FFFFFF !important;
	}

	.theme-4 {
		padding: 0.9em 1.3em !important;
		background-color: #087E68 !important;
		color: #FFFFFF !important;
		border: 3px solid #FFFFFF !important;
	}

	label[for="theme-5"] {
		background: transparent !important;
		border: 3px solid #5041AA !important;
		color: #5041AA !important;
	}

	.theme-5 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #5041AA !important;
		color: #5041AA !important;
	}

	label[for="theme-6"] {
		background: #012657 !important;
		border: 3px solid #FFFFFF !important;
	}

	.theme-6 {
		padding: 0.9em 1.3em !important;
		background: #012657 !important;
		color: #FFFFFF !important;
		border: 3px solid #FFFFFF !important;
	}

	label[for="theme-7"] {
		background: transparent !important;
		border: 3px solid #00BC6f !important;
		color: #3E4545 !important;
	}

	.theme-7 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #00BC6f !important;
		color: #3E4545 !important;
	}

	label[for="theme-8"],
	.theme-8 {
		background-color: #087E68 !important;
		color: #FFFFFF !important;
		border: none !important;
	}

	label[for="theme-9"] {
		background: transparent !important;
		border: 3px solid #FF8300 !important;
		color: #3E4545 !important;
	}

	.theme-9 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #FF8300 !important;
		color: #3E4545 !important;
	}

	label[for="theme-10"],
	.theme-10 {
		background-color: #4734C8 !important;
		color: #FFFFFF !important;
		border: none !important;
	}

	label[for="theme-11"],
	.theme-11 {
		background-color: #087E68 !important;
		color: #FFFFFF !important;
		border: none !important;
	}

	label[for="theme-12"] {
		background: transparent !important;
		border: 3px solid #FF8300 !important;
		color: #3E4545 !important;
	}

	.theme-12 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #FF8300 !important;
		color: #3E4545 !important;
	}

	label[for="theme-13"] {
		background: transparent !important;
		border: 3px solid #FFFFFF !important;
	}

	.theme-13 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #FFFFFF !important;
		color: #FFFFFF !important;
	}

	label[for="theme-14"] {
		background: transparent !important;
		border: 3px solid #F7A350 !important;
	}

	.theme-14 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #F7A350 !important;
		color: #FFFFFF !important;
	}

	label[for="theme-15"] {
		background: transparent !important;
		border: 3px solid #FF8300 !important;
	}

	.theme-15 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #FF8300 !important;
		color: #FFFFFF !important;
	}

	label[for="theme-16"] {
		background: transparent !important;
		border: 3px solid #5041AA !important;
		color: #FFFFFF !important;
	}

	.theme-16 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #5041AA !important;
		color: #FFFFFF !important;
	}

	label[for="theme-17"] {
		background: transparent !important;
		border: 3px solid #00BC6f !important;
		color: #FFFFFF !important;
	}

	.theme-17 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #00BC6f !important;
		color: #FFFFFF !important;
	}

	label[for="theme-18"] {
		background: transparent !important;
		border: 3px solid #087E68 !important;
		color: #087E68 !important;
	}

	.theme-18 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #087E68 !important;
		color: #087E68 !important;
	}

	label[for="theme-19"] {
		background: transparent !important;
		border: 3px solid #087E68 !important;
		color: #FFFFFF !important;
	}

	.theme-19 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #087E68 !important;
		color: #FFFFFF !important;
	}

	label[for="theme-20"] {
		background: transparent !important;
		border: 3px solid #087E68 !important;
		color: #3E4545 !important;
	}

	.theme-20 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #087E68 !important;
		color: #3E4545 !important;
	}

	label[for="theme-21"] {
		background: transparent !important;
		border: 3px solid #FF8300 !important;
		color: #FFFFFF !important;
	}

	.theme-21 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #FF8300 !important;
		color: #FFFFFF !important;
	}

	label[for="theme-22"] {
		background: transparent !important;
		border: 3px solid #F7A350 !important;
		color: #3E4545 !important;
	}

	.theme-22 {
		padding: 0.82em 1.3em !important;
		background: transparent !important;
		border: 3px solid #F7A350 !important;
		color: #3E4545 !important;
	}

</style>

<div class="btn-container">
	<div class="btn-panel">
		<p class="text--white no-margin--bottom bold">Primary BTN</p>
		<label for="theme-1"><input type="radio" id="theme-1" name="theme" value="theme-1" />#5041AA</label>

		<label for="theme-2"><input type="radio" id="theme-2" name="theme" value="theme-2" />#612BF7</label>

		<label for="theme-10"><input type="radio" id="theme-10" name="theme" value="theme-10" />#4734C8</label>

		<label for="theme-3"><input type="radio" id="theme-3" name="theme" value="theme-3" />#EFC99D</label>

		<label for="theme-11"><input type="radio" id="theme-11" name="theme" value="theme-11" />#087E68</label>

		<label for="theme-4"><input type="radio" id="theme-4" name="theme" value="theme-4" />#087E68</label>

		<label for="theme-12"><input type="radio" id="theme-12" name="theme" value="theme-12" />#FF8300</label>

		<label for="theme-15"><input type="radio" id="theme-15" name="theme" value="theme-15" />#FF8300</label>

		<label for="theme-5"><input type="radio" id="theme-5" name="theme" value="theme-5" />#5041AA</label>

		<label for="theme-16"><input type="radio" id="theme-16" name="theme" value="theme-16" />#5041AA</label>

		<label for="theme-6"><input type="radio" id="theme-6" name="theme" value="theme-6" />#012657</label>

		<p class="text--white no-margin--bottom bold" style="margin-top: 25em;">Secondary BTN</p>
		<label for="theme-7"><input type="radio" id="theme-7" name="theme2" value="theme-7" />#00BC6F</label>

		<label for="theme-17"><input type="radio" id="theme-17" name="theme2" value="theme-17" />#00BC6F</label>

		<label for="theme-20"><input type="radio" id="theme-20" name="theme2" value="theme-20" />#087E68</label>

		<label for="theme-19"><input type="radio" id="theme-19" name="theme2" value="theme-19" />#087E68</label>

		<label for="theme-18"><input type="radio" id="theme-18" name="theme2" value="theme-18" />#087E68</label>

		<label for="theme-8"><input type="radio" id="theme-8" name="theme2" value="theme-8" />#087E68</label>

		<label for="theme-9"><input type="radio" id="theme-9" name="theme2" value="theme-9" />#FF8300</label>

		<label for="theme-21"><input type="radio" id="theme-21" name="theme2" value="theme-21" />#FF8300</label>

		<label for="theme-22"><input type="radio" id="theme-22" name="theme2" value="theme-22" />#F7A350</label>

		<label for="theme-14"><input type="radio" id="theme-14" name="theme2" value="theme-14" />#F7A350</label>

		<label for="theme-13"><input type="radio" id="theme-13" name="theme2" value="theme-13" />#FFFFFF</label>
	</div>

	<div class="panel-tab">
		<i style="padding: 0.43em; font-size: 1.5em;" class="text--white fas fa-cog"></i>
	</div>
</div>

<!-- Change Primary BTN & Slide Out Panel -->
<script>
	$jq("input:radio[name=theme]").click(function() {
		changeTheme($jq(this).val());
	});

	$jq(".panel-tab").click(function() {
		$jq(".btn-container").toggleClass("push");
	});

	function changeTheme(value) {
		$jq(".cta_button, .btn--orange, .button-primary, .mag-bg").css({
			background: value
		});
	}

	$jq(document).ready(function() {

		$jq('input:radio[name=theme]').click(function() {
			var others = $jq("[name='" + this.name + "']").map(function() {
				return this.value
			}).get().join(" ")
			console.log(others)
			$jq('.cta_button, .btn--orange, .button-primary, .mag-bg').removeClass(others).addClass(this.value)

		});

	});

</script>

<!-- Change Secondary BTN -->
<script>
	$jq("input:radio[name=theme2]").click(function() {
		changeTheme($jq(this).val());
	});

	$jq(document).ready(function() {

		$jq('input:radio[name=theme2]').click(function() {
			var others = $jq("[name='" + this.name + "']").map(function() {
				return this.value
			}).get().join(" ")
			console.log(others)
			$jq('.btn--orange-gradient, .load-more-ajax').removeClass(others).addClass(this.value)

		});

	});

</script>
<?php
  }
  else{
    print '<!-- CONTENT BLOCKED -->';
  }
}
?>
<!-- END Button Panel Theme Adjuster -->


<!-- CONTENT WRAPPER for Slidebar -->
<div id="sb-site">


	<!-- MAIN CONTENT ================================================================== -->
	<main class="main" role="main">

		<a name="main-content" id="main-content"></a><!-- an accessibility link destination -->

		<div id="page">

			<?php if($messages){ ?>
			<!-- HTML for status and error messages. -->
			<div class="container__centered">
				<div id="messages">
					<div class="section clearfix">
						<?php print $messages; ?>
					</div>
				</div>
			</div>
			<!-- end container__centered -->
			<?php } ?>


			<?php 
      // if current page is a NEWS TAXONOMY page =======================================
      // check to see if current page is a NEWS TAXONOMY PAGE 
      // (it pulls it's main content from SECTION 2 of node--news-article.tpl.php)...
      $term_heading = $page['content']['system_main']['term_heading'];
      if( isset($term_heading) && $term_heading['term']['#bundle'] == 'news_tags' ){
     
        include('inc--taxonomy-page-wrapper.php');
      
      }
      // ALL OTHER pages ==============================================================
      else { ?>

			<!-- start Content REGION -->
			<?php print render($page['content']); ?>
			<!-- end Content REGION -->

			<?php } ?>

		</div>
		<!-- End of #Page -->

	</main>
	<!-- End of MAIN -->


	<!-- Modal Pop-up Overlay -->
	<div id="mask"></div>


	<!-- FOOTER CONTENT ================================================================== -->

	<footer>

		<?php // display customer footer for customer-materials pages...
    if($currentURLarray[3] == 'customer-materials'){
      include('inc--footer--customer.php');
    } 
    else { // START general footer...
      include('inc--footer.php');
    } // END general footer 
    ?>

		<!-- Legal Information =================================  -->
		<div class="container bg--black-coconut no-pad">
			<section class="container__centered">
				<div class="container__one-half">
					<h5 class="no-margin--top">Medical Information Technology, Inc.</h5>
					<p class="text--small">Copyright &copy; 2019 Medical Information Technology, Inc.</p>
					<p class="text--small footer_link_gae"><a href="<?php print $websiteURL; ?>/cookie-policy">Cookie Policy</a> | <a href="<?php print $websiteURL; ?>/privacy-policy">Privacy Policy</a></p>
				</div>
			</section>
		</div>
		<!-- End of Legal Information -->

	</footer>

	<!-- Start PRINT ONLY footer content -->
	<div class="print-footer">
		<div class="container__centered">
			<p>Medical Information Technology, Inc.</p>
			<p class="text--small">Copyright &copy; 2020 Medical Information Technology, Inc.</p>
		</div>
	</div>
	<!-- End PRINT ONLY footer content -->

	<!-- End of FOOTER CONTENT ========================================================== -->

</div>
<!-- End of CONTENT WRAPPER for Slidebar -->


<?php include('inc--mobile-nav.php'); ?>


<?php if( user_is_logged_in() ){ // display SEO tool for logged in users ?>
<script type="text/javascript" src="<?php print $websiteURL; ?>/sites/all/themes/meditech/js/seo-tool.js"></script>
<?php } ?>


<?php // Edit Page button to appear at top of all node pages for logged in users...
if(node_access('update',$node)){ 
  print '<div class="edit text--white" style="position: absolute; top: 70px; right: 0; padding: 1em; background-color: #087E68; border-radius: 1em 0 0 0; font-size: .8em;"><p style="margin:0;">'; 
  print l( t('Edit This Page'),'node/'.$node->nid.'/edit' ); 
  print "</p></div>"; 
}
?>

<!-- end page.tpl.php template
