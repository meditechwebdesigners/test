<!-- main NEWS page news--node-2925.php -->
<h1 style="display:none;">MEDITECH News</h1>

<!-- Hero -->
<?php print views_embed_view('news_hero', 'block'); // adds 'News Hero' Views block... ?>
<!-- End Hero -->

<section class="container__centered">

  <!-- start Content REGION -->
  <?php print views_embed_view('news_main_page_us', 'block'); // adds 'News Main Page - US' Views block... ?>
  <!-- end Content REGION -->

</section>
<!-- main NEWS page news--node-2925.php -->
