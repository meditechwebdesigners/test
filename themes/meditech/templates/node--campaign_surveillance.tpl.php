<?php
  /*
  *
  * This template is set up to control the display of the CAMPAIGN_SURVEILLANCE content type
  *
  */
  $url = $GLOBALS['base_url']; // grabs the site url

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with mulitple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
function multi_field_collection_data($node, $field_name, $quantity){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // create array...
  $multi_field_collections = array();
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}

$block_6 = multi_field_collection_data($node, 'field_fc_head_ltext_1', 4);

?>
<!-- start node--campaign_surveillance.tpl.php template -->

<style>
  .button-vertical-adjustment { margin-top: 5em; }
  @media (max-width: 800px){
    .button-vertical-adjustment { margin-top: 1em; }
  }
</style>
 
 
<!-- BLOCK 1 --> 
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/abstract-hexagon-bg-01.jpg);">
  <div class="container__centered">
    <div class="container__two-thirds">
      <h1 class="js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
      <h2 class="js__seo-tool__title" style="margin-top:0;"><?php print render($content['field_header_1']); ?></h2>
      <div class="js__seo-tool__body-content">
        <?php print render($content['field_long_text_1']); ?>
      </div>
    </div>
    <div class="container__one-third center">
      <?php 
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
        <div class="button--hubspot button-vertical-adjustment">
          <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
        </div>
      <?php }else{ ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
      <?php } ?>
    </div>
  </div>
</div>
<!-- BLOCK 1 -->

<div class="js__seo-tool__body-content">

  <!-- BLOCK 2 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <div class="container no-pad" style="padding:0;">
        <div class="container__two-thirds">
          <figure style="margin-bottom:1em;">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-stroke-surveillance-laptop-screenshot.png" alt="MEDITECH Stroke Surveillance Screen">
          </figure>
        </div>
        <div class="container__one-third">
          <h2 class="text--white" style="margin-top:0;"><?php print render($content['field_header_2']); ?></h2>
          <div class="text--white">
            <?php print render($content['field_long_text_2']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 2 -->


  <!-- BLOCK 3 -->
  <div class="container bg--black-coconut">
    <div class="container__centered">
      <div class="container__one-third">
        <h2 class="text--white" style="margin-top:0;"><?php print render($content['field_header_3']); ?></h2>
        <div style="margin-bottom:2em;">
          <?php print render($content['field_long_text_3']); ?>
        </div>
      </div>
      <div class="container__two-thirds">
        <figure>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-sepsis-monitoring-laptop-screenshot.png" alt="MEDITECH Sepsis Monitoring Screen">
        </figure>
      </div>
    </div>
  </div>
  <!-- BLOCK 3 -->


  <!-- BLOCK 4 -->
  <div class="container">
    <div class="container__centered">
      <div class="container__two-thirds">
        <figure style="margin-bottom:1em;">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-global-watchlist-laptop-screenshot.png" alt="MEDITECH Global Watchlist Screen">
        </figure>
      </div>
      <div class="container__one-third">
        <h2 style="margin-top:0;"><?php print render($content['field_header_4']); ?></h2>
        <div>
          <?php print render($content['field_long_text_4']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 4 -->


  <!-- BLOCK 5 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered">
      <div class="container__two-thirds">
        <h2 class="white text--white"><?php print render($content['field_header_8']); ?></h2>
        <div class="white text--white">
          <?php print render($content['field_long_text_7']); ?>
        </div>
      </div>
      <div class="container__one-third" style="text-align:center;">
        <figure>
          <img style="width:50%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/1st-ESS-ribbon.png" alt="MEDITECH 1ST ESS">
        </figure>
      </div>
    </div>
  </div>
  <!-- BLOCK 5 -->


  <!-- BLOCK 6 -->
  <div class="container" style="padding-top:1em; background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/green-abstract-triangular-shapes.png);">
    <div class="container__centered">
      <div class="page__title--center">
        <h2 class="white text--white"><?php print render($content['field_header_5']); ?></h2>
      </div>
      <div class="container white text--white" style="padding:1em 0 0 0;">
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <h4 class="white text--white"><?php print $block_6[0]->field_header_1['und'][0]['value']; ?></h4>
            <div>
              <?php print $block_6[0]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <h4 class="white text--white"><?php print $block_6[1]->field_header_1['und'][0]['value']; ?></h4>
            <div>
              <?php print $block_6[1]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <h4 class="white text--white"><?php print $block_6[2]->field_header_1['und'][0]['value']; ?></h4>
            <div>
              <?php print $block_6[2]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <h4 class="white text--white"><?php print $block_6[3]->field_header_1['und'][0]['value']; ?></h4>
            <div>
              <?php print $block_6[3]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 6 -->


  <!-- BLOCK 7 -->
  <div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/nurse-and-patient-bg-02.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds">
        <h2><?php print render($content['field_header_6']); ?></h2>
        <div>
          <?php print render($content['field_long_text_5']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 7 -->

</div><!-- end js__seo-tool__body-content -->

<!-- BLOCK 8 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
  <div class="container__centered">
    <h2 class="text--white js__seo-tool__body-content" style="margin-top:0;"><?php print render($content['field_header_7']); ?></h2>
    <div class="text--white js__seo-tool__body-content">
      <?php print render($content['field_long_text_6']); ?>
    </div>
    
    <div style="text-align: center;">
      <h3 class="text--white js__seo-tool__body-content"><?php print render($content['field_text_1']); ?></h3>
      <?php 
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
        <div class="button--hubspot">
          <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
        </div>
      <?php }else{ ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
      <?php } ?>
    </div>
        

  </div>
</div>
<!-- BLOCK 8 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
<!-- end node--campaign_surveillance.tpl.php template -->