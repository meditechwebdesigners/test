<!-- start simple-page--node-1863.tpl.php template -- Case Studies page -->

<style>
      .container__thirds { float: left; display: block; margin: 2% 1.5%; padding: 2%; width: 30%; height: 320px; text-align: center; overflow: hidden;
        -moz-border-radius: 10px;
        -webkit-border-radius: 10px;
        -webkit-font-smoothing: antialiased;
        border-radius: 10px;
        position: relative;
        background-position: bottom left;
        background-repeat: no-repeat;
        background-size: 125px 125px;
      }
      .container__thirds h3 { font-size: 1em; }
      fieldset#topics { padding: 1em; }
      .topic { display: block; float: left; width: 32%; margin-right: 1%; }
      .topic:nth-child(3n) { margin-right: 0; }
      @media all and (max-width: 65em) {
         .container__thirds { float: left; display: block; margin: 2% 1.5%; padding: 2%; width: 30%; height: 400px; text-align: center; overflow: hidden; }
      }
      @media all and (max-width: 50em) {
         .topic { display: block; float: left; width: 100%; margin-right: 0; }
         .container__thirds { float: left; display: block; margin: 2% 1.5%; padding: 2%; width: 94%; height: 200px; text-align: center; overflow: hidden; }
      }

      .container__thirds.edsc { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--ed-specialty--colored.png); }
      .container__thirds.frl { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--fiscal-leader--colored.png); }
      .container__thirds.iqs { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--quality-safety--colored.png); }
      .container__thirds.int { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--interoperability--colored.png); }
      .container__thirds.pne { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--physician-nurse--colored.png); }
      .container__thirds.pop { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/news/case-studies-icon--pop-health--colored.png); }

      .container__thirds .bottom-text { text-align: right; font-size: .9em; color: #999; position: absolute; bottom: 10px; right: 20px; width: 50%; line-height: 1.15em; }
    </style>


    <!-- Hero -->
    <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/innovators--case-study-booklet.jpg);">
      <div class="container__centered bg-overlay--black">
        <div class="container__one-third text--white" style="margin-top:3em; margin-bottom:3em;">
          <h2><span style="font-size:2em;">The Innovators:</span><br>MEDITECH Customers<br>In Action</h2>
          <p>Download our case study booklet to see how our customers are improving outcomes with real results.</p>
          <div class="center" style="margin-top:2em;">
            <?php hubspot_button('5230d784-296e-47df-96e2-88aab7a25cab', "Download The Innovators Booklet"); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Hero -->

    <section class="container__centered">

      <h1 class="js__seo-tool__title"><?php print $title; ?></h1>
      <!--<p>View additional <a href="<?php print $url; ?>/collaborative-solution-case-studies">case studies of MEDITECH customers</a>, provided through our collaborative solution providers.</p>-->

      <fieldset id="topics">
      <legend>Filter by Topic:</legend>
        <?php print views_embed_view('case_study_topics', 'block'); // adds 'case study topics' Views block... ?>
      </fieldset>
      
      <div class="auto-margins">
        <p>Read our <a href="https://ehr.meditech.com/collaborative-solution-case-studies">collaborative solution case studies</a> to learn how MEDITECH works with the industry's leading developers, consultants, and system integrators to explore new functionality and optimized workflows for our EHR.</p>
      </div>
     
      <div class="js__seo-tool__body-content">
        <?php print views_embed_view('case_studies_3_column', 'block'); // adds 'Case Studies - 3-Column' Views block... ?>
      </div>

    </section> 



    <script>  
    // table filter search function...
    // When document is ready: this gets fired before body onload...
    var $jq = jQuery.noConflict();

    $jq(document).ready(function(){
      // FILTER FUNCTION...

      // filter by checkbox selections...
      $jq("fieldset#topics input").click(function(){
        // get checked values using functions below...
        var selectedTopics = getTopicValues();

        // start fresh every time by displaying all DIVs first...
        $jq("div.container__thirds").show();

        if(selectedTopics && selectedTopics.length > 0){
          // hide all DIVs...
          $jq("div.container__thirds").hide();

          for(var t = 0; t < selectedTopics.length; t++){

            $jq("div.container__thirds").each(function(){
              var classList = (this).className.split(/\s+/);
              for(var i = 0; i < classList.length; i++){
                if( classList[i] == selectedTopics[t] ){
                  $jq(this).show();
                }
              }
            });

          }

        }
      });

      function getTopicValues(){
        // create an array to hold the values...
        var topics = [];
        // look for all location check boxes that are checked and add value to array...
        $jq("fieldset#topics input:checkbox:checked").each(function() {
          topics.push($jq(this).val());
        });
        return topics;
      }

      $jq("div.container__thirds").each(function(){
        // get height of container (could be different by device)...
        var containerH = $jq(this).height();
        // get height of inner content...
        var contentH = $jq(this).find("div.container__content").height();
        // get height difference...
        var hDiff = containerH - contentH;
        // set top and bottom margins of inner DIV...
        var newMargin = (hDiff / 2).toFixed(2);
        console.log(containerH + ' | ' + contentH + ' | ' + hDiff + ' | ' + newMargin);
        $jq("div.container__content", this).css('margin', newMargin + 'px 0');
      });

    });
    </script>
    
<!-- end simple-page--node-1863.tpl.php template -->