<!-- start user-login.tpl.php template -->
  <section class="container__centered">
	<div class="container__two-thirds">
      <div>
        <h1>Welcome back to MEDITECH.com</h1>
        <?php print drupal_render_children($form) ?>
      </div>
    </div>
  </section>
<!-- end user-login.tpl.php template -->