<!-- start views-view-fields--events-related--block.tpl.php template -->
<?php 
  // This template is for each row of the Views block: EVENTS - RELATED ....................... 
?>
<h2 class="no-margin--bottom">Related Event</h2>
<div class="sb-article--container" style="border-bottom: 0;">
  <div class="sb-article--left">
    <p class="sidebar--newsarticle__title no-margin"><a class="news_related_article_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></p>
  </div>
  <div class="sb-article--right"><a class="news_related_article_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['field_event_image']->content; ?></a></div>
</div>

<!-- end views-view-fields--events-related--block.tpl.php template -->
