<?php //This template is set up to control the display of the 'Video' content type page -- seen through Google search

$url = $GLOBALS['base_url']; // grabs the site url

// redirect user if they are coming to a lone video page...
$nid = $node->nid;

if( user_is_logged_in() ){
?>

<!-- start node--video.tpl.php template -->
 
    <!-- Title section -->
    <div class="container--page-title">
      <div class="container__centered">
        <p class="page__title--main">Videos <?php if(node_access('update',$node)){ print '<span style="font-size:12px;">'; print l( t('Edit'),'node/'.$node->nid.'/edit' ); print "</span>"; } ?> -- THIS PAGE SHOULD ONLY BE SEEN BY MEDITECH EMPLOYEES WITH DRUPAL ACCESS</p>
      </div>
    </div>  
    
      
    <!-- VIDEO SECTION ---------------------------------------------- -->
    <div class="content__callout">
      <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
         
          <div class="video js__video" data-video-id="<?php print render($content['field_video_url']); ?>">
            <figure class="video__overlay">
              <?php // if the overlay image was added by user, then show overlay image, otherwise show default overlay image...
                if( !empty($content['field_video_overlay_image']) ){
                  print render($content['field_video_overlay_image']); 
                }
                else{
              ?>
                  <img src="<?php print $url; ?>/sites/all/themes/meditech/images/video-overlay.jpg" alt="Video Covershot">
              <?php } ?>
            </figure>
            <a class="video__play-btn" href="https://vimeo.com/<?php print render($content['field_video_url']); ?>?&autoplay=1" class="video__play-btn"></a>
            <div class="video__container"></div>
          </div>   
          
        </div>
      </div>
      <div class="content__callout__content">
        <div class="content__callout__body">
         <h2><?php print $title; ?></h2>
          <p class="content__callout__body__text--large"><?php print render($content['field_video_description']); ?></p>
          <?php if(!empty($content['field_video_button_link'])){ ?>
          <div class="btn-holder--content__callout">
              <a class="btn--orange video_link_gae" href="<?php print render($content['field_video_button_link']); ?>"><?php print render($content['field_video_button_text']); ?></a>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <!-- END VIDEO SECTION -->

<!-- end node--video.tpl.php template -->

<?php 
// non-logged in users get redirected...
} else { 
  
  // use Video Content View to figure out which article the video is connected to...
  $view_content = views_embed_view('video_content', 'block', $nid);

  // pull URL from View content...
  // content uses | in views-view-fields-video-content-block template...
  $view_content_array = explode('|', $view_content);
  $new_URL = $view_content_array[1];
  // redirect to news article that has the video (if exists)...
  if( isset($new_URL) && $new_URL != '' ){
    // redirect to article page...
    header('Location: '.$url.''.$new_URL);
  }
  else{
    // no article association, go to main videos page...
    header('Location: https://ehr.meditech.com/news-tags/videos');
  }
  
}
?>