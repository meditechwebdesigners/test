<?php // This template is set up to control the display of node LANDING-PAGE 

$url = $GLOBALS['base_url']; // grabs the site url 
?>
<!-- start node--landing-page.tpl.php template -->


  <?php // check to see if FORM should be shown or not...
    $showForm = field_get_items('node', $node, 'field_show_form');
    $showFormValue = $showForm[0]['value'];  

    // Check to see what TYPE of landing page is being shown ::::::::::::::::::::::::::::
    $type = field_view_field('node', $node, 'field_landing_page_type');
    // does the Landing Page Type field have a value...
    if(!empty($type)){
      // get field value...
      foreach($type['#items'] as $t){
        $typeTerm = taxonomy_term_load($t['tid']);
        $typeName = $typeTerm->name;
      }
    }
    else{
      $typeName = '';
    }
  ?>

<div class="container__centered" style="padding-bottom:2em;">
  
  <h1 class="no-margin--bottom js__seo-tool__title"><?php print $title; ?></h1>
  
  <?php
    // if form should be shown, show date also if date exists...
    if($showFormValue == 1){      
      // get event date data...
      if( !empty($content['field_date']) ){
        $webinarDate = field_get_items('node', $node, 'field_date');

        // get first date data...
        $date1 = date_create($webinarDate[0]['value']);
        $day1 = date_format($date1, 'l, F j, Y');
        $time1 = date_format($date1, 'g:i A');
        print '<p class="js__seo-tool__body-content"><strong>';
        print $day1.', '.$time1.' EST';
        print '</strong></p>';
      }
    }
  ?>
  
  <div class="container__one-half">
    <div class="js__seo-tool__body-content">
      <?php print render($content[ 'field_body']); ?>
    </div>
    
    <?php
      // if form should NOT be shown, show message...
      if($showFormValue != 1){
        print '<div class="js__seo-tool__body-content">';
        print '<p><strong>This webinar is either full or has all ready taken place.</strong></p>';
        print '</div>';
      }
    
      // which list of landing pages to show...
      if($showFormValue == 1){
        switch($typeName){
          case 'Webinar':
            print '<div class="js__seo-tool__body-content">';
            print '<h3 style="margin-top:2em;">Other Webinars</h3>';
            print views_embed_view('upcoming_webinars', 'block'); // adds 'Upcoming Webinars' Views block...
            print '</div>';
            break;
          case 'White Paper':
          case 'Case Study':
            print '<div class="js__seo-tool__body-content">';
            print '<h3 style="margin-top:2em;">Other White Papers</h3>';
            print views_embed_view('other_white_papers', 'block'); // adds 'Other White Papers' Views block...
            print '</div>';
            break;
          default:
            print '';
        }
      }
    ?>
  </div>

  <div class="container__one-half">

    <?php 
      if($showFormValue == 1){
        // show form...
    ?>   

    <?php
      if($node->nid == 1145){ /* ==================================================================================================== */
        // print "Web Amb Demo for the Media" form...
    ?>
    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">

      <form class="form" action="https://docs.google.com/a/meditech.com/forms/d/1iYWdWFiGztvld0VpxFXPIKQXuhKzhYdPJwqSxoG73AM/formResponse" method="POST" id="ss-form" target="_self" onsubmit="">

        <div class="ss-form-entry">
          <label class="ss-q-item-label" for="entry_280194239">Full Name<span class="form-required" aria-hidden="true">*</span></label>
          <input type="text" name="entry.280194239" value="" class="ss-q-short" id="entry_280194239" dir="auto" aria-label="Full Name  " aria-required="true" required="" title="">
        </div>

        <div class="ss-form-entry">
          <label class="ss-q-item-label" for="entry_1212014326">Business Email<span class="form-required" aria-hidden="true">*</span></label>
          <input type="text" name="entry.1212014326" value="" class="ss-q-short" id="entry_1212014326" dir="auto" aria-label="Business Email  " aria-required="true" required="" title="">
        </div>

        <div class="ss-form-entry">
          <label class="ss-q-item-label" for="entry_1185396401">Publication<span class="form-required" aria-hidden="true">*</span></label>
          <input type="text" name="entry.1185396401" value="" class="ss-q-short" id="entry_1185396401" dir="auto" aria-label="Publication  " aria-required="true" required="" title="">
        </div>

          <input type="hidden" name="draftResponse" value="[]">
          <input type="hidden" name="pageHistory" value="0">

          <input type="submit" name="submit" value="Sign Up" id="ss-submit" class="full-width">

      </form>

    </div>
    
    
    <?php 
      }
      elseif($node->nid == 1764){ /* ==================================================================================================== */
        // print "HIMSS17 demos" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1763');
          print render($webform_form['content']); 
        ?>
      </div>

    </div> 


    <?php 
      }
      elseif($node->nid == 1033){ /* ==================================================================================================== */
        // print "population health white paper" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1046');
          print render($webform_form['content']); 
        ?>
      </div>

    </div> 


    <?php 
      }
      elseif($node->nid == 1384){ /* ==================================================================================================== */
        // print "surveillance white paper" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1386');
          print render($webform_form['content']); 
        ?>
      </div>

    </div> 
    
    
    <?php 
      }
      elseif($node->nid == 1464){ /* ==================================================================================================== */
        // print "avera sepsis white paper" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1465');
          print render($webform_form['content']); 
        ?>
      </div>

    </div> 
    
    
    <?php 
      }
      elseif($node->nid == 1469){ /* ==================================================================================================== */
        // print "avera sepsis white paper" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1470');
          print render($webform_form['content']); 
        ?>
      </div>

    </div> 
    
    
    <?php 
      }
      elseif($node->nid == 1512){ /* ==================================================================================================== */
        // print "Benefits of an Integrated Approach to Critical Care white paper" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1510');
          print render($webform_form['content']); 
        ?>
      </div>

    </div>
    
    <?php 
      }
      elseif($node->nid == 1592){ /* ==================================================================================================== */
        // print "MEDITECH’s EHR Steers Avera McKennan ED Nurse Navigator Program to $475,000 Annual Savings" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1594');
          print render($webform_form['content']); 
        ?>
      </div>

    </div> 
    
    
    <?php 
      }
      elseif($node->nid == 1608){ /* ==================================================================================================== */
        // print "Video: How MEDITECH’s Web EHR Improves Physician Productivity" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1609');
          print render($webform_form['content']); 
        ?>
      </div>

    </div> 
    
    
    <?php 
      }
      elseif($node->nid == 1659){ /* ==================================================================================================== */
        // print "Case Study: It’s in Their DNA - Avera Health Drives Precision Medicine at the Point of Care" form...
    ?>    

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">        

      <div class="form">
        <?php
          // adds webform block...
          $webform_form = module_invoke('webform', 'block_view', 'client-block-1660');
          print render($webform_form['content']); 
        ?>
      </div>

    </div> 
    

    <?php
      }
      else{ /* ==================================================================================================== */ 

        // print all other webinar forms...
        switch($node->nid){
          case 1017: // leveraging data exchange webinar
            $formID = "13DdcRLqicr_obMPfDHIdsefhVrb8K5UaePbzOIl6hms";
            break;
          case 1020: // WEB EHR webinar
            $formID = "1wEyXg1MXuwqJp6abQnb_QWbADkvEYZeIOOYpLWa5Sds";
            break;
          case 1606: // WEB EHR webinar 12-21-16
            $formID = "1OGetmWqTuoCEh6PZXMst81yPLQqPkA-aDLLmXaeLdlc";
            break;
          case 1632: // WEB EHR webinar 12-15-16
            $formID = "1gFu8lKuCQ-y45bceBdlRkBmOyXV97jGoAKXWECyxCjc";
            break;
          case 1130: // patient portal webinar
            $formID = "10lU91Zs9QJ_r48-S7rZHMH9lOxkiMNPy3eDvhUG2K78";
            break;
          case 1156: // WEB Ambulatory webinar
            $formID = "1C9iA4gx32HK0A6aqWzNgXnoSQ8Fv4TIYRYXH8T5WdbI";
            break;
          case 1605: // WEB Ambulatory webinar 12-13-16
            $formID = "1cEtnAVvwKhlF4gf2z-FWXZE0Cw5jLaEq7fQEaS6X8P8";
            break;
          case 1227: // critical care webinar
            $formID = "1sLagpIq0HfoKhhisr5GWYHSyRo_i6MJaCBpXSjHqNTU";
            break;
          case 1252: // business and critical analyst webinar
            $formID = "1h7356B_f0wFyfpCRpSEvx8k72dsLIZVlWBnpeJTjVK0";
            break;
          case 1253: // oncology webinar
            $formID = "1AW2l7_GAhbNCzhTb26qh658ZkXKaVCxIuYbZGKf3NZs";
            break;
          case 1306: // surgical webinar
            $formID = "1KyLbreI2PlUx7H8YMy6JF6JvisUOMjCiJDkt_jqTV-s";
            break;
          case 1441: // care coordination webinar
            $formID = "1Kpq_r-4Om_OxNdiqi_-OtERtLPDLi7qhBXq0NFk4Rxo";
            break;
          case 1597: // Nursing Care Coordination with MEDITECH
            $formID = "1LLkGd4G_-fYKfBbx6J_tRHwE5uLyX_geYPLW1gswFuM";
            break;
          case 1613: // Making Home Care an Integrated Part of Your Healthcare Strategy - 12-06-16
            $formID = "1bZpEy1ppVmV4oGvsQ2mc5OdxP6ShLuwhQcqXuw1noZU";
            break;
        }
     ?>

    <div class="form-required" aria-hidden="true">
      <p style="text-align:right; margin-bottom:0;">* Required</p>
    </div>

    <div class="ss-form">          

      <form class="form" action="https://docs.google.com/a/meditech.com/forms/d/<?php print $formID; ?>/formResponse" method="POST" id="ss-form" target="_self" onSubmit="">

        <div class="ss-form-question errorbox-good" role="listitem">
          <label class="ss-q-item-label" for="entry_1440193910">First Name<span class="form-required" aria-hidden="true">*</span></label>
          <input type="text" name="entry.1440193910" value="" class="ss-q-short" id="entry_1440193910" dir="auto" aria-label="First Name" aria-required="true" required="" title="">
        </div>

        <div class="ss-form-question errorbox-good" role="listitem">
          <label class="ss-q-item-label" for="entry_127049767">Last Name<span class="form-required" aria-hidden="true">*</span></label>
          <input type="text" name="entry.127049767" value="" class="ss-q-short" id="entry_127049767" dir="auto" aria-label="Last Name" aria-required="true" required="" title="">
        </div>

        <div class="ss-form-question errorbox-good" role="listitem">
          <label class="ss-q-item-label" for="entry_487775225">Business Email<span class="form-required" aria-hidden="true">*</span></label>
          <input type="text" name="entry.487775225" value="" class="ss-q-short" id="entry_487775225" dir="auto" aria-label="Business Email " aria-required="true" required="" title="">
        </div>

        <div class="ss-form-question errorbox-good" role="listitem">
          <label class="ss-q-item-label" for="entry_1013105174">Organization<span class="form-required" aria-hidden="true">*</span></label>
          <input type="text" name="entry.1013105174" value="" class="ss-q-short" id="entry_1013105174" dir="auto" aria-label="Organization" aria-required="true" required="" title="">
        </div>

        <div class="ss-form-question errorbox-good" role="listitem">
          <label class="ss-q-item-label" for="entry_558756355">Job Title<span class="form-required" aria-hidden="true">*</span></label>
          <input type="text" name="entry.558756355" value="" class="ss-q-short" id="entry_558756355" dir="auto" aria-label="Job Title" aria-required="true" required="" title="">
        </div>

        <div class="ss-form-question errorbox-good" role="listitem">
          <label class="ss-q-item-label" for="entry_874627665">State/Province<span class="form-required" aria-hidden="true">*</span></label>
          <select name="entry.874627665" id="entry_874627665" aria-label="State/Province" aria-required="true" required="" title="">
          <option value="">-- Select a State, Province or International --</option>
            <option value="international">International</option>
          <optgroup label="US States">
            <option value="AL">Alabama</option>
            <option value="AK">Alaska</option>
            <option value="AZ">Arizona</option>
            <option value="AR">Arkansas</option>
            <option value="CA">California</option>
            <option value="CO">Colorado</option>
            <option value="CT">Connecticut</option>
            <option value="DE">Delaware</option>
            <option value="DC">District Of Columbia</option>
            <option value="FL">Florida</option>
            <option value="GA">Georgia</option>
            <option value="HI">Hawaii</option>
            <option value="ID">Idaho</option>
            <option value="IL">Illinois</option>
            <option value="IN">Indiana</option>
            <option value="IA">Iowa</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
            <option value="LA">Louisiana</option>
            <option value="ME">Maine</option>
            <option value="MD">Maryland</option>
            <option value="MA">Massachusetts</option>
            <option value="MI">Michigan</option>
            <option value="MN">Minnesota</option>
            <option value="MS">Mississippi</option>
            <option value="MO">Missouri</option>
            <option value="MT">Montana</option>
            <option value="NE">Nebraska</option>
            <option value="NV">Nevada</option>
            <option value="NH">New Hampshire</option>
            <option value="NJ">New Jersey</option>
            <option value="NM">New Mexico</option>
            <option value="NY">New York</option>
            <option value="NC">North Carolina</option>
            <option value="ND">North Dakota</option>
            <option value="OH">Ohio</option>
            <option value="OK">Oklahoma</option>
            <option value="OR">Oregon</option>
            <option value="PA">Pennsylvania</option>
            <option value="RI">Rhode Island</option>
            <option value="SC">South Carolina</option>
            <option value="SD">South Dakota</option>
            <option value="TN">Tennessee</option>
            <option value="TX">Texas</option>
            <option value="UT">Utah</option>
            <option value="VT">Vermont</option>
            <option value="VA">Virginia</option>
            <option value="WA">Washington</option>
            <option value="WV">West Virginia</option>
            <option value="WI">Wisconsin</option>
            <option value="WY">Wyoming</option>
          </optgroup>
          <optgroup label="Canadian Provinces">
            <option value="AB">Alberta</option>
            <option value="BC">British Columbia</option>
            <option value="MB">Manitoba</option>
            <option value="NB">New Brunswick</option>
            <option value="NL">Newfoundland and Labrador</option>
            <option value="NS">Nova Scotia</option>
            <option value="ON">Ontario</option>
            <option value="PE">Prince Edward Island</option>
            <option value="QC">Quebec</option>
            <option value="SK">Saskatchewan</option>
          </optgroup>
        </select>
        </div>  

        <div class="ss-form-question errorbox-good" role="listitem">
          <label class="ss-q-item-label" for="entry_569094667">Country/Region<span class="form-required" aria-hidden="true">*</span></label>
          <select name="entry.569094667" id="entry_569094667" aria-label="Country/Region" aria-required="true" required="" title="">
            <option value="">-- Select a Country --</option>
            <option value="US">United States</option>
            <optgroup label="US Territories">
              <option value="AS">American Samoa</option>
              <option value="GU">Guam</option>
              <option value="PR">Puerto Rico</option>
              <option value="VI">United States Virgin Islands</option>
            </optgroup>
            <option value="CAN">Canada</option>
            <option value="AL">Albania</option>
            <option value="AI">Anguilla</option>
            <option value="AG">Antigua and Barbuda</option>
            <option value="AR">Argentina</option>
            <option value="AW">Aruba</option>
            <option value="AU">Australia</option>
            <option value="BS">Bahamas</option>
            <option value="BH">Bahrain</option>
            <option value="BB">Barbados</option>
            <option value="BE">Belgium</option>
            <option value="BZ">Belize</option>
            <option value="BM">Bermuda</option>
            <option value="BW">Botswana</option>
            <option value="BR">Brazil</option>
            <option value="VG">British Virgin Islands</option>
            <option value="BN">Brunei</option>
            <option value="BG">Bulgaria</option>
            <option value="KY">Cayman Islands</option>
            <option value="CL">Chile</option>
            <option value="CN">China</option>
            <option value="CO">Colombia</option>
            <option value="CR">Costa Rica</option>
            <option value="CW">Cura&#231;ao</option>
            <option value="CY">Cyprus</option>
            <option value="DO">Dominican Republic</option>
            <option value="EC">Ecuador</option>
            <option value="EG">Egypt</option>
            <option value="KN">Federation of Saint Kitts and Nevis</option>
            <option value="FI">Finland</option>
            <option value="FR">France</option>
            <option value="DE">Germany</option>
            <option value="GD">Grenada</option>
            <option value="HN">Honduras</option>
            <option value="HK">Hong Kong</option>
            <option value="IS">Iceland</option>
            <option value="IN">India</option>
            <option value="IE">Ireland</option>
            <option value="IM">Isle of Man</option>
            <option value="IT">Italy</option>
            <option value="JM">Jamaica</option>
            <option value="JP">Japan</option>
            <option value="KW">Kuwait</option>
            <option value="MY">Malaysia</option>
            <option value="MH">Marshall Islands</option>
            <option value="MX">Mexico</option>
            <option value="MS">Montserrat</option>
            <option value="NA">Namibia</option>
            <option value="NL">Netherlands</option>
            <option value="NZ">New Zealand</option>
            <option value="NG">Nigeria</option>
            <option value="OM">Oman</option>
            <option value="PK">Pakistan</option>
            <option value="PA">Panama</option>
            <option value="PE">Peru</option>
            <option value="PT">Portugal</option>
            <option value="QA">Qatar</option>
            <option value="KN">Saint Kitts and Nevis</option>
            <option value="LC">Saint Lucia</option>
            <option value="MF">Saint Martin</option>
            <option value="SA">Saudi Arabia</option>
            <option value="SG">Singapore</option>
            <option value="ZA">South Africa</option>
            <option value="KR">South Korea</option>
            <option value="ES">Spain</option>
            <option value="VC">St. Vincent and the Grenadines</option>
            <option value="SE">Sweden</option>
            <option value="CH">Switzerland</option>
            <option value="SY">Syria</option>
            <option value="TH">Thailand</option>
            <option value="TT">Trinidad and Tobago</option>
            <option value="TR">Turkey</option>
            <option value="TC">Turks and Caicos Islands</option>
            <option value="AE">United Arab Emirates</option>
            <option value="GB">United Kingdom</option>
          </select>
        </div>

        <div class="ss-form-question errorbox-good" role="listitem">
          <label class="ss-q-item-label" for="entry_638086325">What is the biggest challenge facing your organization today?</label>
          <textarea name="entry.638086325" class="ss-q-short" id="entry_638086325" dir="auto" aria-label="What is the biggest challenge facing your organization today?"  cols="" rows="5" maxlength="255" style="margin-bottom:0;"></textarea>
          <p class="small"><em>This field has a limit of 255 characters.</em></p>
        </div>

        <input type="hidden" name="draftResponse" value="[]">
        <input type="hidden" name="pageHistory" value="0">

        <input type="submit" class="full-width" name="submit" value="Sign Up" id="ss-submit" style="margin-top:1em;">

      </form>

      <script>
        var $jq = jQuery.noConflict();
        $jq(document).ready(function(){ 
          $jq("textarea[maxlength]").bind('input propertychange', function(){
            var maxLength = $jq(this).attr('maxlength');
            // It's possible that JavaScript is treating a newline as 1 character rather than 2.
            // Detect how many newlines are in the textarea, then be sure to count them twice as part of the length of the input.
            var newlines = ($jq(this).val().match(/\n/g) || []).length
            if($jq(this).val().length + newlines > maxLength){
              $jq(this).val($jq(this).val().substring(0, maxLength - newlines));
            }
          })
        });
      </script>

    <?php        
      } /* ==================================================================================================== */ 

    }
    else{
      // which list of landing pages to show...
      switch($typeName){
        case 'Webinar':
          print '<div class="js__seo-tool__body-content">';
          print '<h3 style="margin-top:2em;">Other Webinars</h3>';
          print views_embed_view('upcoming_webinars', 'block'); // adds 'Upcoming Webinars' Views block...
          print '</div>';
          break;
        case 'White Paper':
        case 'Case Study':
          print '<div class="js__seo-tool__body-content">';
          print '<h3 style="margin-top:2em;">Other White Papers</h3>';
          print views_embed_view('other_white_papers', 'block'); // adds 'Other White Papers' Views block...
          print '</div>';
          break;
        default:
          print '';
      }
    }
    ?>

    </div>

  </div>

</div>

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
<!-- end node--landing-page.tpl.php template -->