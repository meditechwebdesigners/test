<?php // This template is set up to control the display of the CAMPAIGN_CRITICAL_CARE content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_critical_care.tpl.php template -->
 
<style>
  .button-vertical-adjustment { margin-top: 6em; }
  @media only screen and (max-width: 50em) {
    .button-vertical-adjustment { margin-top: 0; }
  }
</style>
  
<div class="js__seo-tool__body-content">  

  <!-- BLOCK 1 --> 
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/two-nurses-in-hallway-blurred-01.jpg); border-bottom:none;">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white text-shadow--black">
        <h1 class="text--white js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
        <h2 class="no-margin--top"><?php print render($content['field_header_1']); ?></h2>
        <div><?php print render($content['field_long_text_1']); ?></div>
      </div>
      <div class="container__one-third">
        <div class="btn-holder--content__callout">
          <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot button-vertical-adjustment">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>    
    </div>
  </div>
  <!-- BLOCK 1 -->


  <!-- BLOCK 2 -->
  <div class="container">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__one-third">
          <h2><?php print render($content['field_header_2']); ?></h2>
          <div>
            <?php print render($content['field_long_text_2']); ?>
          </div>
        </div>
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Critical-Care-Management_screenshot-01.png" alt="MEDITECH Critical Care Management Screenshot">
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 2 -->


  <!-- BLOCK 3 -->
  <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-background-with-faint-lines.jpg);">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Critical-Care-Management_screenshot-02.png" alt="MEDITECH Critical Care Management Screenshot">
          </figure>
        </div>
        <div class="container__one-third text--white">
          <h2><?php print render($content['field_header_3']); ?></h2>
          <div>
            <?php print render($content['field_long_text_3']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 3 -->


  <!-- BLOCK 4 -->
  <div class="container bg--light-gray">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__one-third">
          <h2><?php print render($content['field_header_4']); ?></h2>
          <div>
            <?php print render($content['field_long_text_4']); ?>
          </div>
        </div>
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Critical-Care-Management_screenshot-03.png" alt="MEDITECH Critical Care Management Screenshot">
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 4 -->


  <!-- BLOCK 5 -->
  <div class="container">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/critical-care-monitors-talking.png" alt="hospital monitors communicating with MEDITECH software">
          </figure>
        </div>
        <div class="container__one-third">
          <h2><?php print render($content['field_header_5']); ?></h2>
          <div>
            <?php print render($content['field_long_text_5']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 5 -->


  <!-- BLOCK 6 -->
  <!-- this is usually a View but campaigns tend to use there own text rather than original text upload with video -->
  <div class="content__callout border-none">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="<?php print render($content['field_video_url']); ?>">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--clinically-crafted-for-substantial-savings.jpg" alt="Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/<?php print render($content['field_video_url']); ?>"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
         <h2><?php print render($content['field_header_6']); ?></h2>
          <?php print render($content['field_long_text_6']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 6 -->


  <!-- BLOCK 7 -->
  <div class="container">
    <div class="container__centered center">
      <h2><?php print render($content['field_header_7']); ?></h2>
      <div>
        <?php print render($content['field_long_text_7']); ?>
      </div>
    </div>
  </div>
  <!-- BLOCK 7 -->


  <!-- BLOCK 8 -->
  <?php $block_quote = field_collection_data($node, 'field_fc_quote_1'); ?>
  <div class="container bg--black-coconut">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <?php print $block_quote->field_quote_1['und'][0]['value']; ?>
        </div>
        <p class="text--large no-margin--bottom"><?php print $block_quote->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $block_quote->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  <!-- BLOCK 8 -->
  
</div><!-- end js__seo-tool__body-content -->  


<!-- BLOCK 9 -->
<div class="container">
  <div class="container__centered center">
    <h2 class="js__seo-tool__body-content"><?php print render($content['field_header_8']); ?></h2>
    <div class="btn-holder--content__callout">
      <?php 
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
        <div class="button--hubspot">
          <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
        </div>
      <?php }else{ ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
      <?php } ?>
    </div>
    
       <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
  </div>
</div>
<!-- BLOCK 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
<!-- end node--campaign_critical_care.tpl.php template -->