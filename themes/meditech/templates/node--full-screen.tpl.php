<?php // This template is set up to control the display of the FULL-SCREEN content type
$url = $GLOBALS['base_url']; // grabs the site url

$node_id = $node->nid;
?>
<!-- start node--full-screen.php template -->

<style>
  #skip-link,
  header.header,
  nav.nav,
  div.slideoutnav,
  #outdated,
  #page > div.container--page-title,
  footer.footer
    { display: none; }
</style>

<?php
// HIMSS18 Tweetup Screen page...
if($node_id == 2343){
  $background = $url.'/sites/all/themes/meditech/images/full-screen/tweetup_50in-TV-background-with-tweets_1920x1080.jpg';
  $full_screen_content = '
  <div style="position:relative; width:400px; top:40px; left:1530px;">
  <a class="twitter-timeline" href="https://twitter.com/search?q=%40MEDITECH%20%23HIMSS18" data-width="360" data-height="1000" data-widget-id="966092959274995714">Tweets about @MEDITECH #HIMSS18</a>
  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
  </div>
  ';
}
?>

<div style="width:1920px; height:1080px; margin:0; padding:0; background:url(<?php print $background; ?>) no-repeat top;">
  <?php print $full_screen_content; ?>
</div>
        
<!-- end node--full-screen.tpl.php template -->