<!-- start customer-materials-page-menu--node-2727.php Expanse Materials template -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url  

$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>

<style> 
  a.anchor { display: block; position: relative; top: -195px; visibility: hidden;	} 
  .transparent-overlay { margin-bottom: 2em; }
  .transparent-overlay > div.center { color:#ffffff; padding-bottom:.5em; }
  
  .thumbnails p { width: 33%; float: left; }
  .thumbnails a { border-bottom: 0; }
  .thumbnails a[href$=".pdf"]:after, .thumbnails a[target="_blank"]:after { content: ""; }
</style>



<!-- Main Content -->
<div class="container__centered" style="padding-top:2em; margin-bottom:1em;">

  <div class="container__two-thirds">
    <div>
      <?php
      if( isset($content_sections) && !empty($content_sections) ){
        $number_of_content_sections = count($content_sections);
        for($cs=0; $cs<$number_of_content_sections; $cs++){
          print '<h2>';
          print $content_sections[$cs]->field_header_1['und'][0]['value']; 
          print '</h2>';
          if($cs==1){
            print '<div class="thumbnails">';
          }
          else{
            print '<div>';
          }
          print $content_sections[$cs]->field_long_text_1['und'][0]['value']; 
          print '</div>';
        }
      }
      ?>
    </div>
  </div>
  
  	
  <!--
  <aside class="container__one-third panel">
    <?php
    if( isset($side_menu) && !empty($side_menu) ){
      print '<div class="sidebar__nav solutions_sidebar_gae">';
      print '<ul class="menu">';
      $number_of_menu_links = count($side_menu);
      for($ml=0; $ml<$number_of_menu_links; $ml++){
        print '<li><a href="';
        print $side_menu[$ml]->field_link_url_1['und'][0]['value']; 
        print '">';
        print $side_menu[$ml]->field_link_text_1['und'][0]['value']; 
        print '</a></li>';
      }
      print '</ul>';
      print '</div>';
    }
    ?>
  </aside>
  -->
  
</div>
<!-- end customer-materials-page-menu--node-2727.php Expanse Materials template -->