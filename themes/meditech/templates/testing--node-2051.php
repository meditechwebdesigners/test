<?php
// ************* Hide Published Content for AVP Review **********
if( !isset($_GET['mtid']) || $_GET['mtid'] != 'review' ){
  print '<script type="text/javascript">';
  print 'window.location.replace("https://ehr.meditech.com/not-meant-for-your-eyes");';
  print '</script>';
}
else{
?>

<!-- start testing--node-2051.tpl.php template -->
<!-- CAREERS FILTER TEST -->
<style>
  #jobListings tbody:last-child tr:last-child>td:first-child,
  #jobListings tbody:last-child tr:last-child>td:last-child
   { padding: .5em; }
  fieldset { padding: 1em 1.5em; }
  legend { border: 0; padding: 0 .5em; font-weight: bold; }
</style>
       
<!-- tablesorter js script -->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/jquery.tablesorter.min.js"></script> 
<script>
  var $jq = jQuery.noConflict();

  // ***************************************************************
  // tablesorter - - - https://github.com/christianbach/tablesorter
  // ***************************************************************

  $jq(".tablesorter").tablesorter();

  $jq(document).ready(function(){             
    // [1,0] sets first column to sort in ascending order by default
    // [0,0] leaves 2nd column alone
    $jq("#jobListings").tablesorter( {sortList: [[1,0], [0,0]]} );
  }); 
</script>       
        
  <section class="container__centered">

    <h1 id="jobs">MEDITECH Job Opportunities</h1>

    <?php print render($content['field_body']); ?>

    <div class="container__one-third">
      <fieldset id="locations">
      <legend>Filter by Locations:</legend>
        <?php print views_embed_view('job_locations', 'block'); // adds 'job locations' Views block... ?>
      </fieldset>
      <fieldset id="categories">
      <legend>Filter by Categories:</legend>
        <?php print views_embed_view('job_categories', 'block'); // adds 'job categories' Views block... ?>
      </fieldset>

      <!--
      <label for="kwd_search">Search Job Listings:</label> 
      <input style="padding:.5em;" type="text" id="kwd_search" value=""/>  
      -->
    </div>

    <div class="container__two-thirds">
      <?php print views_embed_view('job_listings_search', 'block'); // adds 'job listings search' Views block... ?>
    </div>

  </section>
        
<script>  
// table filter search function...
// When document is ready: this gets fired before body onload...
$jq(document).ready(function(){
  
  // JOB SEARCH FUNCTION...
  
  // Write on keyup event of keyword input element...
  /*
  $jq("#kwd_search").keyup(function(){
    // When value of the input is not blank
    if( $jq(this).val() != ""){
      // Show only matching TR, hide rest of them
      $jq("#jobListings tbody>tr").hide();
      $jq("#jobListings td:contains-ci('" + $jq(this).val() + "')").parent("tr").show();
    }
    else{
      // When there is no input or clean again, show everything back
      $jq("#jobListings tbody>tr").show();
    }
  });
  */
  
  
  // JOB FILTER FUNCTION...
  
  // filter by checkbox selections...
  $jq("fieldset#locations input, fieldset#categories input").click(function(){
    // get checked values using functions below...
    var selectedLocations = getLocationValues();
    var selectedCategories = getCategoryValues();
    
    // start fresh every time by displaying all rows first...
    $jq("table#jobListings tbody tr").show();
    
    if(selectedLocations && selectedLocations.length > 0 || selectedCategories && selectedCategories.length > 0){
      // hide all rows...
      $jq("table#jobListings tbody tr").hide();
      
      $jq("table#jobListings tbody tr").each( function(){
        var cell3Content = $jq.trim( $jq(this).find('td').eq(2).text() );
        var cell2Content = $jq.trim( $jq(this).find('td').eq(1).text() );
        
        // location(s) is selected but not categories...
        if( selectedLocations.indexOf(cell3Content) > -1 && selectedCategories.length === 0  ){
          $jq(this).show();
        }
        // category(s) is selected but not locations...
        if( selectedCategories.indexOf(cell2Content) > -1 && selectedLocations.length === 0  ){
          $jq(this).show();
        }
        // if both are selected...
        if( selectedLocations.indexOf(cell3Content) > -1 && selectedCategories.indexOf(cell2Content) > -1 ){
          $jq(this).show();
        }
      });

    }
  });
  
  function getLocationValues(){
    // create an array to hold the values...
    var locations = [];
    // look for all location check boxes that are checked and add value to array...
    $jq("fieldset#locations input:checkbox:checked").each(function() {
      locations.push($jq(this).val());
    });
    return locations;
  }
  function getCategoryValues(){
    // create an array to hold the values...
    var categories = [];
    // look for all category check boxes that are checked and add value to array...
    $jq("fieldset#categories input:checkbox:checked").each(function() {
      categories.push($jq(this).val());
    });
    return categories;
  }
  
});

// jQuery expression for case-insensitive filter...
/*
$jq.extend($jq.expr[":"], {
  "contains-ci": function(elem, i, match, array){
    return (elem.textContent || elem.innerText || $jq(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
  }
});
*/
</script>
<!-- end testing--node-2051.tpl.php template -->


<?php } ?>