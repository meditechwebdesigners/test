<!-- START careers-hardcoded--node-2830.php -->

<?php
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .horizontal-menu {
    list-style-type: none;
    padding-left: 0;
  }

  .horizontal-menu li a {
    font-family: "montserrat", Verdana, sans-serif;
    font-weight: 500;
    float: left;
    display: block;
    padding: 1em 1.5em;
    margin-bottom: 0;
    text-decoration: none;
    border-bottom: 0;
    transition: 0.3s ease;
  }

  .horizontal-menu li:first-child {
    padding-left: 0em;
  }

  .horizontal-menu li a:hover {
    color: #fff;
    border-bottom: 0;
    background-color: #0a9178;
    transition: 0.3s ease;
  }

  .accordion__dropdown {
    border-left: none;
    border-right: none;
    border-bottom: 3px solid #087E68;
  }

  .accordion__dropdown ul {
    list-style-type: none;
    text-align: center;
    padding-left: 0;
    margin: 0;
  }

  .accordion__dropdown li {
    padding: .25em 0;
  }

  .video--loop {
    width: auto !important;
    left: 50%;
    top: 70%;
    position: absolute;
    min-width: 100%;
    min-height: 100%;
    transform: translate(-50%, -50%);
  }

  /*  Employee Bubbles */
  .eb-1 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Nhien-Bui.jpg);
    background-position: center top;
  }

  .eb-2 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Galvin-Chow.jpg);
    margin-left: auto;
    background-position: center top;
  }

  .eb-3 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Shaun-DaCunha.jpg);
    background-position: center top;
  }

  .eb-4 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Meaghan-Cloherty.jpg);
    background-position: center top;
    margin: auto;
  }

  .eb-5 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Marijo-Carnino.jpg);
    margin: auto;
    background-position: center top;
  }

  .eb-6 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Mike-Amend.jpg);
    margin-left: auto;
    background-position: center top;
  }

  .employee-bubble {
    width: 250px;
    height: 250px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    /*    position: relative;*/
    cursor: pointer;
    box-shadow: inset 0 0 0 0 rgba(0, 188, 111, 0.95);
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }

  .employee-bubble:hover .eb-info {
    opacity: 1;
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -o-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  .employee-bubble:hover {
    box-shadow: inset 0 0 0 135px rgba(0, 188, 111, 0.95);
  }

  .employee-card {
    padding-bottom: 1.5em;
  }

  .employee-card-plus {
    padding-top: 3em;
    padding-bottom: 1.5em;
  }

  .eb-info {
    position: absolute;
    width: 250px;
    height: 250px;
    border-radius: 50%;
    opacity: 0;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -o-transition: all 0.4s ease-in-out;
    -ms-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
    -webkit-transform: scale(0);
    -moz-transform: scale(0);
    -o-transform: scale(0);
    -ms-transform: scale(0);
    transform: scale(0);
    -webkit-backface-visibility: hidden;
  }

  .eb-info h3 {
    color: #fff;
    position: relative;
    padding: 80px 19px 5px 0;
    text-align: center;
  }

  .eb-info p {
    color: #fff;
    position: relative;
    padding: 0px 19px 0 0;
    text-align: center;
  }

  /*  Modal Window */
  .Modal {
    position: absolute;
    z-index: 99;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, 0);
    visibility: hidden;
  }

  .Modal .content {
    position: absolute;
    left: 50%;
    top: 35%;
    max-width: 1050px;
    width: 100%;
    padding: 3em 2em 3em 2em;
    border-radius: 6px;
    background: #fff;
    color: #3e4545;
    transform: translate(-50%, -30%) scale(0);
  }

  .Modal .close {
    position: absolute;
    top: 5px;
    right: 18px;
    cursor: pointer;
    color: #3e4545;
  }

  .Modal .close:before {
    content: '\2715';
    font-size: 30px;
  }

  .Modal.is-visible {
    visibility: visible;
    background: rgba(0, 0, 0, 0.75);
    transition: background 0.35s;
    transition-delay: 0.1s;
  }

  .Modal.is-visible .content {
    position: fixed;
    top: 55%;
    left: 50%;
    transform: translate(-50%, -50%) scale(1);
    transition: transform 0.35s;
  }

  .modal-employee {
    width: 200px;
    height: 200px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    margin: auto;
    margin-top: 1em;
    background-size: 115%;
  }

  .divided-box {
    background-color: rgba(255, 255, 255, .8);
    overflow: auto;
    display: flex;
    flex-flow: row wrap;
  }

  .left-box {
    width: 60%;
    float: left;
  }

  .right-box {
    width: 40%;
    padding: 6em;
    float: left;
  }

@media all and (min-width: 762px) {
    .dropdown-menu {
      display: none;
    }
  }

  @media all and (max-width: 760px) {
    .horizontal-menu {
      display: none;
    }
  }

  @media all and (max-width: 1040px) {

    .left-box,
    .right-box {
      width: 100%;
      padding: 2em;
    }
  }

  @media all and (min-width: 801px) and (max-width: 1000px) {
    .horizontal-menu li a {
      font-size: 85%;
      padding: 1em;
    }
  }

  @media all and (max-height: 700px) {
    .Modal.is-visible .content {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 90%;
      -webkit-overflow-scrolling: touch;
      border-radius: 0;
      transform: scale(1);
      margin-top: 4em;
      max-width: 100%;
      overflow-y: auto;
      padding-bottom: 6em;
    }
  }

  @media all and (max-width: 50em) {
    .Modal.is-visible .content {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      -webkit-overflow-scrolling: touch;
      border-radius: 0;
      transform: scale(1);
      margin-top: 5em;
      overflow-y: auto;
      padding-bottom: 6em;
    }

    .employee-card {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      margin-bottom: 1em;
    }

    .employee-card-plus {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      padding-top: 0;
      margin-bottom: 1em;
    }

    .employee-bubble {
      margin: auto;
      margin-bottom: 1em;
    }

</style>

<!-- Mobile Navigation -->
<div style="background-color: #087E68;">
  <div class="container__centered no-pad">
    <div class="accordion" style="margin-top: 0;">
      <ul class="accordion__list dropdown-menu">
        <li class="accordion__list__item" style="margin-bottom:0; background-color: #087E68;">
          <a class="accordion__link" href="#" style="color: #fff; font-size:1.2em">Careers Menu<div class="accordion__list__control"></div></a>
          <div class="accordion__dropdown">
            <ul>
              <li>
                <h3><a href="<?php print $url; ?>/careers/job-listings">Job Listings</a></h3>
              </li>
              <li>
                <h3><a href="<?php print $url; ?>/careers/applying-at-meditech">FAQs</a></h3>
              </li>
              <li>
                <h3><a href="<?php print $url; ?>/about-meditech/community">Community</a></h3>
              </li>
              <li>
                <h3><a href="<?php print $url; ?>/careers/benefits-perks">Benefits &amp; Perks</a></h3>
              </li>
              <li>
                <h3><a href="<?php print $url; ?>/careers/career-events">Recruiting Events</a></h3>
              </li>
               <li>
                <h3><a href="https://ehr.meditech.com/careers/veterans">Veterans</a></h3>
              </li>
            </ul>
            <a style="text-align: center;" class="js__accordion__toggle accordion__close" href="#"><i class="fa fa-minus-circle"></i>&nbsp; Close Menu</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- End Mobile Navigation -->

<!-- Block 1 -->
<div class=" container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/MEDITECH-employees-conversing.jpg); background-position:top;">
  <div class="container__centered">
    <div class="container__one-half transparent-overlay--xp" style="background-color: rgba(255,255,255,0.95); margin:4em 0;">
      <h1 class="header-two">Congratulations 2019 Graduates!</h1>
      <p>We know how hard you have worked for your degree, and how challenging the transition into a career can be. Our many entry-level positions provide recent graduates like you the perfect way to begin a career in one of the fastest-growing fields in the world.</p>
      <div class="btn-holder--content__callout no-margin--top">
        <a href="<?php print $url; ?>/careers/job-listings" class="btn--orange">Apply Now!</a>
      </div>
    </div>
  </div>
</div>
<!-- End Block 1 -->

<!-- Horizontal Navigation -->
<div style="background-color: #087E68;">
  <div class="container__centered no-pad">
    <ul class="horizontal-menu text--white" style="margin-top:-1em;">
      <li><a href="<?php print $url; ?>/careers/job-listings">Job Listings</a></li>
      <li><a href="<?php print $url; ?>/careers/applying-at-meditech">FAQs</a></li>
      <li><a href="<?php print $url; ?>/about-meditech/community">Community</a></li>
      <li><a href="<?php print $url; ?>/careers/benefits-perks">Benefits &amp; Perks</a></li>
      <li><a href="<?php print $url; ?>/careers/career-events">Recruiting Events</a></li>
      <li><a href="https://ehr.meditech.com/careers/veterans">Veterans</a></li>
    </ul>
  </div>
</div>
<!-- End Horizontal Navigation -->

<!-- Block 2 -->
<div class="bg--light-gray">
  <div class="container no-pad">
    <div class="divided-box">
      <div class="left-box background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/meeting-the-new-applicant-at-meditech.jpg); min-height:250px; background-position: top;">
      </div>
      <div class="right-box">
        <h2>Face Value</h2>
        <p>Every time we meet a new applicant, the possibilities are endless. We are looking for someone who has a lot of ideas, and is excited to share them.</p>
        <p>Being a MEDITECHer is something folks take pride in. We want to know what sets you apart, and what you can bring to the MEDITECH table!</p>
        <p>So, who are you?</p>
      </div>

    </div>
  </div>
</div>
<!-- End Block 2 -->

<!-- Start Block 3 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/bokeh-lights-bg.jpg); padding-bottom: 8em; background-position:top;">

  <div class="container__centered text--white center auto-margins" style="padding-bottom:2em;">
    <h2>More than software.</h2>
    <p>We’re more than software. Healthcare is all about people, and MEDITECH is no exception. We may be best known for our solutions, but our identity runs deeper than that. Meet some of the unique people at our company.</p>
    <p>Take a look and you’ll quickly see a crowd that exists beyond both healthcare and I.T. Regardless of your background, studies, or experience, MEDITECH is always looking for unique individuals who want to stand apart, and work together.</p>
  </div>

  <div class="container__centered" style="padding-bottom:2em;">

    <div class="container__one-third">
      <a href="#Popup1" class="button">
        <div class="employee-bubble eb-1">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Nhien Bui</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup1" class="Modal">
        <div class="content">
          <div>

            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-1"></div>
              <h3 class="no-margin--bottom">Nhien Bui</h3>
              <p class="no-margin--bottom">Product Manager</p>
              <p>Years at MEDITECH: <strong>2</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
              <p>Ever-changing.</p>
              <h4 class="text--meditech-green no-margin--bottom">What does MEDITECH mean to you?</h4>
              <p>Collaboration.</p>
              <h4 class="text--meditech-green no-margin--bottom">What was your first car?</h4>
              <p>Toyota Cressida.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite TV show to binge-watch?</h4>
              <p>Curb Your Enthusiasm, Gotham, and Veep.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the most interesting place you’ve traveled to?</h4>
              <p>Falmouth luminous lagoon in Jamaica.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>


    <div class="container__one-third">

      <a href="#Popup2" class="button">
        <div class="employee-bubble eb-4">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Meaghan Cloherty</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup2" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-4"></div>
              <h3 class="no-margin--bottom">Meaghan Cloherty</h3>
              <p class="no-margin--bottom">Senior Specialist, Marketing</p>
              <p>Years at MEDITECH: <strong>2</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is...</h4>
              <p>Dynamic and driven.</p>
              <h4 class="text--meditech-green no-margin--bottom">How has healthcare IT made an impact on you personally?</h4>
              <p>It is really nice being able to go to the doctor's office and not have to recount my medical history every single time, because it's all right there for them. All my visits are a lot more efficient now, and time is money!</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the most interesting place you’ve traveled to?</h4>
              <p>Arches National Park, Moab Utah.</p>
              <h4 class="text--meditech-green no-margin--bottom">What famous person would you most like to have dinner with?</h4>
              <p>Ruth Bader Gingsberg or Lisa Vanderpump since that’s the only way I’ll ever get a reservation at one of her restaurants anyway.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the best thing you’ve eaten in the MEDITECH cafeteria? </h4>
              <p>Egg white veggie omelette courtesy of Jim in Canton.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

    <div class="container__one-third">

      <a href="#Popup3" class="button">
        <div class="employee-bubble eb-2">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Galvin Chow</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup3" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-2"></div>
              <h3 class="no-margin--bottom">Galvin Chow</h3>
              <p class="no-margin--bottom">Supervisor, Client Support</p>
              <p>Years at MEDITECH: <strong>13</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
              <p>A great place to work that is staffed with people who are passionate about helping healthcare organizations improve patient care.</p>
              <h4 class="text--meditech-green no-margin--bottom">If you could have one superpower, what would it be?</h4>
              <p>Mind reading!</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your Alma Mater?</h4>
              <p>UMass Amherst.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite outdoor activity?</h4>
              <p>Running.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite breakfast cereal?</h4>
              <p>Banana Nut Crunch.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

  </div>

  <div class="container__centered">

    <div class="container__one-third">
      <a href="#Popup4" class="button">
        <div class="employee-bubble eb-3">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Shaun DaCunha</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup4" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-3"></div>
              <h3 class="no-margin--bottom">Shaun DaCunha</h3>
              <p class="no-margin--bottom">Lead Software Tester</p>
              <p>Years at MEDITECH: <strong>12</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH Is...</h4>
              <p>Changing.</p>
              <h4 class="text--meditech-green no-margin--bottom">How has healthcare IT made an impact on you personally?</h4>
              <p>I'm a big proponent of Patient Portals and utilize mine, if and when possible.</p>
              <h4 class="text--meditech-green no-margin--bottom">What was your first car?</h4>
              <p>1987 Subaru GL.</p>
              <h4 class="text--meditech-green no-margin--bottom">How have you been able to make a difference? </h4>
              <p>By treating everyone I interact with the way I like to be treated, and by displaying empathy, which I believe is an important tool in the modern workplace. In these ways, I feel as though I am promoting a work environment that I enjoy being a part of, and look forward to helping maintain for years to come.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite TV show to binge-watch?</h4>
              <p>The Office.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

    <div class="container__one-third">
      <a href="#Popup5" class="button">
        <div class="employee-bubble eb-5">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Marijo Carnino</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup5" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card-plus">
              <div class="modal-employee eb-5"></div>
              <h3 class="no-margin--bottom">Marijo Carnino</h3>
              <p class="no-margin--bottom">Director, EHR Programs</p>
              <p>Years at MEDITECH: <strong>31</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">Describe your most impactful experience at MEDITECH?</h4>
              <p>Working at MEDITECH provides me daily opportunities to make a difference. I am able to ensure that patient safety is always the #1 priority in our products. There is rarely a day that I leave and don't feel like I've made contributions in areas that make a difference.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is #1 on your travel bucket list?</h4>
              <p>Another trip with family to Italy - go back to prior stomping grounds and explore new areas.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite food and least favorite food?</h4>
              <p>Favorite food is pizza - every place is different; I think I could live on it. Least favorite food - liver?</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite album?</h4>
              <p>Anything James Taylor.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite part of the MEDITECH picnic?</h4>
              <p>Sharing favorite foods with family and friends and volunteering at the Kids Prize Table.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

    <div class="container__one-third">
      <a href="#Popup6" class="button">
        <div class="employee-bubble eb-6">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Mike Amend</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup6" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card-plus">
              <div class="modal-employee eb-6"></div>
              <h3 class="no-margin--bottom">Mike Amend</h3>
              <p class="no-margin--bottom">Supervisor, Recruiting &amp; Staff Development</p>
              <p>Years at MEDITECH: <strong>12</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
              <p>Community.</p>
              <h4 class="text--meditech-green no-margin--bottom">What does MEDITECH mean to you? </h4>
              <p>My son was born at a MEDITECH hospital and recently I had surgery at a MEDITECH site. I knew that behind the talented clinicians were my peers, my friends, and my MEDITECH family. </p>
              <h4 class="text--meditech-green no-margin--bottom">What was your first car?</h4>
              <p>Pontiac Grand AM. It had door rot and a squirrel built a nest in the door.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the one item you would take to a deserted island?</h4>
              <p>A satellite phone. I ain’t no fool. </p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite TV show to binge-watch?</h4>
              <p>Star Wars Clone Wars.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the most interesting place you’ve traveled to?</h4>
              <p>Pompeii.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

  </div>

</div>

<script>
  $jq.fn.expose = function(options) {
    var $modal = $jq(this),
      $trigger = $jq("a[href=" + this.selector + "]");
    $modal.on("expose:open", function() {
      $modal.addClass("is-visible");
      $modal.trigger("expose:opened");
    });
    $modal.on("expose:close", function() {
      $modal.removeClass("is-visible");
      $modal.trigger("expose:closed");
    });
    $trigger.on("click", function(e) {
      e.preventDefault();
      $modal.trigger("expose:open");
    });
    $modal.add($modal.find(".close")).on("click", function(e) {

      e.preventDefault();

      // if it isn't the background or close button, bail
      if (e.target !== this)
        return;

      $modal.trigger("expose:close");
    });
    return;
  }
  $jq("#Popup1").expose();
  $jq("#Popup2").expose();
  $jq("#Popup3").expose();
  $jq("#Popup4").expose();
  $jq("#Popup5").expose();
  $jq("#Popup6").expose();

  // Example Cancel Button
  $jq(".cancel").on("click", function(e) {

    e.preventDefault();
    $jq(this).trigger("expose:close");
  });

</script>
<!-- End Block 3 -->

<!-- Block 4 -->
<div class="bg--light-gray">
  <div class="container no-pad">
    <div class="divided-box">
      <div class="left-box background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/young-man-mentoring-college-students.jpg); min-height:250px;">
      </div>
      <div class="right-box bg--blue-gradient">
        <h2>College &amp; Career</h2>
        <p>Destined to be a writer? Have your heart set on showing off your leadership skills? You don’t need to have worked with technology or in a medical setting to be a perfect fit for MEDITECH. In addition to the College Fairs held regularly, our extensive onsite training will educate you on the company and your role, so you’ll be able to give it your all from the start. And our continual mentoring ensures you never feel left behind.</p>
      </div>

    </div>
  </div>
</div>
<!-- End Block 4 -->

<!-- Block 5 -->
<div class="bg--light-gray">
  <div class="container no-pad">
    <div class="divided-box">

      <div class="right-box">
        <h2 style="margin-top:2em;">Still on the fence about applying?</h2>
        <p>Be sure to follow @meditechcareers on <a href="https://twitter.com/MEDITECHCareers" target="_blank">Twitter</a> and <a href="https://www.instagram.com/meditechcareers/" target="_blank">Instagram</a> and get a first hand glimpse of what MEDITECH is doing, and why so many talented folks from all walks of life continue to join our ranks each year.</p>
        <div style="margin-top:1em; margin-bottom:2em;">
          <?php print $share_link_buttons; ?>
        </div>
      </div>
      <div class="left-box video--loop-container" style="min-height: 400px;">
        <video class="video--loop" preload="auto" autoplay loop muted playsinline>
          <source src="https://player.vimeo.com/external/327093766.hd.mp4?s=916dd125f2eea802f52ac011a84783f32bd4a90e&profile_id=175">
        </video>
      </div>

    </div>
  </div>
</div>
<!-- End Block 5 -->

<!-- END careers-hardcoded--node-2830.php -->
