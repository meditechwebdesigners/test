<!-- START inc--news-article.php -->

<?php
  // check first to see if field collection exists...
  $field_collection = field_get_items('node', $node, 'field_fc_button_1');
  // FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
  if( $field_collection != '' && !empty($field_collection) ){
    $button_1 = field_collection_data($node, 'field_fc_button_1');
  }  
  ?>
<style>
  .news__article__img img {
    border-radius: 7px;
  }

</style>

<!-- NEWS ARTICLE -->
<section class="container__centered">

  <div class="container__two-thirds">

    <article class="no-pad--top">

      <?php 
        // if video exists for node, then proceed to render Video DIV, otherwise look for main image...
        // get value from field_video to pass to View (if video exists)...
        $video = render($content['field_video']);
        $main_image = render($content['field_news_article_main_image']);
  
        if(!empty($video)){ // if the page has a video...
          
          print '<!-- VIDEO -->';
          // remove apostrophes from titles to prevent View from breaking...
          $video_filtered = str_replace("&#039;", "'", $video);
          // adds 'video' Views block...
          print views_embed_view('video_unboxed', 'block', $video_filtered);
          print '<!-- END VIDEO -->';
          
        }
        // if image exists for node, then proceed to render DIV...
        else {
          
          if(!empty($main_image)){
          ?>
      <figure class="news__article__img">
        <?php print render($content['field_news_article_main_image']); ?>
      </figure>
      <?php
          }
        
        }
        ?>

      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

      <?php if(!empty($content['field_subtitle'])){ ?>
      <p class="italic js__seo-tool__body-content"><?php print render($content['field_subtitle']); ?></p>
      <?php } ?>

      <div class="inline__text__wrapper">
        <?php if($node->published_at){ ?>
        <p><span class="news__article__callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date('F j, Y', $node->published_at); ?></time></span>
          <?php } else { // for previewing purposes... ?>
          <p><span class="news__article__callout"><em>Unpublished: No Date</em></span>
            <?php } ?>
            &nbsp;|&nbsp;</p>
          <ul class="news__article__filters tag_link_news_article_gae text--small">
            <?php generate_news_tag_links($node); ?>
          </ul>
      </div>

      <div class="js__seo-tool__body-content">
        <?php 
          if(!empty($content['field_body'])){
            print render($content['field_body']); // main article content 
          }
          else{
            print render($content['field_summary']); // summary content 
          } 
          
          // check to see if this is or has an EXTERNAL LINK ARTICLE or not...
          $external_source = field_view_field('node', $node, 'field_source_name');
          // article is external...
          if(!empty($external_source)){
            print '<p>Read the entire article: <a href="'.render($content['field_source_url']).'" title="external link" target="_blank">';
            print render($content['field_source_name']).'</a></p>';
          }
          ?>
      </div>

      <div>
        <?php print $share_link_buttons; ?>
      </div>


      <?php            
        // check to see if button exists...
        if( isset($button_1) ){
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
      <div class="button--hubspot news_article_cta_gae" style="border-top:1px solid #999; padding-top:1em; margin:1em 0;">
        <?php 
              if(!empty($content['field_text_1'])){
                print '<p><strong>'.render($content['field_text_1']).'</strong></p>'; 
              }
              ?>
        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>">
            <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png" alt="button" /></a></span>
          <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
          <script type="text/javascript">
            hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});

          </script>
        </span><!-- end HubSpot Call-to-Action Code -->
      </div>
      <?php }else{ ?>
      <div class="news_article_cta_gae" style="border-top:1px solid #999; padding-top:1em; margin:1em 0;">
        <?php 
              if(!empty($content['field_text_1'])){
                print '<p><strong>'.render($content['field_text_1']).'</strong></p>'; 
              }
              ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
      </div>
      <?php 
          } 
        }
        ?>


    </article>

    <?php // SEO Tool for internal use...
      if( user_is_logged_in() ){
        print '<!-- SEO Tool is added to this DIV -->';
        print '<div class="container js__seo-tool"></div>';
      } 
      ?>

  </div>

  <!-- SIDEBAR -->
  <style>
    .sb-article--container {
      border-bottom: 1px solid #ccc;
      padding: 1.5em 0;
      /*      margin-left: 1em;*/
      overflow: auto;
    }

    /*
    .sb-article--container:last-of-type {
      border-bottom: none;
    }
*/

    .sb-article--campaign {
      padding-top: 1.5em;
      /*      margin-left: 1em;*/
    }

    .sb-article--campaign img {
      border-radius: 7px;
    }

    .sb-article--container p {
      font-size: 16px;
      line-height: 1.5em;
    }

    .sb-article--container p a {
      border-bottom: none;
    }

    .sb-article--left {
      float: left;
      width: 57%;
      margin-right: 5%;
    }

    .sb-article--right {
      float: right;
      width: 38%;
      height: 75px;
    }

    .sb-article--right img {
      border-radius: 7px;
      object-fit: cover;
      width: auto;
      height: 75px;
    }

    .sb-article--vid-box {
      display: inline-block;
      position: relative;
      cursor: pointer;
    }

    .sb-article--vid-box img {
      display: block;
      filter: brightness(.6);
      -webkit-transition: background-color 0.5s ease;
      -moz-transition: background-color 0.5s ease;
      -o-transition: background-color 0.5s ease;
      transition: background-color 0.5s ease;
    }

    .sb-article--vid-bg {
      position: absolute;
      left: 50%;
      top: 50%;
      margin-left: -15px;
      margin-top: -15px;
      background-color: #00bc6f;
      width: 36px;
      height: 36px;
      -webkit-transition: background-color 0.5s ease;
      -moz-transition: background-color 0.5s ease;
      -o-transition: background-color 0.5s ease;
      transition: background-color 0.5s ease;
      border-radius: 50%;
    }

    .sb-article--vid-icon {
      color: #fff;
      font-size: 18px;
      position: absolute;
      top: 52%;
      left: 52%;
      -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      transform: translate(-50%, -50%);
      text-align: center;
    }

    .sb-article--vid-box:hover .sb-article--vid-bg {
      background-color: #087E68;
    }

    .fa-play:before {
      content: "\f04b";
    }

  </style>
  <aside class="container__one-third">
    <?php print views_embed_view('events_related', 'block'); // adds 'events - related' Views block... ?>

    <h2 class="no-margin--bottom">Related Content</h2>
    <div class="sb-article--container">
      <div class="sb-article--left">
        <p class="sidebar--newsarticle__title no-margin"><a class="news_related_article_gae" href="https://ehr-test.meditech.com/drupal/news/how-expanse-reduced-transcription-and-improved-communication-at-golden-valley-memorial">How Expanse Reduced Transcription at Golden Valley Memorial Healthcare</a></p>
      </div>
      <div class="sb-article--right sb-article--vid-box">
        <a href="https://ehr-test.meditech.com/drupal/news/how-expanse-reduced-transcription-and-improved-communication-at-golden-valley-memorial">
          <img src="https://ehr-test.meditech.com/drupal/sites/default/files/styles/video-overlay-breakpoints_theme_meditech_big-medium_2x/public/images/video/Expanse-Reduces-Transcription-by-70-at-Golden-Valley--VIDEO-OVERLAY.jpg?itok=SJ2MFS2x&timestamp=1557750547">
          <div class="sb-article--vid-bg">
            <i class="sb-article--vid-icon fas fa-play"></i>
          </div>
        </a>
      </div>
    </div>
    <?php print views_embed_view('news_related_articles', 'block'); // adds 'news - related articles' Views block... ?>

    <div class="sb-article--campaign">
      <a href="https://ehr.meditech.com/ehr-solutions/meditech-as-a-service-maas"><img src="https://ehr-test.meditech.com/drupal/sites/all/themes/meditech/images/campaigns/Professional-Services--Twitter.jpg"></a>
    </div>
  </aside>
  <!-- END SIDEBAR -->

</section><!-- END SECTION -->

<!-- END inc--news-article.php -->
