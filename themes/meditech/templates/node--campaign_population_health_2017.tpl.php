<?php // This template is set up to control the display of the CAMPAIGN_POPULATION_HEALTH content type - last edited 10/18

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with mulitple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
function multi_field_collection_data($node, $field_name, $quantity){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // create array...
  $multi_field_collections = array();
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_population_health.tpl.php template -->


<style>
    .pop-health__risk-spectrum-container {
        background-position: bottom;
        background-repeat: repeat-x;
        padding-bottom: 10.5em;
    }

    .pop-health__ambulatory-container {
        background-position: top;
        background-repeat: repeat-x;
    }

    /* Risk Graphic */

    .pop-health__risk-paragraph {
        margin-top: 1.5em;
    }

    .pop-health__risk-graph__triangle {
        position: absolute;
        top: 0;
        right: -30px;
        width: 0;
        height: 0;
        border-top: 15px solid transparent;
        border-left: 30px solid #ff813d;
        border-bottom: 15px solid transparent;
    }

    .pop-health__risk-graph {
        position: relative;
        color: $white;
        text-align: center;
        margin: 3em 40px 0 0;
        font-weight: 700;
        height: 30px;
        line-height: 30px;

        /* Gradient for the graph
  Tool Used: http://www.cssmatic.com/gradient-generator#'\-moz\-linear\-gradient\%28left\%2C\%20rgba\%2816\%2C147\%2C114\%2C1\%29\%200\%25\%2C\%20rgba\%28255\%2C184\%2C61\%2C1\%29\%2048\%25\%2C\%20rgba\%28255\%2C129\%2C61\%2C1\%29\%20100\%25\%29\%3B' */
        background: rgba(16, 147, 114, 1);
        background: -moz-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -webkit-gradient(left top, right top, color-stop(0%, rgba(16, 147, 114, 1)), color-stop(48%, rgba(255, 184, 61, 1)), color-stop(100%, rgba(255, 129, 61, 1)));
        background: -webkit-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -o-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -ms-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: linear-gradient(to right, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#109372', endColorstr='#ff813d', GradientType=1);
    }

    /* -----------------------------------  RISK GRAPH TABLE  ----------------------------------- */

    .pop-health__table {
        margin: 0 0 2em 0;
        border: none;
    }

    .pop-health__table th {
        border-left: none;
    }

    .pop-health__table tr:last-child td {
        border-bottom: 0;
    }

    .pop-health__table tr td {
        padding-right: 1em;
    }

    .pop-health__table--mobile {
        display: none;
    }

    .pop-health__table--mobile thead tr th:first-child,
    .pop-health__table--mobile tbody tr td:first-child {
        width: 3em;
    }

    /* Rotate Text */

    .pop-health__table--mobile .rotate {
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
        text-align: center;
        white-space: nowrap;
        height: 10em;
        width: 9em;
        font-weight: bold;
        padding-top: 0.7em;
    }

    @media all and (max-width: 37.5em) {
        .pop-health__table--desktop,
        .pop-health__risk-graph {
            display: none;
        }
        .pop-health__table--mobile {
            display: table;
        }
    }

</style>


<div class="js__seo-tool__body-content">

    <!-- BLOCK 1 -->
    <div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physician-reaching-hand-out.jpg); background-color: #e6e6e6;">
        <div class="container__centered">
            <h1 class="js__seo-tool__title">
                <?php print render($content['field_header_1']); ?>
            </h1>
            <div class="container__two-thirds no-pad">
                <?php print render($content['field_long_text_1']); ?>
            </div>
            <div class="container__one-third center">
                <?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
                <div class="button--hubspot container--pop-health__button">
                    <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>">
                            <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png" alt="button" /></a></span>
                        <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                        <script type="text/javascript">
                            hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});

                        </script>
                    </span>
                    <!-- end HubSpot Call-to-Action Code -->
                </div>
                <?php }else{ ?>
                <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange">
                    <?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- BLOCK 1 -->


    <!-- BLOCK 2 -->
    <div class="container pop-health__risk-spectrum-container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/population-health_background--connected-people--top.png); padding-top:2em;">

        <div class="container__centered">
            <h2>
                <?php print render($content['field_header_2']); ?>
            </h2>
            <div class="pop-health__risk-paragraph">
                <?php print render($content['field_long_text_2']); ?>
            </div>

            <!-- Risk Graph Arrow -->
            <div class="pop-health__risk-graph">
                <div class="pop-health__risk-graph__triangle">
                </div>
            </div>

            <?php $column_headers = field_get_items('node', $node, 'field_header_unl_1'); ?>
            <?php $row_headers = field_get_items('node', $node, 'field_header_unl_2'); ?>
            <?php $cell_content = field_get_items('node', $node, 'field_long_text_unl_1'); ?>

            <!-- Risk Table - Mobile -->
            <table class="pop-health__table pop-health__table--mobile">
                <thead>
                    <tr>
                        <th></th>
                        <th>
                            <h4>
                                <?php print $row_headers[0]['value']; ?>
                            </h4>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="rotate">
                                <?php print $column_headers[0]['value']; ?>
                            </div>
                        </td>
                        <td>
                            <?php print $cell_content[0]['value']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="rotate">
                                <?php print $column_headers[1]['value']; ?>
                            </div>
                        </td>
                        <td>
                            <?php print $cell_content[1]['value']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="rotate">
                                <?php print $column_headers[2]['value']; ?>
                            </div>
                        </td>
                        <td>
                            <?php print $cell_content[2]['value']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="rotate">
                                <?php print $column_headers[3]['value']; ?>
                            </div>
                        </td>
                        <td>
                            <?php print $cell_content[3]['value']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- Risk Table - Desktop -->
            <table class="pop-health__table pop-health__table--desktop">
                <thead>
                    <tr>
                        <th></th>
                        <th>
                            <h4>
                                <?php print $column_headers[0]['value']; ?>
                            </h4>
                        </th>
                        <th>
                            <h4>
                                <?php print $column_headers[1]['value']; ?>
                            </h4>
                        </th>
                        <th>
                            <h4>
                                <?php print $column_headers[2]['value']; ?>
                            </h4>
                        </th>
                        <th>
                            <h4>
                                <?php print $column_headers[3]['value']; ?>
                            </h4>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="padding:0.5em !important;">
                            <h4>
                                <?php print $row_headers[0]['value']; ?>
                            </h4>
                        </td>
                        <td>
                            <?php print $cell_content[0]['value']; ?>
                        </td>
                        <td>
                            <?php print $cell_content[1]['value']; ?>
                        </td>
                        <td>
                            <?php print $cell_content[2]['value']; ?>
                        </td>
                        <td style="padding:0.5em !important;">
                            <?php print $cell_content[3]['value']; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- BLOCK 2 -->


    <!-- BLOCK 3 -->
    <div class="container bg--emerald">
        <div class="container__centered">
            <div class="page__title--center">
                <div class="container no-pad">
                    <h2>
                        <?php print render($content['field_header_3']); ?>
                    </h2>
                    <div class="auto-margins">
                        <?php print render($content['field_long_text_3']); ?>
                    </div>
                </div>
                <div class="container no-pad--top left">
                    <p class="center">
                        See how MEDITECH customers are addressing social determinants of health in their communities:
                    </p>
                    <div class="container__one-fourth transparent-overlay">
                        <h3 class="center">
                            <a href="https://blog.meditech.com/beyond-numbers-creative-ways-to-address-population-health">Frisbie Memorial Hospital</a>
                        </h3>
                        <p>
                            To address the food insecurity issue that led some patients to overutilize the ED, Frisbie partnered with local organizations to host weekly potluck dinners in their community.
                        </p>
                    </div>
                    <div class="container__one-fourth transparent-overlay">
                        <h3 class="center">
                            <a href="https://blog.meditech.com/grocery-store-walkthroughs-one-step-toward-improving-health-literacy">Kalispell Regional Medical Center</a>
                        </h3>
                        <p>
                            Took CHF patients on walkthroughs of their local grocery store to combat health illiteracy and encourage better diets.
                        </p>
                    </div>
                    <div class="container__one-fourth transparent-overlay">
                        <h3 class="center">
                            <a href="https://blog.meditech.com/how-providers-can-bridge-care-gaps-caused-by-social-determinants-of-health">Southwestern Vermont Medical Center</a>
                        </h3>
                        <p>
                            Established a transitional care program to address care gaps in their rural community.
                        </p>
                    </div>
                    <div class="container__one-fourth transparent-overlay">
                        <h3 class="center">
                            <a href="https://blog.meditech.com/the-importance-of-social-determinants-in-behavioral-health">Bristol Hospital</a>
                        </h3>
                        <p>
                            Distributed surveys to new patients at their outpatient behavioral health counseling center, and enlisted local nursing students to follow up with patients needing additional resources or support.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BLOCK 3 -->



    <!-- BLOCK 4 -->
    <div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/health-care-staff-background-blurred.jpg);">
        <div class="container__centered">
            <div class="container no-pad">
                <div class="container__two-thirds">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot-wellness-registry.jpg" alt="MEDITECH Wellness Registry screenshot">
                    </figure>
                </div>
                <div class="container__one-third">
                    <h2 class="no-margin--top">
                        <?php print render($content['field_header_4']); ?>
                    </h2>
                    <div>
                        <?php print render($content['field_long_text_4']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BLOCK 4 -->


    <!-- BLOCK 5 -->
    <?php $block_5 = field_collection_data($node, 'field_fc_video_1'); ?>
    <div class="content__callout border-none">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="<?php print $block_5->field_video_url['und'][0]['value']; ?>">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--PPGH-Bridge-to-Health.jpg" alt="Palo Pinto Mobile Health Initiative Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="http://vimeo.com/<?php print $block_5->field_video_url['und'][0]['value']; ?>"></a>
                    <div class="video__container">
                    </div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div>
                    <h2>
                        <?php print $block_5->field_header_1['und'][0]['value']; ?>
                    </h2>
                    <?php print $block_5->field_long_text_1['und'][0]['value']; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- BLOCK 5 -->

    <!-- BLOCK 6 -->
    <div class="container bg--green-gradient">
        <div class="container__centered">
            <h2>
                Aggregated data to help you see the complete picture.
            </h2>
            <div class="container__two-thirds">
                <p>
                    MEDITECH is collaborating with Arcadia Healthcare Solutions to add aggregated data into our Ambulatory and BCA solutions, for a more complete patient record and improved population health management. See and use information from across the continuum (including claims and other-vendor EHR data) to get a better understanding of patient risks and utilization.
                </p>
            </div>
            <div class="container__one-third center">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Arcadia--logo.png" alt="Arcadia Logo">
            </div>
        </div>
    </div>
    <!-- BLOCK 6 -->


    <!-- BLOCK 7 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/woman-yellow-dress-on-laptop-blurred-darkened.jpg);">
        <div class="container__centered">
            <div class="container no-pad">
                <div class="container__two-thirds text--white">
                    <h2 class="no-margin--top">
                        <?php print render($content['field_header_5']); ?>
                    </h2>
                    <div>
                        <?php print render($content['field_long_text_5']); ?>
                    </div>
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot-patient-portal.jpg" alt="MEDITECH Patient Portal screenshot" />
                    </figure>
                </div>
                <div class="container__one-third text--white">
                    <div class="transparent-overlay">
                        <h3 class="text--white">
                            <?php print render($content['field_header_6']); ?>
                        </h3>
                        <div>
                            <?php print render($content['field_long_text_6']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BLOCK 6 -->


    <!-- BLOCK 7 -->
    <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
        <div class="container__centered">
            <div class="container no-pad">
                <div class="container__two-thirds">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot-bca-quality.jpg" alt="MEDITECH BCA Quality screenshot">
                    </figure>
                </div>
                <div class="container__one-third">
                    <h2 class="no-margin--top">
                        <?php print render($content['field_header_7']); ?>
                    </h2>
                    <div>
                        <?php print render($content['field_long_text_7']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BLOCK 7 -->


    <!-- BLOCK 8 -->
    <div class="container bg--emerald" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/financial-sketches-on-green-background.png);">
        <div class="container__centered">
            <div class="container no-pad">
                <div class="container__two-thirds">
                    <h2 class="no-margin--top">
                        <?php print render($content['field_header_8']); ?>
                    </h2>
                    <div>
                        <?php print render($content['field_long_text_8']); ?>
                    </div>
                </div>
                <div class="container__one-third">
                    <div class="transparent-overlay">
                        <?php print render($content['field_long_text_10']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BLOCK 8 -->

    <!-- BLOCK 9 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Female-Doctor-Helping-Patient-in-lobby-with-tablet.jpg);">
        <div class="container__centered">
            <div class="container__two-thirds">
                &nbsp;
            </div>
            <div class="container__one-third transparent-overlay--white">
                <h2>
                    Interoperability leads to seamless, coordinated care.
                </h2>
                <p>
                    True interoperability connects communities and enables better population health strategies by allowing for the sharing of data across all systems. As a Contributor Member of the CommonWell Health Alliance, MEDITECH is committed to technology that will give you the full patient story - so you can always treat the person and not just the problem.
                </p>
            </div>
        </div>
    </div>
    <!-- BLOCK 9 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- BLOCK 10 -->
<div class="container center" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
        <h2 class="js__seo-tool__body-content">
            <?php print render($content['field_header_9']); ?>
        </h2>
        <div class="js__seo-tool__body-content">
            <?php print render($content['field_long_text_9']); ?>
        </div>
        <?php 
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
        <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>">
                    <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png" alt="button" /></a></span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                    hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});

                </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
        </div>
        <?php }else{ ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange">
            <?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>


        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>
    </div>
</div>
<!-- BLOCK 10 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>
<!-- end node--campaign_population_health.tpl.php template -->
