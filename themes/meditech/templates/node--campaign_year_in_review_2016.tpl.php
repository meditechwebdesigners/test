<?php
  /*
  *
  * This template is set up to control the display of the CAMPAIGN_YEAR_IN_REVIEW_2016 content type
  *
  */
  $url = $GLOBALS['base_url']; // grabs the site url
?>
  <!-- start node--campaign_end_of_year_2016.tpl.php template -->

  <div class="js__seo-tool__body-content">

    <!-- animate.css -->
    <style>
      .animated {
        -webkit-animation-duration: 4s;
        animation-duration: 4s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
      }
      
      .animated2 {
        -webkit-animation-duration: 2s;
        animation-duration: 2s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
      }
      
      @-webkit-keyframes fadeIn {
        from {
          opacity: 0;
        }
        to {
          opacity: 1;
        }
      }
      
      @keyframes fadeIn {
        from {
          opacity: 0;
        }
        to {
          opacity: 1;
        }
      }
      
      .fadeIn {
        -webkit-animation-name: fadeIn;
        animation-name: fadeIn;
      }
      
      @-webkit-keyframes fadeInUp {
        from {
          opacity: 0;
          -webkit-transform: translate3d(0, 100%, 0);
          transform: translate3d(0, 100%, 0);
        }
        to {
          opacity: 1;
          -webkit-transform: none;
          transform: none;
        }
      }
      
      @keyframes fadeInUp {
        from {
          opacity: 0;
          -webkit-transform: translate3d(0, 100%, 0);
          transform: translate3d(0, 100%, 0);
        }
        to {
          opacity: 1;
          -webkit-transform: none;
          transform: none;
        }
      }
      
      .fadeInUp {
        -webkit-animation-name: fadeInUp;
        animation-name: fadeInUp;
      }
    </style>
    <!-- End animate.css -->

    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blurred-city-lights-bg2.jpg);">
      <div class="container__centered">
        <div class="animated fadeIn padding--large text--white text--containment">
          <h1>What’s your year-end reflection?</h1>
          <p>We’re grateful year-round for this wonderful opportunity, to help you transform the way you deliver care with our EHR.</p>
        </div>
      </div>
    </div>
    <!-- End Block 1 -->

    <!-- Block 2 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/howard-messing-speaking.jpg); overflow:hidden;">
      <div class="container__centered">
        <div class="floating-quote-box animated2 transparent-overlay os-animation text--white" data-os-animation="fadeInUp">
          <h2>“We knew we were moving in the right direction when we began designing our 6.1 and Web EHR products. Just how much it has changed the face of healthcare, however, continues to humble us. We are thankful to our customers, who are supporting us on this journey together.”</h2>
          <p class="text--large">Howard Messing
            <br>President &amp; CEO, MEDITECH</p>
        </div>
      </div>
    </div>
    <!-- End Block 2 -->

    <!-- Block 3 -->
    <div class="bg--green-gradient">
      <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/world-map.png);">
        <div class="container__centered page__title--center padding--large text--white">
          <div id="waypoint1">
            <h1>In 2016, we continued to see a global evolution. Over
          <br><span id="counter1">2,390</span>
          <br>hospitals in 22 different countries.</h1>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 3 -->

    <!-- Block 4 -->
    <div class="container page__title--center padding--medium" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg); overflow:hidden;">
      <div class="container__centered os-animation" data-os-animation="fadeIn">
        <h2 style="margin-bottom:2em;">An EHR for all your care settings. Let us help transform the way you deliver care.</h2>
      </div>

      <div class="container no-pad--top no-pad--bottom text--white animated2 os-animation" data-os-animation="fadeInUp">
        <div class="container__one-third background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/care-setting-ambulatory.jpg);">
          <div class="margin-container transparent-overlay">
            <div style="text-align:center; padding-bottom:1em;"><i class="text--fresh-mint fas fa-arrow-circle-right fa-3x"></i></div>
            <h3>Ambulatory</h3>
            <p>Experience a whole new way to manage your practice with MEDITECH’s EHR and web-based Ambulatory solution.</p>
          </div>
        </div>

        <div class="container__one-third background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/care-setting-emergency-room.jpg);">
          <div class="margin-container transparent-overlay">
            <div style="text-align:center; padding-bottom:1em;"><i class="text--fresh-mint fas fa-ambulance fa-3x"></i></div>
            <h3>Emergency Department</h3>
            <p>Meet the specialized needs of your ED while sharing information easily among all care settings.</p>
          </div>
        </div>

        <div class="container__one-third background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/care-setting-acute.jpg);">
          <div class="margin-container transparent-overlay">
            <div style="text-align:center; padding-bottom:1em;"><i class="text--fresh-mint far fa-hospital fa-3x"></i></div>
            <h3>Acute</h3>
            <p>No matter your role, MEDITECH’s Web EHR will make it easier to do your thing and be more productive.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 4 -->

    <!-- Block 5 -->
    <div class="bg--blue-gradient">
      <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-curves-bg.png);">
        <div class="container__centered page__title--center text--white padding--medium">
          <h2 class="animated fadeIn" style="padding-bottom:1.5em;">We’ve been tackling the biggest challenges healthcare has to offer with our sophisticated EHR solutions.</h2>
          <div class="container__one-third">
            <h4 class="os-animation" data-os-animation="fadeIn">Population Health</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="1s">Interoperability</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="2.5s">HIEs</h4>

            <h4 class="os-animation" data-os-animation="fadeIn">Patient Engagement</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="3.5s">Quality Reporting</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="1.5s">Critical Care</h4>
          </div>

          <div class="container__one-third">
            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="2s">Transitions of Care</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="3s">Regulatory Standards</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay=".25s">Physician Engagement</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="1.25s">Physician Productivity</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="1.75s">Usability</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay=".5s">Care Coordination</h4>
          </div>

          <div class="container__one-third">
            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay=".9s">Patient &amp; Consumer Experience</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay=".65s">Cybersecurity</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="1.75s">MACRA</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="3.5s">Data and Analytics</h4>

            <h4 class="os-animation" data-os-animation="fadeIn">Revenue Cycle</h4>

            <h4 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="2.5s">Care Standardization</h4>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 5 -->

    <!-- Block 6 -->
    <div class="video--loop-container">
      <div class="video--overlay"></div>
      <video class="video--loop" preload="auto" autoplay loop muted playsinline>
        <source src="https://player.vimeo.com/external/192464882.hd.mp4?s=e29a0b2ddc3680a75320324b1e168025e66d9214&profile_id=174">
      </video>
      <div class="container__centered">
        <h2 class="video--title">In the new year and beyond, MEDITECH is honored to support you. Nine offices, three states, four thousand employees, limitless possibilities.</h2>
      </div>
    </div>
    <!-- End Block 6 -->

  </div>
  <!-- end js__seo-tool__body-content -->

  <!-- Block 7 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blurred-city-lights-bg.jpg);">
    <div class="container__centered page__title--center padding--medium os-animation no-target-icon" data-os-animation="fadeIn">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/thank-you.png" style="padding-bottom:3em;">
      <h2 class="text--white">Let’s connect and go forward together, in 2017.</h2>
      <a href="https://www.facebook.com/MeditechEHR?ref=br_tf" target="_blank">
        <i class="fab fa-facebook-square fa-3x" aria-hidden="true"></i></a>
      <a href="https://twitter.com/MEDITECH" target="_blank">
        <i class="fab fa-twitter-square fa-3x" aria-hidden="true"></i></a>
      <a href="https://www.linkedin.com/company/meditech" target="_blank">
        <i class="fab fa-linkedin fa-3x" aria-hidden="true"></i></a>
    </div>
  </div>
  <!-- End Block 7 -->

  <!-- Waypoints Script & Controls -->
  <script src="https://ehr.meditech.com/sites/all/themes/meditech/js/jquery.waypoints.min.js"></script>

  <script>
    function onScrollInit(items, trigger) {
      var $jq = jQuery.noConflict();
      items.each(function () {
        var osElement = $jq(this),
          osAnimationClass = osElement.attr('data-os-animation'),
          osAnimationDelay = osElement.attr('data-os-animation-delay');

        osElement.css({
          '-webkit-animation-delay': osAnimationDelay,
          '-moz-animation-delay': osAnimationDelay,
          'animation-delay': osAnimationDelay
        });

        var osTrigger = (trigger) ? trigger : osElement;

        osTrigger.waypoint(function () {
          osElement.addClass('animated').addClass(osAnimationClass);
        }, {
          triggerOnce: true,
          offset: '90%'
        });
      });
    }

    onScrollInit($jq('.os-animation'));
    onScrollInit($jq('.staggered-animation'), $jq('.staggered-animation-container'));
  </script>
  <!-- End Waypoints Script & Controls -->

  <!-- Counting Numbers Script & Controls -->
   <script src="https://ehr.meditech.com/sites/all/themes/meditech/js/countup.js"></script>
  
  <script>
    var counter1 = new CountUp("counter1", 13, 2390, 0, 5, {  
      useEasing: true,
      useGrouping: true,
    });

    var waypoint1 = new Waypoint({
      element: document.getElementById('waypoint1'),
      handler: function (direction) {

        if (direction == "up") {
          counter1.reset();
        } else {
          counter1.start();
        }

      },
      offset: '100%'
    });
  </script>
  <!-- End Counting Numbers Script & Controls -->

  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

    <!-- end node--campaign_end_of_year_2016.tpl.php template -->