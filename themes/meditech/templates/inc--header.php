<!-- START inc--header.php -->
<style>
	form.gsc-search-box {
		font: inherit;
		margin-top: 0;
		margin-right: 0;
		margin-bottom: 0;
		margin-left: 0;
		width: 100%;
		position: relative;
	}

	table.gsc-search-box {
		border-style: none;
		border-width: 0;
		border-spacing: 0 0;
		width: 100%;
		margin-bottom: 0;
		margin-top: 0;
	}

	.gsc-input-box {
		border: 0;
		background: #fff;
		height: auto;
		border-color: white;
	}

	table.gsc-search-box {
		border: 0;
		margin: 0;
	}

	table.gsc-search-box tbody,
	table.gsc-search-box tr:hover>td,
	table.gsc-search-box tr:hover>th {
		background-color: inherit;
	}

	td.gsc-search-button {
		width: 20%;
		padding: 0;
	}

	table.gsc-search-box td.gsc-input,
	tbody:last-child tr:last-child>td:first-child,
	tbody:last-child tr:last-child>td:last-child {
		border: 0;
		border-bottom-left-radius: 0;
		padding: 0;
	}

	tbody:last-child tr:last-child>td:first-child {
		width: 80%;
	}

	table#gs_id50,
	table#gs_id51 {
		border: 1px solid #7b8888;
	}

	td#gs_tti50,
	td#gs_tti51 {
		padding: .7em 3%;
	}

	table.gsc-search-box td.gsc-search-button {
		border: 0;
	}

	table.gsc-search-box td {
		vertical-align: inherit;
	}

	table.gsc-search-box td.gsib_b {
		vertical-align: middle;
	}

	table.gsc-search-box table#gs_id50,
	table.gsc-search-box table#gs_id51 {
		margin: 0;
	}

	.gsc-search-box-tools .gsc-search-box .gsc-input {
		padding-right: 0;
	}

	.gsc-input table {
		border: none;
	}

	input.gsc-input {
		font-size: 1em;
	}

	.gsib_a {
		padding: 0;
		width: 80%;
	}

	.gsib_b {
		padding: 0;
		width: 10%;
		text-align: center;
	}

	input.gsc-search-button,
	input.gsc-search-button:hover,
	input.gsc-search-button:focus {
		border-color: #109372;
		background-color: #109372;
		background-image: none;
		filter: none;
		color: white;
	}

	input.gsc-search-button-v2 {
		width: 13px;
		height: 13px;
		padding: 6px 27px;
		min-width: 13px;
		margin-top: 2px;
	}

	input.gsc-search-button {
		font-family: inherit;
		font-size: inherit;
		font-weight: bold;
		color: #fff;
		padding: 1em 1.5em;
		margin: 0;
		height: 3.1em;
		width: 80px;
		border-radius: inherit;
		-moz-border-radius: inherit;
		-webkit-border-radius: inherit;
		border: 0;
	}

	.gcsc-find-more-on-google {
		display: none;
	}






	/* newest styles */

	@media (min-width: 65.063em) {
		.sb-slidebar.sb-active {
			width: auto;
		}
	}

	#header__search {
/*		margin-top: -.35em;*/
	}

	/*
  input[type="submit"], button {
  border-radius: 25px !important;
  border: none !important;
  color: #fff !important;
  font-size: 16px !important;
  font-weight: 700 !important;
  font-family: "source-sans-pro", helvetica, arial, sans-serif !important;
  text-align: center !important;
  padding: 0.563em 1.5em !important;
  line-height: inherit !important;
  flex: 1 1 auto !important;
  transition: all 200ms ease-in-out !important;
  background-size: 200% auto !important;
  }
  a.btn--orange-gradient, button.btn--orange, a.btn--orange, .button--hubspot a, a.cta_button, .btn--orange {
  background-image: linear-gradient(45deg, #fe9418 0%, #f5634b 75%) !important;
  }
  .form__search__button, .form__search__button[type="submit"], button.load-more-ajax, .btn--green {
  background-image: linear-gradient(45deg, #00BC6F 0%, #00706C 75%) !important;
  }
*/

	#sb-site {
		margin-top: 6.9em;
	}

	header.header {
		position: fixed;
		top: 0;
		left: 0;
		z-index: 100;
		width: 100%;
		background: rgba(255, 255, 255, .97);
		/*    border-bottom: 1px solid rgba(238, 232, 243, .097);*/
		/*    box-shadow: 0 0 8px 0 rgba(72, 72, 72, .2);*/
	}

	@media (max-width: 65em) {
		header.header {
			display: block;
		}
	}

	.logos {
		float: left;
	}

	.nav__menu,
	#logo--m {
		display: none;
	}

	#logo--meditech {
		margin-right: 20em;
		margin-top: 6px;
	}

	#logo--meditech img {
		width: 210px;
	}

	#logo--m {
		margin-right: 1em;
		position: relative;
		top: 3px;
	}

	#logo--m img {
		width: 40px;
		height: 40px;
	}

	nav {
		font-size: 1em;
		font-weight: 500;
		padding-left: 0;
	}

	nav ul.menu {
		padding-left: 0;
		display: inline-block;
	}

	nav ul.menu li {
		display: inline-block;
		margin: 0;
		margin-right: 1.5em;
	}

	nav ul.menu li {
		line-height: 55px;
		position: relative;
		margin-right: 0;
		padding-right: 1.4em;
	}

	nav ul.menu li a.active:after {
		width: 0;
		height: 0;
		position: absolute;
		content: " ";
		display: block;
		border-left: 15px solid transparent;
		border-right: 15px solid transparent;
		border-top: 15px solid #087e68;
		bottom: -7px;
		left: 51%;
		cursor: default;
		margin-left: -30px;
		transform: rotate(180deg);
	}

	nav ul.menu ul.menu {
		display: none;
	}


	.form__search {
		width: 250px;
		display: inline-block;
		margin-top: 2.5em;
	}

	input[type="search"] {
		height: 45px;
		min-width: 200px;
		padding: 0 2.2em 0 1.2em;
		border-radius: 25px;
		border: 2px solid #e0e0e0;
		margin-bottom: 0;
	}

	input[type="search"]:focus {
		border: 2px solid #bec4c4;
		transition: border .4s;
		box-shadow: none;
	}

	input[type="search"]:hover {
		box-shadow: none;
	}

	.form__search input[type='image'] {
		width: 25px;
		height: 25px;
		position: absolute;
		right: 15px;
		top: 10px;
		border: none;
		background: transparent;
	}

	#customers_icon img {
		width: 30px;
		position: relative;
		top: 9px;
	}

	/* Secondary Nav */
	.secondary-nav ul {
		list-style: none;
		padding: 0em;
	}

	.secondary-nav ul li {
		border-right: 2px solid #00bc6f;
		display: inline-block;
		padding: 0em 1em;
		line-height: 90%;
		font-size: 16px;
		overflow: visible;
		margin-top: .75em;
		margin-bottom: .75em;
	}

	.secondary-nav ul li:first-child {
		padding-left: 1px;
	}

	.secondary-nav ul li:last-child {
		border-right: 0;
	}

	header.shrink {}

	header.shrink #logo--meditech {
		display: none;
	}

	header.shrink #logo--m {
		display: inline-block;
		margin-right: 1em;
	}

	header.shrink .form__search {
		margin-top: .25em;
	}

	button.btn--unstyled {
		border-radius: 0;
		color: inherit;
		padding: .15em 0 0 0 !important;
		background-color: unset;
	}

	/* mobile setup */
	@media all and (max-width: 1080px) {
		header {
			padding-bottom: .5em;
		}

		header #logo--m,
		header.shrink #logo--m {
			display: none;
		}

		header #logo--meditech,
		header.shrink #logo--meditech {
			display: inline-block;
			margin-right: 1em;
		}

		header .form__search,
		header.shrink .form__search {
			margin-top: 0;
		}

		header .form__search,
		header.shrink .form__search {
			width: 150px;
		}

		header .form__search input#searchBox,
		header.shrink .form__search input#searchBox {
			min-width: 100px;
		}

		#sb-site {
			margin-top: -1em;
		}

		.nav__menu {
			display: inline-block;
			margin-right: 1em;
		}

		header .nav__desktop .menu,
		header #secondary-nav {
			display: none;
		}

		#nav .nav__desktop .container__two-thirds {
			width: auto;
			float: left;
			margin-right: 0;
		}

		#nav .nav__desktop .container__one-third {
			width: auto;
			float: right;
			margin-right: 0;
		}

		.sb-slidebar.sb-active {
			width: 40%;
		}
	}

	@media all and (max-width: 600px) {

		header #logo--m,
		header.shrink #logo--m {
			display: inline-block;
			margin-right: 1em;
		}

		header #logo--meditech,
		header.shrink #logo--meditech {
			display: none;
		}

		.sb-slidebar.sb-active {
			width: 70%;
		}
	}

</style>


<?php $currentURL = $_SERVER[REQUEST_URI]; // get current page URL info (minus domain)... ?>

<!-- HEADER ================================================================== -->

<!-- Start PRINT ONLY header content -->
<div class="print-header">
	<img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.svg" style="width:200px; margin-bottom: -2.5em;" alt="MEDITECH logo">
</div>
<!-- End PRINT ONLY header content -->

<?php
$active_solutions = '';
$active_news = '';
$active_events = '';
$active_about = '';
$active_contact = '';

$currentURLarray = explode('/', $currentURL);

// need to check this since DEV server has a different folder structure...
if($currentURLarray[1] == 'drupal'){
  $folder_name = $currentURLarray[2];
}
else{
  $folder_name = $currentURLarray[1];
}

// because contact is getting variables sent to its URL, need to split it...
$explode_folder_name = explode('?', $folder_name);

switch($explode_folder_name[0]){
  case 'ehr-solutions':
    $active_solutions = 'active';
    break;
  case 'news':
  case 'news-tags':
    $active_news = 'active';
    break;
  case 'events':
    $active_events = 'active';
    break;
  case 'about-meditech':
  case 'careers':
    $active_about = 'active';
    break;
  case 'contact':
    $active_contact = 'active';
    break;
  case 'global':
    $active_global = 'active';
    break;
}
?>

<header class="header">

	<div class="container no-pad" style="margin-top:1em;">

		<nav id="nav" class="container__centered header_link_gae">

			<div class="nav__desktop">

				<div class="container__two-thirds">

					<!-- Navigation Menu Button -->
					<div class="nav__menu">
						<button class="btn--unstyled sb-toggle-left menu_mobile_gae"><img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/icon--menu.svg" width="40" height="40" alt="menu icon"><span class="element-invisible">Menu</span></button>
					</div>

					<div class="logos">
						<a id="logo--m" class="logo_M_gae" href="<?php print $websiteURL; ?>" title="Home Page"><img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/M-Logo--2020.svg" alt="MEDITECH M logo"></a>
						<a id="logo--meditech" class="logo_desktop_gae" href="<?php print $websiteURL; ?>" title="Home Page"><img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.svg" alt="MEDITECH logo"></a>
					</div>

					<ul class="menu">
						<li class="first"><a class="<?php if( isset($active_solutions) ){ print $active_solutions; } ?>" href="<?php print $websiteURL; ?>/ehr-solutions">EHR Solutions</a>
							<?php
              // generate Drupal menu...
              $menu = module_invoke('menu', 'block_view', 'menu-sec-nav---solutions');
              print render($menu['content']);
              ?>
						</li>
						<li><a class="<?php if( isset($active_news) ){ print $active_news; } ?>" href="<?php print $websiteURL; ?>/news">News</a>
							<?php
              // generate Drupal menu...
              $menu = module_invoke('menu', 'block_view', 'menu-sec-nav---news');
              print render($menu['content']);
              ?>
						</li>
						<li><a href="https://blog.meditech.com">Blog</a></li>
						<li><a class="<?php if( isset($active_events) ){ print $active_events; } ?>" href="<?php print $websiteURL; ?>/events">Events</a>
							<?php
              // generate Drupal menu...
              $menu = module_invoke('menu', 'block_view', 'menu-sec-nav---events');
              print render($menu['content']);
              ?>
						</li>
						<li><a class="<?php if( isset($active_about) ){ print $active_about; } ?>" href="<?php print $websiteURL; ?>/about-meditech/about-meditech">About</a>
							<?php
              // generate Drupal menu...
              $menu = module_invoke('menu', 'block_view', 'menu-sec-nav---about');
              print render($menu['content']);
              ?>
						</li>
						<li><a class="<?php if( isset($active_global) ){ print $active_global; } ?>" href="<?php print $websiteURL; ?>/global">Global</a>
							<?php
              // generate Drupal menu...
              $menu = module_invoke('menu', 'block_view', 'menu-sec-nav---global');
              print render($menu['content']);
              ?>
						</li>
						<li class="last"><a class="<?php if( isset($active_contact) ){ print $active_contact; } ?>" href="<?php print $websiteURL; ?>/contact">Contact</a></li>
					</ul>
				</div>

				<div id="header__search" class="container__one-third">
					<a style="float:right; margin-left:.75em;" href="https://home.meditech.com/en/d/customer/" id="customers_icon" class="btn--orange customers_icon_gae" title="Customers">Customers</a>
					<a style="float:right;" href="https://home.meditech.com/en/d/customer/" id="customers_icon" class="btn--orange-gradient customers_icon_gae" title="Customers">Search</a>
				</div>

				<!--
				<div id="header__search" class="container__one-third">
					<form class="form__search" method="get" action="<?php print $websiteURL; ?>/search-results" id="search-block-form" accept-charset="UTF-8">
						<label class="element-invisible" for="searchBox">Site Search</label>
						<input id="searchBox" title="Enter the terms you wish to search for." placeholder="Search" type="search" name="as_q">
						<input class="search-icon" type="image" name="search" alt="Search" src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/icon--magnifying-glass--green.svg">
					</form>

					<a href="https://home.meditech.com/en/d/customer/" id="customers_icon" class="btn--orange-gradient customers_icon_gae" title="Customers"><img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/icon--customer--white.svg" alt="Customers"></a>
				</div>
-->

			</div>

		</nav>

		<div id="secondary-nav" class="bg--emerald" style="border-bottom: 1px solid #087E68;">
			<div class="container__centered">
				<div class="secondary-nav">
					<?php
            // get nav menu name based off content type (function is in template.php)...
            $sec_nav = get_secondary_nav($node->type);
            // generate Drupal menu...
            $menu = module_invoke('menu', 'block_view', $sec_nav);
            print render($menu['content']);
            ?>
				</div>
			</div>
		</div>

	</div>

</header>
<!-- END inc--header.php -->
