<!-- start career-page--node-2438.php template CAREERS: Veteran's Page -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>
    <!-- start career-page--node-2438.php template CAREERS: Veteran's Page -->
    <style>
        .video--loop-container {
            min-height: 450px;
            max-height: 650px;
        }

        @media (max-width: 800px) {
            .video--loop-container {
                min-height: 480px;
            }
        }

        .video--loop-container video {
            /* Make video to at least 100% wide and tall */
            min-width: 100%;
            min-height: 100%;

            /* Setting width & height to auto prevents the browser from stretching or squishing the video */
            width: auto;
            height: auto;

            /* Center the video */
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .video--title {
            max-width: 1152px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 0em;
            padding-right: 0em;
            position: absolute;
            top: 10%;
            bottom: 0;
            right: 0%;
            text-align: left;
            color: #fff;
            z-index: 3;
        }

        @media (max-width: 87.5em) {
            .video--title {
                right: -10%;
            }
        }

        @media (max-width: 50em) {
            .video--title {
                text-align: center;
                left: 6%;
                padding-left: 2em;
                padding-right: 8em;
            }

            .video--loop-container {
                min-height: 375px;
            }

            .grid-item--white {
                border: none !important;
            }
        }

        .transparent-overlay--white {
            padding: 1em;
            background-color: rgba(255, 255, 255, 0.8);
            min-height: 5em;
        }

        .grid-item--white {
            float: left;
            padding: 0em 1em 0em 0em;
            text-align: center;
            border-right: 1px solid #ffffff;
            height: 18em;
        }

        .grid-item-last--white {
            float: left;
            padding: 0em 0em 0em 0em;
            text-align: center;
            height: 18em;
        }

    </style>
    <!-- Hero Block -->
    <div class="video--loop-container">
        <video class="video--loop" preload="auto" autoplay loop muted playsinline>
    <source src="https://player.vimeo.com/external/273583648.hd.mp4?s=6e71ccd5ce08314c5930554907df2491382c57dd&profile_id=174">
  </video>
        <div class="container__centered video--title text--white">
            <div class="container__two-thirds transparent-overlay center">
                <div class="text-shadow--black">
                    <h1>
                        <?php print $title; ?>
                    </h1>
                </div>
                <?php if( !empty($content['field_sub_header_1']) ){ ?>
                <h2>
                    <?php print render($content['field_sub_header_1']); ?>
                </h2>
                <?php } ?>
                <div class="btn-holder--content__callout no-margin--top">
                    <?php
      // button code...
      if( isset($buttons) && !empty($buttons) ){
        if( !empty($buttons[0]->field_hubspot_embed_code_1['und'][0]['value']) ){
          $button_code = $buttons[0]->field_hubspot_embed_code_1['und'][0]['value']; 
          print '<div class="center" style="margin-top:2em;">';
          hubspot_button($button_code, "button");
          print '</div>';
        }
        else{
          print '<div class="center"><a href="'.$buttons[0]->field_button_url_1['und'][0]['value'].'" class="btn--orange">'.$buttons[0]->field_button_text_1['und'][0]['value'].'</a></div>';
        }
      }
      ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Hero Block -->

    <!-- Block 2 -->
    <div class="container bg--green-gradient content__callout">
        <div class="container__centered text--white">
            <div class="page__title--center auto-margins">
                <div class="container__one-third grid-item--white">
                    <?php
      if( !empty($content_sections[0]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[0]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[0]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[0]->field_long_text_1['und'][0]['value'];   
      }
      ?>
                </div>
                <div class="container__one-third grid-item--white">
                    <?php
      if( !empty($content_sections[1]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[1]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[1]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[1]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
                <div class="container__one-third grid-item-last--white">
                    <?php
      if( !empty($content_sections[2]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[2]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[2]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[2]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Block 2 -->

    <!-- Block 3 Quote -->
    <div class="container bg--white">
        <article class="container__centered">
            <figure class="container__one-fourth center">
                <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/EvHazel.png" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;">
            </figure>
            <div class="container__three-fourths">
                <!--               Going to need to come back to this with Sean, he mentioned using Javascript to make it look like it does in the mock up-->
                <div class="quote__content__text text--large">
                    <?php
      if( !empty($content_sections[3]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[3]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[3]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[3]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
            </div>
        </article>
    </div>
    <!-- End Block 3 -->

    <!-- Block 4 Video 1-->
    <div class="content__callout border-none">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="250633907">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/video-overlay--What-MEDITECHs-Veterans-Want-to-Share-With-You.jpg" alt="MEDITECH's Veterans Want to Share With You Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/250633907"></a>
                    <div class="video__container"></div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <?php
      if( !empty($content_sections[4]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[4]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[4]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[4]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 4 -->

    <!-- Block 5 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/military-personnel-working-on-multiple-monitors.jpg);">
        <div class="container__centered">
            <div class="page__title--center">
                <div class="text--white">
                    <?php
      if( !empty($content_sections[5]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[5]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[5]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[5]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
                <div class="container__one-third transparent-overlay--white">
                    <?php
      if( !empty($content_sections[6]->field_header_1['und'][0]['value']) ){
        print '<h4>';
        print $content_sections[6]->field_header_1['und'][0]['value']; 
        print '</h4>';
      }
      if( !empty($content_sections[6]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[6]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
                <div class="container__one-third transparent-overlay--white">
                    <?php
      if( !empty($content_sections[7]->field_header_1['und'][0]['value']) ){
        print '<h4>';
        print $content_sections[7]->field_header_1['und'][0]['value']; 
        print '</h4>';
      }
      if( !empty($content_sections[7]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[7]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
                <div class="container__one-third transparent-overlay--white">
                    <?php
      if( !empty($content_sections[8]->field_header_1['und'][0]['value']) ){
        print '<h4>';
        print $content_sections[8]->field_header_1['und'][0]['value']; 
        print '</h4>';
      }
      if( !empty($content_sections[8]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[8]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 5 -->

    <!-- Block 6 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/person-proofreading-with-red-pen.jpg);">
        <div class="container__centered transparent-overlay" style="padding: 1em 3em;">
            <div class="page__title text--white">
                <?php
      if( !empty($content_sections[9]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[9]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[9]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[9]->field_long_text_1['und'][0]['value'];
      }
      ?>
                    <div class="container__one-half">
                        <?php
      if( !empty($content_sections[10]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[10]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[10]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[10]->field_long_text_1['und'][0]['value'];
      }
      ?>
                    </div>
                    <div class="container__one-half">
                        <?php
      if( !empty($content_sections[11]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[11]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[11]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[11]->field_long_text_1['und'][0]['value'];
      }
      ?>
                    </div>
            </div>
        </div>
    </div>
    <!-- End Block 6 -->

    <!--Block 7 Video 2 
<div class="content__callout border-none">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper">
      <div class="video js__video" data-video-id="250633834">
        <figure class="video__overlay">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/video-overlay--Veterans-Portal-Video-Content-Hear-Their-Stories.jpg" alt="MEDITECH's Veterans Want to Share With You Video Covershot">
        </figure>
        <a class="video__play-btn" href="http://vimeo.com/250633834"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <?php
      if( !empty($content_sections[12]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[12]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[12]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[12]->field_long_text_1['und'][0]['value'];
      }
      ?>
      </div>
    </div>
  </div>
</div>
End of Block 7 -->

    <!-- Block 8 -->
    <div class="container bg--blue-gradient">
        <div class="container__centered" style="padding: 1em 3em;">
            <div class="page__title text--white">
                <div class="container" style="padding-top:0em; padding-bottom:0em;">
                    <div class="center">
                        <?php
      if( !empty($content_sections[13]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[13]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[13]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[13]->field_long_text_1['und'][0]['value'];
      }
      ?>
                    </div>
                    <div class="container__two-thirds">
                        <?php
      if( !empty($content_sections[14]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[14]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[14]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[14]->field_long_text_1['und'][0]['value'];
      }
      ?>
                    </div>
                    <div class="container__one-third center">
                        <img style="max-width:50%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/Icon--Question-Mark.png" alt="Question Mark Icon">
                    </div>
                </div>

                <div class="container__one-half">
                    <?php
      if( !empty($content_sections[15]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[15]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[15]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[15]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
                <div class="container__one-half">
                    <?php
      if( !empty($content_sections[16]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[16]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[16]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[16]->field_long_text_1['und'][0]['value'];
      }
      ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End Block 8 -->
    <!-- end career-page--node-2438.php template -->
