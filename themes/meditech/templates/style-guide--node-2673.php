<!-- START style-guide--node-2673.php Modal Pop-up -->

<!-- Javascript & CSS reference for Prism -->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">
    <!-- Start Multiple Modal Pop-ups Demo -->
    <div class="container__one-third">
      <!-- Start Modal 1 -->
      <div id="modal1" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg" />
        </div>
      </div>

      <div class="open-modal" data-target="modal1">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg">
        <div class="mag-bg">
          <i class="mag-icon fas fa-search-plus"></i>
        </div>
      </div>
      <!-- End Modal 1 -->
    </div>

    <div class="container__one-third">
      <!-- Start Modal 2 -->
      <div id="modal2" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg" />
        </div>
      </div>

      <div class="open-modal" data-target="modal2">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg">
        <div class="mag-bg">
          <i class="mag-icon fas fa-search-plus"></i>
        </div>
      </div>
      <!-- End Modal 2 -->
    </div>

    <div class="container__one-third">
      <!-- Start Modal 3 -->
      <div id="modal3" class="modal">
        <a class="close-modal" href="javascript:void(0)">&times;</a>
        <div class="modal-content">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg" />
        </div>
      </div>

      <div class="open-modal" data-target="modal3">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg">
        <div class="mag-bg">
          <i class="mag-icon fas fa-search-plus"></i>
        </div>
      </div>
      <!-- End Modal 3 -->
    </div>
    <!-- End Multiple Modal Pop-ups Demo -->

  </div>

  <!-- SIDEBAR -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- END SIDEBAR -->

</section>
<!-- END style-guide--node-2673.php Modal Pop-up -->
