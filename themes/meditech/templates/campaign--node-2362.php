<!-- START campaign--node-2362.php -->
<?php // This template is set up to control the display of the EHR Mobility content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
  .microheader {
    letter-spacing: 1.5px;
    font-size: 14px;
  }

  .button-vertical-adjustment {
    margin-top: 6em;
  }

  @media (max-width: 800px) {
    .button-vertical-adjustment {
      margin-top: 1em;
    }
  }

  @media only screen and (max-width: 35em) {
    .tablet-half {
      float: left;
      display: block;
      margin-right: 2.35765%;
      width: 100%;
    }

    .top-margin {
      margin-top: 1em;
    }

    .mobile-smaller-image {
      max-width: 230px;
      display: block;
      margin: auto !important;
      float: none;
    }
  }

  .squish-list li {
    margin-bottom: .5em;
  }

  .floating-quote-box {
    margin-bottom: 0em !important;
  }

  @media all and (max-width: 50em) {
    .extra-pad-top {
      padding-top: 1em;
    }
  }

</style>

<div class="js__seo-tool__body-content">

  <!-- Block 1 - Hero -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-mobile-apps.jpg);">
    <div class="container__centered">
      <div class="container__one-half">
        &nbsp;
      </div>
      <div class="container__one-half transparent-overlay text--white">
        <h1 class="js__seo-tool__title">Keep moving with MEDITECH Mobile Solutions and Apps.</h1>
        <p>Set the pace with an <a href="https://ehr.meditech.com/ehr-solutions/web-ambulatory">EHR</a> that sticks with you — in or out of the <a href="https://ehr.meditech.com/ehr-solutions/ambulatory-physician-practices">office</a>, at the point of care, and on the go. MEDITECH Expanse mobile solutions and apps extend your reach beyond the continuum, for mobility in healthcare.</p>

        <div class="btn-holder--content__callout center" style="margin-bottom:1.5em;">
          <div class="center" style="margin-top:2em;">
            <?php hubspot_button($cta_code, "Watch The Point of Care Webinar Recording"); ?>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- End of Block 1 - Hero -->


  <!-- New Expanse Now Block To be added -->

  <div class="container bg--light-gray">
    <div class="container__centered">
      <div class="container__one-third center">
        <div class="expanse-logo">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/expanse-now-logo.png" alt="MEDITECH Expanse logo" style="width: 275px;">
        </div>
        <!-- Start modal -->
        <div id="modal1" class="modal">
          <a class="close-modal" href="javascript:void(0)">&times;</a>
          <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/results-expanse-now-2.png">
          </div>
        </div>

        <div class="open-modal" data-target="modal1">
          <div class="phone--black">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/results-expanse-now-2.png" alt="Screenshot of Expanse Now Lab Results" style="width: 250px;">
          </div>
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End modal -->
      </div>
      <div class="container__two-thirds">
        <p class="microheader">MOBILITY FOR PHYSICIANS</p>
        <h2>Get to your EHR, wherever you are.</h2>
        <p>Expanse Now — MEDITECH’s new physician mobility app for Android and iOS devices — lets physicians securely access their Expanse EHR wherever they are, using intuitive mobile device conventions and voice commands. Providers on call or covering for others can remotely manage routine tasks without the need to connect to the full EHR from a desktop or laptop computer, ensuring anytime/anywhere access to the most essential elements of their Expanse EHR.</p>


        <div class="container__centered">
          <p><strong>With Expanse Now, physicians can:</strong></p>

          <div class="container__one-half">
            <ul>
              <li>View their own workload items, and items of providers for whom they are covering</li>
              <li>Receive notifications of new workload items</li>
              <li>View patient result notifications</li>
              <li>View patient demographics, allergies, medications, and problems</li>
            </ul>
          </div>
          <div class="container__one-half">
            <ul>
              <li>Review relevant patient clinical data</li>
              <li>Manage prescription refill requests</li>
              <li>View and return patient calls</li>
              <li>Compose, update messages/tasks</li>
              <li>Mark items as read/unread.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- BLOCK 2 -->
  <div id="modal2" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--patient-data.jpg" alt="MEDITECH's Web Point of Care software - patient data">
    </div>
  </div>

  <div id="modal3" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--medication.jpg" alt="MEDITECH's Web Point of Care software - patient data">
    </div>
  </div>

  <div id="modal4" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--worklists.jpg" alt="MEDITECH's Web Point of Care software - worklists.">
    </div>
  </div>

  <div id="modal5" class="modal">
    <a class="close-modal" href="javascript:void(0)">&times;</a>
    <div class="modal-content">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--interventions.jpg" alt="MEDITECH's Web Point of Care software - interventions.">
    </div>
  </div>

  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
    <div class="container__centered text--white">
      <p class="microheader">MOBILITY FOR NURSES</p>
      <h2>A point of care solution that fits in your pocket.</h2>
      <p>Sometimes a workstation on wheels is too cumbersome and disruptive for the situation. <a href="https://vimeo.com/meditechehr/review/261876988/e340363f53" target="_blank">MEDITECH's Expanse Point of Care software</a> complements the mobile capabilities of our Patient Care System, giving you the flexibility to perform the most common interventions whenever and wherever they're needed through a <a href="https://info.forwardadvantage.com/create-a-secure-clinical-mobile-device-workflow-for-meditech-expanse" target="_blank">smartphone-like device</a>.</p>
      <div class="slider">
        <div>
          <div class="container__three-fourths tablet-half">
            <h2 class="text--white no-margin--top">Patient Data and Chart Views</h2>
            <div class="text--white squish-list">
              <p>View real-time patient data or launch into patient charts to see historical data, such as:</p>
              <ul>
                <li>Current and historical medications</li>
                <li>Detailed allergies/adverse drug reactions</li>
                <li>Recent orders and results</li>
                <li>Problem lists.</li>
              </ul>
            </div>
          </div>
          <div class="container__one-fourth mobile-smaller-image tablet-half center">

            <div class="open-modal" data-target="modal2">
              <div class="phone--white">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--patient-data.jpg" alt="MEDITECH's Web Point of Care software - patient data">
              </div>
              <div class="mag-bg">
                <i class="mag-icon fas fa-search-plus"></i>
              </div>
            </div>

          </div>
        </div>
        <div>
          <div class="container__three-fourths tablet-half">
            <h2 class="text--white no-margin--top">Medication Administration</h2>
            <div class="text--white squish-list">
              <p>Embedded Medication Administration Record and Bedside Verification components let clinicians:</p>

              <ul>
                <li>Scan, acknowledge, review, administer, and co-sign medications</li>
                <li>Document medication assessments and reassessments</li>
                <li>View and override medication conflicts and renewal warnings</li>
                <li>Display the protocol and taper schedule.</li>
              </ul>
            </div>
          </div>
          <div class="container__one-fourth mobile-smaller-image tablet-half center">

            <div class="open-modal" data-target="modal3">
              <div class="phone--white">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--medication.jpg" alt="MEDITECH's Web Point of Care software - patient data">
              </div>
              <div class="mag-bg">
                <i class="mag-icon fas fa-search-plus"></i>
              </div>
            </div>

          </div>
        </div>
        <div>
          <div class="container__three-fourths tablet-half">
            <h2 class="text--white no-margin--top">Worklists</h2>
            <div class="text--white squish-list">
              <p>Prioritize and sort summaries of due and overdue interventions and administrations. Other features help clinicians to:</p>

              <ul>
                <li>Enhance patient safety through mobile barcode scanning</li>
                <li>Receive real-time updates of patient information</li>
                <li>Access the patient's chart from the worklist.</li>
              </ul>
            </div>
          </div>
          <div class="container__one-fourth mobile-smaller-image tablet-half center">

            <div class="open-modal" data-target="modal4">
              <div class="phone--white">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--worklists.jpg" alt="MEDITECH's Web Point of Care software - worklists.">
              </div>
              <div class="mag-bg">
                <i class="mag-icon fas fa-search-plus"></i>
              </div>
            </div>

          </div>
        </div>
        <div>
          <div class="container__three-fourths tablet-half">
            <h2 class="text--white no-margin--top">Interventions</h2>
            <div class="text--white squish-list">
              <p>Immediately access intuitive documentation capabilities, to:</p>

              <ul>
                <li>Add, document, and edit interventions and assessments</li>
                <li>Receive immediate notification of due and overdue interventions</li>
                <li>Recall data on assessments</li>
                <li>Initiate rules-based logic.</li>
              </ul>
            </div>
          </div>
          <div class="container__one-fourth mobile-smaller-image tablet-half center">

            <div class="open-modal" data-target="modal5">
              <div class="phone--white">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/screenshot--interventions.jpg" alt="MEDITECH's Web Point of Care software - interventions.">
              </div>
              <div class="mag-bg">
                <i class="mag-icon fas fa-search-plus"></i>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>

  <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick.css">
  <link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick-theme.css">
  <script src="<?php print $url; ?>/sites/all/themes/meditech/js/slick.min.js"></script>
  <script>
    jQuery(document).ready(function($) {
      $('.slider').slick({
        dots: true,
        prevArrow: '<a class="slick-prev">&#10092;</a>',
        nextArrow: '<a class="slick-next">&#10093;</a>',
      });
    });

  </script>
  <!-- BLOCK 2 -->


  <!-- Block 3 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/woman-viewing-patient-details-on-smartphone.jpg);">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__one-third text--white">
          <p class="microheader">MOBILITY FOR HOME CARE PROVIDERS</p>
          <h2>Home Care, to go.</h2>
          <div>
            <p>Access your <a href="https://ehr.meditech.com/ehr-solutions/meditech-home-care">home care</a> schedules from any smartphone, document on the fly, and view patient details with MEDITECH Expanse. Home care staff can use common mobile device conventions to dial phone numbers and launch maps, for even greater efficiency.</p>
          </div>
        </div>
        <div class="container__one-third center">
          <!-- Start modal -->
          <div id="modal6" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-access-schedules-smartphone-screenshot.jpg" alt="MEDITECH On The Go Schedules Screenshot">
            </div>
          </div>

          <div class="open-modal" data-target="modal6">
            <div class="phone--white">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-access-schedules-smartphone-screenshot.jpg" alt="MEDITECH On The Go Schedules Screenshot" style="width: 250px;">
            </div>
            <div class="mag-bg">
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal -->

        </div>
      </div>
    </div>
  </div>
  <!-- End Block 3 -->


  <!-- Block 4 - Video -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="261876988">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--transform-how-you-deliver-care.jpg" alt="Learn how a more mobile MEDITECH improves the physician and patient experience video.">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/261876988"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>Transform how you deliver care. </h2>
          <p>Learn how the mobility of MEDITECH's EHR improves the physician and patient experience, from Deborah Harrigan, MD, a family medicine physician at Frisbie Memorial Hospital, and John Tollerson, DO, the CMIO at Kalispell Regional Medical Center.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 4 - Video -->


  <!-- End Block 5 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="Quote bubble">
      </figure>
      <div class="container__three-fourths text--white">
        <div class="quote__content__text text--large">
          <p class="italic">
            "I do mostly outpatient practice, using a mobile device for pretty much everything I do. I like the layout: it's appealing to the eye; it makes sense. Interacting with a touch screen definitely just seems to make it more natural in terms of documentation."</p>
        </div>
        <p class="text--large no-margin--bottom">Jeffrey Schleich, MD, Family Practice Physician</p>
        <p>FHN Memorial Hospital (Freeport, IL)</p>

      </div>
    </div>
  </div>
  <!-- End Block 5 -->



  <!-- Block 6 -->
  <div class="container bg--light-gray">
    <div class="container__centered">
      <div class="container__one-half center">
        <!-- Start modal -->
        <div class="open-modal" data-target="modal7">
          <div class="phone--black">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MHealth--screenshot.png" alt="Screenshot of the MHealth app" style="width: 15em;">
          </div>
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <div id="modal7" class="modal">
          <a class="close-modal" href="javascript:void(0)">&times;</a>
          <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MHealth--screenshot.png">
          </div>
        </div>
        <!-- End modal -->
      </div>
      <div class="container__one-half">
        <h2>Make patient engagement more accessible.</h2>
        <p>Encourage <a href="https://ehr.meditech.com/ehr-solutions/to-improve-patient-engagement-focus-on-the-patient">patients</a> to take an active role in their own healthcare, wherever they roam. Our MHealth app gives patients direct access to the MEDITECH patient <a href="https://ehr.meditech.com/ehr-solutions/patient-portal">portal</a> via a smartphone or tablet.</p>
        <h3>MHealth App</h3>
        <ul>
          <li>View records</li>
          <li>Book appointments</li>
          <li>Renew prescriptions</li>
          <li>Message providers</li>
          <li>Complete questionnaires</li>
          <li>Pay bills</li>
          <li>Attend virtual visits</li>
          <li>Share PGHD</li>
        </ul>
      </div>
    </div>
  </div>
  <!-- End Block 6 -->


  <!-- Block  -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
    <div class="container__centered text--white">

      <div class="container__one-half center">
        <!-- Start modal -->
        <div class="open-modal" data-target="modal8">
          
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/iPhoneX_AllRecords_PR-SCREEN.png" alt="Screenshot of the Health Records on iPhone" style="width: 15em;">

          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <div id="modal8" class="modal">
          <a class="close-modal" href="javascript:void(0)">&times;</a>
          <div class="modal-content">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/iPhoneX-AllRecords--screen-shot--2019.jpg">
          </div>
        </div>
        <!-- End modal -->
      </div>

      <div class="container__one-half">
        <p>The patient portal also syncs with Health Records on iPhone&reg;, so consumers can access their health data, aggregated with information from other supporting EHRs, through the Health app.</p>
        <h3>Health Records on iPhone</h3>
        <p>View the following:</p>
        <ul>
          <li>Allergies</li>
          <li>Lab results</li>
          <li>Medications</li>
          <li>Procedures</li>
          <li>Conditions</li>
          <li>Procedures</li>
          <li>Immunizations</li>
          <li>PGHD</li>
        </ul>
      </div>
    </div>
  </div>
  <!-- End Block  -->


  <!-- Block 7 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/home-care-worker.jpg);">
    <div class="container__centered">
      <div class="container__one-third text--white transparent-overlay">
        <h2>
          Mobile phlebotomy workflow support.
        </h2>
        <p>Bring your workflow to the patient's location using MEDITECH's web-based <a href="https://home.meditech.com/en/d/functionalitybriefs/otherfiles/phlebotomy.pdf" target="_blank">Mobile Phlebotomy</a>. Our solution supports point of care specimen collection, positive patient identification, and specimen/test review using a variety of mobile devices.</p>
      </div>
      <div class="container__one-third">
        <figure class="center extra-pad-top">
          <!-- Start modal -->
          <div id="modal9" class="modal">
            <a class="close-modal" href="javascript:void(0)">&times;</a>
            <div class="modal-content">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-mobile-screen-shot--patient-experience.jpg"> 
            </div>
          </div>
          <div class="open-modal" data-target="modal9">
           <div class="phone--black">
            <img style="width: 225px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-mobile-screen-shot--patient-experience.jpg" alt="Smartphone screenshot using MEDITECH's web-based Mobile Phlebotomy."> 
            </div>
            <div class="mag-bg">
              <!-- Include if using image trigger -->
              <i class="mag-icon fas fa-search-plus"></i>
            </div>
          </div>
          <!-- End modal -->
        </figure>
      </div>
      <div class="container__one-third">&nbsp;</div>
    </div>
  </div>
  <!-- End Block 7 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 8 - CTA -->
<div class="container bg--black-coconut" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
  <div class="container__centered center">

    <?php if( $cta->field_header_1['und'][0]['value'] != '' ){ ?>
    <h2 class="text--white">
      <?php print $cta->field_header_1['und'][0]['value']; ?>
    </h2>
    <?php } ?>

    <?php if( $cta->field_long_text_1['und'][0]['value'] != '' ){ ?>
    <div class="text--white">
      <?php print $cta->field_long_text_1['und'][0]['value']; ?>
    </div>
    <?php } ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Sign Up for the Expanse Point of Care Webinar"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End Block 8 CTA -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2362.php -->
