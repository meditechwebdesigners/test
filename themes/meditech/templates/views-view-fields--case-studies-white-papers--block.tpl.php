<!-- start views-view-fields--case-studies-white-papers--block.tpl.php template -->
<?php 
  // This template is for each row of the Views block: CASE STUDIES WHITE PAPERS ....................... 

  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);

  // get landing page type...
  $tags = field_view_field('node', $node, 'field_landing_page_type'); 
  // 'field_landing_page_type' is the machine name of the field in the content type that contains the taxonomy
  $cta_terms = array();
  foreach($tags['#items'] as $tag){
    $cta_terms[] = $tag["taxonomy_term"]->name;
  }
  $cta_type = $cta_terms[0];

?>
<div>
 
  <h3 style="margin-bottom:0;"><a class="cta_link_cs_wp_page_gae" href="<?php print $fields['field_text_1']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
  <p style="font-size:.8em; margin:0 0 .6em 0; font-style:italic;"><strong><?php print $cta_type; ?></strong> published on <?php print $fields['published_at']->content; ?></p>
  <div class="inline__text__wrapper">
    <?php print $fields['field_summary']->content; ?>
  </div>
  <?php // add Edit Video link...
    if( user_is_logged_in() ){ 
      print '<div style="display:block; text-align:right;"><span style="font-size:12px;">'; print l( t('Edit This'),'node/'. $fields['nid']->content .'/edit' ); print "</span></div>"; 
    } 
  ?> 

  <hr>

</div>
<!-- end views-view-fields--case-studies-white-papers--block.tpl.php template -->