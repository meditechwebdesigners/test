<?php // This template is for each row of the Views block:  UPCOMING WEBINARS ....................... 

// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

// get_webinar_image function is in the template.php file
?>
<!-- start views-view-fields--events-upcoming-webinars--block.tpl.php template -->
<section class="article--card">
    <?php $webinar_image = get_webinar_image($node); ?>
    <figure class="article--img">
        <a class="webinars_link_gae" href="<?php print $fields['field_text_1']->content; ?>"><img src="<?php print $webinar_image['url']; ?>" alt="webinar thumbnail"></a>
    </figure>
    <div class="article--info">
        <h3 class="header-four"><a class="webinar_live_link_gae" href="<?php print $fields['field_text_1']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
        <h5 class="no-margin--bottom"><?php print $fields['field_date_and_time']->content.', '.$fields['field_date_and_time_1']->content; ?> (Eastern)
            <?php 
    if( !empty($fields['field_duration']->content) ){ 
      print ' | '.$fields['field_duration']->content; 
    }
    ?></h5>

        <div class="line-clamp"><?php print $fields['field_summary']->content; ?></div>
    </div>
    <?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:center; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
</section>



<!-- end views-view-fields--events-upcoming-webinars--block.tpl.php template -->
