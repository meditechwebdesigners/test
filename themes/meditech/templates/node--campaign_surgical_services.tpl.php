<?php // This template is set up to control the display of the CAMPAIGN_6_1 content type
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$button_1 = field_collection_data($node, 'field_fc_button_1');
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
?>
<!-- start node--campaign_surgical_services.tpl.php template -->

<style>
  .button__vertical-center { margin-top: 5.5em; }
  @media only screen and (max-width: 50em) {
    .button__vertical-center { margin-top: 0; }
  }
</style>


<div class="js__seo-tool__body-content">

  <!-- Block 1 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/three-surgeons-in-surgery.jpg);">
    <div class="container__centered">

      <div class="container__two-thirds transparent-overlay text--white">
        <h1 class="text--white js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
        <h2><?php print render($content['field_header_1']); ?></h2>
        <?php print render($content['field_long_text_1']); ?>
      </div>
      <div class="container__one-third button__vertical-center">
        <div class="btn-holder--content__callout">
          <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>

    </div>
  </div>
  <!-- End of Block 1 -->


  <!-- Block 2 -->
  <?php $block_2 = field_get_items('node', $node, 'field_long_text_unl_1'); ?>
  <div class="container text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-pattern-01.png);">
    <div class="container__centered center">

      <div class="page__title--center" style="margin-bottom:1em;">
        <h2><?php print render($content['field_header_2']); ?></h2>
        <?php print render($content['field_long_text_2']); ?>
        <img style="max-width: 70%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Surgical-Services-Scheduling.png" alt="MEDITECH Surgical Services Scheduling screenshot">
      </div>

      <div class="container no-pad">
       <?php for($b2A=0; $b2A<3; $b2A++){ ?>
       <div class="container__one-third">
          <div class="large-numbered-list"><?php print $b2A + 1; ?></div>
          <div class="large-numbered-list--description">
            <?php print $block_2[$b2A]['value']; ?>
          </div>
        </div>
        <?php } ?>
      </div>

      <div class="container no-pad">
       <?php for($b2B=3; $b2B<6; $b2B++){ ?>
       <div class="container__one-third">
          <div class="large-numbered-list"><?php print $b2B + 1; ?></div>
          <div class="large-numbered-list--description">
            <?php print $block_2[$b2B]['value']; ?>
          </div>
        </div>
        <?php } ?>
      </div>

    </div>
  </div>
  <!-- End of Block 2 -->


  <!-- Block 3 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <article class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $quotes[0]->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $quotes[0]->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $quotes[0]->field_company['und'][0]['value']; ?></p>
      </div>
    </article>
  </div>
  <!-- End of Block 3  -->


  <!-- Block 4 -->
  <?php $block_4 = field_get_items('node', $node, 'field_text_unl_1'); ?>
  <div class="container background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctors-and-tablet-background.jpg);">
    <div class="container__centered">
      <div class="page__title--center">
        <h2><?php print render($content['field_header_3']); ?></h2>
      </div>
      <h4 class="" style="text-align:center;"><?php print render($content['field_sub_header_1']); ?></h4>
      <br>
      <div>
        <div class="container__one-half">
          <p class="transparent-overlay"><?php print $block_4[0]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[1]['value']; ?></p>
        </div>
        <div class="container__one-half">
          <p class="transparent-overlay"><?php print $block_4[2]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[3]['value']; ?></p>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Block 4 -->


  <!-- Block 5 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <article class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $quotes[1]->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $quotes[1]->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $quotes[1]->field_company['und'][0]['value']; ?></p>
      </div>
    </article>
  </div>
  <!-- End of Block 5  -->


  <!-- Block 6 -->
  <div class="container text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-pattern-01.png);">
    <div class="container__centered">
      <div class="page__title--center">
        <h2><?php print render($content['field_header_4']); ?></h2>
      </div>
      <br>
      <div class="container__one-third">
        <?php print render($content['field_long_text_3']); ?>
      </div>
      <div class="container__two-thirds">
        <figure>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Surgical-Services-Patient-Record.png" alt="MEDITECH Surgical Services Patient Record screenshot">
        </figure>
      </div>
    </div>
  </div>
  <!-- End of Block 6 -->


  <!-- Block 7 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <article class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $quotes[2]->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $quotes[2]->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $quotes[2]->field_company['und'][0]['value']; ?></p>
      </div>
    </article>
  </div>
  <!-- End of Block 7  -->


  <!-- Block 8 -->
  <?php $block_8 = field_get_items('node', $node, 'field_long_text_unl_2'); ?>
  <div class="container text--white background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/medical-supply-room.jpg);">
    <div class="container__centered">
      <div class="page__title--center">
        <h2><?php print render($content['field_header_5']); ?></h2>
      </div>

      <div style="margin-bottom:2em;"><?php print $block_8[0]['value']; ?></div>

      <div class="container__one-half">
        <?php print $block_8[1]['value']; ?>
      </div>
      <div class="container__one-half">
        <?php print $block_8[2]['value']; ?>
      </div>
    </div>
  </div>
  <!-- End of Block 8 -->


  <!-- Block 9 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Surgical-Services-Patient-Tracker.png" alt="MEDITECH Surgical Services Patient Tracker screenshot">
          </figure>
        </div>
        <div class="container__one-third">
          <h2 class="text--white"><?php print render($content['field_header_6']); ?></h2>
          <?php print render($content['field_long_text_5']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 9 -->

</div><!-- end js__seo-tool__body-content -->

<!-- Block 10 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
  <div class="container__centered center">
   
    <h2 class="js__seo-tool__body-content"><?php print render($content['field_header_7']); ?></h2>
    <?php 
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
      <div class="button--hubspot">
        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
      </div>
    <?php }else{ ?>
      <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
    <?php } ?>

      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
  </div>
</div>
<!-- End of Block 10 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
<!-- end node--campaign_surgical_services.tpl.php template -->