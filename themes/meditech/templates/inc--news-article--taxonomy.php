<!-- START inc--news-article--taxonomy.php template -->

<?php 
  // get node URL from node ID...
  $nodeURL = url('node/'. $node->nid); 
  // check node's taxonomy news terms...
  $news_tags = field_view_field('node', $node, 'field_news_tags'); 
  // 'field_news_tags' is the machine name of the field in the content type that contains the taxonomy terms

  // list taxonomy news term associated with article...
  $tag_array = array();
  foreach($news_tags['#items'] as $news_tag){
    $term = taxonomy_term_load($news_tag['tid']);
    $term_name = $term->name;
    $tag_array[] = $term_name;
  }  

  // if taxonomy has the VIDEOS tag and an actual video is attached to the node, then show image thumbnail...
  if( in_array('Videos', $tag_array) && !empty($node->field_video) ){
      
    // use node to look for existance of video overlay image...
    foreach($node->field_video['und'] as $v){
      $video_id = $v['target_id'];
    }
    $video_node = node_load($video_id);
    $video_overlay_image = $video_node->field_video_overlay_image['und'][0]['filename'];
    ?>
<figure class="container no-pad">
  <div class="container__one-third">
    <?php
        // if video overlay image exists, use supplied image...
        if($video_overlay_image != ''){

          // get crop orientation option to set proper class name for image...
          $video_image_orientation_array = $video_node->field_image_1_crop_orientation;
          $video_image_orientation_id = $video_image_orientation_array['und'][0]['tid'];
          $video_image_orientation_term = taxonomy_term_load($video_image_orientation_id);
          $video_image_orientation_term_name = $video_image_orientation_term->name;

          if($video_image_orientation_term_name == 'Center Image'){
            $crop = 'crop-center';
          }
          elseif($video_image_orientation_term_name == 'Show Right Side of Image'){
            $crop = 'crop-right';
          }
          else{
            $crop = 'crop-left';
          }
          $video_overlay_image_url = 'https://ehr.meditech.com/sites/default/files/styles/video-overlay-breakpoints_theme_meditech_big-medium_2x/public/images/video/'.$video_overlay_image;

        }
        else{

          $video_overlay_image_url = 'https://ehr.meditech.com/sites/all/themes/meditech/images/news/main-news-hero_video-background.png';
          $crop = 'crop-left';

        }
        ?>
    <div class="square-img-cropper <?php print $crop; ?>">
      <a class="tag_link_news_article_gae" href="<?php print $nodeURL; ?>"><img src="<?php print $video_overlay_image_url; ?>" style="width:auto;" /></a>
    </div>
  </div>
  <figcaption class="container__two-thirds" style="margin-right:0;">
    <h3 class="header-four no-margin"><a class="tag_link_news_article_gae" href="<?php print $nodeURL; ?>"><?php print $title; ?></a></h3>
    <div class="inline__text__wrapper">
      <p><span class="snippet__card__text--callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date("F j, Y", $node->published_at); ?></time></span>&nbsp;&mdash;&nbsp;</p>
      <?php
        $summary = field_view_field('node', $node, 'field_summary');
        print render($summary); 
        ?>
    </div>
  </figcaption>
  <div style="float:left; width:100%;">
    <ul class="news__article__filters tag_link_news_article_gae" style="float:left; width:100%;">
      <?php generate_news_tag_links($node); ?>
    </ul>
  </div>
</figure>

<hr>
<?php
  }
  // no VIDEOS tag, don't show a thumbnail...
  else{
  ?>
<div>

  <h3><a class="taxonomy_result_link_gae" href="<?php print $nodeURL; ?>"><?php print $title; ?></a></h3>

  <div class="inline__text__wrapper">
    <p><span class="snippet__card__text--callout"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date("F j, Y", $node->published_at); ?></time></span>&nbsp;&mdash;&nbsp;</p>
    <?php
        $summary = field_view_field('node', $node, 'field_summary');
        print render($summary); 
        ?>
  </div>

  <ul class="news__article__filters tag_link_news_article_gae">
    <?php generate_news_tag_links($node); ?>
  </ul>

  <hr>

</div>
<?php
  }
  ?>

<!-- END inc--news-article--taxonomy.php template -->