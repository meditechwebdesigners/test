<?php // This template is for each row of the Views block:  NEXT BIG EVENT - AP view ....................... 
  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);
?>
<!-- start views-view-fields--next-big-event-ap--block.tpl.php template -->
  <h2>Upcoming Events</h2>
  <h3 class="header-four no-margin"><a class="_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
  <h5 class="no-margin--bottom"><?php print $fields['field_event_date']->content; ?></h5>
  <h5 class="no-margin--top">
    <?php 
      print $fields['field_location_city']->content; 
      if($fields['field_location_state']->content){
        $stateTerms = field_view_field('node', $node, 'field_location_state'); 
        if(!empty($stateTerms)){
          foreach($stateTerms["#items"] as $sTerm){
            $eventState = ', '.$sTerm["taxonomy_term"]->description;
          }
        }
        else{
          $eventState = '';
        }
        print $eventState; 
      }
      if($fields['field_location_country']->content){
        if($fields['field_location_country']->content != 'United States' && $fields['field_location_country']->content != 'Canada'){
          print ', '.$fields['field_location_country']->content;
        }
      }
    ?>
  </h5>
  <p><?php print $fields['field_summary']->content; ?></p>
<!-- end views-view-fields--next-big-event-ap--block.tpl.php template -->