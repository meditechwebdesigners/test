<!-- start events-simple-page--node-1841.php MAIN EVENTS page template -->
<?php $url = $GLOBALS['base_url']; // grabs the site url ?>

<style>
    .article--img a {
        box-shadow: none;
    }

    .article--img a:focus {
        box-shadow: none !important;
    }

    /*    important tag above overwrites another important tag in the css*/

</style>

<h1 style="display:none;">MEDITECH Events</h1>

<!-- Hero -->
<?php print views_embed_view('events_next_big_event', 'block'); // adds 'Events - Next Big Event' Views block... ?>
<!-- End Hero -->

<section class="container__centered">

    <div class="js__seo-tool__body-content">

        <h2>Upcoming Events</h2>

        <?php print views_embed_view('events_upcoming_events', 'block'); // adds 'Upcoming Events' Views block... ?>

        <h2 style="margin-top:2em;">Upcoming Webinars</h2>

        <?php print views_embed_view('events_upcoming_webinars', 'block'); // adds 'Upcoming Webinars' Views block... ?>

    </div>

</section>

<!-- END events-simple-page--node-1841.php MAIN EVENTS page template -->
