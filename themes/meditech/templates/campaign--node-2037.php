<!-- START campaign--node-2037.php INTEROPERABILITY CAMPAIGN -->
<!-- ************** PLACE YOUR HTML CODE BETWEEN THESE LINES *********************** -->

<style>
  .quote__content__text { font-size: 1em; }
  .quote__content__name { font-size: 1.5em; }

  /******************************************************
    VIDEO
  ******************************************************/
  .container--video--loop { min-height: 580px; position: relative; height: 400px; overflow: hidden; }
  .container--video--loop--overlay { height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; z-index: 2; background: #000; opacity: 0.5; }
  .video--loop--video { min-width: 100%; min-height: 500px; margin-bottom: -.5em; }
  .video--loop--title { max-width: 780px; padding-right: 2em; position: absolute; top: 12%; left: 6%; right: 6%; text-align: left; color: #fff; z-index: 3; }
  @media screen and (max-width: 27em){
    .video--loop--title { font-size: inherit; }
  }
  @media screen and (max-width: 800px){
    .container--video--loop { min-height: 400px; }
  }

  /******************************************************
    YOUTUBE
  ******************************************************/
  .video--shadow {border-radius: 6px; box-shadow: 10px 13px 32px rgba(0, 0, 0, 0.2);}
  .youtube { position: relative; padding-bottom: 56.25%; padding-top: 35px; height: 0; overflow: hidden; margin-bottom: 1em; }
  .youtube iframe { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }

  /******************************************************
    GRID
  ******************************************************/
  .grid-item{ border-right: 1px solid #ffffff; border-bottom: 1px solid #ffffff; background: none; height: 13em; padding: 2em 2em 1em 2em; }
  @media (max-width: 715px){
    .grid-item { height: 16em; }
  }
  @media (max-width: 480px){
    .grid-item { height: 12em; border: none !important; }
  }
  .grid-item--numbers { margin-top: 0; }
</style>

<script>
  var vid = document.getElementById("bgVideo");
  function getPlaySpeed() {
    alert(vid.playbackRate);
  }
  function setPlaySpeed() {
    vid.playbackRate = 0.5;
  }
</script>

<?php
// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
$cta = field_collection_data($node, 'field_fc_cta_block');

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
?>

<div class="js__seo-tool__body-content">


<!-- Block 1 -->
<div class="container--video--loop">
  <div class="container--video--loop--overlay"></div>
  <video id="bgVideo" class="video--loop--video" preload="auto" autoplay loop muted playsinline>
    <source src="https://player.vimeo.com/external/228703373.hd.mp4?s=fbded432a9ba6cf589e924db4a66f7dffc18c9c4&profile_id=175">
  </video>
  <div class="video--loop--title transparent-overlay">
    <h1 class="text--white text-shadow--black js__seo-tool__title">Unlock the Value of Our EHR Interoperability</h1>
    <p class="text--white">Standards-based interoperability benefits everyone in the care network and helps you connect across the continuum of care, HIEs, and federal and local public health agencies with ease. Get a leg up on care coordination by seamlessly exchanging information through C-CDAs, APIs, FHIR, HL7, and Direct Messaging using MEDITECH's Expanse. Rest easy knowing that MEDITECH supports industry standards and complies with interoperability requirements defined by ONC and CMS for Meaningful Use.</p>

    <div class="btn-holder--content__callout no-margin--top">
      <?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $cta->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
        <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>" ><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png"  alt="button"/></a></span>
            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
            <script type="text/javascript">
                hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});
            </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
        </div>
        <?php } ?>
    </div>
  </div>
</div>
<!-- Close Block 1 -->


<!-- VIDEO -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
				<!-- <div class="video js__video" data-video-id="217657040"> -->
				<div class="youtube video--shadow">
				  <iframe width="560" height="315" src="https://www.youtube.com/embed/fkfZbwDM6B4?rel=0&controls=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>
        <div class="video__container">
        </div>
				<!-- </div> -->
			</div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
       <h2>MEDITECH Announces Support For CommonWell Services</h2>
        <p>MEDITECH providers can <a href="https://www.prnewswire.com/news-releases/alliance-community-hospital-first-meditech-customer-to-deploy-commonwell-services-300673490.html?tc=eml_cleartime" target="_blank">access and share</a> patient information nationwide, providing a total and uninterrupted view of the patient's story. MEDITECH has embedded <a href="http://www.commonwellalliance.org/" target="_blank">CommonWell Health AllianceⓇ</a> interoperability services, including patient enrollment capabilities and C-CDA exchange via FHIR, directly into its Electronic Health Record, providing near real-time access to patient's data. Today, more than 9,300 provider sites are live on CommonWell services across the nation, and more than 30 million individuals are enrolled.</p>
        <p>Soon, MEDITECH customers will have the opportunity to query participating Carequality Implementers' providers as part of the <a href="http://www.commonwellalliance.org/news/carequality-commonwell-health-alliance-collaboration/" target="_blank">CommonWell Carequality agreement</a>.</p>
        <p>Interested in the value the CommonWell services bring? Watch MEDITECH's Marketing Solutions Manager, Stephen Valutkevich's interview at the HIMSS18 Interoperability Showcase on <a href="https://www.youtube.com/watch?v=fkfZbwDM6B4" target="_blank">CommonWell TV</a>.</p>
      </div>
    </div>
  </div>
<!-- End of VIDEO -->


<!-- Block 3 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/lines-on-light-green-bg.png);">
    <div class="container__centered center">
      <h2 class="text--white no-margin">The Numbers Don't Lie</h2>
      <p class="text--white">Count the ways MEDITECH is making healthcare interoperability a reality:</p>
    </div>

    <div class="container__centered">
      <div class="grid-items-lines" style="padding-top:2em;">
        <div class="grid-item">
          <figure>
            <img width="80" src="<?php print $url; ?>/sites/all/themes/meditech/images/touch-interface.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/touch-interface.png';this.onerror=null;" alt="finger tapping on a touch screen">
          </figure>
          <h3 class="grid-item--numbers text--white no-margin--top">200,000</h3>
          <p class="text--white">data-exchanging interfaces</p>
        </div>

        <div class="grid-item">
          <figure>
            <img width="80" src="<?php print $url; ?>/sites/all/themes/meditech/images/patient-discharge.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/patient-discharge.png';this.onerror=null;" alt="stick figure walking with suitcase leaving the hostpital">
          </figure>
          <h3 class="grid-item--numbers text--white no-margin--top">7 Million</h3>
          <p class="text--white">inpatient discharges a year</p>
        </div>

        <div class="grid-item" style="border-right:none;">
          <figure>
            <img width="80" src="<?php print $url; ?>/sites/all/themes/meditech/images/united-states-map.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/united-states-map.png';this.onerror=null;" alt="outline of the united states">
          </figure>
          <h3 class="grid-item--numbers text--white no-margin--top">50 States</h3>
          <p class="text--white">exchange immunizations data</p>
        </div>

        <div class="grid-item" style="border-bottom:none;">
          <figure>
            <img width="80" src="<?php print $url; ?>/sites/all/themes/meditech/images/patient-record.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/patient-record.png';this.onerror=null;" alt="patient record on an ipad">
          </figure>
          <h3 class="grid-item--numbers text--white no-margin--top">30 Million</h3>
          <p class="text--white">C-CDAs exchanged annually</p>
        </div>

        <div class="grid-item" style="border-bottom:none;">
          <figure>
            <img width="80" src="<?php print $url; ?>/sites/all/themes/meditech/images/data-exchange.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/data-exchange.png';this.onerror=null;" alt="arrows showing data flowing multiple ways">
          </figure>
          <h3 class="grid-item--numbers text--white no-margin--top">300 Billion</h3>
          <p class="text--white">data transactions a year</p>
        </div>

        <div class="grid-item" style="border-right:none; border-bottom:none;">
          <figure>
            <img width="80" src="<?php print $url; ?>/sites/all/themes/meditech/images/no-fees.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/no-fees.png';this.onerror=null;" alt="no fees">
          </figure>
          <h3 class="grid-item--numbers text--white no-margin--top">No Fees</h3>
          <p class="text--white">for transactions</p>
        </div>
      </div>
    </div>
</div>
<!-- END Block 3 -->

<!-- Block 4 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Vanessa-Carter.jpg);">
  <div class="container__centered">
    <div class="container__one-half transparent-overlay">
      <h2 class="text--white">A Game Changer for the Patient Experience</h2>
      <p class="text--white">Vanessa Carter is an e-Patient advocate and founder of <a href="http://www.hcsmsa.co.za/" target="_blank">Health Care Social Media South Africa</a> (Facebook <a href="https://www.facebook.com/hcsmsa/" target="_blank">@hcsmSA</a>). A car accident in South Africa left her with severe injuries, requiring multiple surgeries and doctor consultations. As a result of these many visits, Vanessa identified the lack of interoperability as a major issue for herself and other patients.</p>
      <p class="text--white">As noted in her recent <a href="http://blog.meditech.com/empowering-patients-with-digital-innovation-and-design-thinking?hs_preview=gxDgzXTp-5140635518">MEDITECH blog post</a>, “gaps in data sharing and room for error” happen without interoperability. Specialists caring for her did not have a central record to utilize or effective tools to support collaboration. Her recovery was hampered by MRSA, which may have been avoidable with improved data sharing, thus enabling more coordinated care.</p>
      <p class="text--white">Vanessa advocates for bridging the digital divide by “working towards building infrastructure that supports connectivity.” She promotes the concept of data following the patient by breaking down siloed health data and using a simplified system design with doctor and patient experience in mind to gain wider adoption. <a href="http://blog.meditech.com/empowering-patients-with-digital-innovation-and-design-thinking?hs_preview=gxDgzXTp-5140635518">Read her full blog post here.</a></p>
    <div class="container__two-half">
    </div>
  </div>
</div>
</div>
<!-- CLOSE Block 4 -->


<!-- Block 5 -->
<div class="container bg--black-coconut">
  <!-- style="background-image: url('../images/interoperability-background.jpg');" -->
  <div class="container__centered">
    <h2>Get Mobile with Interoperability on the Go</h2>
    <div class="container__two-thirds">
      <p class="bold">To ease the burden on care providers and patients, our standards-based APIs:</p>
      <ul>
        <li>Integrate with a variety of personal health devices through Validic.</li>
        <li>Meet Meaningful Use requirements for patient access to data using The Argonaut Project's FHIR implementation guide.</li>
        <li>Can be queried by third-party-developed or MEDITECH apps. </li>
      </ul>
      <p class="bold">MEDITECH is providing apps for that! Our native mobile apps include:</p>
      <ul>
        <li>MHealth - for patient/proxy use with the Patient and Consumer Health Portal.</li>
        <li>MConnect - for clinician use with our web products. <span class="italic">(coming soon)</span></li>
      </ul>
      <p>Our apps will also have key system integration points with third-party vendors, including support for video visits and speech recognition.</p>
    </div>
    <div class="container__one-third" style="text-align: center">
      <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MHealth-App-Icon.png" alt="MHealth App Icon" />
      <br>
      <a href="https://itunes.apple.com/us/app/meditech-mhealth/id1143209032?mt=8" title="Download from the app store" style="border-bottom: none;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/App-Store-Badge.png" alt="App Store logo" /></a>
      <br>
      <a href="https://play.google.com/store/apps/details?id=com.meditech.PatientPhm&hl=en" title="Download from google play" style="border-bottom: none;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Google-Play-Badge.png" alt="Google Play logo" /></a>
    </div>
  </div>
</div>
<!-- Close Block 5 -->


<!-- Block 6 -->
<div class="container">
  <div class="container__centered">
    <div class="container__two-thirds">
      <h2>Make Interoperability a Part of Your Population Health Strategy</h2>
      <p>At MEDITECH, we provide an integrated and interoperable EHR well-suited to support all your expanding <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health" title="population health">population health</a> demands. Our seamless care infrastructure shares data across all systems and locations enabling providers to better manage patient populations and quickly coordinate care with greater efficiency. <a href="https://ehr.meditech.com/expanse">MEDITECH Expanse</a> provides C-CDA exchange and consumption, HIE integration, and public health reporting that contribute towards better population health strategies.</p>
    </div>
    <div class="container__one-third" style="text-align: center">
      <figure>
        <img width="500" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Coordinate-Care-Across-Settings.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Coordinate-Care-Across-Settings.png';this.onerror=null;" alt="coordinate care across healthcare settings">
      </figure>
    </div>
  </div>
</div>
<!-- Close Block 6 -->

<!-- Block 7 -->
<div class="container bg--emerald">
  <div class="container__centered">
    <h2 class="center">Our Customers Are Our Best Advocates</h2>

    <div class="container__one-third">
      <div class="bg--fresh-mint" style="height: 8em; width: 8em; padding: 2em; border-radius: 50%; margin: 0 auto; margin-bottom: 2em;">
        <i class="fas fa-sitemap fa-4x white"></i>
      </div><!-- quote graphic -->
      <p class="quote__content__text">We were anxious for the MEDITECH CCD to effectively displace the competitor's CCD solution because of the robustness of the MEDITECH solution. Much needed for our interoperability with e-MD's ambulatory system and <a href="https://blog.meditech.com/why-3-separate-hospitals-started-an-hie-together">new HIE</a> and patient portal solutions from McKesson RelayHealth. MEDITECH's CCD did not disappoint and we're pleased with the success to date!</p>
      <p class="quote__content__name">Audrey Parks</p>
      <p class="quote__content__title">CIO, Salinas Valley Memorial Healthcare System</p>
    </div>

    <div class="container__one-third">
      <div class="bg--fresh-mint" style="height: 8em; width: 8em; padding: 2em; border-radius: 50%; margin: 0 auto; margin-bottom: 2em;">
        <i class="fas fa-cogs fa-4x white"></i>
      </div><!-- quote graphic -->
      <p class="quote__content__text">We're excited to be at the forefront of interoperability breakthroughs and technical innovation in healthcare. Even more importantly, we're excited to bridge gaps in patient care, and deliver the best outcomes possible for our community.</p>
      <p class="quote__content__name">David Shroades</p>
      <p class="quote__content__title">Vice President Operations and CIO, Alliance Community Hospital</p>
    </div>

    <div class="container__one-third">
      <div class="bg--fresh-mint" style="height: 8em; width: 8em; padding: 2em; border-radius: 50%; margin: 0 auto; margin-bottom: 2em;">
        <i class="fas fa-exchange-alt fa-4x white"></i>
      </div><!-- quote graphic -->
      <p class="quote__content__text">We leverage MEDITECH's CCD exchange suite to send summaries to Medway Country Manor and Caretenders sites via the state HIE, and bidirectionally exchange summaries with our physician group which runs GE Centricity in their offices.  In addition, we exchange summaries with other physicians who use Epic.</p>
      <p class="quote__content__name">Nicole A. Heim</p>
      <p class="quote__content__title">Executive Vice President and COO, Milford Regional Medical Center</p>
    </div>

  </div>
</div>
<!-- Close Block 7 -->

<!-- Block 8 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/interoperability-background.jpg);">
    <div class="container__centered">
        <div class="page__title--center">
            <h2><?php print $cta->field_header_1['und'][0]['value']; ?></h2>
            <?php 
            // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
            if( $cta->field_long_text_1['und'][0]['value'] != '' ){
            ?>
              <div><?php print $cta->field_long_text_1['und'][0]['value']; ?></div>
            <?php } ?>

            <?php 
            // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
            if( $cta->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
              $cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
            ?>
            <div class="button--hubspot">
                <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>" ><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png"  alt="button"/></a></span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                    hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});
                </script>
                </span>
                <!-- end HubSpot Call-to-Action Code -->
            </div>
            <?php } ?>    
            
            <div style="margin-top:1em;">
              <?php print $share_link_buttons; ?>
            </div>      
        </div>
    </div>
</div>
<!-- Close Block 8 -->

</div>
<!-- END js__seo-tool__body-content -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>


  <!-- ************** PLACE YOUR HTML CODE BETWEEN THESE LINES *********************** -->
<!-- END campaign--node-2037.php -->