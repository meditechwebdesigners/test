<?php // This template is set up to control the display of the International content type

$url = $GLOBALS['base_url']; // grabs the site url

$block_2 = multi_field_collection_data($node, 'field_fc_head_ltext_1', 5); 
$block_4 = multi_field_collection_data($node, 'field_fc_head_text_1', 6);
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
$block_9 = multi_field_collection_data($node, 'field_fc_head_ltext_2', 4); 

$block_10 = field_collection_data($node, 'field_fc_head_ltext_btn_1');
$button_1 = field_collection_data($node, 'field_fc_button_1');
$button_2 = field_collection_data($node, 'field_fc_button_2');
$button_3 = field_collection_data($node, 'field_fc_button_3');
?>
<!-- start node--new-international.tpl.php template -->

<div class="js__seo-tool__body-content">

    <style>
        /* **** VIDEO ************************************** */

        .video--loop-container {
            min-height: 600px;
            max-height: 650px;
        }

        @media (max-width: 800px) {
            .video--loop-container {
                min-height: 480px;
            }
        }

        .video--loop-container video {
            /* Make video to at least 100% wide and tall */
            min-width: 100%;
            min-height: 100%;
            /* Setting width & height to auto prevents the browser from stretching or squishing the video */
            width: auto;
            height: auto;
            /* Center the video */
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .video--title-3 {
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            margin-top: 4em;
            margin-bottom: 1em;
            position: relative;
            color: #fff;
            z-index: 3;
        }

        .hero-image-container {
            position: relative;
        }

        .hero-image-container img {
            border-radius: 7px;
            margin: 2em 0 0 0;
            max-width: 100%;
            z-index: 1;
            position: relative;
        }

        @media all and (max-width: 50em) {
            .flex-order--reverse {
                order: -1;
            }

            .hero-image-container img {
                margin: 0;
            }
        }

        .quote-element {
            padding-top: 1em;
        }

        .quote-person {
            margin-left: 50%;
            padding-top: 1em;
        }

        .card--wrapper {
            display: flex;
            flex-wrap: wrap;
            padding: 0;
        }

        .article--card {
            width: 48.821175%;
            padding: 0;
            margin-bottom: 2.35765%;
            background-color: #fff;
            overflow: hidden;
            border-radius: 7px;
            box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
        }

        .article--card-full-width {
            width: 100%;
            padding: 0;
            margin: 0 0.6% 2.35765% 0.6%;
            background-color: #fff;
            overflow: hidden;
            border-radius: 7px;
            box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
        }

        .article--img {
            position: relative;
        }

        .article--img img {
            height: 150px;
            object-fit: cover;
            width: 100%;
        }

        .article--info {
            padding: .5em 1em 1em 1em;
            line-height: 1em;
        }

        .article--icon {
            float: left;
            display: inline-block;
        }

        .article--icon img {
            width: 45px;
            height: auto;
            margin: 10px;
        }

        .article--header {
            float: left;
            display: inline-block;
            width: 470px;
            position: absolute;
            bottom: 0;
            margin-bottom: 0.25em;
        }

        .angled-bg--white-left {
            background-image: linear-gradient(-70deg, transparent 39.9%, rgba(255, 255, 255, .8) 40%);
            padding: 3em 0;
        }

        @media all and (max-width: 50em) {
            .angled-bg--white-left {
                background-image: linear-gradient(-70deg, rgba(255, 255, 255, .8) 49.9%, rgba(255, 255, 255, .8) 50%);
            }

        }

        @media all and (max-width: 71.875em) {
            .article--card {
                width: 48.821175%;
            }
            .article--card:nth-child(odd) {
                margin-left: 0;
            }
            .article--card:nth-child(even) {
                margin-right: 0;
            }
            .article--header {
                width: 350px;
            }
        }

        @media all and (max-width: 56.250em) {
            .article--card {
                width: 100%;
                margin-bottom: 1em;
            }
        }

        @media all and (max-width: 31.250em) {
            .article--header {
                width: 220px;
            }
        }

    </style>
    <!--Block 1-->
    <div class="video--loop-container">
        <div class="video--overlay" style="background:none;"></div>
        <video class="video--loop" preload="auto" autoplay loop muted playsinline>
            <source src="https://player.vimeo.com/external/267652677.hd.mp4?s=f6bd1afcfe2c8f517cdb239f9ab8b73220de7642&profile_id=174">
        </video>
        <div class="container__centered video--title-3">
            <div class="container__two-thirds">
                <div class="transparent-overlay--xp text--white">
                    <h1>
                        MEDITECH's Global Impact
                    </h1>
                    <h2>
                        We’re making a world of difference.
                    </h2>
                    <p>
                        Healthcare organizations around the world are using our modern, web-based EHR, <a href="https://ehr.meditech.com/ehr-solutions/expanse-success">Expanse</a>. Our innovative technology untethers doctors and nurses from their desktops and provides the data they need to treat the whole patient, no matter where they’re receiving care.
                    </p>
                </div>
            </div>
            <div class="container__one-third">&nbsp;</div>
        </div>
    </div>
    <!--End of Block 1-->

    <!--Block 2-->
    <div class="container bg--white">
        <div class="container__centered">
            <div class="center auto-margins">
                <h2>
                    Extending Our Reach
                </h2>
                <p>
                    MEDITECH has a long history of shaping healthcare around the globe. Offices in the <a href="https://ehr.meditech.com/global/meditech-uk-ireland">UK</a>, <a href="https://ehr.meditech.com/global/meditech-south-africa">South Africa</a>, <a href="https://ehr.meditech.com/global/meditech-asia-pacific">Australia</a>, and Singapore now extend our reach throughout the following regions:
                </p>
            </div>
            <div class="container no-pad">
                <div class="card--wrapper">
                    <section class="article--card">
                        <figure class="article--img">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--uk-ireland.jpg">
                            <div style="position: absolute; bottom: 0;">
                                <div class="article--icon">
                                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--uk-ireland.png" alt="UK & Ireland outline graphic">
                                </div>
                                <div class="article--header">
                                    <h4 class="text--white">
                                        MEDITECH in the United Kingdom &amp; Ireland:
                                    </h4>
                                </div>
                            </div>
                        </figure>
                        <div class="article--info">
                            <ul>
                                <li>
                                    22 customers in the UK
                                </li>
                                <li>
                                    3 customers in Ireland
                                </li>
                                <li>
                                    They represent numerous NHS trusts and public and private sector hospitals and clinics.
                                </li>
                            </ul>
                        </div>
                    </section>
                    <section class="article--card">
                        <figure class="article--img">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--middle-east-south-asia.jpg">
                            <div style="position: absolute; bottom: 0;">
                                <div class="article--icon">
                                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--middle-east-south-asia.png" alt="outline graphic">
                                </div>
                                <div class="article--header">
                                    <h4 class="text--white">
                                        MEDITECH in Asia Pacific:
                                    </h4>
                                </div>
                            </div>
                        </figure>
                        <div class="article--info">
                            <ul>
                                <li>
                                    72 private hospitals in Australia
                                </li>
                                <li>
                                    2 private hospitals and 4 labs in Singapore
                                </li>
                            </ul>
                        </div>
                    </section>
                    <section class="article--card-full-width">
                        <figure class="article--img">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--africa.jpg">
                            <div style="position: absolute; bottom: 0;">
                                <div class="article--icon">
                                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--africa.png" alt="Africa outline graphic">
                                </div>
                                <div class="article--header">
                                    <h4 class="text--white">
                                        MEDITECH in Africa:
                                    </h4>
                                </div>
                            </div>
                        </figure>
                        <div class="article--info">
                            <div class="container__one-half">
                                <ul>
                                    <li>100% public hospitals in Botswana</li>
                                    <li>1 University Hospital in Botswana</li>
                                    <li>100% public and private sector labs in Namibia</li>
                                </ul>
                            </div>
                            <div class="container__one-half">
                                <ul>
                                    <li>100% blood transfusion services Botswana</li>
                                    <li>5 hospitals in Nigeria</li>
                                    <li>1 Private Hospital in Kenya</li>
                                </ul>
                            </div>
                        </div>
                    </section>


                    <section class="article--card" style="display:none;">
                    </section>
                    <!--FIX CSS REMOVE THIS IF DESIGN IS APPROVED-->

                    <section class="article--card">
                        <figure class="article--img">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--asia-pacific.jpg">
                            <div style="position: absolute; bottom: 0;">
                                <div class="article--icon">
                                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--asia-pacific.png" alt="Asia Pacific Countries outline graphic">
                                </div>
                                <div class="article--header">
                                    <h4 class="text--white">
                                        MEDITECH in South Africa:
                                    </h4>
                                </div>
                            </div>
                        </figure>
                        <div class="article--info">
                            <ul>
                                <li>
                                    24 hospitals
                                </li>
                                <li>
                                    95% commercial labs (760 sites)
                                </li>
                            </ul>
                        </div>
                    </section>
                    <section class="article--card">
                        <figure class="article--img">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-card-background--south-africa.jpg">
                            <div style="position: absolute; bottom: 0;">
                                <div class="article--icon">
                                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--south-africa.png" alt="South Africa outline graphic">
                                </div>
                                <div class="article--header">
                                    <h4 class="text--white">
                                        MEDITECH in the Middle East &amp; South Asia:
                                    </h4>
                                </div>
                            </div>
                        </figure>
                        <div class="article--info">
                            <ul>
                                <li>
                                    1 private hospital in Dubai
                                </li>
                                <li>
                                    1 hospital in Kuwait
                                </li>
                                <li>
                                    1 private hospital in Pakistan
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>
    <!--End of Block 2-->

    <!--Block 3-->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/hikers-looking-over-lake-in-canadian-wilderness.jpg);">
        <div class="container__centered">
            <div class="container__one-half transparent-overlay--xp text--white">
                <h2>Connecting Canada with MEDITECH’s EHR</h2>
                <p>Across Canada, MEDITECH customers have a rich history of leading the way in delivering the highest quality healthcare. Find out how we’re connecting clinicians, hospitals, and health authorities with more efficient EHR tools and integration that spans all settings.</p>
                <div class="btn-holder--content__callout" style="margin-bottom:1.5em;">

                    <div style="text-align:center;">
                        <?php $button_2_code = '9dcbc4c4-bfcb-41ab-a4a3-4bfa592ed9b2'; ?>
                        <div class="button--hubspot">
                            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_2_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_2_code; ?>" id="hs-cta-<?php print $button_2_code; ?>">
                                    <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_2_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_2_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_2_code; ?>.png" alt="Explore MEDITECH Expanse" /></a></span>
                            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                            <script type="text/javascript">
                                hbspt.cta.load(2897117, '<?php print $button_2_code; ?>', {});

                            </script>
                            </span>
                            <!-- end HubSpot Call-to-Action Code -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--End of Block 3-->

    <!--Block 4-->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-blurred-humber-river-hospital-command-centre--background.jpg);">
        <article class="container__centered text--white center auto-margins">
            <figure>
                <!--                <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/quote-headshot--Barbara-Collins.png" style="width: 150px;" alt="Quote bubble">-->
                <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.png';this.onerror=null;" style="width: 150px;" alt="Quote bubble">
            </figure>
            <div class="text--large italic" style="font-size:1.4em; padding-top: .75em;">
                <p>“As a longtime customer, we've experienced the many benefits of utilizing MEDITECH's EHR. These benefits don't only cover patient care but they span across the whole organization. We're fortunate to have wisely invested in a solution that's been sustainable for the last two decades and that we foresee will continue to be well into the future.”</p>
            </div>
            <p class="no-margin--bottom text--large bold text--meditech-green">Barbara Collins, CEO</p>
            <p>Humber River Hospital</p>
        </article>
    </div>
    <!--End of Block 4-->

    <!--Block 5-->
    <div class="container bg--white">
        <div class="container__centered">
            <div class="container no-pad auto-margins">
                <h2 class="center">Uniting Care in the United Kingdom and Ireland</h2>
                <div class="container__two-thirds">
                    <p>Leading the charge to safer, more holistic care with MEDITECH’s integrated solutions also leads to achieving firsts from NHS England and the analytics unit of HIMSS Europe.</p>
                </div>
                <div class="container__one-third">
                    <a href="<?php print $url; ?>/international/meditech-uk-ireland" class="btn--orange">Learn More About MEDITECH UK &amp; Ireland</a>
                </div>
            </div>
            <div class="container__centered">
                <div class="container no-pad">
                    <div class="gl-container bg--emerald" style="background-color: inherit;">
                        <div class="container__one-half bg--dark-blue">
                            <p>In the UK, <a href="https://alderhey.nhs.uk/">Alder Hey Children’s</a> and <a href="https://www.stsft.nhs.uk/">South Tyneside and Sunderland</a> were part of the first set of <a href="https://ehr.meditech.com/news/meditech-s-global-digital-exemplar-customers-leading-uk-s-healthcare-revolution">Global Digital Exemplars announced</a> by the NHS.</p>
                        </div>
                        <div class="container__one-half bg--emerald">
                            <p><a href="https://www.galwayclinic.com/">Galway Clinic</a> (Galway City, Ireland) was the first hospital in Ireland to achieve HIMSS Stage 6 designation, as well as the <a href="https://ehr.meditech.com/news/leading-himss-stage-6-hospital-first-to-go-live-with-meditech-expanse-in-ireland-and-uk">first to launch Expanse</a> throughout the UK and Ireland.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 5-->

    <!--Block 6-->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Silhouette-of-two-giraffes-at-an-african-sunset--background.jpg);">
        <div class="container__centered">
            <div class="container text--white transparent-overlay--xp">
                <div class="container__one-half">
                    <h2>MEDITECH South Africa</h2>
                    <p>For nearly four decades, <a href="https://ehr.meditech.com/global/meditech-south-africa">MEDITECH South Africa</a> has provided integrated software solutions to healthcare organisations across the globe.</p>
                    <p>We’re proud to be partnering with Aga Khan University, an international institution of distinction providing health services to millions of people in the developing world. The Expanse deployment will create the first integrated EHR in East Africa, with a subsequent deployment in Pakistan.</p>
                    <div class="center">
                        <a href="https://ehr.meditech.com/international/meditech-south-africa" class="btn--orange">Learn More About MEDITECH South Africa</a>
                    </div>
                </div>
                <div class="container__one-half flex-order--reverse" style="margin-top: 2.5em;">
                    <div class="hero-image-container">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Aga-Khan-University-Signing-Group-Shot.jpg" alt="Aga-Khan-University-Signing-Group-Photo" title="" style="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 6-->


    <!--Block 7-->
    <div class="content__callout">
        <div class="content__callout__media">

            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="174224144">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay-Botswana-The-Power-and-Impact-of-Serving-a-Global-Healthcare-Mission.jpg" alt="Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/174224144"></a>
                    <div class="video__container">
                    </div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>
                        One Patient, One Record in Botswana
                    </h2>
                    <p>
                        From sophisticated urban hospitals in the UK and Canada to remote desert health posts in the heart of Africa, we’re working with local communities and governments to improve people’s health no matter where they live, and no matter the level of IT connectivity.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 7-->

    <!--Block 8-->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-asia-pacific-city-with-connecting-lines-overlaid--background.jpg);">
        <div class="container__centered">
            <div class="container no-pad">
                <h2 class="center text--white">MEDITECH Asia Pacific</h2>
                <div class="gl-container bg--white" style="background-color: inherit;">
                    <div class="container__one-half bg--white">
                        <p>Together, MEDITECH Australia and MEDITECH Singapore are delivering effective and sustainable healthcare solutions to the Asia Pacific region. Our world-class partnerships include <a href="https://www.ramsayhealth.com/Our-Businesses/Ramsay-Australia">Ramsay Health Care</a>, <a href="https://ehr.meditech.com/news/chris-obrien-lifehouse-signs-for-meditech-expanse">Chris O’Brien Lifehouse</a>, and <a href="https://ehr.meditech.com/news/farrer-park-hospital-celebrated-for-using-innovation-and-technology-to-improve-patient">Farrer Park Hospital</a>.</p>
                        <div class="center">
                            <a href="https://ehr.meditech.com/international/meditech-asia-pacific" class="btn--orange" style="margin-top:0.5em;">Learn More About MEDITECH Asia Pacific</a>
                        </div>
                    </div>
                    <div class="container__one-half bg--meditech-green">
                        <p>"This hospital was planned to be technologically relevant for the next 20 years, because the future of healthcare is going to be digitally-focused and technology-based."</p>
                        <div class="quote-element">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/quote_element_right--white.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote_element_right--white.png';this.onerror=null;" alt="quote graphic">
                        </div>
                        <div class="quote-person">
                            <p>Dr. Timothy Low<br> CEO Farrer Park Hospital</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 8-->

    <!--Block 9-->
    <div class="container background--cover no-pad" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/caribbean-beach.jpg);">
        <div class="angled-bg--white-left" style="padding:5em 0;">
            <div class="container__centered">
                <h2>
                    Riding Waves of Change in the Caribbean
                </h2>
                <div class="container__one-half">
                    <p>
                        As digital health records gain momentum in the Caribbean, MEDITECH is expanding its presence with customers throughout the US Virgin Islands, Puerto Rico, the British Virgin Islands, and the Bahamas. We’re connecting providers across these island populations to achieve longitudinal health records for all citizens.
                    </p>

                    <ul>
                        <li>
                            Since implementing MEDITECH, Wilma N. Vázquez Medical Center (Vega Baja, PR) has documented a 20% increase in their cash flow and 30% reduction in ED wait times.
                        </li>
                        <li>
                            The British Virgin Islands Health Authority will soon connect patients and care providers nationwide and across the islands using MEDITECH Expanse. Wherever patients receive care — Peeble's Hospital, outpatient clinics, urgent care centers —&nbsp;their records will follow.
                        </li>
                        <li>
                            Patients at Doctors Hospital (Nassau, Bahamas) rate the hospital above the 90th percentile. Their MEDITECH solution helps them keep their patients engaged and healthier over the long term.
                        </li>
                    </ul>

                </div>
                <div class="container__one-half">&nbsp;</div>
            </div>
        </div>
    </div>
    <!--End of Block 9-->

    <!--Block 10-->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/map-of-globe-with-lines-connecting-cities.jpg);">
        <div class="container__centered">
            <h2 class="text--white center">
                Our Global Market Presence
            </h2>
            <div class="container__centered list-style--none">
                <div class="container__one-fourth text--white">
                    <ul>
                        <li>Australia</li>
                        <li>Bahamas</li>
                        <li>British Virgin Islands</li>
                        <li>Botswana</li>
                        <li>Canada</li>
                        <li>Ghana</li>
                        <li>Honduras</li>
                    </ul>

                </div>
                <div class="container__one-fourth text--white">
                    <ul>
                        <li>Ireland</li>
                        <li>Kenya</li>
                        <li>Kuwait</li>
                        <li>Lesotho</li>
                        <li>Mozambique</li>
                        <li>Namibia</li>
                    </ul>

                </div>
                <div class="container__one-fourth text--white">
                    <ul>
                        <li>Nigeria</li>
                        <li>Pakistan</li>
                        <li>Puerto Rico*</li>
                        <li>Singapore</li>
                        <li>South Africa</li>
                        <li>Swaziland</li>
                        <li>Uganda</li>
                    </ul>

                </div>
                <div class="container__one-fourth text--white">
                    <ul>
                        <li>United Arab Emirates</li>
                        <li>United Kingdom</li>
                        <li>United States</li>
                        <li>Virgin Islands*</li>
                        <li>Zambia</li>
                        <li>Zimbabwe</li>
                    </ul>

                </div>
            </div>
            <div class="container__centered no-pad text--white">
                <p>
                    * United States territory
                </p>
            </div>
        </div>
    </div>
    <!--End of Block 10-->

    <!--Block 11 - CTA-->
    <!--     style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);"-->
    <div class="container bg--white">
        <div class="container__centered" style="text-align: center;">
            <h2>
                <?php print render($content['field_header_10']); ?>
            </h2>

            <?php 
            // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
            if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
              $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
            ?>
            <div class="button--hubspot">
                <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>">
                        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png" alt="button" /></a></span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                    hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});

                </script>
                </span>
                <!-- end HubSpot Call-to-Action Code -->
            </div>
            <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
            <?php } ?>


        </div>
    </div>
    <!--End of Block 11-->

</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- end node--new-international.tpl.php template -->
