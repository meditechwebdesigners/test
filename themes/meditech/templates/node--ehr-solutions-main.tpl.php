<?php // This template is set up to control the display of the 'EHR SOLUTIONS MAIN' content type 
$url = $GLOBALS['base_url']; // grabs the site url

// check first to see if field collection exists...
$field_collection_1 = field_get_items('node', $node, 'field_fc_head_ltext_ltext_unl_1');
$field_collection_2 = field_get_items('node', $node, 'field_fc_3_ltext_boxes');
  
if( $field_collection_1 != '' && !empty($field_collection_1) ){
  $sections = multi_field_collection_data($node, 'field_fc_head_ltext_ltext_unl_1', 7); 
} 
if( $field_collection_2 != '' && !empty($field_collection_2) ){
  $text_boxes = field_collection_data($node, 'field_fc_3_ltext_boxes');
} 
?>
<!-- start node--ehr-solutions-main.tpl.php template -->
<style>
  h2.subtitle { padding-right: 2em; }
  
  @media all and (max-width: 800px){
    .center-mobile { text-align: center; }
    h2.subtitle { padding-right: 0; }
    .image-width-limit { width: 300px; }
  }
</style>

<div class="container ehr-solutions-main-gae" style="background:url(<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/crossroads-sunset-horizon--white-fade-out-bottom.jpg) no-repeat; background-position: top center;">

 
  <!-- Top Section ***************************************************************** -->
  <div class="container__centered" style="margin-top: 2em;">
   
    <div class="container__two-thirds center-mobile">
      <h1 style="margin-bottom:.25em;"><?php print $title; ?></h1>
      <h2 class="subtitle"><?php print render($content['field_sub_header_1']); ?></h2>
    </div>
    
    <div class="container__one-third">
      <div class="center">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/expanse-logo.png" alt="MEDITECH Expanse Logo" class="image-width-limit" style="margin-top: 1em;">
      </div>
      <div class="btn-holder--content__callout" style="margin-bottom:1.5em;">
        <div style="text-align:center;">
          <?php $button_1_code = '33997285-e326-4093-ac3e-d72779e0a5f1'; ?>
          <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code -->
            <span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>">
                <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png" alt="Explore MEDITECH Expanse" /></a></span>
              <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
              <script type="text/javascript">
                hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});
              </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
          </div>
        </div>
      </div>
    </div>
    
  </div>
  <!-- END Top Section -->
  
  
  <!-- Mid Section ***************************************************************** -->
  <div class="container__centered ehr-block" style="margin-top: 4em;">
   
    <div class="container__one-half ehr-block-spacing">
      <div id="interoperability">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/orbit-shot-of-earth-with-lights-and-connective-grid.jpg" alt="family eating at backyard picnic" style="width:100%;">
      </div>
      <h2><?php print $sections[0]->field_text_1['und'][0]['value']; ?></h2>
      <h3>Learn the Benefits</h3>
      <div class="squish-list">
        <?php print $sections[0]->field_long_text_1['und'][0]['value']; ?>
      </div>
      <h3>See the Results</h3>
      <div class="squish-list">
        <?php print $sections[0]->field_long_text_2['und'][0]['value']; ?>
      </div>
    </div>
  
    <div class="container__one-half">
      <div id="technologies-transform-care">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/nurse-hands-on-mobile-device.jpg" alt="family eating at backyard picnic" style="width:100%;">
      </div>
      <h2><?php print $sections[1]->field_text_1['und'][0]['value']; ?></h2>
      <h3>Learn the Benefits</h3>
      <div class="squish-list">
        <?php print $sections[1]->field_long_text_1['und'][0]['value']; ?>
      </div>
      <h3>See the Results</h3>
      <div class="squish-list">
        <?php print $sections[1]->field_long_text_2['und'][0]['value']; ?>
      </div>
    </div>
    
  </div>
  
  
  <div class="container__centered ehr-block hide-on-tablets">
    <div class="container__one-half"><hr/></div>
    <div class="container__one-half"><hr/></div>
  </div>
  
  
  <div class="container__centered ehr-block" style="margin-top: 2em;">
   
    <div class="container__one-half ehr-block-spacing">
      <div id="population-health">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/family-backyard-picnic.jpg" alt="family eating at backyard picnic" style="width:100%;">
      </div>
      <h2><?php print $sections[2]->field_text_1['und'][0]['value']; ?></h2>
      <h3>Learn the Benefits</h3>
      <div class="squish-list">
        <?php print $sections[2]->field_long_text_1['und'][0]['value']; ?>
      </div>
      <h3>See the Results</h3>
      <div class="squish-list">
        <?php print $sections[2]->field_long_text_2['und'][0]['value']; ?>
      </div>
    </div>
  
    <div class="container__one-half">
      <div id="physician-efficiency">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/male-physician-showing-tablet-to-female-patient.jpg" alt="family eating at backyard picnic" style="width:100%;">
      </div>
      <h2><?php print $sections[3]->field_text_1['und'][0]['value']; ?></h2>
      <h3>Learn the Benefits</h3>
      <div class="squish-list">
        <?php print $sections[3]->field_long_text_1['und'][0]['value']; ?>
      </div>
      <h3>See the Results</h3>
      <div class="squish-list">
        <?php print $sections[3]->field_long_text_2['und'][0]['value']; ?>
      </div>
    </div>
    
  </div>
  
  
  <div class="container__centered ehr-block hide-on-tablets">
    <div class="container__one-half"><hr/></div>
    <div class="container__one-half"><hr/></div>
  </div>
  
  
  <div class="container__centered ehr-block" style="margin-top: 2em;">
   
    <div class="container__one-half ehr-block-spacing">
      <div id="quality-outcomes">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/physician-and-nurse-assisting-patient-outside-in-wheelchair.jpg" alt="family eating at backyard picnic" style="width:100%;">
      </div>
      <h2><?php print $sections[4]->field_text_1['und'][0]['value']; ?></h2>
      <h3>Learn the Benefits</h3>
      <div class="squish-list">
        <?php print $sections[4]->field_long_text_1['und'][0]['value']; ?>
      </div>
      <h3>See the Results</h3>
      <div class="squish-list">
        <?php print $sections[4]->field_long_text_2['und'][0]['value']; ?>
      </div>
    </div>
  
    <div class="container__one-half">
      <div id="nurse-specialty-care">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/female-nurse-smiling-hugging-female-patient.jpg" alt="family eating at backyard picnic" style="width:100%;">
      </div>
      <h2><?php print $sections[5]->field_text_1['und'][0]['value']; ?></h2>
      <h3>Learn the Benefits</h3>
      <div class="squish-list">
        <?php print $sections[5]->field_long_text_1['und'][0]['value']; ?>
      </div>
      <h3>See the Results</h3>
      <div class="squish-list">
        <?php print $sections[5]->field_long_text_2['und'][0]['value']; ?>
      </div>
    </div>
    
  </div>
  
  
  <div class="container__centered ehr-block hide-on-tablets" id="fiscal-responsibility"><?php // id is located here because there is no image above it like other sections ?>
    <div class="container__one-half"><hr/></div>
    <div class="container__one-half"><hr/></div>
  </div>
  

  <div class="container__centered ehr-block" style="margin-top: 2em;">
   
    <div class="container__one-half ehr-block-spacing">
      <div class="center">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/jar-of-coins-with-growing-plant--small.jpg" alt="family eating at backyard picnic" style="width:100%;">
      </div>
    </div>
  
    <div class="container__one-half">
      <h2 style="margin-top:0;"><?php print $sections[6]->field_text_1['und'][0]['value']; ?></h2>
      <h3>Learn the Benefits</h3>
      <div class="squish-list">
        <?php print $sections[6]->field_long_text_1['und'][0]['value']; ?>
      </div>
      <h3>See the Results</h3>
      <div class="squish-list">
        <?php print $sections[6]->field_long_text_2['und'][0]['value']; ?>
      </div>
    </div>
    
  </div>
  
  
  <div class="container__centered ehr-block">
    <hr/>
  </div>
  <!-- END Mid Section -->
  
  
  <!-- Bottom Section ***************************************************************** -->
  <div class="container__centered ehr-block" style="margin-top: 2em;">
   
    <h2 class="center" style="margin-bottom: 1.5em;"><?php print render($content['field_text_1']); ?></h2>
    
    <div class="container__one-third squish-list">
      <?php print $text_boxes->field_long_text_1['und'][0]['value']; ?>
    </div>
    <div class="container__one-third squish-list">
      <?php print $text_boxes->field_long_text_2['und'][0]['value']; ?>
    </div>
    <div class="container__one-third squish-list">
      <?php print $text_boxes->field_long_text_3['und'][0]['value']; ?>
    </div>
    
  </div>
  <!-- END Bottom Section -->
  
  
</div>
<!-- end node--ehr-solutions-main.tpl.php template -->