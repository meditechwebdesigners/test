<!-- start views-view-fields--case-studies--block.tpl.php template -->
<?php // This template is for each row of the Views block: CASE STUDIES ....................... ?>
<div>
 
  <h3 style="margin-bottom:0;"><a class="cta_link_case_studies_page_gae" href="<?php print $fields['field_text_1']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
  <p style="font-size:.8em; margin:0 0 .6em 0; font-style:italic;">Published on <?php print $fields['published_at']->content; ?></p>
  <div class="inline__text__wrapper">
    <?php print $fields['field_summary']->content; ?>
  </div>
  <?php // add Edit Video link...
    if( user_is_logged_in() ){ 
      print '<div style="display:block; text-align:right;"><span style="font-size:12px;">'; print l( t('Edit This'),'node/'. $fields['nid']->content .'/edit' ); print "</span></div>"; 
    } 
  ?> 

  <hr>

</div>
<!-- end views-view-fields--case-studies--block.tpl.php template -->