<!-- start views-view-fields--news-main-page-us--block.tpl.php template -->

<?php 
// This template is for each row of the Views block: NEWS-MAIN-PAGE-US BLOCK...

// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);
// get node URL from node ID...
$nodeURL = url('node/'. $nid); 
$websiteURL = $GLOBALS['base_url']; // grabs the site url

// find out if article is set as "sticky"...
if($node->sticky == 1){
  $sticky_class = 'article--sticky'; 
  $sticky_border = 'article--sticky-border'; 
  $sticky_icon = '<i class="fas fa-thumbtack text--white"></i>';
}
else{
  $sticky_class = '';
  $sticky_border = '';
  $sticky_icon = '';
}
?>

<section class="article--card <?php print $sticky_border; ?>">

  <figure class="article--img">
    <?php 
    // if video exists for node, then proceed to render Video DIV, otherwise look for main image...
    // get value from field_video to pass to View (if video exists)...
    $video = $fields['field_video']->content;

    if(!empty($video)){ // if the page has a video...
      $node_video = node_load($video);
      $video_image = field_get_items('node', $node_video, 'field_video_overlay_image');
      $video_image_filename = $video_image[0]['filename'];
      print '<img src="'.$websiteURL.'/sites/default/files/images/video/'.$video_image_filename.'">';
    }
    else{
      $article_image = field_get_items('node', $node, 'field_news_article_main_image');
      $article_image_filename = $article_image[0]['filename'];
      $article_image_alt = $article_image[0]['field_file_image_alt_text']['und'][0]['safe_value'];
      print '<img src="'.$websiteURL.'/sites/default/files/images/news/'.$article_image_filename.'" alt="'.$article_image_alt.'">';
    }
    ?>
    <div class="<?php print $sticky_class; ?>"><?php print $sticky_icon; ?></div>
    <div class="article--date text--small"><time datetime="<?php print date('Y-m-d', $node->published_at); ?>" itemprop="datePublished"><?php print date("F j, Y", $node->published_at); ?></time></div>
  </figure>
  <div class="article--info">
    <h3 class="header-four"><a class="news_main_link_gae" href="<?php print $nodeURL; ?>"><?php print $fields['title']->content; ?></a></h3>
    <div class="line-clamp"><?php
    $summary = field_view_field('node', $node, 'field_summary');
    print render($summary); 
    ?></div>
    <ul class="news__article__filters tag_link_news_main_gae text--small">
      <?php generate_news_tag_links($node); ?>
    </ul>
  </div>
</section>
<!-- end views-view-fields--news-main-page-us--block.tpl.php template -->