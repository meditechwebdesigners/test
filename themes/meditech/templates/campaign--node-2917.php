<!-- START campaign--node-2917.php -->
<?php // This template is set up to test new Events homepage designs

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    .btn--orange-gradient {
        background-image: linear-gradient(45deg, #fe9418 0%, #f5634b 75%);
        border-radius: 25px;
        color: #fff;
        font-size: 16px;
        font-weight: 700;
        display: inline-block;
        text-align: center;
        padding: 0em 1.5em;
        line-height: 2.8;
        flex: 1 1 auto;
        transition: 0.5s;
        background-size: 200% auto;
        margin-bottom: 1.5em;
    }

    .btn--orange-gradient:hover {
        color: #fff;
        background-position: right center;
    }

    .header__button {
        float: right;
    }


    .hero-image-container {
        position: relative;
    }

    .hero-image-container img {
        border-radius: 0.5em;
        margin-top: 2em;
        max-width: 100%;
        z-index: 1;

    }

    .background-rectangle--right {
        content: "";
        position: absolute;
        border-radius: 0.5em;
        height: 108%;
        width: 95%;
        z-index: -1;
        background-color: #ffffbf;
        top: 0;
        bottom: 0;
        left: 5em;
        right: 0;
    }

    /*
    .background-rectangle::after { 
        content: "";
        position: absolute;
        border-radius: 0.5em;
        height: 100%;
        width: 90%;
        z-index: -1;
        background-color: #fbfdd7;
        top: 0;
        bottom: 0;
        left: 5em;
        right: 0;
    }
*/
    /*Start of Flexbot stlying*/

    .cards {
        display: flex;
        flex-wrap: wrap;
        list-style: none;
        margin-bottom: 2em;
        padding: 0;
    }

    .cards__item {
        width: 33.3333%;
        display: flex;
        padding: 1.5%;
    }

    .cards li {
        margin-bottom: 0;
    }

    .card {
        background-color: #fff;
        border-radius: 6px;
        box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
        display: flex;
        flex-direction: column;
        overflow: hidden;
    }

    @media (max-width: 50em) {
        .cards__item {
            width: 50%;
        }
    }

    @media all and (max-width: 31.250em) {
        .cards__item {
            width: 100%;
        }
    }

    /*End of Flexbot stlying*/

    /*Start of Flexbot & Masonry stlying*/
    .article--wrap {
        display: flex;
        flex-flow: column wrap;
        align-content: space-between;
        height: 2275px;
        align-items: center;
        /* Wrapping container needs a fixed height, and it needs to be taller than the tallest column. */
    }

    .article--card {
        width: 31.33333%;
        background: #fff;
        border-radius: 6px;
        box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
        position: relative;
        margin-bottom: 3%;
        box-sizing: border-box;
    }

    .article--info {
        padding: .5em 1em 1em 1em;
    }

    .article--img {
        position: relative;
    }

    .article--img img {
        border-radius: 6px 6px 0 0;
        max-height: 250px;
    }

    .article--date {
        padding: .5em 1em;
        background-color: #e5f8f0;
        position: absolute;
        border-radius: 0 6px 0 0;
        bottom: 9px;
    }

    .text--small a {
        border-bottom: none;
    }

    /* Re-order items into 3 rows */
    .article--card:nth-child(3n+1) {
        order: 1;
    }

    .article--card:nth-child(3n+2) {
        order: 2;
    }

    .article--card:nth-child(3n) {
        order: 3;
    }

    /* Force new columns */
    .article--wrap::before,
    .article--wrap::after {
        content: "";
        flex-basis: 100%;
        width: 0;
        order: 2;
    }

    .btn--orange-gradient {
        background-image: linear-gradient(45deg, #fe9418 0%, #f5634b 75%);
        border-radius: 25px;
        color: #fff;
        font-size: 16px;
        font-weight: 700;
        display: inline-block;
        text-align: center;
        /*    white-space: nowrap;*/
        padding: 0em 1.5em;
        line-height: 2.8;
        flex: 1 1 auto;
        transition: 0.5s;
        background-size: 200% auto;
        /*    box-shadow: 0 0 20px #eee;*/
    }

    .btn--orange-gradient:hover {
        color: #fff;
        background-position: right center;
    }

    @media all and (max-width: 50em) {
        .article--card {
            width: 48.5%;
        }

        .article--wrap {
            height: 2500px;
        }

        .article--date {
            bottom: 7px;
        }

        /* Re-order items into 2 rows */
        .article--card:nth-child(2n+1) {
            order: 1;
        }

        .article--card:nth-child(2n) {
            order: 2;
        }
    }

    @media all and (max-width: 31.250em) {
        .article--card {
            width: 100%;
        }

        .article--wrap {
            height: 3500px;
        }

        /* Re-order items into 2 rows */
        .article--card:nth-child(1n) {
            order: 1;
        }
    }

    /*End of Flexbot & Masonry stlying*/

</style>


<!-- Hero -->
<div class="container no-pad--bottom">
    <div class="container__centered" style="padding-bottom:2em;">
        <div class="container__one-half" style="padding-top: 2em;">
            <h2 class="header-one">
                2019 Physician and CIO Forum
            </h2>
            <h3 class="header-two">
                Foxborough, MA
                <br>
                September 18th - 19th
            </h3>
            <p>
                Join MEDITECH and home care leaders at the 2019 National
                Association for Home Care &amp; Hospice Conference and Expo.
            </p>
            <div class="center" tabindex="0"><a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Register Today!">Register Today!</a>
            </div>
        </div>
        <div class="container__one-half">
            <div class="hero-image-container">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/event-test-1.jpg" alt="Events Hero Image">
                <div class="background-rectangle--right"></div>
            </div>
        </div>

    </div>
</div>

<!-- Start Events Block 1 - Same Height Flexbox -->
<div class="container  no-pad--bottom">
    <div class="container__centered">
        <h2>Upcoming Events &amp; Webinars</h2>
        <ul class="cards">
            <!--Card 3-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-Nursing--webinar.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">Gain Mobility with MEDITECH's Expanse Point of Care Solution</a></h3>
                        <h4 class="header-five">August 12th, 2:00 PM (EST)
                            <br>30 Minutes</h4>
                        <p>Learn how Expanse Point of Care puts medication administration, documentation and chart review in the palm of your hand.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 4-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-webinar--labor-and-delivery--drupal.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">Introducing MEDITECH's Labor and Delivery Solution</a></h3>
                        <h4 class="header-five">August 21st, 2:00 PM (EST)
                            <br>60 Minutes</h4>
                        <p>Find out how Expanse Labor and Delivery is designed for nurses to take on the unique responsibilities of caring for little ones, as well as their mothers.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 11-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/trade-shows--ambulatory--article_0.jpg?itok=wc3IbglO&timestamp=1564599278">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">FDB User Group Conference</a></h3>
                        <h4 class="header-five">October 1st to October 2nd
                            <br>Indianapolis, IN</h4>
                        <p>Attend the First Databank User Group Conference and explore how to increase efficiencies while you save time and resources.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 2-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/trade-shows--executives--article_4.jpg?itok=Cu3Cke-o&timestamp=1566488786">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">2019 Ontario Health IT Conference</a></h3>
                        <h4 class="header-five">October 3rd
                            <br>Etobicoke, ON</h4>
                        <p>Mark your calendars for the Ontario Health
                            IT Conference. Visit MEDITECH in booth #44
                            during the event.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 8-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/rev_cycle_summit_hero.jpg?itok=LsHvDPfJ&timestamp=1560196215">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">2019 Revenue Cycle Summit</a></h3>
                        <h4 class="header-five">October 8th to October 9th
                            <br>Foxborough, MA</h4>
                        <p>View the agenda for our 2019 Revenue Cycle Summit.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 7-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-Revenue-Cycle--webinar.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">Expanse Revenue Cycle: A Proactive Approach to Self Pay Collections</a></h3>
                        <h4 class="header-five">October 10th, 2:00 PM (EST)
                            <br>30 Minutes</h4>
                        <p>Learn how MEDITECH Expanse Revenue Cycle optimizes self-pay collections and responds to increased patient liability and consumerism.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 1-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/rev_cycle_summit_hero.jpg?itok=LsHvDPfJ&timestamp=1560196215">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">2019 National Association for
                                Home Care &amp; Hospice Conference
                                and Expo</a></h3>
                        <h4 class="header-five">October 13th to October 15th
                            <br>Seattle, WA</h4>
                        <p>Join MEDITECH and home care leaders at
                            the 2019 National Association for Home
                            Care &amp; Hospice Conference and Expo.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 5-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/trade-shows--ambulatory--article_0.jpg?itok=wc3IbglO&timestamp=1564599278">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">MGMA19 | The Annual Conference</a></h3>
                        <h4 class="header-five">October 13th to October 16th
                            <br>New Orleans, LA</h4>
                        <p>We’re bringing Expanse to New Orleans and MGMA19. Visit booth #1522 to explore Expanse and chat with MEDITECH!</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 12-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/trade-shows--home-care--article_0.jpg?itok=PLZcmUeH&timestamp=1564598907">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">
                                2019 National Association for Home Care &amp; Hospice Conference and Expo</a></h3>
                        <h4 class="header-five">October 13th to October 15th
                            <br>Seattle, WA</h4>
                        <p>Join MEDITECH and home care leaders at the 2019 National Association for Home Care &amp; Hospice Conference and Expo.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 9-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-Ambulatory--webinar.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">Practice Makes Perfect with Expanse Ambulatory</a></h3>
                        <h4 class="header-five">October 17th, 2:00 PM (EST)
                            <br>60 Minutes</h4>
                        <p>During this webinar, you'll learn how Expanse Ambulatory Practice Management drives revenue while improving the patient experience.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 6-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/Halton_Expanse_Event--Drupal.jpg?itok=cjE6x6C4&timestamp=1564082881">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">MEDITECH Expanse: A New Vision for Healthcare in Canada</a></h3>
                        <h4 class="header-five">October 29th
                            <br>Oakville, ON</h4>
                        <p>Join us for our leadership event, MEDITECH Expanse: A New Vision for Healthcare in Canada.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 10-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-Ambulatory--webinar.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">A New View of Ambulatory Care with MEDITECH Expanse</a></h3>
                        <h4 class="header-five">October 29th, 2:00 PM (EST)
                            <br>60 Minutes</h4>
                        <p>See a high level front-to-back walkthrough of MEDITECH’s modern, mobile, Ambulatory EHR.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
        </ul>
        <div class="center">
            <a href="#" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Load More</a>
        </div>
    </div>
</div>


<div class="container  no-pad--bottom">
    <div class="container__centered">
        <h2>Upcoming Events</h2>
        <ul class="cards">
            <!--Card 11-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/trade-shows--ambulatory--article_0.jpg?itok=wc3IbglO&timestamp=1564599278">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">FDB User Group Conference</a></h3>
                        <h4 class="header-five">October 1st to October 2nd
                            <br>Indianapolis, IN</h4>
                        <p>Attend the First Databank User Group Conference and explore how to increase efficiencies while you save time and resources.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 2-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/trade-shows--executives--article_4.jpg?itok=Cu3Cke-o&timestamp=1566488786">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">2019 Ontario Health IT Conference</a></h3>
                        <h4 class="header-five">October 3rd
                            <br>Etobicoke, ON</h4>
                        <p>Mark your calendars for the Ontario Health
                            IT Conference. Visit MEDITECH in booth #44
                            during the event.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 8-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/rev_cycle_summit_hero.jpg?itok=LsHvDPfJ&timestamp=1560196215">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">2019 Revenue Cycle Summit</a></h3>
                        <h4 class="header-five">October 8th to October 9th
                            <br>Foxborough, MA</h4>
                        <p>View the agenda for our 2019 Revenue Cycle Summit.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 1-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/rev_cycle_summit_hero.jpg?itok=LsHvDPfJ&timestamp=1560196215">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">2019 National Association for
                                Home Care &amp; Hospice Conference
                                and Expo</a></h3>
                        <h4 class="header-five">October 13th to October 15th
                            <br>Seattle, WA</h4>
                        <p>Join MEDITECH and home care leaders at
                            the 2019 National Association for Home
                            Care &amp; Hospice Conference and Expo.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 5-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/trade-shows--ambulatory--article_0.jpg?itok=wc3IbglO&timestamp=1564599278">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">MGMA19 | The Annual Conference</a></h3>
                        <h4 class="header-five">October 13th to October 16th
                            <br>New Orleans, LA</h4>
                        <p>We’re bringing Expanse to New Orleans and MGMA19. Visit booth #1522 to explore Expanse and chat with MEDITECH!</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 12-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/trade-shows--home-care--article_0.jpg?itok=PLZcmUeH&timestamp=1564598907">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">
                                2019 National Association for Home Care &amp; Hospice Conference and Expo</a></h3>
                        <h4 class="header-five">October 13th to October 15th
                            <br>Seattle, WA</h4>
                        <p>Join MEDITECH and home care leaders at the 2019 National Association for Home Care &amp; Hospice Conference and Expo.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 6-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/default/files/styles/square-img-cropper-breakpoints_theme_meditech_extra-large_2x/public/images/events/Halton_Expanse_Event--Drupal.jpg?itok=cjE6x6C4&timestamp=1564082881">
                        <div class="article--date text--small">Event</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">MEDITECH Expanse: A New Vision for Healthcare in Canada</a></h3>
                        <h4 class="header-five">October 29th
                            <br>Oakville, ON</h4>
                        <p>Join us for our leadership event, MEDITECH Expanse: A New Vision for Healthcare in Canada.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<!-- Start Events Block 2 - Same Height Flexbox -->
<div class="container  no-pad--bottom">
    <div class="container__centered">
        <h2>Upcoming Webinars</h2>
        <ul class="cards">
            <!--Card 3-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-Nursing--webinar.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">Gain Mobility with MEDITECH's Expanse Point of Care Solution</a></h3>
                        <h4 class="header-five">August 12th, 2:00 PM (EST)
                            <br>30 Minutes</h4>
                        <p>Learn how Expanse Point of Care puts medication administration, documentation and chart review in the palm of your hand.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 4-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-webinar--labor-and-delivery--drupal.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">Introducing MEDITECH's Labor and Delivery Solution</a></h3>
                        <h4 class="header-five">August 21st, 2:00 PM (EST)
                            <br>60 Minutes</h4>
                        <p>Find out how Expanse Labor and Delivery is designed for nurses to take on the unique responsibilities of caring for little ones, as well as their mothers.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 7-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-Revenue-Cycle--webinar.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">Expanse Revenue Cycle: A Proactive Approach to Self Pay Collections</a></h3>
                        <h4 class="header-five">October 10th, 2:00 PM (EST)
                            <br>30 Minutes</h4>
                        <p>Learn how MEDITECH Expanse Revenue Cycle optimizes self-pay collections and responds to increased patient liability and consumerism.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 9-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-Ambulatory--webinar.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">Practice Makes Perfect with Expanse Ambulatory</a></h3>
                        <h4 class="header-five">October 17th, 2:00 PM (EST)
                            <br>60 Minutes</h4>
                        <p>During this webinar, you'll learn how Expanse Ambulatory Practice Management drives revenue while improving the patient experience.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
            <!--Card 10-->
            <li class="cards__item">
                <div class="card">
                    <figure class="article--img">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/events/webinars/MEDITECH-Ambulatory--webinar.jpg">
                        <div class="article--date text--small">Webinar</div>
                    </figure>
                    <div class="article--info">
                        <h3 class="header-four"><a class="news_main_link_gae" href="#">A New View of Ambulatory Care with MEDITECH Expanse</a></h3>
                        <h4 class="header-five">October 29th, 2:00 PM (EST)
                            <br>60 Minutes</h4>
                        <p>See a high level front-to-back walkthrough of MEDITECH’s modern, mobile, Ambulatory EHR.</p>
                        <!--                            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>-->
                    </div>
                </div>
            </li>
        </ul>
        <!--
                        <div class="center">
                            <a href="#" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Load More</a>
                        </div>
-->
    </div>
</div>
<!-- End Events Block - Same Height Flexbox -->
<!-- End Events Block - Masonry & Flexbox -->
