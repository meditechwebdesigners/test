<!-- START about-hardcoded--node-2030.php -->

<style>
  /******************************************************

  		VIDEO

		******************************************************/
  .video--loop-container {
    min-height: 600px;
    max-height: 650px;
  }

  @media (max-width: 800px) {
    .video--loop-container {
      min-height: 480px;
    }
  }

  .video--loop-container video {
    /* Make video to at least 100% wide and tall */
    min-width: 100%;
    min-height: 100%;

    /* Setting width & height to auto prevents the browser from stretching or squishing the video */
    width: auto;
    height: auto;

    /* Center the video */
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .video--title {
    max-width: 1080px;
    margin-left: auto;
    margin-right: auto;
    padding-left: 1em;
    padding-right: 2em;
    position: absolute;
    top: 15%;
    bottom: 0;
    left: 6%;
    text-align: left;
    color: #fff;
    z-index: 3;
  }

</style>

<div class="video--loop-container">
  <video class="video--loop" preload="auto" autoplay loop muted playsinline>
    <source src="https://player.vimeo.com/external/315530122.sd.mp4?s=57b6f58cea068bf5e6a70ea96a3aad11fe52b21e&profile_id=165">
  </video>
  <div class="container__centered video--title">
    <div class="container__two-thirds transparent-overlay" style="background-color: rgba(0, 0, 0, 0.7);">
      <h1 class="text--white text-shadow--black">We make healthcare better</h1>
      <p class="text--white">We don't just make software — we empower organizations all over the world to take on the challenges of healthcare’s ever-expanding landscape. From giving providers more satisfaction in their day to fostering a safe, comfortable patient experience, we understand that the best technology is the kind that improves people’s lives. </p>
      <div class="btn-holder--content__callout no-margin--top">
        <a href="https://ehr.meditech.com/ehr-solutions" class="btn--orange about_buttons_gae">See how we do it</a>
      </div>
    </div>
    <div class="container__one-third">
    </div>
  </div>
</div>
<!-- END Hero -->

<!-- VIDEO -->
<div class="content__callout">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper">
      <div class="video js__video" data-video-id="305095475">
        <figure class="video__overlay">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay--50th-Neil-Pappalardo.jpg" alt="50th Anniversary Video">
        </figure>
        <a class="video__play-btn" href="https://vimeo.com/305095475"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>

  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2>We explore new possibilities</h2>
        <p>A. Neil Pappalardo pioneered the idea that computers could transform the way we care for one another. Fifty years later, MEDITECH’s reputation for excellence resounds across our industry. This didn’t happen overnight — it’s something that we’ve earned over the course of five decades of vision, innovation, and trust. But we don’t rest on those laurels. We embrace change. We keep exploring. We keep asking <span class="italic">how can this be better?</span> It’s just part of our DNA.</p>
        <div style="margin-bottom: 1em;">
          <a href="https://ehr.meditech.com/about-meditech/executives" class="btn--orange about_buttons_gae">Meet our innovative executives</a>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- End of VIDEO -->


<!-- MISSION -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/howard-messing-speaking.jpg);">
  <div class="container__centered">
    <div class="container__one-half transparent-overlay">
      <h2 class="text--white">We're driven</h2>
      <div style="border-left: 2px solid #fff;">
        <blockquote>
          <p class="text--white italic">Our mission is to help build a world where every patient can access their health information and participate in their own care, and every healthcare organization can serve their community quicker and more safely through cutting-edge technology with instant access to records, knowledge, and data.</p>
        </blockquote>
      </div>
      <p class="text--white">How will we do it? We're building solutions and mobile apps that elevate care on both sides of the table, for satisfied clinicians and <a href="https://ehr.meditech.com/news/mhealth-meditech-in-the-app-store">engaged patients</a>. We're partnering with <a href="https://ehr.meditech.com/news/meditech-joins-commonwell-health-alliance-to-break-down-interoperability-barriers">influential organizations</a> to break down barriers and create roads to true connectivity &mdash; <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">interoperability</a> has always been at the heart of MEDITECH, and our solutions have a proven track record of seamlessly and securely sharing data across all care environments. Our passion for advancing healthcare has fueled our company since Day 1, and we're not taking our foot off the gas anytime soon.</p>
      <div class="btn-holder--content__callout no-margin--top">
        <a href="https://ehr.meditech.com/about-meditech/community" class="btn--orange about_buttons_gae">See how we're working to improve our local communities.</a>
      </div>
      <div class="container__two-half">
      </div>
    </div>
  </div>
</div>
<!-- END MISSION -->

<!-- And we're here for you. -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/meditech-common-area.jpg);">
  <div class="container__centered">
    <div class="container__two-thirds transparent-overlay">
      <h2 class="text--white text-shadow--black">And we're here for you.</h2>
      <p class="text--white">85% of our sites have been with us for over a decade. That kind of loyalty doesn't result from a fleeting fad, or even your typical business relationship. The bond between MEDITECH and our customers reflects <a href="https://ehr.meditech.com/ehr-solutions/long-term-partnerships">genuine partnerships</a> and the common goal of making both healthcare and technology better than they are today. We've got smart, passionate people who are ready to work to make sure your patients — and your organization — are in good health.</p>
      <p class="text--white">Our goal is to make healthcare work for everyone. Want to be a part of the movement?</p>
      <div class="btn-holder--content__callout no-margin--top">
        <a href="https://ehr.meditech.com/careers/careers" class="btn--orange about_buttons_gae">Visit our Careers page</a>
      </div>
    </div>
    <div class="container__one-third">
    </div>
  </div>
</div>
<!-- END And we're here for you. -->


<!-- END about-hardcoded--node-2030.php -->
