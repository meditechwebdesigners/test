<?php
// This template is set up to control the display of the HIMSS content type

$url = $GLOBALS['base_url']; // grabs the site url

$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
?>
  <!-- start node--himss.tpl.php template -->


    <div class="js__seo-tool__body-content">

      <!-- BLOCK 1 -->
      <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/MEDITECH-HIMSS-Orlando-2017.jpg);">
        <div class="container__centered" style="padding:2em 0 2em;">
          <div class="container__one-half transparent-overlay text--white">
            <h1 class="js__seo-tool__title"><?php print render($content['field_header_1']); ?></h1>
            <p>
              <?php print render($content['field_long_text_1']); ?>
            </p>
          </div>
          
        </div>
      </div>
      <!-- END BLOCK 1 -->


      <!-- BLOCK 2 -->
      <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
        <div class="container__centered center">

          <div class="container__one-half">
            <div class="quote__content__icon center text--white">
              <i class="fas fa-users fa-4x"></i>
            </div>

            <h2><?php print render($content['field_header_2']); ?></h2>
            <p>
              <?php print render($content['field_long_text_2']); ?>
            </p>
          </div>
          <div class="container__one-half">
            <div class="quote__content__icon center text--white">
              <i class="fas fa-microphone fa-4x"></i>
            </div>
            <h2><?php print render($content['field_header_3']); ?></h2>
            <p>
              <?php print render($content['field_long_text_3']); ?>
            </p>
          </div>

        </div>
      </div>
      <!-- END BLOCK 2 -->


      <!-- BLOCK 3 -->
      <div class="container background--cover" style="border:none; background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Reclaim-Your-Productivity.jpg);">
        <div class="container__centered" style="padding:1em 0 1em;">
          <div class="container__one-third transparent-overlay text--white center">
            <h2><?php print render($content['field_header_4']); ?></h2>
            <p>
              <?php print render($content['field_long_text_4']); ?>
            </p>
            <a href="https://ehr.meditech.com/events/meditech-at-himss17-meet-the-experts" class="btn--orange campaign_button_gae">Meet the Experts</a>
          </div>
        </div>
      </div>
      <!-- END BLOCK 3 -->


      <!-- BLOCK 4 -->
      <div class="container bg--emerald">
        <div class="container__centered center">
          <div class="quote__content__icon center text--white">
            <i class="fas fa-exchange-alt fa-4x"></i>
          </div>
          <h2 class="text--white"><?php print render($content['field_header_5']); ?></h2>
          <p class="text--white">
            <?php print render($content['field_long_text_5']); ?>
          </p>
        </div>
      </div>
      <!-- END BLOCK 4 -->


      <!-- BLOCK 5 -->
      <?php $block_5 = field_get_items('node', $node, 'field_long_text_unl_1'); ?>
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Real-Solutions-Real-Care.jpg);">
          <div class="container__centered">
            <div class="page__title--center text--white" style="margin-bottom:2em;">
              <h2><?php print render($content['field_header_6']); ?></h2>
              <p>
                <?php print render($content['field_long_text_6']); ?>
              </p>
              <div class="page__title__ribbon"></div>
            </div>

            <?php for($n=0; $n<3; $n++){ ?>
              <div class="container__one-third">
                <div class="transparent-overlay">

                  <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>

                  <div class="text--white">
                    <p>"
                      <?php print $quotes[$n]->field_quote_1['und'][0]['value']; ?>"</p>
                    <p class="text--large no-margin--bottom">
                      <?php print $quotes[$n]->field_full_name['und'][0]['value']; ?>
                    </p>
                    <p>
                      <?php print $quotes[$n]->field_company['und'][0]['value']; ?>
                    </p>
                    <p>
                      <?php print $block_5[$n]['value']; ?>
                    </p>
                  </div>

                </div>
              </div>
              <?php } ?>
          </div>
        </div>
        <!-- END BLOCK 5 -->


        <!-- BLOCK 6 -->
        <?php $block_6 = field_collection_data($node, 'field_fc_quote_2'); ?>
          <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
            <div class="container__centered">
             
              <h2 class="center" style="padding-bottom:1em;"><?php print render($content['field_header_7']); ?></h2>
              <figure class="container__one-fourth center">
                <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
              </figure>
              
              <div class="container__three-fourths">
                <div class="quote__content__text text--large">
                  <?php print $block_6->field_quote_1['und'][0]['value']; ?>
                </div>
                <p class="text--large no-margin--bottom"><?php print $block_6->field_full_name['und'][0]['value']; ?></p>
                <p><?php print $block_6->field_company['und'][0]['value']; ?></p>
                <div class="panel no-margin">
                  <?php print render($content['field_long_text_7']); ?>
                </div>
              </div>            

            </div>
          </div>
          <!-- BLOCK 6 -->


          <!-- BLOCK 7 -->
          <?php $block_7 = field_collection_data($node, 'field_fc_head_ltext_btn_1'); ?>
            <div class="content__callout">
              <div class="content__callout__media content__callout__bg__img" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Minus-5-Ice-Bar-RA-Sushi.jpg);">
                <!-- Title -->
                <div class="content__callout__title-wrapper">
                  <h3 class="content__callout__title"><?php print $block_7->field_header_1['und'][0]['value']; ?></h3>
                </div>
              </div>
              <div class="content__callout__content">
                <div class="content__callout__body">
                  <div style="text-align:center;">
                    <?php print $block_7->field_long_text_1['und'][0]['value']; ?>
                  </div>
                  <div class="btn-holder--content__callout">
                    <a href="<?php print $block_7->field_button_url_1['und'][0]['value']; ?>" target="_blank" class="btn--orange event_reg_gae"><?php print $block_7->field_button_text_1['und'][0]['value']; ?></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- BLOCK 7 -->


            <!-- BLOCK 8 -->
            <section class="container__centered">

              <div class="page__title--center">
                <h3><?php print render($content['field_header_9']); ?></h3>
                <div class="page__title__ribbon"></div>
              </div>

              <div class="container">
                <?php 
                $vendors = field_get_items('node', $node, 'field_vendors');
                for($v=0; $v<3; $v++){
                  $vendor = $vendors[$v];
                  $vendorNID = $vendor['entity']->nid;
                  $vendorNode = node_load($vendorNID);
                  $logoFileName = $vendorNode->field_logo['und'][0]['filename'];
                  $logoAlt = $vendorNode->field_logo['und'][0]['alt'];
                  $vendorURL = $vendorNode->field_website_url['und'][0]['safe_value'];
                  print '<div class="container__one-third no-target-icon center">';
                  print '<a href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" class="vendor_link_gae"></a>';
                  print '</div>';
                } 
              ?>
              </div>

              <div class="container no-pad">
                <?php 
                for($v=3; $v<6; $v++){
                  $vendor = $vendors[$v];
                  $vendorNID = $vendor['entity']->nid;
                  $vendorNode = node_load($vendorNID);
                  $logoFileName = $vendorNode->field_logo['und'][0]['filename'];
                  $logoAlt = $vendorNode->field_logo['und'][0]['alt'];
                  $vendorURL = $vendorNode->field_website_url['und'][0]['safe_value'];
                  print '<div class="container__one-third no-target-icon center">';
                  print '<a href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" class="vendor_link_gae"></a>';
                  print '</div>';
                } 
              ?>
              </div>

            </section>
            <!-- BLOCK 8 -->

    </div>
    <!-- end js__seo-tool__body-content -->

    <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

      <!-- end node--himss.tpl.php template -->