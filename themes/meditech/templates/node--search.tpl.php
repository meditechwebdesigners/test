<?php
// This template is set up to control the display of the 'search' content type 

// get Action from URL...
if( isset($_GET['action']) ){
  $action = $_GET['action'];
}
else{
 $action = '';
}

// get page URL...
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// break up URL by equal signs...
$currentURLarray = explode('=', $currentURL);

if($action == 'pnf'){
  // PAGE NOT FOUND version...
  // break up segment by ampersand...
  $arrayendOfURL = count($currentURLarray) -2;
  $search_variables = explode('&', $currentURLarray[$arrayendOfURL]);
  // isolate the search term...
  $search_term = $search_variables[0];
}
else{
  // SEARCH PAGE version...
  // break up segment by ampersand...
  $arrayendOfURL = count($currentURLarray) -1;
  // isolate the search term...
  $search_term = $currentURLarray[$arrayendOfURL];
}

$url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--search.tpl.php template -->
  
<?php if($node->nid == 1638){ /* ================================ SEARCH BOX ============================================= */ ?>
  
  <style>
    form.gsc-search-box { font: inherit; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; width: 100%; position: relative; }
    table.gsc-search-box { border-style: none; border-width: 0; border-spacing: 0 0; width: 100%; margin-bottom: 0; margin-top: 0;  }
    .gsc-input-box { border: 0; background: #fff; height: auto; border-color: white; }
    table.gsc-search-box { border: 0; margin: 0; }
    table.gsc-search-box tbody, table.gsc-search-box tr:hover>td, table.gsc-search-box tr:hover>th { background-color: inherit; }
    td.gsc-search-button { width: 20%; padding: 0; }
    table.gsc-search-box td.gsc-input, 
    tbody:last-child tr:last-child>td:first-child,
    tbody:last-child tr:last-child>td:last-child { border: 0; border-bottom-left-radius: 0; padding: 0; }
    tbody:last-child tr:last-child>td:first-child { width: 80%; }
    table#gs_id50 { border: 1px solid #7b8888; }
    td#gs_tti50 { padding: .82em 3%; }
    table.gsc-search-box td.gsc-search-button { border: 0; }
    .gsc-search-box-tools .gsc-search-box .gsc-input { padding-right: 0; }
    .gsc-input table { border: none; }
    input.gsc-input { font-size: 1em; }
    .gsib_a { padding: 0; width: 80%; }
    .gsib_b { padding: 0; width: 10%; text-align: center; }
    input.gsc-search-button, input.gsc-search-button:hover, input.gsc-search-button:focus {
      border-color: #109372;
      background-color: #109372;
      background-image: none;
      filter: none;
      width: 100%;
      color: white;
    }
    input.gsc-search-button-v2 { width: 13px; height: 13px; padding: 6px 27px; min-width: 13px; margin-top: 2px; }
    input.gsc-search-button { 
      font-family: inherit; font-size: inherit; font-weight: bold; color: #fff; 
      padding: 1em 1.5em; margin: .5em 0 0 0; 
      height: 3.35em; width: 100%;
      border-radius: inherit;
      -moz-border-radius: inherit;
      -webkit-border-radius: inherit; 
      border: 0; 
      }
  </style>
         
    <div class="container__centered">
    
	  <div class="container__two-thirds" style="margin:2em 0;"> 
             
        <h1>Google Search</h1>
              
        <div class="search-results">
          
          <script>
            (function() {
              var cx = '004459460329414904570:jebrei6tb4w';
              var gcse = document.createElement('script');
              gcse.type = 'text/javascript';
              gcse.async = true;
              gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
              var s = document.getElementsByTagName('script')[0];
              s.parentNode.insertBefore(gcse, s);
            })();
          </script>
          <gcse:searchbox-only></gcse:searchbox-only>
           
        </div>
        
      </div>
      
    </div>
    
<?php } ?>


<?php if($node->nid == 1637){ /* ================================ SEARCH RESULTS ============================================= */ ?>
  
<style>
  .gsc-control-cse, .gsc-control-cse .gsc-table-result 
    { margin: 0; padding: 0; font-size: 1.125em; font-family: inherit; font-weight: normal; line-height: 1.6875em; }
  
  .gsc-above-wrapper-area-container { margin: 0; }
  .gsc-above-wrapper-area, .gsc-result-info-container, .gsc-orderby-container, .gsc-result-info { padding: 0; }
  
  .gsc-result-info, .gsc-orderby-label.gsc-inline-block, .gs-bidi-start-align.gs-visibleUrl.gs-visibleUrl-short, .gs-bidi-start-align.gs-visibleUrl.gs-visibleUrl-long { font-size: .8em; line-height: 1.25em; }
  
  .gsc-selected-option-container { max-width: 100%; font-size: .8em; }
  .gsc-option-menu-item { padding: 0 15px; }
  
  .gsc-adBlock, .gcsc-branding, .gsc-resultsHeader { display: none; }
  .gsc-above-wrapper-area { border-bottom: 0; }
  .gsc-above-wrapper-area-container { border: 0; }
  
  .gsc-results { width: 100%; }
  
  .gsc-thumbnail-inside, .gsc-url-top { padding-left: 0; padding-right: 0; }
  
  .gs-bidi-start-align.gs-snippet { font-size: .8em; line-height: 1.5em; }
  
  table, tbody, tr, td { background-color: #fff; border: 0; }
  tr:hover>td, tr:hover>th { background: #fff; }
  
  .gsc-result .gs-title { height: auto; }
  .gsc-control-cse .gs-result .gs-title, .gsc-control-cse .gs-result .gs-title * 
    { font-family: "montserrat", Verdana, sans-serif; font-size: 1em; }
  
  .gs-result .gs-title, .gs-result .gs-title *,
  .gs-webResult.gs-result a.gs-title:link,
  .gs-webResult.gs-result a.gs-title:link b,
  .gs-webResult.gs-result a.gs-title:visited, 
  .gs-webResult.gs-result a.gs-title:visited b, 
  .gs-imageResult a.gs-title:visited, 
  .gs-imageResult a.gs-title:visited b 
    { color: #109372; }
  .gs-webResult.gs-result a.gs-title:hover,
  .gs-webResult.gs-result a.gs-title:hover b,
  .gs-webResult div.gs-visibleUrl
    { color: #3e4545; }
  
  .gs-webResult div.gs-visibleUrl { font-style: italic; }
  
  .gs-result .gs-title, .gs-result .gs-title * { text-decoration: none; }
  
  .gsc-table-result { margin: .5em 0; }
  
  .gsc-cursor-box.gs-bidi-start-align { margin: 10px 0; }
  .gsc-results .gsc-cursor-box .gsc-cursor-page { color: #087E68; margin-right: 1em; }
  .gsc-results .gsc-cursor-box .gsc-cursor-page.gsc-cursor-current-page, .gsc-results .gsc-cursor-box .gsc-cursor-page:hover { color: #3e4545; }
  
  .gs-fileFormat { display: none; }
</style>  
  
 
    <div class="container__centered" style="margin-top:2em; margin-bottom:2em;">
    
	  <div class="container__two-thirds"> 
             
        <?php 
        if($action == 'pnf'){ // page not found results
          print '<div>';
            print '<div class="container__two-thirds">';
              print '<h1>Oops, it looks like something is wrong.</h1>';
              print '<p>We hate extra taps and clicks too, but does one of the links below seem close to what you were looking for?</p>';
            print '</div>';
            print '<div class="container__one-third center">';
              print '<img src="'.$url.'/sites/all/themes/meditech/images/404-hands-grey.png" />';
            print '</div>';
          print '</div>';
        }
        else{
          print '<h1>'.$title.'</h1>';
        }
        ?>
              
        <div class="search-results">
 
          <script>
            (function() {
              var cx = '004459460329414904570:jebrei6tb4w';
              var gcse = document.createElement('script');
              gcse.type = 'text/javascript';
              gcse.async = true;
              gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
              var s = document.getElementsByTagName('script')[0];
              s.parentNode.insertBefore(gcse, s);
            })();
          </script>
          <gcse:searchresults-only></gcse:searchresults-only>

        </div>
        
      </div>
      
      <aside class="container__one-third panel">
        <div class="sidebar__nav">
          <h4>Looking for customer-specific content? <br><br>Please <a href="https://home.meditech.com/en/d/newsroom/pages/searchresults.htm?as_q=<?php print $search_term; ?>"> search Customer Service</a>.</h4>
        </div>
      </aside>
      
    </div>
    
<?php } ?>

<!-- end node--search.tpl.php template -->
