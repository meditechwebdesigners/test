<!-- start views-view-fields--on-demand-webinars-page--block.tpl.php template -->
<?php // This template is for each row of the Views block: ON-DEMAND WEBINARS PAGE ....................... ?>

<?php
// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

// get_webinar_image function is in the template.php file
?>
<figure class="container no-pad">
  <div class="container__one-third">
    <?php $webinar_image = get_webinar_image($node); ?>
    <div class="square-img-cropper <?php print $webinar_image['crop']; ?>">
      <a class="webinars_link_gae" href="<?php print $fields['field_text_1']->content; ?>"><img src="<?php print $webinar_image['url']; ?>" alt="webinar thumbnail"></a>
    </div>
  </div>
  <figcaption class="container__two-thirds">
    <h3 class="header-four no-margin"><a class="webinar_on_demand_link_gae" href="<?php print $fields['field_text_1']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
    <p><?php print $fields['field_summary']->content; ?></p>
    <?php 
    if( !empty($fields['field_duration']->content) ){ 
      print '<p>'.$fields['field_duration']->content.'</p>'; 
    }
    ?>
  </figcaption>
</figure>

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
<hr>
<!-- end views-view-fields--on-demand-webinars-page--block.tpl.php template -->