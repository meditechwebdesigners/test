<?php // This template is for each row of the Views block: NEWS HERO ....................... 

$url = $GLOBALS['base_url']; // grabs the site url
// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);
?>
<!-- Start views-view-fields--news-hero--block.tpl.php template -->

<div class="container">
  <div class="container__centered flex-order--container">
    <div class="container__one-half content--pad-right">
      <h2 class="header-one"><?php print $fields['title']->content; ?></h2>
      <p><?php print $fields['field_long_text_1']->content; // summary ?></p>
      <a href="https://ehr.meditech.com/news/meditech-announces-professional-services" role="button" class="btn--orange-gradient" title="MEDITECH Professional Services">MEDITECH Professional Services</a>
      <?php // add Edit Video link...
          if( user_is_logged_in() ){ 
            print '<div style="display:block; margin:1em 0;"><span style="font-size:12px;">'; print l( t('Edit Hero Text'),'node/'. $fields['nid']->content .'/edit', array('attributes' => array('style' => array('color:#3e4545') ) ) ); print "</span>";
            print ' | <span style="font-size:12px;"><a href="https://app.hubspot.com/cta/2897117" target="_blank" style="color:#3e4545;">Edit Hero Button in Hubspot</a></span></div>'; 
          } 
        ?>
    </div>
    <div class="container__one-half flex-order--reverse">
      <div class="bg-pattern--container">
        <img src="<?php
        // if feature article has an image...
        if( !empty($fields['field_text_1']->content) ){
          print $fields['field_text_1']->content; // image URL
        }
        // use backup image if main image is missing...
        else{
          print $url.'/sites/all/themes/meditech/images/light-grey-squares-pattern.jpg';  
        }
        ?>">
        <div class="bg-pattern--green-gradient bg-pattern--right bg-pattern--full-height"></div>
      </div>
    </div>

  </div>
</div>
<!-- end views-view-fields--news-hero--block.tpl.php template -->
