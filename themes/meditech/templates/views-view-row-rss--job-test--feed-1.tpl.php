<!-- views-view-row-rss--job-test--feed-1.tpl.php -->
<?php

/**
 * @file
 * Default view template to display a item in an RSS feed.
 *
 * @ingroup views_templates
 */
?>
<item>
    <title><?php print $title; ?></title>
    <link><?php print $link; ?></link>
    <description><?php print $description; ?></description>
    <?php print $item_elements; ?>
    <?php
        // FACILITIES...
      $facilityTag = $node->field_meditech_location; 
      $facID = $facilityTag[und][0]['tid'];
    $term = taxonomy_term_load($facID);
    $term_name = $term->name;
    ?>
    <location><?php print $term_name; ?></location>
</item>
<!-- views-view-row-rss--job-test--feed-1.tpl.php -->
