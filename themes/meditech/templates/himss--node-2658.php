<!-- END campaign--node-2658.php HIMSS 19 -->
<?php include('inc-share-buttons.php'); ?>

<style>
  .clinicians { 
    padding: 1em; margin: 1em 2% 0 0; width: 30%; float: left; min-height: 370px; background-position: top right; background-repeat: no-repeat; 
  }
  .clinicians .transparent-overlay { margin-top: 10em; }
  .clinician-info { font-size:.78em; }
  .clinicians h3 { margin-top: 0; margin-bottom: 0; font-size: 1em; }
  
  @media (max-width: 900px){  
    .full-width--tablet { width: 100%; }
    .clinicians { width: 100%; }
  }
  
  @media (max-width: 800px){  
    .clinicians .transparent-overlay { margin-top: 17em; }
  }
  
  .demos { padding-bottom: 2em; }
  .demos .transparent-overlay { background-color: rgba(0,0,0,.7); }
  .demos h4 { font-size: 1.25em; margin-top: 0; }
  .divider { width: 50%; margin: .5em 25%; }
  
  .container__one-fifth { width: 18%; margin-right: 2%; padding: 1em 1%; float: left;  }
  .container__one-fifth:last-child { margin-right: 0; }
  @media (max-width: 1000px){
    .container__one-fifth { width: 100%; margin-right: 0; margin-top: 1em; padding: 1em 5%; float: left; }
  }
  
  @media (max-width: 940px){
    .full-width--tablet { width: 100%; }
  }
  
  .outside-booth h3 { font-size: 2em; line-height: 1.15em; margin: 1em 0 .5em; }
  .outside-booth h4 { font-size: 1.5em; line-height: 1.25em; margin: 0 0 .5em; }
  .outside-booth p { line-height: 1.35em; }
  
  .expanse-logo { text-align: center; width: 80%; margin: 0 auto; }
  @media (max-width: 1400px){
    .expanse-logo { text-align: left; width: 50%; margin: 0; }
  }
  @media (max-width: 940px){
    .expanse-logo { text-align: center; width: 80%; margin: 0 auto; }
  }
</style>


<div class="js__seo-tool__body-content">


  <!-- BLOCK 1 -->
  <div class="container hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/african-american-nurse-on-mobile-device.jpg); background-color:#d4e5ed; background-position: top right; background-repeat: no-repeat;">
    <div class="container__centered">
      <div class="container__two-thirds full-width--tablet">
        <h1>MEDITECH at HIMSS19</h1>
        <h2 style="font-size: 3em; line-height: 1.15em;">Let's create a new vision for healthcare.</h2>
        <p>Healthcare looks a lot more complex than it used to. Let us do our part to simplify it. Step into booth #3921 and find out how MEDITECH Expanse can help you to see the big picture, as well as sharpen your focus on what patients and clinicians need most.</p>
        <h3>Speed. Innovation. Mobility. See what Expanse can do for you.</h3>
        <div class="expanse-logo"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/expanse-logo_color_600px.png" alt="MEDITECH Expanse logo" style="margin-top: 1em;"></div>
      </div>
    </div>
  </div>
  <!-- BLOCK 1 -->



  <!-- BLOCK - Demos -->
  <div class="container background--cover hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/grand-canyon.jpg); background-color:#99e7f0;">
    <div class="container__centered demos">

      <div class="container__centered" style="margin-bottom:2em;">
        <h2 style="font-size: 2.5em; line-height: 1.15em;">See MEDITECH Expanse in action.</h2>
        <p>Engage patients, improve clinical workflows, and adapt to value-based care with Expanse.</p>
      </div>

      <div style="width:100%; float:left;" class="text--white">
        <div class="container__one-fifth transparent-overlay">
          <h4 class="center">Expanse Population Health</h4>
          <hr class="divider" />
          <p>Engage patients, improve clinical workflows, and adapt to value-based care with Expanse.</p>
        </div>
        <div class="container__one-fifth transparent-overlay">
          <h4 class="center">Care Across the Continuum</h4>
          <hr class="divider" />
          <p>Serve patients more effectively, no matter where or when they seek care. </p>
        </div>
        <div class="container__one-fifth transparent-overlay">
          <h4 class="center">Mobility with Expanse Point of Care</h4>
          <hr class="divider" />
          <p>Park your WOWs and experience mobile workflows for nurses, LPNs, aides, and therapists across care environments.</p>
        </div>
        <div class="container__one-fifth transparent-overlay">
          <h4 class="center">A Healthy Bottom Line with Expanse Revenue Cycle</h4>
          <hr class="divider" />
          <p>Meet your financial goals by harnessing the power of an integrated revenue cycle and advanced analytics.</p>
        </div>
        <div class="container__one-fifth transparent-overlay">
          <h4 class="center">Patient Consumerism</h4>
          <hr class="divider" />
          <p>Encourage patients to be informed, empowered members of their care team.</p>
        </div>
      </div>

    </div>
  </div>
  <!-- BLOCK - Demos -->



  <!-- Block - Nurses and Physicians -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/abstract-x-background-beige.jpg);">
    <div class="container__centered">
      <h2 class="no-margin" style="font-size: 2.5em; line-height: 1.15em;">Hear from our clinicians.</h2>
      <p style="margin-top:1em;">Want to work more efficiently, access information faster, and build stronger relationships with patients? Come to our booth and chat with medical professionals who have done all this and more. <a href="https://ehr.meditech.com/events/meditech-clinicians-share-their-expanse-stories-at-himss19">Click here</a> to learn which clinicians are giving presentations during the week.</p>
    </div>

    <div class="container__centered text--white">

      <div style="float: left; width: 100%;">

        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Andy-Burchett-DO.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Andy Burchett, DO</h3>
            <p class="clinician-info">Medical Information Officer
              <br>Avera Health
              <br>Sioux Falls, SD</p>
          </div>
        </div>
        
        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/William-Dailey-MD.jpg);">
          <div class="transparent-overlay text--white">
            <h3>William Dailey, MD, MS, MSMI</h3>
            <p class="clinician-info">CMIO
              <br>Golden Valley Memorial Healthcare
              <br>Clinton, MO</p>
          </div>
        </div>

        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Joe-Farr-RN.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Joe Farr, RN</h3>
            <p class="clinician-info">Clinical Applications Coordinator
              <br>King's Daughters Medical Center
              <br>North Brookhaven, MS</p>
          </div>
        </div>

      </div>

      <div style="float: left; width: 100%;">

        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Parminder-Ghuman-RN.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Parminder Ghuman, RN</h3>
            <p class="clinician-info">Medical Informatics Liaison
              <br>Humber River Hospital
              <br>Toronto, ON</p>
          </div>
        </div>

        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/William-Gustin-MD.jpg);">
          <div class="transparent-overlay text--white">
            <h3>William Gustin, MD</h3>
            <p class="clinician-info">Physician Consultant
              <br>MEDITECH
              <br></p>
          </div>
        </div>
        
        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Steve-Jones-MD.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Steve Jones, MD</h3>
            <p class="clinician-info">Physician Consultant
              <br>MEDITECH
              <br></p>
          </div>
        </div>

      </div>

      <div style="float: left; width: 100%;">

        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Doug-Kanis-DO.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Doug Kanis, DO</h3>
            <p class="clinician-info">CMIO, Internal Medicine
              <br>Pella Regional Health
              <br>Pella, IA</p>
          </div>
        </div>
        
        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Ginny-Kwong-MD.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Ginny Kwong, MD, FAAFP</h3>
            <p class="clinician-info">Vice President & CMIO
              <br>Halifax Health
              <br>Daytona Beach, Florida</p>
          </div>
        </div>
         
        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Steven-McPherson-BSN-RN.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Steven McPherson, BSN, RN</h3>
            <p class="clinician-info">Nurse Manager
              <br>Union Hospital of Cecil County
              <br>Elkton, MD</p>
          </div>
        </div>

      </div>

      <div style="float: left; width: 100%;">
        
        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/David-Niland-MD.jpg);">
          <div class="transparent-overlay text--white">
            <h3>David Niland, MD</h3>
            <p class="clinician-info">Medical Information Officer
              <br>Galway Clinic
              <br>Galway, IRE</p>
          </div>
        </div>
         
        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Stephen-Tingley-MD.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Stephen Tingley, MD</h3>
            <p class="clinician-info">CMIO
              <br>Mount Nittany Medical Center
              <br>State College, PA</p>
          </div>
        </div>
         
        <div class="clinicians" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Patrick-Tupa-DO.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Patrick Tupa, DO</h3>
            <p class="clinician-info">CMIO, ED Physician
              <br>Firelands Regional Medical Center
              <br>Sandusky, OH</p>
          </div>
        </div>

      </div>

    </div>

  </div>
  <!-- END Block - Nurses and Physicians -->



  <!-- BLOCK - Outside the booth -->
  <div class="container outside-booth">
    <div class="container__centered">

      <h2 class="center" style="font-size: 2.5em; line-height: 1.15em; margin-bottom: 1.5em;">Come find us beyond our booth.</h2>
     
      <div class="container__centered background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/abstract-x-background-green.jpg); background-color:#087e68;">
        <div class="container__centered center" style="float: left; width: 100%;">
          <h3>Customers on the HIMSS Stage</h3>
          <p style="margin-bottom: 2em;">These <a href="<?php print $url; ?>/events/meditech-customers-on-the-himss19-main-stage" target="_blank">MEDITECH customers</a> will be sharing their strategies at HIMSS. Come check them out.</p>
        </div>
      </div>
     
      <div class="container__centered background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/abstract-x-background-beige.jpg); background-color:#d7d1c4; margin-top:1em;">
        <div class="container__centered" style="float: left; width: 100%;">
          <h3 class="center">MEDITECH Leads the Way</h3>
        </div>
        <div>
          <div class="container__one-half">
            <div style="padding:1em 2em;">
              <h4>Leverage Your EHR to Support Behavioral Health Interventions</h4>
              <p><strong>Tuesday, February 12
              <br/>CMIO Roundtable Discussion: 4 - 5 p.m.
              <br/>CMIO Networking Reception: 5 - 6 p.m.
              <br/>Valencia Ballroom D</strong></p>
              <p>Hear strategies from key industry thought leaders, exchange best practices, and build relationships during this exclusive event.</p>
            </div>
          </div>
          <div class="container__one-half">
            <div style="padding:1em 2em;">
              <h4>HIMSS Canadian Forum</h4>
              <p><strong>Tuesday, Feb. 12 | 10:30 a.m. - 2:30 p.m.
              <br/>W315B</strong></p>
              <p>As Canada's market share leader, we're excited to be the sole sponsor of the HIMSS Canadian Forum. Join your peers at this event, which consists of three separate sessions sharing Canada's digital health initiatives. </p>
            </div>
          </div>
        </div>
        
        <div>
          <div class="container__one-half center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Dr-Jim-Jirjis.jpg" alt="Dr. Jim Jirjis" style="max-width:200px;">
            <p><strong>Jim Jirjis, MD</strong><br>
            Chief Health Information Officer, HCA<br>
            Moderator, <em>CMIO Roundtable Discussion</em></p>
          </div>
          <div class="container__one-half center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Dr-Mark-Pearce.jpg" alt="Dr. Mark Pearce" style="max-width:200px;">
            <p><strong>Dr. Mark Pearce, Forensic Psychiatrist</strong><br>
            Centre for Addiction and Mental Health, Ontario Shores Centre for Mental Health Sciences<br>
            Panelist, <em>CMIO Roundtable Discussion</em><br>
            Introduction, <em>Canadian Forum</em></p>
          </div>
        </div>
        
      </div>

      <div class="container__centered background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/abstract-x-background-green.jpg); background-color:#087e68; margin-top:1em;">
        <div class="container__centered" style="float: left; width: 100%;">
          <h3 class="center">MEDITECH's Strategic Direction</h3>
        </div>
        <div class="container__one-half">
          <div style="padding:1em 2em;">
            <h4><a href="https://www.himssconference.org/session/meditech-expanse-population-health" target="_blank">Market Debut: MEDITECH Expanse Population Health</a></h4>
            <p><strong>Tuesday, February 12 | 11:15 - 11:35 a.m.
            <br/>Lobby E Market Debut Theater</strong></p>
            <p>Stop by this session for a sneak peak of MEDITECH Expanse Population Health. See how this solution's comprehensive care management and population analytics features empower care teams to manage patient populations across all settings.</p>
          </div>
          <div style="padding:1em 2em;">
            <h4>The State of A.I. in MEDITECH Expanse, Featuring Nuance Communications</h4>
            <p><strong>Tuesday, February 12 | 3:15 - 4:30 p.m.
            <br/>Room 202A</strong></p>
            <p>Learn how MEDITECH and Nuance are integrating voice commands into the Expanse EHR to streamline physician workflows.</p>
            
            <?php $button_7_code = '5681dead-5b5e-49e7-a954-98ae2c4c5c61'; ?>
            <div class="button--hubspot center himss_button_gae" style="margin-top:1em; margin-bottom:1em;">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_7_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_7_code; ?>" id="hs-cta-<?php print $button_7_code; ?>">
                  <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_7_code; ?>" target="_blank"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_7_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_7_code; ?>.png" alt="button" /></a></span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                  hbspt.cta.load(2897117, '<?php print $button_7_code; ?>', {});
                </script>
              </span><!-- end HubSpot Call-to-Action Code -->
            </div>
            
          </div>
        </div>
        <div class="container__one-half">
          <div style="padding:1em 2em;">
            <h4>Create, Share, and Grow with MEDITECH Greenfield, Featuring Forward Advantage</h4> 
            <p><strong>Wednesday, February 13 | 3:15 - 4:30 p.m.
            <br/>Room 202A</strong></p> 
            <p>Learn about MEDITECH Greenfield, our new app development environment supported by RESTful APIs. Attend this interactive panel with our API experts alongside early adopter Forward Advantage, to see how Greenfield will help cultivate user-friendly innovations in healthcare.</p>
            
            <div style="margin:1.5em 0;">
              <div class="video js__video" data-video-id="288801648">
                <figure class="video__overlay">
                  <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--meditech-greenfield-promo.jpg" alt="MEDITECH Greenfield API Promo - Video Covershot">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/288801648"></a>
                <div class="video__container"></div>
              </div>
            </div>
            
            <?php $button_6_code = '9dc841e8-16be-48f3-a0fe-8bc68e9534a2'; ?>
            <div class="button--hubspot center himss_button_gae" style="margin-top:1em; margin-bottom:1em;">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_6_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_6_code; ?>" id="hs-cta-<?php print $button_6_code; ?>">
                  <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_6_code; ?>" target="_blank"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_6_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_6_code; ?>.png" alt="button" /></a></span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                  hbspt.cta.load(2897117, '<?php print $button_6_code; ?>', {});
                </script>
              </span><!-- end HubSpot Call-to-Action Code -->
            </div>
            
          </div>
        </div>
      </div>
      
      <div class="container__centered background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/abstract-x-background-beige.jpg); background-color:#d7d1c4; margin-top:1em;">
        <div class="container__centered center" style="float: left; width: 100%; padding-bottom:2em;">
          <h3>Join our #HIMSS19 Tweetup!</h3>
          <h4>Tuesday, February 12 | 12:30pm</h4>
          <p><strong>Topic:</strong> The Nurses' Voice: Incorporating Nurse Feedback to Improve Care</p>
          <p>Find out more information about <a href="<?php print $url; ?>/events/the-nurses-voice-incorporating-nurse-feedback-to-improve-care">our HIMSS19 Tweetup</a>.</p>
        </div>
      </div>

    </div>
  </div>
  <!-- BLOCK - Outside the booth -->




  <!-- BLOCK - Interoperability -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/woman-on-phone-by-water-in-city.jpg);">
    <div class="container__centered">
      <div class="container__one-half transparent-overlay text--white" style="padding:2em;">
        <h2 style="font-size: 2.5em; line-height: 1.15em;">Interoperability isn't just about data; it's about people. </h2>
        <div style="width:80%; margin:1em auto;" class="center">
          <h3><a href="https://www.interoperabilityshowcase.org/orlando/2019" target="_blank">HIMSS Interoperability<br/>Showcase</a>
            <br>February 12-14 | Booth #9100</h3>
        </div>
        <p>We'll be participating in a use case with CommonWell Health Alliance.</p>
      </div>
    </div>
  </div>
  <!-- BLOCK - Interoperability -->



  <!-- BLOCK - Customer Event -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Mangos-Tropical-Cafe--background.jpg); background-color: black;">

    <div class="container__centered text--white">
      <div class="container__one-half transparent-overlay">

        <h2 style="font-size: 3em; line-height: 1.15em;">A night that's all about our customers.</h2>
        <p>Step away from the busy trade show floor and join us for our annual customer appreciation night. We look forward to seeing you for a night of food, music, and fun!</p>

        <div class="center">
          <h3 style="margin-bottom: 0;">Mango's Tropical Cafe</h3>
          <p><strong>Wednesday, February 13 | 7-10 p.m.</strong></p>
          
          <?php $button_8_code = '7c6ee094-f598-4d43-9265-ea3d8208ee57'; ?>
          <div class="button--hubspot center himss_button_gae" style="margin-top:1em; margin-bottom:1em;">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_8_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_8_code; ?>" id="hs-cta-<?php print $button_8_code; ?>">
                <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_8_code; ?>" target="_blank"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_8_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_8_code; ?>.png" alt="button" /></a></span>
              <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
              <script type="text/javascript">
                hbspt.cta.load(2897117, '<?php print $button_8_code; ?>', {});
              </script>
            </span><!-- end HubSpot Call-to-Action Code -->
          </div>
        </div>

        <p class="center">Questions? Contact Event Coordinator <a href="https://home.meditech.com/webforms/contact.asp?rcpt=pcorcoran|meditech|com&rname=pcorcoran">Patti Corcoran</a>.</p>

      </div>
      <div class="container__one-half">
        <img style="border:1px solid white;" src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Mangos-Tropical-Cafe--photo-01.jpg"><br>
        <img style="border:1px solid white;" src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Mangos-Tropical-Cafe--photo-02.jpg">
      </div>
    </div>

  </div>
  <!-- BLOCK - Customer Event -->





  <!-- BLOCK - Vendors -->
  <div class="container__centered" style="padding-bottom: 1em;">

    <div class="container center" style="font-size:2em; line-height:1em; padding:1em 0;">
      <?php print render($content['field_event_vendors']); ?>
    </div>

    <?php 
    $vendors = field_get_items('node', $node, 'field_vendors');
  
    if( count($vendors) > 0 ){
      print '<div class="container no-pad">';
      for($v=0; $v<3; $v++){
        $vendor = $vendors[$v];
        $vendorNID = $vendor['entity']->nid;
        $vendorNode = node_load($vendorNID);
        $logoFileName = $vendorNode->field_logo['und'][0]['filename'];
        $logoAlt = $vendorNode->field_logo['und'][0]['alt'];
        $vendorURL = $vendorNode->field_website_url['und'][0]['safe_value'];
        print '<div class="container__one-third no-target-icon center" style="margin-bottom:2em;">';
        print '<a href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" class="vendor_link_gae"></a>';
        print '</div>';
      } 
      print '</div>';
    }

    if( count($vendors) > 3 ){
      print '<div class="container no-pad">';
      for($v=3; $v<6; $v++){
        $vendor = $vendors[$v];
        $vendorNID = $vendor['entity']->nid;
        $vendorNode = node_load($vendorNID);
        $logoFileName = $vendorNode->field_logo['und'][0]['filename'];
        $logoAlt = $vendorNode->field_logo['und'][0]['alt'];
        $vendorURL = $vendorNode->field_website_url['und'][0]['safe_value'];
        print '<div class="container__one-third no-target-icon center" style="margin-bottom:2em;">';
        print '<a href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" class="vendor_link_gae"></a>';
        print '</div>';
      } 
      print '</div>';
    }

    if( count($vendors) > 6 ){
      print '<div class="container no-pad" style="padding: 0 20%;">';
      for($v=6; $v<8; $v++){
        $vendor = $vendors[$v];
        $vendorNID = $vendor['entity']->nid;
        $vendorNode = node_load($vendorNID);
        $logoFileName = $vendorNode->field_logo['und'][0]['filename'];
        $logoAlt = $vendorNode->field_logo['und'][0]['alt'];
        $vendorURL = $vendorNode->field_website_url['und'][0]['safe_value'];
        print '<div class="container__one-half no-target-icon center" style="margin-bottom:2em;">';
        print '<a href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" class="vendor_link_gae"></a>';
        print '</div>';
      } 
      print '</div>';
    }
    ?>
    
    <?php if(node_access('update',$node)){ ?>
    <div class="center" style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div> 
    <?php } ?>

  </div>
  <!-- BLOCK - Vendors -->

</div><!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END campaign--node-2658.php HIMSS 19 -->