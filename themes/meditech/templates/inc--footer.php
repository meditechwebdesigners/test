<!-- START inc--footer.php -->
 
  <!-- NEWSLETTER Signup Link -->
  <div class="container bg--emerald" style="padding-bottom: 1.75em;">
    <div class="container__newsletter" style="width:80%;">
      <h3 class="newsletter__title">Sign Up for MEDITECH Email Updates</h3>
      <p>Find out about our events, webinars, blog posts, and more when you sign up for our mailing list!</p>

      <div class="center">
        <?php hubspot_button('864299ec-5abf-4004-9c6d-2d051794101f', "Subscribe to receive the MEDITECH email newsletter"); ?>
      </div>

    </div>
  </div>
  <!-- END NEWSLETTER -->


  <style>
    .footer__sitemap__list__item { margin: 1em 0; line-height: 1.25em; }
  </style>

  <!-- SITEMAP ================================================================== -->
  <section class="container__centered" role="navigation" aria-labelledby="footer-nav">
    <div class="footer_link_gae" id="footer-nav">
      <div class="container center" style="padding: 1em 0 2em 0;">
        <a class="footer_logo_gae" href="<?php print $websiteURL; ?>">
          <img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.svg" onerror="this.src='<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.png'; this.onerror=null;" style="width:250px;" alt="MEDITECH footer logo">
        </a>
      </div>

      <div class="container__one-fourth">
        <h4><a href="<?php print $websiteURL; ?>/ehr-solutions">EHR Solutions</a></h4>
        <ul>
          <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#fiscal-responsibility">Fiscal Responsibility</a></li>
          <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#interoperability">Interoperability</a></li>
          <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#nurse-specialty-care">Nurse & Specialty Care</a></li>
          <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#physician-efficiency">Physician Efficiency</a></li>
          <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#population-health">Population Health</a></li>
          <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#quality-outcomes">Quality Outcomes</a></li>
          <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#technologies-transform-care">Technologies to Transform Care</a></li>
        </ul>
      </div>
      <!-- End of Container -->

      <div class="container__one-fourth">
        <h4><a href="<?php print $websiteURL; ?>/news">News</a></h4>
        <ul>
          <li><a href="<?php print $websiteURL; ?>/news-tags/press-releases">Press Releases</a></li>
          <li><a href="<?php print $websiteURL; ?>/news-tags/signings">Signings</a></li>
          <li><a href="<?php print $websiteURL; ?>/news-tags/videos">Videos</a></li>
        </ul>
        <h4><a href="https://blog.meditech.com">Blog</a></h4>
        <ul>
          <li><a href="<?php print $websiteURL; ?>/case-studies">Case Studies</a></li>
          <li><a href="https://blog.meditech.com/topic/podcast">Podcasts</a></li>
          <li><a href="<?php print $websiteURL; ?>/ebooks">eBooks</a></li>
        </ul>
        <h4><a href="<?php print $websiteURL; ?>/events">Events</a></h4>
        <ul>
          <li><a href="<?php print $websiteURL; ?>/events/meditech-webinars">Upcoming Webinars</a></li>
          <li><a href="<?php print $websiteURL; ?>/events/meditech-on-demand-webinars">On Demand Webinars</a></li>
        </ul>
      </div>
      <!-- End of Container -->

      <div class="container__one-fourth">
        <h4><a href="<?php print $websiteURL; ?>/global">Global</a></h4>
        <ul>
          <li><a href="<?php print $websiteURL; ?>/global/meditech-canada">MEDITECH in Canada</a></li>
          <li><a href="<?php print $websiteURL; ?>/global/meditech-asia-pacific">MEDITECH Asia Pacific</a></li>
          <li><a href="<?php print $websiteURL; ?>/global/meditech-south-africa">MEDITECH South Africa</a></li>
          <li><a href="<?php print $websiteURL; ?>/global/meditech-uk-ireland">MEDITECH UK & Ireland</a></li>
        </ul>
        <h4><a href="<?php print $websiteURL; ?>/careers/careers">Careers</a></h4>
        <ul>
          <li><a href="<?php print $websiteURL; ?>/careers/benefits-perks">Benefits &amp; Perks</a></li>
          <li><a href="<?php print $websiteURL; ?>/careers/life-at-meditech">Life at MEDITECH</a></li>
          <li><a href="<?php print $websiteURL; ?>/careers/college-to-career">College to Career</a></li>
          <li><a href="<?php print $websiteURL; ?>/careers/career-events">Career Events</a></li>
          <li><a href="<?php print $websiteURL; ?>/careers/applying-at-meditech">Applying At MEDITECH</a></li>
          <li><a href="<?php print $websiteURL; ?>/careers/veterans">Veterans</a></li>
        </ul>
      </div>
      <!-- End of Container -->

      <div class="container__one-fourth">
        <h4><a href="<?php print $websiteURL; ?>/about-meditech/about-meditech">About MEDITECH</a></h4>
        <ul>
          <li><a href="<?php print $websiteURL; ?>/about-meditech/executives">Executives</a></li>
          <li><a href="<?php print $websiteURL; ?>/about-meditech/community">Community</a></li>
          <li><a href="<?php print $websiteURL; ?>/about-meditech/directions-to-meditech">Directions</a></li>
          <li><a href="<?php print $websiteURL; ?>/about-meditech/area-hotels">Area Hotels</a></li>
        </ul>

        <?php
        // for South Africa pages...
        if($node->type == 'international_south_africa'){
        ?>
        <h4><a href="<?php print $websiteURL; ?>/global/meditech-south-africa/contact-meditech-south-africa">Contact MEDITECH South Africa</a></h4>
        <ul>
          <li><a href="https://www.facebook.com/MEDITECH-SA-317306641687653" target="_blank">Facebook</a></li>
          <li><a href="https://twitter.com/MEDITECHSA" target="_blank">Twitter</a></li>
          <li><a href="https://za.linkedin.com/in/meditech-south-africa-a5340453" target="_blank">LinkedIn</a></li>
        </ul>
        <?php
        }
        // for Asia Pacific pages...
        elseif($node->type == 'international_asia_pacific'){
        ?>
        <h4><a href="<?php print $websiteURL; ?>/global/meditech-south-africa/contact-meditech-asia-pacific">Contact MEDITECH Asia Pacific</a></h4>
        <ul>
          <li><a href="https://www.facebook.com/Meditech-Australia-360851333989795/" target="_blank">Facebook</a></li>
          <li><a href="https://twitter.com/au_meditech" target="_blank">Twitter</a></li>
          <li><a href="https://au.linkedin.com/in/meditech-australia-63715257/de" target="_blank">LinkedIn</a></li>
        </ul>
        <?php
        }
        // for UK/IRE pages...
        elseif($node->type == 'international_uk_ire'){
        ?>
        <h4><a href="<?php print $websiteURL; ?>/global/meditech-uk-ireland/contact-meditech-uk-ireland">Contact MEDITECH UK & Ireland</a></h4>
        <ul>
          <li><a href="https://twitter.com/MEDITECHUK1" target="_blank">Twitter</a></li>
          <!--<li><a href="https://au.linkedin.com/in/meditech-australia-63715257/de" target="_blank">LinkedIn</a></li>-->
        </ul>
        <?php
        }
        // for US pages...
        else{
        ?>
        <h4><a href="<?php print $websiteURL; ?>/contact">Contact MEDITECH</a></h4>
        <ul>
          <li><a href="https://www.facebook.com/MeditechEHR?ref=br_tf" target="_blank">Facebook</a></li>
          <li><a href="https://twitter.com/MEDITECH" target="_blank">Twitter</a></li>
          <li><a href="https://www.linkedin.com/company/meditech" target="_blank">LinkedIn</a></li>
        </ul>
        <?php
        }
        ?>

        <h4><a href="https://www.meditech.com/shareholder/reports.html" target="_blank">Shareholders</a></h4>
        <h4><a href="https://home.meditech.com/en/d/customer/" target="_blank">Customers</a></h4>
      </div>
      <!-- End of Container -->

    </div>
    <!-- End of .footer_link_gae -->
  </section>
  <!-- End of SITEMAP -->
  
<!-- END inc--footer.php -->