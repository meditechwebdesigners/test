<?php // This template is for each row of the Views block: VIDEO BOXED \\\\\\\\\\\\\\\\\\\\\\\\ ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start views-view-fields--video-boxed--block.tpl.php template -->
    <div class="content__callout border-none">
      <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
         
          <div class="video js__video" data-video-id="<?php print $fields['field_video_url']->content; ?>">
            <figure class="video__overlay">
              <?php // if the overlay image was added by user, then show overlay image, otherwise show default overlay image...
              if( !empty($fields['field_video_overlay_image']->content) ){
                print $fields['field_video_overlay_image']->content; 
              }
              else{
              ?>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/video-overlay.jpg" alt="Video Covershot">
              <?php } ?>
            </figure>
            <a href="https://vimeo.com/<?php print $fields['field_video_url']->content; ?>?&autoplay=1" class="video__play-btn"></a>
            <div class="video__container"></div>
          </div>
          <?php // add Edit Video link...
            if( user_is_logged_in() ){ 
              print '<div style="display:block; text-align:center; margin:1em 0;"><span style="font-size:12px;">'; print l( t('Edit Video'),'node/'. $fields['nid']->content .'/edit', array('attributes' => array('style' => array('color:white') ) ) ); print "</span></div>"; 
            } 
          ?>                  
          
        </div>
      </div>
      <div class="content__callout__content">
        <div class="content__callout__body">
         <h2><?php print $fields['title']->content; ?></h2>
          <p><?php print $fields['field_video_description']->content; ?></p>
          <?php if( !empty($fields['field_video_button_link']->content) ){ ?>
          <div class="btn-holder--content__callout">
              <a class="btn--orange video_link_gae" href="<?php print $fields['field_video_button_link']->content; ?>"><?php print $fields['field_video_button_text']->content; ?></a>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
<!-- end views-view-fields--video-boxed--block.tpl.php template -->