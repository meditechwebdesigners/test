<?php // This template is set up to control the display of the 'Event' content type 
$url = $GLOBALS['base_url']; // grabs the site url
$node_id = $node->nid;
?>

<!-- start node--events-simple-page.tpl.php template -->

  <?php
  // IF any of these specific nodes are the current page, then use specific template ===================
  // any events - simple page that uses a view or have a "different" layout should be added to this list...
  $specific_templates = array('609','1761','1841','2374','2582','2926');
  if( in_array($node_id, $specific_templates) ){

    include('events-simple-page--node-'.$node_id.'.php');

  } else { // for other simple event pages (that don't use Views) just use the following... ============================
  ?>

  <section class="container__centered">
    <div class="container__two-thirds">

      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

      <div class="js__seo-tool__body-content">
        <?php print render($content['field_body']); ?>
      </div>
        

    </div><!-- END container__two-thirds -->

    <!-- SIDEBAR -->
    <aside class="container__one-third panel">
      <div class="sidebar__nav">
        <?php
          $messnBlock = module_invoke('menu', 'block_view', 'menu-events-section-side-nav');
          print render($messnBlock['content']);
        ?>
      </div>
    </aside>
    <!-- END SIDEBAR -->
  </section>

  <?php
  }
  ?>
    
  
  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    // list the node IDs that should NOT include the SEO tool (pages that list other pages)...
    $no_seo = array('609','1761','1841','2374','2926');
    if( !in_array($node_id, $no_seo) ){
      print '<!-- SEO Tool is added to this div -->';
      print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
    }
  } 
?>  
 
<!-- end node--events-simple-page.tpl.php template -->