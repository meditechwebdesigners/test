<?php // This template is for each row of the Views block: EXECUTIVES ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start views-view-fields--executives--block.tpl.php template -->
<?php
if( !empty($fields['field_image']->content) ){ 
  // get image data...
  $image_data = $fields['field_image']->content;
  // example data result: <img typeof="foaf:Image" src="https://at_drudev.meditech.com/drupal/sites/default/files/images/campaigns/<FILENAME>.jpg" width="800" height="250" />
  $image_HTML_array = explode('/', $image_data);
  // grab this part: MEDITECH-surgical-services_grid-short.jpg" width="800" height="250" ...
  // oddly it removed the < and />, but there is a trailing space...
  $image_HTML_ending = $image_HTML_array[8];
  // find the first set of quotes...
  $first_quotes = strpos($image_HTML_ending, '"');
  // remove everything from the first set of quotes back...
  $image_filename = substr($image_HTML_ending, 0, $first_quotes);
}  
?>

<figure class="info__card" style="margin-bottom: 0;">
  <a href="<?php print $fields['path']->content; ?>"><div class="circle-people" style="background-image: url(<?php print $url; ?>/sites/default/files/images/people/<?php print $image_filename; ?>);">&nbsp;</div></a>
  <figcaption class="center" style="margin-top: 1em;">
    <h3 class="header-five no-margin"><a href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
    <p><?php print $fields['field_job_title']->content; ?></p>
  </figcaption>
</figure>
<!-- end views-view-fields--executives--block.tpl.php template -->