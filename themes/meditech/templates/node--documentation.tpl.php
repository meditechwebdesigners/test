<?php
  // This template is set up to control the display of the 'documentation' content type 
  $url = $GLOBALS['base_url']; // grabs the site url
?>

<!-- start node--documentation.tpl.php template -->
<?php if( user_is_logged_in() ){ ?>
  
   
   <?php if($node->nid == 2928){ // News Articles with CTA Buttons page ?>
   
      <section class="container__centered">
        <div class="container__two-thirds">
          <h1><?php print $title; ?></h1>
          <!-- start Content REGION -->
          <?php print views_embed_view('news_articles_with_cta_buttons', 'block'); // adds 'News Articles with CTA Buttons' Views block... ?>
          <!-- end Content REGION -->
        </div>
        <!-- END container__two-thirds -->
      </section>
      
    <?php } else { // other documentation pages... ?>
 
      <style>
        table, .small { font-size: .8em; }
        table td, table th { padding:.25em 1em; }
        table th { text-align: left; }
        table tr:nth-child(odd) { background: #f2ece6; }
        table tr:nth-child(even) { background: #ffe3d5; }

        .shortcodes li { margin:10px 0; }
        .shortcodes li ul { list-style-type: circle; }
        .beige { background:#e8d8cc; border:1px solid #999; padding: .5em 2em; }
        .white { background:#ffffff; border:1px solid #999; padding: .5em 2em; } 
        .page-example { 
          overflow: hidden; 
          border: 1px solid #ccc; 
          padding: 1em; margin: 0 auto;
          background-color: beige;
          -moz-box-shadow: 1px 1px 2px 1px #ccc;
          -webkit-box-shadow: 1px 1px 2px 1px #ccc;
          box-shadow: 1px 1px 2px 1px #ccc;
          margin-bottom: 1em;
        }
        .page-small { font-size: 0.8em; height: auto; line-height: 1.15em; min-height: 150px; width: 90%; }
        .page-large { width: 90%; height: auto; margin-top: 2em; font-size: .8em; line-height: 1.15em; }
        hr { margin: 2em 0; }
        .half-hr { width: 50%; margin: 2em auto; float: none; }

        .sidebar__nav ul.menu ul.menu { padding-left: 1.5em; margin-top: 0; }
        .sidebar__nav ul.menu ul.menu li { padding: .4em 0 .5em 0; font-size: .8em; }
        .sidebar__nav ul.menu ul.menu li.last { padding-bottom: 0; }

        @media (max-width: 800px){
          .sidebar__nav ul.menu ul.menu li { font-size: 1em; }
        }
      </style> 

      <section class="container__centered">
        <div class="container__two-thirds">
    
          <h1 class="page__title"><?php print $title; ?></h1>

          <?php print render($content['field_body']); // main content if any ?>
          
        </div>

        <!-- SIDEBAR -->
        <aside class="container__one-third">

          <div class="sidebar__nav panel documentation_sidebar_gae">
            <?php
            $docuBlock = module_invoke('menu', 'block_view', 'menu-documentation-side-nav');
            print render($docuBlock['content']); 
            ?>
          </div>

        </aside>
        <!-- END SIDEBAR -->
      </section>

      <section class="container__centered">
        <div class="container__two-thirds">

            <?php echo '<!-- <pre>'; echo print_r($node); echo '</pre> -->'; ?>

            <?php
            $user_id = $node->uid;
            $user = user_load($user_id);
            $username = $user->name;
            $date = date('m/d/Y h:i A', $node->revision_timestamp);
            ?>

            <hr />

            <p>This page was last updated on <strong><?php print $date; ?></strong> by <strong><?php print $username; ?></strong>.</p>

            <p>This documentation is a work-in-progress. Any questions or problems you have while using Drupal may help us to improve this document and reduse the amount of questions and problems in the future.</p>

        </div>
      </section>

  <?php } ?>
  

<?php } else { ?>

  <section class="container__centered">
    <div class="container__two-thirds">
      <h1 class="page__title">Access Denied</h1>
      <p>The content of this page is meant for MEDITECH employees only.</p>
    </div>
  </section>

<?php } ?>
<!-- end node--documentation.tpl.php template -->