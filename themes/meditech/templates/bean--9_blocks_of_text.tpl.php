<?php
/**
 * @file
 * Default theme implementation for beans (block-types).
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 $url = $GLOBALS['base_url']; // grabs the site url 
?>
<!-- start bean--9_blocks_of_text.tpl.php template -->
<?php 
$objectData = $content['field_header_1']['#object'];

//check to see which bean block this is for...
if( $objectData->label == 'South Africa Numbers' ){
?>

  <div class="container no-pad--top">
    
    <!--<div class="grid-items-lines" style="padding-top:1em;">-->
    <!--div class="grid-item">
      <h3 class="header-one grid-item--numbers"><?php //print $header; ?></h3>
      <?php //print $long_text; ?>
    </div>-->
    
    <style>
      .bar-arrow-container { width:100%; height:100px; float:left; padding:.25em; margin-bottom: .35em; }      
      .narrow-box { width: 25%; height: 100px; position: relative; float: left; font-size: 2em; }
      .narrow-box h3 { height: 1.3em; text-align: center; margin: auto; position: absolute; top: 0; left: 0; bottom: 0; right: 0; }
      .long-box { width: 65%; height: 100px; float: left; font-size: 1.25em; color: white; position: relative; }
      .long-box p { height: 1.5em; line-height: 1.4em; text-align: center; margin: auto; padding: 0 .15em; position: absolute; top: 0; left: 0; bottom: 0; right: 0; }
      .triangle--left, .triangle--right { float: left; border-top: 50px solid #ffffff; border-bottom: 50px solid #ffffff; }
      .triangle--left { border-left: 35px solid; }
      .triangle--right { border-right: 35px solid; }
      
      @media (max-width: 900px){
        .triangle--left { border-left: 25px solid; }
        .triangle--right { border-right: 25px solid; }
        .narrow-box { font-size: 1.5em; }
      }
      
      @media (max-width: 400px){
        .triangle--left { display: none; }
        .triangle--right { display: none; }
        .narrow-box { margin-left: 5%; margin-right: 5%; }
      }
    </style>

    <?php // padding-left:5px; padding-right:5px; header-one
    $n = 1;
    do{
      $header = $content['field_header_'.$n]['#items'][0]['value'];
      $long_text = $content['field_long_text_'.$n]['#items'][0]['value'];
      $text_length = strlen($long_text);
      
      if($text_length > 55){
        print '<style> #n'.$n.' .long-box p { top: -30px; } </style>';
      }
      
      if($n % 3 == 0){
        $color = '#3e4545';
      }
      else if($n % 2 == 0){
        $color = '#00BC6F';
      }
      else{
        $color = '#0075A8';
      }
      
      if($n % 2 == 0){
      ?>      
        <div id="n<?php print $n; ?>" class="bar-arrow-container">
          <div class="narrow-box" style="color:<?php print $color; ?>;"><h3><?php print $header; ?></h3></div>
          <div class="triangle--right" style="border-right-color: <?php print $color; ?>;"></div>
          <div class="long-box" style="background-color:<?php print $color ?>;"><?php print $long_text; ?></div>
        </div>
      <?php
      }
      else{
      ?>
        <div id="n<?php print $n; ?>" class="bar-arrow-container">
          <div class="long-box" style="background-color:<?php print $color ?>;"><?php print $long_text; ?></div>
          <div class="triangle--left" style="border-left-color: <?php print $color; ?>;"></div>
          <div class="narrow-box" style="color:<?php print $color; ?>"><h3><?php print $header; ?></h3> </div>
        </div>
      <?php
      }
      $n++;
    } while ($n < 10);
    ?>

    <!-- </div>END grid-items-lines -->

  </div>

<?php      
}
?>  

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/block/'.$objectData->delta.'/edit?destination=admin/content/blocks">Edit the above content</a></p>';
}
?>
<!-- end bean--9_blocks_of_text.tpl.php template -->