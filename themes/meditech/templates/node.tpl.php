<?php // This template is set up to control the display of the content on content type pages that don't have specific templates setup

  $url = $GLOBALS['base_url']; // grabs the site url

  if(node_access('update',$node)){ 
    print '<span style="font-size:12px;">'; print l( t('Edit Page'),'node/'.$node->nid.'/edit' ); print "</span>"; 
  }
?>
<!-- start node.tpl.php template -->
    
  <section class="container__centered">
	<div class="container__two-thirds">

      <?php print render($content); ?>

    </div><!-- END container__two-thirds -->
  </section>

<!-- end node.tpl.php template -->