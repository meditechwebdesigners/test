<!-- start views-view-fields--news-related-articles--block.tpl.php template -->
<?php 
  // This template is for each row of the Views block: NEWS - RELATED ARTICLES ....................... 
?>

<div class="sb-article--container">
  <div class="sb-article--left">
    <p class="sidebar--newsarticle__title no-margin"><a class="news_related_article_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></p>
  </div>
  <div class="sb-article--right"><a class="news_related_article_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['field_news_article_main_image']->content; ?></a></div>
</div>

<!-- end views-view-fields--news-related-articles--block.tpl.php template -->
