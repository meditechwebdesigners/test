<?php
/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 *
 * @ingroup themeable
 */

  // get page URL...
  $currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $currentURLarray = explode('/', $currentURL);
  $arrayendOfURL = count($currentURLarray) -1;
  $endOfURL = $currentURLarray[$arrayendOfURL];

  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- START search-results.tpl.php -->
    <?php if ($search_results): ?>
    
      <?php 
        // Get the total number of search results...
        $totalResults = $GLOBALS['pager_total_items'][0];
      ?>
           
      <section class="container__centered">
	    <div class="container__two-thirds">
         
          <h1>Search Results (<?php print $totalResults; ?>)</h1>
          
          <div class="search-results">
            <?php print $search_results; // relies on search-result.tpl.php  ?>
          </div>
          
          <?php print $pager; ?>
          
          <?php if($totalResults > 10){ // don't show Load More if less than 11 results ?>
            <div>
              <ul class="pager-load-more">
                <li><button class="load-more-ajax">Load more</button></li>
              </ul>
            </div> 
          <?php } ?>
          
          <script type="text/javascript">
            (function($){
              Drupal.behaviors.loadMoreAjax = {
                attach: function (context, settings){
                  $('.load-more-ajax', context).click(function(){
                    var nextPage = $('.pager .pager-next a').attr('href');
                    var lastPage = $('.pager .pager-last a').attr('href');
                    $.get(nextPage, function(data){
                      $(data).find('.search-results').insertBefore($('.item-list'));
                      $('.item-list .pager').remove();
                      if(nextPage == lastPage){
                        $('.load-more-ajax').remove();
                      }
                      else{
                        $(data).find('.item-list .pager').appendTo($('.item-list'));
                        Drupal.attachBehaviors($('.item-list'));
                      }
                    });
                  });
                  $('.item-list .pager').hide();
                }
              };
            })(jQuery);
          </script>
        
        </div>
        
        <aside class="container__one-third panel">
          <div class="sidebar__nav">
            <h4>Looking for customer-specific content? <br>Please <a href="https://home.meditech.com/en/d/newsroom/pages/searchresults.htm?as_q=<?php print $endOfURL; ?>"> search Customer Service</a>.</h4>
          </div>
        </aside>
        
      </section>
       
    <?php else : ?>
      
      <!-- Grey Bar -->
      <div class="container--page-title">
        <div class="container__centered">
          <h1 class="page__title--main">Search Results</h1>
        </div>
      </div>
      <!-- END Grey Bar -->
             
      <section class="container__centered">
	    <div class="container__two-thirds">
          <h2><?php print t('Your search yielded no results');?></h2>
          <?php print search_help('search#noresults', drupal_help_arg()); ?>
       </div>
      </section>
      
    <?php endif; ?>

<!-- END search-results.tpl.php -->