<?php // This template is set up to control the display of the CAMPAIGN_6_1 content type

$url = $GLOBALS['base_url']; // grabs the site url

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with mulitple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
function multi_field_collection_data($node, $field_name, $quantity){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // create array...
  $multi_field_collections = array();
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 4);
$block_8 = multi_field_collection_data($node, 'field_fc_head_ltext_1', 4);

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');

?>
<!-- start node--campaign_6_1.tpl.php template -->
 
<div class="js__seo-tool__body-content">

  <!-- BLOCK 1 --> 
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/6-1-path-background.jpg); padding-top:2em;">
    <div class="container__centered">
     <h1 style="margin-top:0;"><?php print $title; ?></h1>
     <h2 style="margin-top:0;"><?php print render($content['field_header_1']); ?></h2>
      <div class="container__one-half">
        <?php print render($content['field_long_text_1']); ?>
        <div style="margin-bottom:1em; text-align:center;">
          <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>
      <div class="container__one-half center-img">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/6-1-on-a-tablet.png" alt="MEDITECH 6.1 on a tablet">
      </div>
    </div>
  </div>
  <!-- BLOCK 1 -->


  <!-- BLOCK 2 -->
  <div class="container bg--black-coconut">
    <div class="container__centered">

      <div class="page__title--center">
        <h2 class="text--white"><?php print render($content['field_header_2']); ?></h2>
        <div class="page__title__ribbon"></div>
      </div>

      <div class="container no-pad--bottom">

       <?php $block_2 = field_get_items('node', $node, 'field_long_text_unl_1'); ?>

       <?php for($b2A=0; $b2A<3; $b2A++){ ?>
       <div class="container__one-third">
          <div class="large-numbered-list text--fresh-mint"><?php print $b2A + 1; ?></div>
          <div class="large-numbered-list--description">
            <?php print $block_2[$b2A]['value']; ?>
          </div>
        </div>
        <?php } ?>

      </div>

      <div class="container no-pad">

       <?php for($b2B=3; $b2B<6; $b2B++){ ?>
       <div class="container__one-third">
          <div class="large-numbered-list text--fresh-mint"><?php print $b2B + 1; ?></div>
          <div class="large-numbered-list--description">
            <?php print $block_2[$b2B]['value']; ?>
          </div>
        </div>
        <?php } ?>

      </div>

    </div>
  </div>
  <!-- BLOCK 2 -->


  <!-- BLOCK 3 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $quotes[0]->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $quotes[0]->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $quotes[0]->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  <!-- BLOCK 3 -->


  <!-- BLOCK 4 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctors-and-tablet-background.jpg);">
    <div class="container__centered">

      <div class="page__title--center text--white">
        <h2><?php print render($content['field_header_3']); ?></h2>
      </div>

      <div class="container">

        <?php $block_4 = field_get_items('node', $node, 'field_text_unl_1'); ?>

        <div class="container__one-third text--white">
          <p class="transparent-overlay"><?php print $block_4[0]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[1]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[2]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[3]['value']; ?></p>
        </div>

        <div class="container__one-third text--white">
          <p class="transparent-overlay"><?php print $block_4[4]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[5]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[6]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[7]['value']; ?></p>
        </div>

        <div class="container__one-third text--white">
          <p class="transparent-overlay"><?php print $block_4[8]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[9]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[10]['value']; ?></p>
          <p class="transparent-overlay"><?php print $block_4[11]['value']; ?></p>
        </div>

      </div>

    </div>
  </div>
  <!-- BLOCK 4 -->


  <!-- BLOCK 5 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/laurence-spector-md.png" alt="Laurence Spector photo">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $quotes[1]->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $quotes[1]->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $quotes[1]->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  <!-- BLOCK 5 -->


  <!-- BLOCK 6 -->
  <div class="container bg--black-coconut">
    <div class="container__centered">

      <div class="page__title--center">
        <h2 class="text--white"><?php print render($content['field_header_4']); ?></h2>
        <div class="page__title__ribbon"></div>
      </div>

      <div class="container">
        <div class="text--white center"><?php print render($content['field_long_text_3']); ?></div>
      </div>

      <?php $block_6 = field_get_items('node', $node, 'field_long_text_unl_2'); ?>    

      <div class="container__one-third">
        <div class="center"><i class="fas fa-bullhorn fa-4x" style="margin-bottom: .5em; color: #6AC4A9;"></i></div>
        <div><?php print $block_6[0]['value']; ?></div>
      </div>

      <div class="container__one-third">
        <div class="center"><i class="fas fa-user-md fa-4x" style="margin-bottom: .5em; color: #6AC4A9;"></i></div>
        <div><?php print $block_6[1]['value']; ?></div>
      </div>

      <div class="container__one-third">
        <div class="center"><i class="fas fa-bolt fa-4x" style="margin-bottom: .5em; color: #6AC4A9;"></i></div>
        <div><?php print $block_6[2]['value']; ?></div>
      </div>

    </div>
  </div>
  <!-- BLOCK 6 -->


  <!-- BLOCK 7 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="Quote bubble">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $quotes[2]->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $quotes[2]->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $quotes[2]->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  <!-- BLOCK 7 -->


  <!-- BLOCK 8 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/nurses-in-hall-background.jpg);">
    <div class="container__centered">
      <div class="page__title--center">
        <h2 class="text--white"><?php print render($content['field_header_5']); ?></h2>
        <div class="page__title__ribbon"></div>
      </div>

      <div class="container">

        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <div style="text-align: center; color: #6AC4A9; padding-bottom: 1em;"><i class="fas fa-certificate fa-4x"></i></div>
            <h4 class="page__title--center text--white"><?php print $block_8[0]->field_header_1['und'][0]['value']; ?></h4>
            <div class="text--white">
              <?php print $block_8[0]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <div style="text-align: center; color: #6AC4A9; padding-bottom: 1em;"><i class="fas fa-certificate fa-4x"></i></div>
            <h4 class="page__title--center text--white"><?php print $block_8[1]->field_header_1['und'][0]['value']; ?></h4>
            <div class="text--white">
              <?php print $block_8[1]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <div style="text-align: center; color: #6AC4A9; padding-bottom: 1em;"><i class="fas fa-certificate fa-4x"></i></div>
            <h4 class="page__title--center text--white"><?php print $block_8[2]->field_header_1['und'][0]['value']; ?></h4>
            <div class="text--white">
              <?php print $block_8[2]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <div style="text-align: center; color: #6AC4A9; padding-bottom: 1em;"><i class="fas fa-certificate fa-4x"></i></div>
            <h4 class="page__title--center text--white"><?php print $block_8[3]->field_header_1['und'][0]['value']; ?></h4>
            <div class="text--white">
              <?php print $block_8[3]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- BLOCK 8 -->


  <!-- BLOCK 9 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="Quote bubble">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $quotes[3]->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $quotes[3]->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $quotes[3]->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  <!-- BLOCK 9 -->

</div><!-- end js__seo-tool__body-content -->

<!-- BLOCK 10 -->
<div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
  <div class="container__centered center">
   
    <h2 class="text--white js__seo-tool__body-content"><?php print render($content['field_header_6']); ?></h2>
    
    <?php 
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
      <div class="button--hubspot">
        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
      </div>
    <?php }else{ ?>
      <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
    <?php } ?>
        
    <div class="sharethis-centered">
      <?php
        $block = block_load('sharethis', 'sharethis_block');
        $render_array = _block_get_renderable_array(_block_render_blocks(array($block)));
        print render($render_array);
      ?>
    </div>
    
  </div>
</div>

<!-- BLOCK 10 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
        
<!-- end node--campaign_6_1.tpl.php template -->