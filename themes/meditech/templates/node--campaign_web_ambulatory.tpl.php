<?php // This template is set up to control the display of the CAMPAIGN_WEB_AMBULATORY content type

$url = $GLOBALS['base_url']; // grabs the site url

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with mulitple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
function multi_field_collection_data($node, $field_name, $quantity){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // create array...
  $multi_field_collections = array();
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}

$url = $GLOBALS['base_url']; // grabs the site url

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_web_ambulatory.tpl.php template -->

<div class="js__seo-tool__body-content">

  <!-- BLOCK 1 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/web-amb-bg.jpg);">
    <div class="container__centered">

      <h1 style="margin-top:0;"><?php print $title; ?></h1>
      <h2><?php print render($content['field_header_1']); ?></h2>

      <div class="container no-pad">
        <div class="container__two-thirds">
          <?php print render($content['field_long_text_1']); ?>
        </div>
        <div class="container__one-third">
          <div class="no-margin--top center" style="margin-bottom:1em;">
            <?php
            // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
            if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
              $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
            ?>
              <div class="button--hubspot">
                <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
              </div>
            <?php }else{ ?>
              <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
            <?php } ?>
          </div>
        </div>
      </div>

      <div class="center">
        <img style="max-width: 70%;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--home-screen.png" alt="MEDITECH Web Ambulatory">
      </div>

    </div>
  </div>
  <!-- BLOCK 1 -->

  <!-- VIDEO BLOCK  -->
    <div class="content__callout">
      <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
          <div class="video js__video" data-video-id="239126049">
            <figure class="video__overlay">
              <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/meditech-web-ambulatory-video-still.jpg" alt="MEDITECH Web Ambulatory Video Still">
            </figure><!-- .video__overlay -->
            <a class="video__play-btn" href="http://vimeo.com/239126049"></a>
            <div class="video__container">
            </div><!-- .video__container -->
          </div><!-- .video -->
        </div><!-- .content__callout__image-wrapper -->
      </div><!-- .content__callout__media -->
      <div class="content__callout__content">
        <div class="content__callout__body">
          <div class="content__callout__body__text">
           <h2>What are your favorite parts of Ambulatory?</h2>
            <p>Our Ambulatory EHR brings provider usability, mobility, and flexibility to a whole <a href="https://ehr.meditech.com/ehr-solutions/transformative-technology">new level</a>. With instant access to the complete patient record <a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">across the continuum</a>, physicians can spend less time navigating technology and more time connecting with patients. The only hard part is trying to decide which of its many benefits is your favorite.</p>
          </div><!-- .content__callout__body__text -->
        </div><!-- .content__callout__body -->
      </div><!-- .content__callout__content -->
    </div><!-- .content__callout -->
  <!-- END VIDEO BLOCK -->


  <!-- BLOCK 2 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--home-screen.png" alt="MEDITECH Web Ambulatory Patient Screen">
          </figure>
        </div>
        <div class="container__one-third">
          <h2><?php print render($content['field_header_2']); ?></h2>
          <div>
            <?php print render($content['field_long_text_2']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 2 -->


  <!-- BLOCK 3 -->
  <div class="container bg--black-coconut">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__one-third">
          <h2><?php print render($content['field_header_3']); ?></h2>
          <div>
            <?php print render($content['field_long_text_3']); ?>
          </div>
        </div>
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--Patient-Chart-Summary.png" alt="MEDITECH Web Ambulatory Patient Screen">
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 3 -->


  <!-- BLOCK 4 -->
  <div class="container">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--Registry-screen.png" alt="MEDITECH Web EHR Registry Screenshot">
          </figure>
        </div>
        <div class="container__one-third">
          <h2><?php print render($content['field_header_4']); ?></h2>
          <div>
            <?php print render($content['field_long_text_4']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 4 -->


  <!-- BLOCK 5 -->
  <div class="container bg--black-coconut">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__one-third">
          <h2><?php print render($content['field_header_5']); ?></h2>
          <div>
            <?php print render($content['field_long_text_5']); ?>
          </div>
        </div>
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--Office-Manager-Home.png" alt="MEDITECH Web Ambulatory Patient Screen">
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 5 -->


  <!-- BLOCK 6 -->
  <div class="container">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--Scheduling-Grid.png" alt="MEDITECH Web Ambulatory Patient Screen">
          </figure>
        </div>
        <div class="container__one-third">
          <h2><?php print render($content['field_header_6']); ?></h2>
          <div>
            <?php print render($content['field_long_text_6']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 6 -->


  <!-- BLOCK 7 -->
      <!-- VIDEO SECTION ---------------------------------------------------------------- -->
      <?php
        // if video exists for node, then proceed to render DIV ======================================================
        if(!empty($content['field_video_url'])){
          $videoID = field_get_items('node', $node, 'field_video_url');
          print views_embed_view('video', 'block', $videoID[0]['value']); // adds 'video' Views block...
        }
      ?>
  <!-- BLOCK 7 -->


  <!-- BLOCK 8 -->
  <?php $block_quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3); ?>
  <div class="container bg--emerald">
    <div class="container__centered">
      <?php
        // add font awesome icons to array...
        $fa = array('far fa-clock','fas fa-sitemap','fas fa-bullseye');
        $f=0;
        foreach($block_quotes as $block_quote){
      ?>
      <div class="container__one-third">
        <div class="quote__content__icon">
          <i class="<?php print $fa[$f]; ?> fa-4x"></i>
        </div>
        <div class="quote__content__text">
          <p><?php print $block_quote->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="no-margin--bottom"><strong><?php print $block_quote->field_full_name['und'][0]['value']; ?></strong></p>
        <p class="text--small"><?php print $block_quote->field_company['und'][0]['value']; ?></p>
      </div>
      <?php
        $f++;
        }
      ?>
    </div><!-- .container__centered -->
  </div><!-- .container--key-west -->
  <!-- BLOCK 8 -->

</div><!-- end js__seo-tool__body-content -->

<!-- BLOCK 9 -->
<div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
  <div class="container__centered center">

    <h2 class="js__seo-tool__body-content"><?php print render($content['field_header_7']); ?></h2>
    <?php
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
      <div class="button--hubspot">
        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
      </div>
    <?php }else{ ?>
      <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
    <?php } ?>

 

  </div>
</div>
<!-- BLOCK 9 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- end node--campaign_web_ambulatory.tpl.php template -->