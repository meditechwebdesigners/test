<!-- node 2913 - Toolkit Icons tests -->
            
  <section class="container__centered">

    <h1 id="jobs"><?php print $title; ?></h1>

    <dl style="list-style-type:none; display:flex; flex-wrap:wrap; align-items:center; justify-content:flex-start;">
    <?php print render($content['field_body']); ?>
    </dl>
    
    
    
    <?php 
    // use same function as in campaigns to pull field collection data where they have unlimited quantity...
    $toolkit_icons = multi_field_collection_data_unrestricted($node, 'field_fc_toolkit_icons_unl'); 
    ?>
    
    <?php if( !empty($toolkit_icons[0]) ){ // check to see if field collection is empty before creating HTML ?>
      <dl style="list-style-type:none; display:flex; flex-wrap:wrap; align-items:center; justify-content:flex-start;">
        <?php 
        // count how many instances of the field collection there are...
        $num = count($toolkit_icons);
        // loop through each instance...
        for($ti=0; $ti<$num; $ti++){
          // grab the taxonomy data to know what icon to use...
          $taxonomy_term = $toolkit_icons[$ti]->field_landing_page_type['und'][0]['taxonomy_term'];
          // grab the actual term they chose from that taxonomy...
          $term_name = $taxonomy_term->name;
          // generate different HTML based on which term they chose...
          if( $term_name == 'Case Study' ){
            print '<dt style="flex-basis: 20%; margin-bottom: 1em;"><img alt="Case Study Icon" src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/Case-Study--Icon.png"></dt><dd style="flex-basis: 80%; margin-bottom: 1em;">';
          }
          else{
            print '<dt style="flex-basis: 20%; margin-bottom: 1em;"><img alt="Video Icon" src="https://ehr.meditech.com/sites/all/themes/meditech/images/ehr-solutions/Video--Icon.png"></dt><dd style="flex-basis: 80%; margin-bottom: 1em;">'; 
          }
          // generate text link...
          print '<a href="'.$toolkit_icons[$ti]->field_link_url_1['und'][0]['value'].'">'.$toolkit_icons[$ti]->field_link_text_1['und'][0]['value'].'</a>';
          // close the second DD tag...
          print '</dd>';
        } // end of loop 
        ?>
      </dl>
    <?php } // if field collection is empty, do nothing ?>
    
  </section>
        