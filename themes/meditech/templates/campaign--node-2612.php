<!-- START campaign--node-2612.php -->

<?php // This template is set up to control the display of the 50th Anniversary content type
  
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
?>

<style>
  /*  Employee Bubbles */
  .eb-1 {
    background-image: url(<?php print $url;
    ?>/sites/all/themes/meditech/images/campaigns/Nhien-Bui.jpg);
    background-position: center top;
  }

  .eb-2 {
    background-image: url(<?php print $url;
    ?>/sites/all/themes/meditech/images/campaigns/Galvin-Chow.jpg);
    background-position: center top;
    margin-left: auto;
  }

  .eb-3 {
    background-image: url(<?php print $url;
    ?>/sites/all/themes/meditech/images/campaigns/Kim-Fischer.jpg);
    background-position: center top;
  }

  .eb-4 {
    background-image: url(<?php print $url;
    ?>/sites/all/themes/meditech/images/campaigns/Dr-Larry-Spector.jpg);
    background-position: center top;
    margin: auto;
  }

  .eb-5 {
    background-image: url(<?php print $url;
    ?>/sites/all/themes/meditech/images/campaigns/Caryn-Budd.jpg);
    background-position: center top;
    margin: auto;
  }

  .eb-6 {
    background-image: url(<?php print $url;
    ?>/sites/all/themes/meditech/images/campaigns/Javier-Guzman.jpg);
    background-position: center top;
    margin-left: auto;
  }

  .employee-bubble {
    width: 250px;
    height: 250px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    cursor: pointer;
    box-shadow: inset 0 0 0 0 rgba(0, 188, 111, 0.95);
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }

  .employee-bubble:hover .eb-info {
    opacity: 1;
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -o-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  .employee-bubble:hover {
    box-shadow: inset 0 0 0 135px rgba(0, 188, 111, 0.95);
  }

  .employee-card {
    padding-bottom: 1em;
  }

  .employee-card-plus {
    padding-top: 3em;
    padding-bottom: 1em;
  }

  .eb-info {
    position: absolute;
    width: 250px;
    height: 250px;
    border-radius: 50%;
    opacity: 0;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -o-transition: all 0.4s ease-in-out;
    -ms-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
    -webkit-transform: scale(0);
    -moz-transform: scale(0);
    -o-transform: scale(0);
    -ms-transform: scale(0);
    transform: scale(0);
    -webkit-backface-visibility: hidden;
  }

  .eb-info h3 {
    color: #fff;
    position: relative;
    padding: 80px 19px 5px 0;
    text-align: center;
  }

  .eb-info p {
    color: #fff;
    position: relative;
    padding: 0px 19px 0 0;
    text-align: center;
  }

  .slick-slide {
    padding: .5% 1.5% 0 1.5% !important;
  }

  .transparent-overlay {
    padding: 1em 2em;
  }

  /*  Modal Window */

  .Modal {
    position: absolute;
    z-index: 99;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, 0);
    visibility: hidden;
  }

  .Modal .content {
    position: absolute;
    left: 50%;
    top: 35%;
    max-width: 1050px;
    width: 100%;
    padding: 3em 2em 3em 2em;
    border-radius: 6px;
    background: #fff;
    transform: translate(-50%, -30%) scale(0);
  }

  .Modal .close {
    position: absolute;
    top: 5px;
    right: 18px;
    cursor: pointer;
    color: #3e4545;
  }

  .Modal .close:before {
    content: '\2715';
    font-size: 30px;
  }

  .Modal.is-visible {
    visibility: visible;
    background: rgba(0, 0, 0, 0.75);
    transition: background 0.35s;
    transition-delay: 0.1s;
  }

  .Modal.is-visible .content {
    position: fixed;
    top: 55%;
    left: 50%;
    transform: translate(-50%, -50%) scale(1);
    transition: transform 0.35s;
  }

  .modal-employee {
    width: 200px;
    height: 200px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    margin: auto;
    margin-top: 1em;
    background-size: 115%;
  }

  /*  Collage Banner */

  .collage-banner {
    opacity: 1;
    filter: alpha(opacity=100);
    /* For IE8 and earlier */
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
  }

  .collage-banner:hover {
    opacity: 0;
    filter: alpha(opacity=0);
    /* For IE8 and earlier */
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
  }

  .logo-position {
    margin-left: 13%;
  }
  
  .logo-box-height {
      height: 200px;
    }

  @media all and (max-width: 104.688em) {
    .logo-position {
      margin-left: 8%;
    }
  }

  @media all and (max-width: 93.75em) {
    .logo-position {
      margin-left: 8%;
    }
  }

  @media all and (max-width: 90.625em) {
    .logo-position {
      margin-left: 2%;
    }
  }
  
  @media all and (max-width: 82em) {
    .logo-position {
      margin-left: 6%;
    }
    .logo-box-height {
      height: 100px;
    }
  }
  
  @media all and (max-width: 78.125em) {
    .logo-position {
      margin-left: 2%;
    }
    .logo-box-height {
      height: 50px;
    }
  }
  
  @media all and (max-width: 68.750em) {
    .logo-position {
      display: none;
    }
    .logo-box-height {
      height: 200px;
    }
  }
  
  @media all and (max-height: 700px) {
    .Modal.is-visible .content {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 90%;
      -webkit-overflow-scrolling: touch;
      border-radius: 0;
      transform: scale(1);
      margin-top: 4em;
      max-width: 100%;
      overflow-y: auto;
      padding-bottom: 6em;
    }
  }

  @media all and (max-width: 50em) {
    .Modal.is-visible .content {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      -webkit-overflow-scrolling: touch;
      border-radius: 0;
      transform: scale(1);
      margin-top: 5em;
      overflow-y: auto;
      padding-bottom: 6em;
    }

    .employee-card {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      margin-bottom: 1em;
    }

    .employee-card-plus {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      padding-top: 0;
      margin-bottom: 1em;
    }

    .employee-bubble {
      margin: auto;
      margin-bottom: 1em;
    }

</style>



<!-- Block 1 -->
<div class="background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Collage.jpg);">
  <div class="background--cover collage-banner hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Collage--bw.jpg);">
    <div class="container__centered">
      <div class="container center">
        <div style="height:450px;"></div>
      </div>
    </div>
  </div>
</div>
<!-- End Block 1 -->

<!-- Block 2 - Video -->
<div class="content__callout">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper">
      <div class="video js__video" data-video-id="305095475">
        <figure class="video__overlay">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--50th-Neil-Pappalardo.jpg" alt="50th Anniversary Video">
        </figure>
        <a class="video__play-btn" href="https://vimeo.com/305095475"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2>50 years of bold innovation.</h2>
        <p>A. Neil Pappalardo founded MEDITECH with the revolutionary belief that computers could transform the way we care for one another. Five decades later, that innovative spark burns brighter than ever. MEDITECH has stood at the vanguard of healthcare IT since Day One, embracing whatever changes the industry throws at us, and finding new ways to improve healthcare for patients, providers, and organizations everywhere. The most exciting part? We feel like we’re just getting started.</p>
      </div>
    </div>
  </div>
</div>
<!-- End Block 2 - Video -->

<!-- Start Block 3 -->
<div class="container background--cover border-none" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Employee-Bubbles-Block.jpg); background-position: top; padding-bottom: 8em;">

  <div class="container__centered auto-margins center text--white" style="padding-bottom:2em;">
    <h2>More than software.</h2>
    <p>Healthcare is all about people, and MEDITECH is no exception. We may be best known for our solutions, but our identity runs deeper than that. Meet some of the unique people behind your EHR.</p>
  </div>


  <div class="container__centered" style="padding-bottom:2em;">

    <div class="container__one-third">
      <a href="#Popup1" class="button">
        <div class="employee-bubble eb-1">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Nhien Bui</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup1" class="Modal">
        <div class="content">
          <div>

            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-1"></div>
              <h3 class="no-margin--bottom">Nhien Bui</h3>
              <p class="no-margin--bottom">Product Manager</p>
              <p>Years at MEDITECH: <strong>2</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
              <p>Ever-changing.</p>
              <h4 class="text--meditech-green no-margin--bottom">What does MEDITECH mean to you?</h4>
              <p>Collaboration.</p>
              <h4 class="text--meditech-green no-margin--bottom">What was your first car?</h4>
              <p>Toyota Cressida.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite TV show to binge-watch?</h4>
              <p>Curb Your Enthusiasm, Gotham, and Veep.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the most interesting place you’ve traveled to?</h4>
              <p>Falmouth luminous lagoon in Jamaica.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>


    <div class="container__one-third">

      <a href="#Popup2" class="button">
        <div class="employee-bubble eb-4">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Dr. Larry Spector</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup2" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-4"></div>
              <h3 class="no-margin--bottom">Dr. Larry Spector</h3>
              <p class="no-margin--bottom">Physician Informaticist</p>
              <p>Years at MEDITECH: <strong>6</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
              <p>Like family without the drama!</p>
              <h4 class="text--meditech-green no-margin--bottom">Describe your most impactful experience at MEDITECH.</h4>
              <p>My involvement at the ground level in the development/design and deployment of the Expanse ED application.</p>
              <h4 class="text--meditech-green no-margin--bottom">What would you tell your best friend about MEDITECH?</h4>
              <p>We have 5 cent sodas and free coffee, beautiful work areas/buildings — what else is there??</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the best advice you've ever received?</h4>
              <p>From my Turkish grandmother: "Tomorrow is another day!”</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

    <div class="container__one-third">

      <a href="#Popup3" class="button">
        <div class="employee-bubble eb-2">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Galvin Chow</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup3" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-2"></div>
              <h3 class="no-margin--bottom">Galvin Chow</h3>
              <p class="no-margin--bottom">Supervisor, Client Support</p>
              <p>Years at MEDITECH: <strong>13</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
              <p>A great place to work that is staffed with people who are passionate about helping healthcare organizations improve patient care.</p>
              <h4 class="text--meditech-green no-margin--bottom">If you could have one superpower, what would it be?</h4>
              <p>Mind reading!</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your Alma Mater?</h4>
              <p>UMass Amherst.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite outdoor activity?</h4>
              <p>Running.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite breakfast cereal?</h4>
              <p>Banana Nut Crunch.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

  </div>

  <div class="container__centered">

    <div class="container__one-third">
      <a href="#Popup4" class="button">
        <div class="employee-bubble eb-3">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Kim Fischer</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup4" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card-plus">
              <div class="modal-employee eb-3"></div>
              <h3 class="no-margin--bottom">Kim Fischer</h3>
              <p class="no-margin--bottom">Director, Client Support</p>
              <p>Years at MEDITECH: <strong>14</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">What does MEDITECH mean to you?</h4>
              <p>MEDITECH was my first "real" job out of college and has provided great opportunities for me, both in terms of career growth and in developing long-lasting friendships with many of my co-workers over the years.</p>
              <h4 class="text--meditech-green no-margin--bottom">How has healthcare IT made an impact on you personally?</h4>
              <p>It has helped me understand the healthcare system as a whole, which helps me know what to expect for my personal or family member's treatments.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the one item you would take to a deserted island?</h4>
              <p>A boat.</p>
              <h4 class="text--meditech-green no-margin--bottom">What famous person would you most like to have dinner with?</h4>
              <p>Ellen DeGeneres so I can become friends with her and she can invite me to her birthday parties. Or, if not being completely superficial, Nelson Mandela. :-)</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the best thing you’ve eaten in the MEDITECH cafeteria?</h4>
              <p>Ice cream sundaes during summer. </p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

    <div class="container__one-third">
      <a href="#Popup5" class="button">
        <div class="employee-bubble eb-5">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Caryn Budd</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup5" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card-plus">
              <div class="modal-employee eb-5"></div>
              <h3 class="no-margin--bottom">Caryn Budd</h3>
              <p class="no-margin--bottom">Account Manager</p>
              <p>Years at MEDITECH: <strong>22</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
              <p>So much more than a ‘job’, it’s been an exciting, challenging career and journey!</p>
              <h4 class="text--meditech-green no-margin--bottom">What is a surprising fact about yourself?</h4>
              <p>I was the first girl in Randolph (1978) to play Little League (opposed to softball), I just didn't want to play with the girls ( I was a complete tomboy).</p>
              <h4 class="text--meditech-green no-margin--bottom">Where is the most interesting place you've traveled to?</h4>
              <p>Switzerland...especially Zermatt and taking the tram to the top and staring out at the Matterhorn that stands 1500 ft. tall.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your most prized possession?</h4>
              <p>Well besides my son, I would have to say my grandmothers diamond originally purchased in the 1920s. Also my grandfather's desk purchased in the 1930s.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite part of the MEDITECH picnic?</h4>
              <p>Seeing friends/coworkers from the many years I have worked here and no longer see on a regular basis. Especially seeing how everyone's kids have gone from strollers to now being all grown up!</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

    <div class="container__one-third">
      <a href="#Popup6" class="button">
        <div class="employee-bubble eb-6">
          <div class="eb-info">
            <h3 class="no-margin--bottom">Javier Guzmán</h3>
            <p>Learn more</p>
          </div>
        </div>
      </a>

      <div id="Popup6" class="Modal">
        <div class="content">
          <div>
            <div class="container__one-third center employee-card">
              <div class="modal-employee eb-6"></div>
              <h3 class="no-margin--bottom">Javier Guzmán</h3>
              <p class="no-margin--bottom">Senior Marketing Consultant</p>
              <p>Years at MEDITECH: <strong>9</strong></p>
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
            </div>
            <div class="container__two-thirds">
              <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
              <p>A great company to work for.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is the biggest change in healthcare you’ve seen in recent years?</h4>
              <p>Meaningful Use. </p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite sports team?</h4>
              <p>New England Patriots.</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite part of the MEDITECH picnic?</h4>
              <p>Free food!</p>
              <h4 class="text--meditech-green no-margin--bottom">What is your favorite band/artist today?</h4>
              <p>Daddy Yankee and Bad Bunny.</p>
            </div>

          </div>
          <div class="close"></div>
        </div>
      </div>
    </div>

  </div>

</div>
<!-- End Block 3 -->

<!-- Start Block 4 -->
<div class="container background--cover border-none" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/quote-bg--starry-northern-lights.jpg); background-position: top;">
  <div class="slider text--white" style="padding-bottom:2em;">
   <div class="container">
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">"From its beginnings, MEDITECH has embraced a family-oriented culture that makes caring for people a priority. It's reflected in the software we create, and it's something that we've continued to instill in our employees as our company has grown. Whether it's giving back to our local communities or advancing healthcare on the global scale, we feel lucky to be in the position to improve people's lives." </p>
        <p class="text--large no-margin--bottom"><span class="bold">Lawrence A. Polimeno</span></p>
        <p>Vice Chairman, MEDITECH</p>
      </div>
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">"Many hospitals, like ours, have been with MEDITECH for 20+ years. I think that's unique in the industry. MEDITECH really empowers their customers at both the corporate level -- asking for input on product development -- and the more local level, educating the users as part of the implementation to be able to make changes they need to make on their own. It's been very gratifying for our users and our organization."</p>
        <p class="text--large no-margin--bottom"><span class="bold">Joe Farr, RN</span></p>
        <p>King's Daughters Medical Center</p>
      </div>
    </div>
    <div class="container">
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">“Fifty years later, we are still just people with a desire <a href="https://ehr.meditech.com/about-meditech/community">to help care for others</a>. The possibilities of technology will continue to move far beyond what even the young teams of today can envision. But the goal remains the same. Giving caregivers back their home life. Helping patients take <a href="https://ehr.meditech.com/ehr-solutions/to-improve-patient-engagement-focus-on-the-patient">control of their own health</a>. Putting people first.”</p>
        <p class="text--large no-margin--bottom"><span class="bold">Howard Messing</span></p>
        <p>President &amp; CEO, MEDITECH</p>
      </div>
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">“Throughout our 25-year relationship, we’ve seen healthcare undergo massive shifts. MEDITECH has always found ways to evolve with us, support new strategic directions, and deliver results. As our two organizations have grown, the dedication to this partnership and achieving our common goals has only deepened. The foundation of such a relationship is trust.”</p>
        <p class="text--large no-margin--bottom"><span class="bold">P. Martin Paslick</span></p>
        <p>Senior Vice President and CIO, HCA</p>
      </div>
    </div>
    <div class="container">
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">"In the beginning we had no great and far reaching plan. Each event, every decision, a success here, a mistake there, would take us another step forward."</p>
        <p class="text--large no-margin--bottom"><span class="bold">A. Neil Pappalardo</span></p>
        <p>Chairman &amp; Founder, MEDITECH</p>
      </div>
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">“From MAGIC, to C/S, to Expanse, MEDITECH has always found ways to bring something new to the table. Over the course of our 30-year partnership, they’ve provided Avera Health with EHR solutions that have helped us to grow into a multi-state regional healthcare system. They understand what an organization needs from their EHR, and what providers and patients want from it.”</p>
        <p class="text--large no-margin--bottom"><span class="bold">Jim Veline</span></p>
        <p>Senior VP and CIO, Avera Health</p>
      </div>
    </div>
    <div class="container">
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">“I always feel MEDITECH’s focus is on us, the customer, and the patients that we care for. It’s not about next quarter’s earnings. It’s about delivering a long-term investment, and building a long-term relationship with your partners that matters.”</p>
        <p class="text--large no-margin--bottom"><span class="bold">Denni McColm</span></p>
        <p>CIO, Citizens Memorial Hospital</p>
      </div>
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">“I think MEDITECH listens to their customers and their needs. They recognize that physicians and nurses are experts too, and they’ve evolved their product in a way that makes customers say, ‘You know, they heard what we wanted, and found a creative way to build a system that works best for us.’ That’s something that sets MEDITECH apart from other vendors.”</p>
        <p class="text--large no-margin--bottom"><span class="bold">Bruce McNulty, MD</span></p>
        <p>VP and CMO, Swedish Covenant Hospital</p>
      </div>
    </div>
    <div class="container">
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">“From the early days of our MIIS install (yes, I remember that), MEDITECH has always had a vision of the future. Technology has changed in ways we could never dream of, and yet MEDITECH has been a leader throughout this journey. Once again, they are on the cutting edge as they introduce Expanse and new ways to envision healthcare data. It is hard to believe it has been 50 years, or that I have had the pleasure of working with Neil, Howard and the team for over 34 years. Congratulations on 50 years of innovation and customer service!”</p>
        <p class="text--large no-margin--bottom"><span class="bold">Mark Farrow</span></p>
        <p>VP HITS, CIO, Hamilton Health Science</p>
      </div>
      <div class="container__one-half transparent-overlay">
        <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
        <p class="italic">“As a customer and consultant, there’s a family component to MEDITECH. It’s not just software. It’s not just delivering products. It’s the experience. Expanse is designed with a thoughtfulness that shows that MEDITECH is paying attention to their customers and how they interact and grow with the platform. It’s like family — you know that MEDITECH is going to take care of you.”</p>
        <p class="text--large no-margin--bottom"><span class="bold">William Gustin, MD</span></p>
        <p>MEDITECH</p>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick.css">
<link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick-theme.css">
<script src="<?php print $url; ?>/sites/all/themes/meditech/js/slick.min.js"></script>
<script>
  jQuery(document).ready(function($) {
    $('.slider').slick({
      dots: true,
      prevArrow: '<a class="slick-prev">&#10092;</a>',
      nextArrow: '<a class="slick-next">&#10093;</a>',
    });
  });

</script>
<!-- End Block 4 -->

<!-- Start Block 5 -->
<!--
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/50th-group-photo--centered.jpg);">
  <div class="container__centered text--white">
    <div class="container transparent-overlay center" style="margin-top:3em;">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/50th-anniversary-logo.png" style="width:225px; margin-bottom:1em;" alt="MEDITECH 50th Logo">
      <p class="text--large">50 years in healthcare has given us so much to celebrate, and we want <span class="italic">you</span> to be part of the conversation. Use the hashtag <span class="bold">#MEDITECH50</span> on social media, and join the celebration!</p>
    </div>
  </div>
</div>
-->
<div class="container background--cover border-none" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/50th-group-photo--centered.jpg);">
  <div class="container no-pad--top logo-position">
    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/50th-anniversary-logo--white.png" style="width:175px; margin-bottom:1em;" alt="MEDITECH 50th Logo">
  </div>
  <div class="container__centered logo-box-height">&nbsp;</div>
  <div class="container__centered text--white">
    <div class="container transparent-overlay center" style="margin: 3em auto 0 auto; max-width: 57em;">
      <p class="text--large" style="margin: 0;">50 years in healthcare has given us so much to celebrate, and we want <span class="italic">you</span> to be part of the conversation. Use <span class="bold header-four">#MEDITECH50</span> on social media, and join the celebration!</p>
      
      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
      
    </div>
  </div>
</div>

<!-- End Block 5 -->
<script>
  $jq.fn.expose = function(options) {
    var $modal = $jq(this),
      $trigger = $jq("a[href=" + this.selector + "]");
    $modal.on("expose:open", function() {
      $modal.addClass("is-visible");
      $modal.trigger("expose:opened");
    });
    $modal.on("expose:close", function() {
      $modal.removeClass("is-visible");
      $modal.trigger("expose:closed");
    });
    $trigger.on("click", function(e) {
      e.preventDefault();
      $modal.trigger("expose:open");
    });
    $modal.add($modal.find(".close")).on("click", function(e) {

      e.preventDefault();

      // if it isn't the background or close button, bail
      if (e.target !== this)
        return;

      $modal.trigger("expose:close");
    });
    return;
  }
  $jq("#Popup1").expose();
  $jq("#Popup2").expose();
  $jq("#Popup3").expose();
  $jq("#Popup4").expose();
  $jq("#Popup5").expose();
  $jq("#Popup6").expose();

  // Example Cancel Button
  $jq(".cancel").on("click", function(e) {

    e.preventDefault();
    $jq(this).trigger("expose:close");
  });

</script>

<!-- END campaign--node-2612.php -->
