<!-- start user-register-form.tpl.php template -->
  <div class="container--page-title">
    <div class="container__centered">
      <p class="page__title--main">Welcome to MEDITECH.com</p>
    </div>
  </div>

  <section class="container__centered">
	<div class="container__two-thirds">
      <div>
        <?php print drupal_render_children($form) ?>
      </div>
    </div>
  </section>  
<!-- end user-register-form.tpl.php template -->
