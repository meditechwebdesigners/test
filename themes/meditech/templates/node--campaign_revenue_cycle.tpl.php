<?php // This template is set up to control the display of the CAMPAIGN REVENUE CYCLE content type

$url = $GLOBALS['base_url']; // grabs the site url
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_revenue_cycle.tpl.php template -->


<div class="js__seo-tool__body-content">

  <!-- Block 1 -->
  <div class="container background--cover hide__bg-image--mobile" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Keep-Consumers-at-the-Center-of-Your-Revenue-Cycle.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds">
        <h1 class="no-margin js__seo-tool__title"><?php print render($content['field_header_1']); ?></h1>
        <div class="">
          <?php print render($content['field_long_text_1']); ?>
        </div>
        <div class="btn-holder--content__callout">
          <?php
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 1 -->


  <!-- Block 2 -->
  <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
      <div class="container no-pad">

        <div class="container__one-half">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Patient-Accounting-Integration-diagram.png" alt="patient accounting integration diagram">
          </figure>
        </div>

        <div class="container__one-half">
          <h2><?php print render($content['field_header_2']); ?></h2>
          <div>
            <?php print render($content['field_long_text_2']); ?>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- Close Block 2 -->


  <!-- Block 3 -->
  <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Revenue-cycle-is-about-more-than-just-billing.jpg);">
    <div class="container__centered">
      <div class="container no-pad">

        <div>
          <div class="container__two-thirds">
            <h2 class="text--white"><?php print render($content['field_header_3']); ?></h2>
            <div class="text--white">
              <?php print render($content['field_long_text_3']); ?>
            </div>
          </div>
          <div class="container__one-third"></div>
        </div>

        <style>
          .tighter-bullets li{
            margin-bottom: 0;
          }
        </style>

        <div class="container__one-third text--white">
          <h3 class="text--white"><?php print render($content['field_header_4']); ?></h3>
          <?php print render($content['field_long_text_4']); ?>
        </div>

        <div class="container__one-third text--white">
          <h3 class="text--white"><?php print render($content['field_header_5']); ?></h3>
          <?php print render($content['field_long_text_5']); ?>
        </div>

        <div class="container__one-third text--white">
          <h3 class="text--white"><?php print render($content['field_header_6']); ?></h3>
          <?php print render($content['field_long_text_6']); ?>
        </div>

      </div>
    </div>
  </div>
  <!-- Close Block 3 -->


  <!-- Block 4 -->
  <div class="container bg--emerald">
    <div class="container__centered text--white">
      <h2 class="no-margin--top"><?php print render($content['field_header_7']); ?></h2>
      <div>
        <?php print render($content['field_long_text_7']); ?>
      </div>
      <div class="container__one-half text--white">
        <div class="transparent-overlay">
          <h3><?php print render($content['field_header_8']); ?></h3>
          <?php print render($content['field_long_text_8']); ?>
        </div>
      </div>
      <div class="container__one-half text--white">
        <div class="transparent-overlay">
          <h3><?php print render($content['field_header_9']); ?></h3>
          <?php print render($content['field_long_text_9']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 4 -->


  <!-- BLOCK 5 -->
  <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered">
      <div class="container__one-third text--white">
        <h2 class="text--white"><?php print render($content['field_header_10']); ?></h2>
        <div>
          <?php print render($content['field_long_text_10']); ?>
        </div>
      </div>
      <div class="container__two-thirds">
        <figure style="margin-top: 1em;">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Revenue-Cycle-Management--screenshot.png" alt="MEDITECH Revenue Cycle screenshot">
        </figure>
      </div>
    </div>
  </div>
  <!-- Close Block 5 -->


  <!-- Block 6 -->
  <?php $block_6 = field_collection_data($node, 'field_fc_quote_1'); ?>
  <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Melissa-Swanfeldt.png" alt="headshot of Melissa Swanfeldt, AVP MEDITECH">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $block_6->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $block_6->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $block_6->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  <!-- Close Block 6 -->


  <!-- Block 7 -->
  <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Harness-the-power-of-your-revenue-cycle-management.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white">
        <h2 class="no-margin--top"><?php print render($content['field_header_11']); ?></h2>
        <div>
          <?php print render($content['field_long_text_11']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 7 -->


  <!-- Block 8 -->
  <?php $block_8 = field_collection_data($node, 'field_fc_quote_2'); ?>
  <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble graphic">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p><?php print $block_8->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $block_8->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $block_8->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  <!-- Close Block 8 -->


  <!-- BLOCK 9 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <div class="container__two-thirds">
        <figure style="margin-top: 1em;">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/FSD-RVU_BAR.jpg" alt="MEDITECH Revenue Cycle screenshot">
        </figure>
      </div>
      <div class="container__one-third text--white">
        <h2 class="text--white"><?php print render($content['field_header_12']); ?></h2>
        <div>
          <?php print render($content['field_long_text_12']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 9 -->


  <!-- BLOCK 10 -->
  <div class="container bg--black-coconut" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered center">
      <h2 class="text--white center"><?php print render($content['field_header_13']); ?></h2>
      <div class="btn-holder--content__callout">
        <?php
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
          <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
          </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
      </div>


      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
    </div>
  </div>
  <!-- Close Block 10 -->


</div><!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- end node--campaign_revenue_cycle.tpl.php template -->