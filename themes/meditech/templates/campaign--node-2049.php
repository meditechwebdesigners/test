<!-- START campaign--node-2049.php -->
<?php // This template is set up to control the display of the IMPROVE PATIENT ENGAGEMENT campaign

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>

<div class="js__seo-tool__body-content">

<!--Block 1-->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/doctor-performing-eye-exam-on-child.jpg);">
	<div class="container__centered">
		<div class="container__one-half">
			&nbsp;
		</div>
		<div class="container__one-half transparent-overlay text--white">
			<h1 class="text--white js__seo-tool__title"><?php print $title; ?></h1>
			<p>For patients, convenient access to information and the care team is no longer a luxury, but a necessity. Empower them to take charge of their health and engage them with MEDITECH's EHR solutions. <a href="https://info.meditech.com/case-study-ontario-shores-advances-patient-engagement-with-meditech-0">Whether connecting with patients through our portal</a> or using Expanse to review results and care plans during visits, you'll be able to provide the empathetic care experience they need and deserve.</p>
			<div style="text-align:center;">
				<?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
          <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
          </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
			</div>
		</div>
	</div>
</div>
<!--End of Block 1-->



<div class="content__callout">
	<div class="content__callout__media">
		<div class="content__callout__image-wrapper">
			<div class="video js__video" data-video-id="256641270">
				<figure class="video__overlay">
					<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MEDITECH-virtual-visits-functionality--video-overlay.jpg" alt="Video cover shot for MEDITECH's virtual visit functionality">
				</figure>
				<a class="video__play-btn" href="http://vimeo.com/256641270"></a>
				<div class="video__container">
				</div>
			</div>
		</div>
	</div>
	<div class="content__callout__content">
		<div class="content__callout__body">
				<h2>The doctor will see you <em>NOW</em>.</h2>
			<p>See healthcare through a new lens. MEDITECH's Virtual Visit functionality offers you convenient, remote interaction with your patients embedded into your workflow, including the ability to bill and document care. Patients no longer have to wait anxiously to have questions answered — all they need is their device of choice. </p>
		</div>
	</div>
</div>



<!-- mHealth -->
<div class="container" style="background-image: url(/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
	<div class="container__centered">
		<div class="container no-pad">
			<div class="container__three-fourths">
				<h2>MEDITECH's MHealth app goes where your patients go.</h2>
				<p>Every day, millions of patients worldwide access MEDITECH's Patient Portal. Now, they can also use our <a href="https://ehr.meditech.com/news/mhealth-meditech-in-the-app-store">MHealth app</a> for streamlined, direct access. <a href="https://vimeo.com/meditechehr/review/230981854/1e92abf10b" target="_blank">See it in action</a>! </p>
				<figure class="no-target-icon" style="display: inline-block; margin-right:1em;">
					<a href="https://itunes.apple.com/us/app/meditech-mhealth/id1143209032?mt=8" target="_blank"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/App-Store-Badge.png" alt="App Store Download Badge"></a>
				</figure>
				<figure class="no-target-icon" style="display: inline-block;">
					<a href="https://play.google.com/store/apps/details?id=com.meditech.PatientPhm&hl=en" target="_blank"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Google-Play-Badge.png" alt="Google Play Download Badge"></a>
				</figure>
			</div>
			<div class="container__one-fourth">
				<figure>
					<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MHealth-App-Icon.png" alt="MHealth app icon">
				</figure>
			</div>
		</div>
	</div>
</div>
<!--End of mHealth-->



<!--Block 4 Screenshot-->
<div class="container" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Green-Triangles-1.png);">
	<div class="container__centered text--white">
		<div class="container__two-thirds">
			<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/screenshot--patient-portal-white-tablet.png" alt="MEDITECH's Patient Portal Screenshot">
		</div>
		<div class="container__one-third">
			<h2>
				Convenience is key.
			</h2>
			<p>
				New, interactive features make it easier than ever for patients and consumers to "own" their healthcare. With MEDITECH's <a href="https://ehr.meditech.com/ehr-solutions/patient-portal">Patient Portal</a>, they can:
			</p>
			<ul>
				<li>Directly schedule appointments</li>
				<li><a href="https://www.evopayments.us/accept-payments/meditech/" target="_blank">Pay bills</a></li>
				<li>Access visit summaries</li>
				<li>Communicate with providers via secure messaging</li>
				<li>Print immunization records.</li>
			</ul>
		</div>
	</div>
</div>
<!--End of Block 4-->

<!--Video-->
<div class="content__callout">
	<div class="content__callout__media">
		<div class="content__callout__image-wrapper">
			<div class="video js__video" data-video-id="212903910">
				<figure class="video__overlay">
					<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay--SHINE-Patient-Experience.jpg" alt="Video Covershot for SHINE Patient Experience">
				</figure>
				<a class="video__play-btn" href="http://vimeo.com/212903910"></a>
				<div class="video__container">
				</div>
			</div>
		</div>
	</div>
	<div class="content__callout__content">
		<div class="content__callout__body">
			<h2>Partnering to empower patients.</h2>
			<p>
				Markham Stouffville Hospital, Southlake Regional Health Centre, and Stevenson Memorial Hospital established their SHINE partnership to share Expanse across their facilities. Together, they're creating one patient record, fostering continuity of care, and improving the patient experience.
			</p>
		</div>
	</div>
</div>
<!--End Video-->

<!--Block 6 Screenshot Blog-->
<div class="container" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Green-Triangles-2.png);">
	<div class="container__centered text--white">
		<div class="container__one-third">
			<h2>Hear it from the experts.</h2>
			<p>
				What do industry leaders say about patient engagement? Read the latest <a href="http://blog.meditech.com/topic/patient-engagement">strategies</a> on our blog to see what they have to share.
			</p>
			<p>
				<a href="http://info.meditech.com/get-great-meditech-content">Sign up</a> for the MEDITECH blog to receive the latest from our patient engagement thought leaders.
			</p>
		</div>
		<div class="container__two-thirds">
			<img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/screenshot--blog-8-17-white-tablet.png" alt="MEDITECH's Blog Screenshot">
		</div>
	</div>
</div>
<!--End of Block 6-->

<!--Block 7-->
<div class="content__callout border-none" style="background-color:#3e4545;">
	<div class="content__callout__media">
		<h2 class="content__callout__title">Capture every chapter of your patient's story, wherever it happens.</h2>
		<div class="content__callout__image-wrapper">
			<div style="position:relative; height: 0; min-height: 400px; overflow:hidden; max-width:84.5%; margin:0 auto;">
				<figure style="text-align: center;">
					<img style="max-width: 400px" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/CoordinatedCare--updated-2017.png" alt="Achieving the Quadruple Aim">
				</figure>
			</div>
		</div>
	</div>

	<div class="content__callout__content">
		<div class="content__callout__body">
			<div class="content__callout__body__text--large">
				<p>"By treating patients as unique individuals and not just a compilation of conditions, caregivers can better anticipate common sources of patient anxiety and take steps to mitigate them… Empathy also impacts your organization's bottom line by fostering patient loyalty and affecting patient satisfaction scores, which in turn impact value-based purchasing penalties."<br />Source: <a href="https://www.advisory.com/-/media/Advisory-com/Technology/iRound/Success-Pages/2016/Patient-Experience-iRound-Cultivating-Empathy.pdf" target="_blank">The Advisory Board Company, Cultivating Empathy: Why Empathy is Important and How to Build an Empathetic Culture, ©2016</a></p>
			</div>
		</div>
	</div>
</div>

<!--End of Block 7-->

<!--Block 8-->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/two-nurses-working-together-using-tablet.jpg);">
	<div class="container__centered text--white">
		<div class="container__one-half transparent-overlay">
			<h2>Teamwork matters.</h2>
			<p>MEDITECH provides the tools and workflow efficiencies you need to effectively treat and transition patients across all levels of care. Your patients are in good hands with solutions that support clinician-driven workflows, <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">clinically-integrated networks</a>, and <a href="https://ehr.meditech.com/ehr-solutions/meditech-surveillance">predictive</a> and <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">self-service analytics</a>.</p>
      <ul>
        <li><a href="https://www.meditech.com/productbriefs/flyers/Care_Coordination_Infographic.pdf">Coordinate care across venues</a> by employing a single record, medication list, and problem list that travels anywhere your patient goes.</li>
        <li><a href="https://www.meditech.com/productbriefs/flyers/TelehealthApproach.pdf">Get patients engaged in their own health</a> by encouraging them to share metrics from monitors and fitness devices.</li>
        <li>Use <a href="http://info.meditech.com/meditechs-building-a-foundation-for-population-health-patient-registries-white-paper">registries</a> and case management tools to stay proactive and <a href="http://blog.meditech.com/tips-to-prevent-readmissions-using-your-ehr-for-home-care">prevent readmissions</a>.</li>
        <li>Provide compassionate, empathetic care from the <a href="https://ehr.meditech.com/ehr-solutions/meditech-home-care">comfort of home</a>.</li>
        <li>Empower patients to book appointments, update their information, <a href="https://ehr.meditech.com/ehr-solutions/meditechs-revenue-cycle">pay bills, and view their account balances online</a>.</li>
      </ul>
		</div>
		<div class="container__two-thirds">
		</div>
	</div>
</div>
<!--End of Block 8-->


<!--Block 10 CTA-->
<div class="container" style="background-image: url(/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
	<div class="container__centered" style="text-align: center;">
		<h2>Learn how Ontario Shores uses our Patient and Consumer Health Portal to improve patient engagement metrics.</h2>
		<?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
          <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
          </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
        
       <div style="margin-top:1em;">
         <?php print $share_link_buttons; ?>
       </div>
	</div>
</div>
<!--End of Block 10-->

</div>

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?> 
<!-- END campaign--node-2049.php -->