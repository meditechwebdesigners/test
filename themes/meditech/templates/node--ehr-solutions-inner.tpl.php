<?php 
// This template is set up to control the display of the 'ehr solutions inner' content type 
$url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--ehr-solutions-inner.tpl.php template -->

<style>
  .panel.ehr-inner-side-menu { background: none; padding: 0 1em 1em 1em; }
  .panel.ehr-inner-side-menu h3 { margin-top: 0; }
  .panel.ehr-inner-side-menu .sidebar__nav ul.menu li { padding: .6em 0 .5em 0; }
  .panel.ehr-inner-side-menu .sidebar__nav ul.menu li a { padding-left: .5em; }
  .panel.ehr-inner-side-menu .sidebar__nav ul.menu li:hover { background: #F2F2F2; }
  .sidebar__nav ul.menu a:before { content: ''; }
  .sidebar__nav ul.menu a.active:before { 
    content: "\00ab";
    position: absolute;
    left: 0; 
  }
  .sidebar__nav ul.menu a.active { color: #087E68; font-weight: bold; }
  @media (max-width: 50em){
    .panel.ehr-inner-side-menu .sidebar__nav ul.menu li { padding: 1em 0; }
    .sidebar__nav ul.menu a.active:before { content: ""; }
  }
</style>

<?php
// determine hero graphic...
switch($node->nid){
  case 147:
    $hero_image = 'illustration--ehr-solutions--ambulatory';
    break;
  case 1507:
    $hero_image = 'illustration--ehr-solutions--behavioral';
    break;
  case 134:
    $hero_image = 'illustration--ehr-solutions--clinical';
    break;
  case 138:
    $hero_image = 'illustration--ehr-solutions--executives';
    break;
  case 140:
    $hero_image = 'illustration--ehr-solutions--financial';
    break;
  case 136:
    $hero_image = 'illustration--ehr-solutions--him';
    break;
    /*
  case 'Home Care':
    $hero_image = 'illustration--ehr-solutions--home-care';
    break;
    */
  case 763:
    $hero_image = 'illustration--ehr-solutions--hospice';
    break;
  case 144:
    $hero_image = 'illustration--ehr-solutions--it';
    break;
  case 1513:
    $hero_image = 'illustration--ehr-solutions--long-term';
    break;
  case 129:
    $hero_image = 'illustration--ehr-solutions--nurses';
    break;
  case 363:
    $hero_image = 'illustration--ehr-solutions--oncology';
    break;
  case 126:
    $hero_image = 'illustration--ehr-solutions--physicians';
    break;
  case 146:
    $hero_image = 'illustration--ehr-solutions--surgical';
    break;
  default:
    $hero_image = 'illustration--ehr-solutions--ambulatory';
}
?>

<!-- HERO section -->
<div class="container" style="padding-bottom:0.5em;">
    <div class="container__centered gl-container">
        <div class="container__one-half" style="padding:0;">
          <h1 class="js__seo-tool__title"><?php print $title; ?></h1>
          <?php if( isset($content['field_subtitle']) && $content['field_subtitle'] != '' ){ ?>
            <h2 class="js__seo-tool__body-content"><?php print render($content['field_subtitle']); ?></h2>
          <?php } ?>
          <?php if(node_access('update',$node)){ print '<span style="font-size:12px;">'; print l( t('Edit'),'node/'.$node->nid.'/edit' ); print "</span>"; } ?>
        </div>
        <div class="container__one-half hide--mobile" style="padding:0;">
          <img src="<?php echo $url; ?>/sites/all/themes/meditech/images/ehr-solutions/<?php echo $hero_image; ?>.svg" onerror="this.src='<?php echo $url; ?>/sites/all/themes/meditech/images/ehr-solutions/<?php echo $hero_image; ?>.png'; this.onerror=null;" alt="">
        </div>
    </div>
</div>
<!-- End of HERO section -->


<section class="container__centered">

    <div class="container__two-thirds">
        <div class="js__seo-tool__body-content">
            <?php print render($content['field_body']); // *** MAIN CONTENT *** ?>
        </div><!-- End .js__seo-tool__body-content -->

        <?php // SEO tool for internal use...
        if(node_access('update',$node)){
          print '<!-- SEO Tool is added to this div -->';
          print '<div class="container no-pad--top js__seo-tool"></div>';
        } 
      ?>
    </div>

    <aside class="container__one-third panel ehr-inner-side-menu">
        <div class="sidebar__nav solutions_sidebar_gae">
            <h3>Learn how Expanse can help you in your role</h3>
            <?php
            $solutionsInnerMenu = module_invoke('menu', 'block_view', 'menu-ehr-solutions-inner');
            print render($solutionsInnerMenu['content']); 
            ?>
        </div>
    </aside>

</section>
<!-- end node--ehr-solutions-inner.tpl.php template -->