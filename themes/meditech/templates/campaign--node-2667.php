<!-- START campaign--node-2667.php This template is set up to control the display of the Careers Refresh content type -->

<?php 
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$cta = field_collection_data($node, 'field_fc_cta_block');
?>

<style>
  .horizontal-menu {
    list-style-type: none;
  }

  .horizontal-menu li a {
    font-family: "montserrat", Verdana, sans-serif;
    font-weight: 500;
    float: left;
    display: block;
    padding: 1em 1.5em;
    margin-bottom: 0;
    text-decoration: none;
    border-bottom: 0;
    transition: 0.3s ease;
  }

  .horizontal-menu li:first-child {
    padding-left: 0em;
  }

  .horizontal-menu li a:hover {
    color: #fff;
    border-bottom: 0;
    background-color: #0a9178;
    transition: 0.3s ease;
  }
  
  .video--loop {
    width: auto !important;
    left: 50%;
    top: 50%;
    position: absolute;
    min-width: 100%;
    min-height: 100%;
    transform: translate(-50%, -50%);
  }

  /*  Employee Bubbles */

  .eb-1 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Nhien-Bui.jpg);
    background-position: center top;
  }

  .eb-2 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Galvin-Chow.jpg);
    margin-left: auto;
    background-position: center top;
  }

  .eb-3 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Kim-Fischer.jpg);
    background-position: center top;
  }

  .eb-4 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Eric-Bernsen.jpg);
    background-position: center top;
    margin: auto;
  }

  .eb-5 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Marijo-Carnino.jpg);
    margin: auto;
    background-position: center top;
  }

  .eb-6 {
    background-image: url(<?php print $url;
 ?>/sites/all/themes/meditech/images/campaigns/Mike-Amend.jpg);
    margin-left: auto;
    background-position: center top;
  }

  .employee-bubble {
    width: 250px;
    height: 250px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    /*    position: relative;*/
    cursor: pointer;
    box-shadow: inset 0 0 0 0 rgba(0, 188, 111, 0.95);
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }

  .employee-bubble:hover .eb-info {
    opacity: 1;
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    -o-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  .employee-bubble:hover {
    box-shadow: inset 0 0 0 135px rgba(0, 188, 111, 0.95);
  }

  .employee-card {
    border-right: 1px solid #e6e9ee;
    padding-bottom: 1.5em;
  }

  .employee-card-plus {
    border-right: 1px solid #e6e9ee;
    padding-top: 3em;
    padding-bottom: 1.5em;
  }

  .eb-info {
    position: absolute;
    width: 250px;
    height: 250px;
    border-radius: 50%;
    opacity: 0;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -o-transition: all 0.4s ease-in-out;
    -ms-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
    -webkit-transform: scale(0);
    -moz-transform: scale(0);
    -o-transform: scale(0);
    -ms-transform: scale(0);
    transform: scale(0);
    -webkit-backface-visibility: hidden;
  }

  .eb-info h3 {
    color: #fff;
    position: relative;
    padding: 80px 19px 5px 0;
    text-align: center;
    font-family: "montserrat", Verdana, sans-serif;
  }

  .eb-info p {
    color: #fff;
    position: relative;
    padding: 0px 19px 0 0;
    text-align: center;
  }

  /*  Modal Window */

  .Modal {
    position: absolute;
    z-index: 99;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, 0);
    visibility: hidden;
  }

  .Modal .content {
    position: absolute;
    left: 50%;
    top: 35%;
    max-width: 1050px;
    width: 100%;
    padding: 3em 2em 3em 2em;
    border-radius: 6px;
    background: #fff;
    color: #3e4545;
    transform: translate(-50%, -30%) scale(0);
  }

  .Modal .close {
    position: absolute;
    top: 5px;
    right: 18px;
    cursor: pointer;
    color: #3e4545;
  }

  .Modal .close:before {
    content: '\2715';
    font-size: 30px;
  }

  .Modal.is-visible {
    visibility: visible;
    background: rgba(0, 0, 0, 0.75);
    transition: background 0.35s;
    transition-delay: 0.1s;
  }

  .Modal.is-visible .content {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) scale(1);
    transition: transform 0.35s;
  }

  .modal-employee {
    width: 200px;
    height: 200px;
    border: 10px solid #e6e9ee;
    border-radius: 50%;
    margin: auto;
    margin-top: 1em;
    background-size: 115%;
  }

  .divided-box {
    background-color: rgba(255, 255, 255, .8);
    overflow: auto;
    display: flex;
    flex-flow: row wrap;
  }

  .left-box {
    width: 60%;
    float: left;
  }

  .right-box {
    width: 40%;
    padding: 6em;
    float: left;
  }

  @media all and (max-width: 1040px) {

    .left-box,
    .right-box {
      width: 100%;
      padding: 2em;
    }
  }

  @media all and (max-width: 50em) {
    .Modal.is-visible .content {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      -webkit-overflow-scrolling: touch;
      border-radius: 0;
      transform: scale(1);
      margin-top: 5em;
      overflow-y: auto;
      padding-bottom: 6em;
    }

    .employee-card {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      margin-bottom: 1em;
    }

    .employee-card-plus {
      border-right: none;
      border-bottom: 1px solid #e6e9ee;
      padding-top: 0;
      margin-bottom: 1em;
    }

    .employee-bubble {
      margin: auto;
      margin-bottom: 1em;
    }

</style>

<div class="js__seo-tool__body-content">

  <!-- Block 1 -->
  <div class=" container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/MEDITECH-employees-conversing.jpg); background-position:top;">
    <div class="container__centered">
      <div class="container__one-half transparent-overlay--xp" style="background-color: rgba(255,255,255,0.95); margin:6em 0;">
        <h2>Congratulations 2019 Graduates!</h2>
        <p>We know how hard you have worked for your degree, and how challenging the transition into a career can be. Whether you’ve studied medicine or music, I.T. or fashion, MEDITECH always has a place for you!</p>
        <div class="btn-holder--content__callout no-margin--top">
          <a href="#" class="btn--orange">Apply Now!</a>
        </div>
      </div>
    </div>
    <!-- Horizontal Navigation -->
    <div style="background-color: rgba(8,126,104,0.95);">
      <div class="container__centered no-pad">
        <ul class="horizontal-menu text--white">
          <li><a href="<?php print $url; ?>/careers/benefits-perks">Benefits &amp; Perks</a></li>
          <li><a href="#">Veterans</a></li>
          <li><a href="<?php print $url; ?>/about-meditech/community">Community</a></li>
          <li><a href="<?php print $url; ?>/careers/career-events">Recruiting Events</a></li>
          <li><a href="<?php print $url; ?>/careers/applying-at-meditech">FAQs</a></li>
          <li><a href="<?php print $url; ?>/content/test-page">Job Listings</a></li>
        </ul>
      </div>
    </div>
    <!-- End Horizontal Navigation -->
  </div>
  <!-- End Block 1 -->

  <!-- Block 2 -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="288221143">
          <figure class="video__overlay">
            <img src="https://media.gettyimages.com/photos/partnerships-are-the-backbone-to-strengthening-business-picture-id912547898" alt="MEDITECH Careers">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/288221143"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>Face Value</h2>
          <p>Every time we meet a new applicant, the possibilities are endless. We are looking for someone who has a lot of ideas, and is excited to share them.</p>
          <p>Being a MEDITECHer is something folks take pride in. We want to know what sets you apart, and what you can bring to the MEDITECH table!</p>
          <p>So, who are you?</p>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 2 -->

  <!-- Start Block 3 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/careers/city-blurred-background.jpg); padding-bottom: 8em;">

    <div class="container__centered text--white center auto-margins" style="padding-bottom:2em;">
      <h2>More than software.</h2>
      <p>We’re more than software. Healthcare is all about people, and MEDITECH is no exception. We may be best known for our solutions, but our identity runs deeper than that. Meet some of the unique people at our company.</p>
      <p>Take a look and you’ll quickly see a crowd that exists beyond both healthcare and I.T. Regardless of your background, studies, or experience, MEDITECH is always looking for unique individuals who want to stand apart, and work together.</p>
    </div>

    <div class="container__centered" style="padding-bottom:2em;">

      <div class="container__one-third">
        <a href="#Popup1" class="button">
          <div class="employee-bubble eb-1">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Nhien Bui</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup1" class="Modal">
          <div class="content">
            <div>

              <div class="container__one-third center employee-card">
                <div class="modal-employee eb-1"></div>
                <h3 class="no-margin--bottom">Nhien Bui</h3>
                <p class="no-margin--bottom">Product Manager</p>
                <p>Years at MEDITECH: <strong>2</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                <p>Ever-changing.</p>
                <h4 class="text--meditech-green no-margin--bottom">What does MEDITECH mean to you?</h4>
                <p>Collaboration.</p>
                <h4 class="text--meditech-green no-margin--bottom">What was your first car?</h4>
                <p>Toyota Cressida.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite TV show to binge-watch?</h4>
                <p>Curb Your Enthusiasm, Gotham, and Veep.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the most interesting place you’ve traveled to?</h4>
                <p>Falmouth luminous lagoon in Jamaica.</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>


      <div class="container__one-third">

        <a href="#Popup2" class="button">
          <div class="employee-bubble eb-4">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Eric Bernsen</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup2" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card">
                <div class="modal-employee eb-4"></div>
                <h3 class="no-margin--bottom">Eric Bernsen</h3>
                <p class="no-margin--bottom">Promotional Writer, Marketing</p>
                <p>Years at MEDITECH: <strong>1</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">What would you tell your best friend about MEDITECH?</h4>
                <p>MEDITECH isn't just a healthcare organization. It's a passionate community of people who truly believe in the fact that their work helps make people's lives better.</p>
                <h4 class="text--meditech-green no-margin--bottom">Describe MEDITECH in three words.</h4>
                <p>Passionate, caring, and empowering.</p>
                <h4 class="text--meditech-green no-margin--bottom">What’s the best thing you’ve eaten in the MEDITECH cafeteria?</h4>
                <p>Jim's breakfast omelette in Canton.</p>
                <h4 class="text--meditech-green no-margin--bottom">Favorite family tradition:</h4>
                <p>Summers on Nantucket (where my grandfather founded its first Italian restaurant).</p>
                <h4 class="text--meditech-green no-margin--bottom">Travel bucket list:</h4>
                <p>Driving cross-country across the U.S.</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

      <div class="container__one-third">

        <a href="#Popup3" class="button">
          <div class="employee-bubble eb-2">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Galvin Chow</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup3" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card">
                <div class="modal-employee eb-2"></div>
                <h3 class="no-margin--bottom">Galvin Chow</h3>
                <p class="no-margin--bottom">Supervisor, Client Support</p>
                <p>Years at MEDITECH: <strong>13</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                <p>A great place to work that is staffed with people who are passionate about helping healthcare organizations improve patient care.</p>
                <h4 class="text--meditech-green no-margin--bottom">If you could have one superpower, what would it be?</h4>
                <p>Mind reading!</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your Alma Mater?</h4>
                <p>UMass Amherst.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite outdoor activity?</h4>
                <p>Running.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite breakfast cereal?</h4>
                <p>Banana Nut Crunch.</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

    </div>

    <div class="container__centered">

      <div class="container__one-third">
        <a href="#Popup4" class="button">
          <div class="employee-bubble eb-3">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Kim Fischer</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup4" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card-plus">
                <div class="modal-employee eb-3"></div>
                <h3 class="no-margin--bottom">Kim Fischer</h3>
                <p class="no-margin--bottom">Director, Client Support</p>
                <p>Years at MEDITECH: <strong>14</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">What does MEDITECH mean to you?</h4>
                <p>MEDITECH was my first "real" job out of college and has provided great opportunities for me, both in terms of career growth and in developing long-lasting friendships with many of my co-workers over the years.</p>
                <h4 class="text--meditech-green no-margin--bottom">How has healthcare IT made an impact on you personally?</h4>
                <p>It has helped me understand the healthcare system as a whole, which helps me know what to expect for my personal or family member's treatments.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the one item you would take to a deserted island?</h4>
                <p>A boat.</p>
                <h4 class="text--meditech-green no-margin--bottom">What famous person would you most like to have dinner with?</h4>
                <p>Ellen DeGeneres so I can become friends with her and she can invite me to her birthday parties. Or, if not being completely superficial, Nelson Mandela. :-)</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the best thing you’ve eaten in the MEDITECH cafeteria?</h4>
                <p>Ice cream sundaes during summer. </p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

      <div class="container__one-third">
        <a href="#Popup5" class="button">
          <div class="employee-bubble eb-5">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Marijo Carnino</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup5" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card-plus">
                <div class="modal-employee eb-5"></div>
                <h3 class="no-margin--bottom">Marijo Carnino</h3>
                <p class="no-margin--bottom">Director, EHR Programs</p>
                <p>Years at MEDITECH: <strong>31</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">Describe your most impactful experience at MEDITECH?</h4>
                <p>What I've found most impactful is being a part of the hiring, training, mentoring of employees. Being here 30+ years I've had the privilege to see the growth and transformation of many from new college grads to passionate, confident, knowledgeable leaders - the work ethic and commitment of so many has been key to MEDITECH's prosperity.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is #1 on your travel bucket list?</h4>
                <p>Another trip with family to Italy - go back to prior stomping groups and explore new areas.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite food and least favorite food?</h4>
                <p>Favorite food is pizza - every place’s is different; I think I could live on it. Least favorite food - liver?</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite album?</h4>
                <p>Anything James Taylor.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite part of the MEDITECH picnic?</h4>
                <p>Sharing favorite foods with family and friends and volunteering at the Kids Prize Table.</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

      <div class="container__one-third">
        <a href="#Popup6" class="button">
          <div class="employee-bubble eb-6">
            <div class="eb-info">
              <h3 class="no-margin--bottom">Mike Amend</h3>
              <p>Learn more</p>
            </div>
          </div>
        </a>

        <div id="Popup6" class="Modal">
          <div class="content">
            <div>
              <div class="container__one-third center employee-card-plus">
                <div class="modal-employee eb-6"></div>
                <h3 class="no-margin--bottom">Mike Amend</h3>
                <p class="no-margin--bottom">Supervisor, Recruiting &amp; Staff Development</p>
                <p>Years at MEDITECH: <strong>12</strong></p>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Logo-2018.png" style="width:175px;" alt="MEDITECH Logo">
              </div>
              <div class="container__two-thirds">
                <h4 class="text--meditech-green no-margin--bottom">MEDITECH is…</h4>
                <p>Community.</p>
                <h4 class="text--meditech-green no-margin--bottom">What does MEDITECH mean to you? </h4>
                <p>My son was born at a MEDITECH hospital and recently I had surgery at a MEDITECH site. I knew that behind the talented clinicians were my peers, my friends, and my MEDITECH family. </p>
                <h4 class="text--meditech-green no-margin--bottom">What was your first car?</h4>
                <p>Pontiac Grand AM. It had door rot and a squirrel built a nest in the door.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the one item you would take to a deserted island?</h4>
                <p>A satellite phone. I ain’t no fool. </p>
                <h4 class="text--meditech-green no-margin--bottom">What is your favorite TV show to binge-watch?</h4>
                <p>Star Wars Clone Wars.</p>
                <h4 class="text--meditech-green no-margin--bottom">What is the most interesting place you’ve traveled to?</h4>
                <p>Pompeii</p>
              </div>

            </div>
            <div class="close"></div>
          </div>
        </div>
      </div>

    </div>

  </div>

  <script>
    $jq.fn.expose = function(options) {
      var $modal = $jq(this),
        $trigger = $jq("a[href=" + this.selector + "]");
      $modal.on("expose:open", function() {
        $modal.addClass("is-visible");
        $modal.trigger("expose:opened");
      });
      $modal.on("expose:close", function() {
        $modal.removeClass("is-visible");
        $modal.trigger("expose:closed");
      });
      $trigger.on("click", function(e) {
        e.preventDefault();
        $modal.trigger("expose:open");
      });
      $modal.add($modal.find(".close")).on("click", function(e) {

        e.preventDefault();

        // if it isn't the background or close button, bail
        if (e.target !== this)
          return;

        $modal.trigger("expose:close");
      });
      return;
    }
    $jq("#Popup1").expose();
    $jq("#Popup2").expose();
    $jq("#Popup3").expose();
    $jq("#Popup4").expose();
    $jq("#Popup5").expose();
    $jq("#Popup6").expose();

    // Example Cancel Button
    $jq(".cancel").on("click", function(e) {

      e.preventDefault();
      $jq(this).trigger("expose:close");
    });

  </script>
  <!-- End Block 3 -->

  <!-- Block 4 -->
  <div class="bg--light-gray">
    <div class="container no-pad">
      <div class="divided-box">
        <div class="left-box background--cover" style="background-image: url(https://media.gettyimages.com/photos/hell-help-them-pass-this-semester-picture-id871203928); min-height:250px;">
        </div>
        <div class="right-box bg--blue-gradient">
          <h2>College &amp; Career</h2>
          <p>Destined to be a writer? Have your heart set on showing off your leadership skills? You don’t need to have worked with technology or in a medical setting to be a perfect fit for MEDITECH. In addition to the College Fairs held regularly, our extensive onsite training will educate you on the company and your role, so you’ll be able to give it your all from the start. And our continual mentoring ensures you never feel left behind.</p>
        </div>

      </div>
    </div>
  </div>
  <!-- End Block 4 -->

  <!-- Block 4 -->
  <div class="bg--light-gray">
    <div class="container no-pad">
      <div class="divided-box">

        <div class="right-box">
          <h2 style="margin-top:2em;">Still on the fence about applying?</h2>
          <p>Be sure to follow <a href="https://twitter.com/MEDITECHCareers" target="_blank">@meditechcareers</a> and get a first hand glimpse of what MEDITECH is doing, and why so many talented folks from all walks of life continue to join our ranks each year.</p>
          <div style="margin-top:1em; margin-bottom:2em;">
            <?php print $share_link_buttons; ?>
          </div>
        </div>
        <div class="left-box video--loop-container" style="min-height: 400px;">
          <video class="video--loop" preload="auto" autoplay loop muted playsinline>
            <source src="https://player.vimeo.com/external/192464882.hd.mp4?s=e29a0b2ddc3680a75320324b1e168025e66d9214&profile_id=174">
          </video>
        </div>

      </div>
    </div>
  </div>
  <!-- End Block 4 -->

  <!-- Block 5 -->
  <!--
  <div class="video--loop-container" style="max-height: 700px;">
    <div class="video--overlay" style="opacity: 0.25;"></div>
    <video class="video--loop" preload="auto" autoplay loop muted playsinline>
      <source src="https://player.vimeo.com/external/192464882.hd.mp4?s=e29a0b2ddc3680a75320324b1e168025e66d9214&profile_id=174">
    </video>
  </div>
-->
  <!-- End Block 5 -->

  <!-- Block 6 -->
<!--
  <div class="container">
    <div class="container__centered center auto-margins">
      <h2>Still on the fence about applying?</h2>
      <p>Be sure to follow <a href="https://twitter.com/MEDITECHCareers" target="_blank">@meditechcareers</a> and get a first hand glimpse of what MEDITECH is doing, and why so many talented folks from all walks of life continue to join our ranks each year.</p>
      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
    </div>
  </div>
-->
  <!-- End Block 6 -->

</div>

<!-- END campaign--node-2667.php -->
