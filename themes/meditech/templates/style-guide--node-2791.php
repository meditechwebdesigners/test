<!-- START style-guide--node-2791.php Gutterless Grid -->

<!--Javascript & CSS reference for Prism-->
<script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/prism.js"></script>

<link href="<?php print $url; ?>/sites/all/themes/meditech/css/prism.css" rel="stylesheet" />

<section class="container__centered">

  <h1 class="page__title">
    <?php print $title; ?>
  </h1>

  <div class="container__two-thirds">

    <p>Piggybacking the <a href="<?php print $url; ?>/style-guide/grid">Grid</a> layouts, we have a variant called Gutterless Grid. It is pretty straightforward in that it is a grid without gutters (aka no margin between divs). In the example below, you will see that we have a <code class="language-css">.gl-container</code> (gutterless container) class which wraps the grid elements and styles them a bit differently.</p>
    <p>This class removes the margin from the right side of each div, adds extra padding, and also uses <a href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank">Flexbox</a> to keep the child divs the same height no matter how much content is inside them. Long story short, all you have to do is wrap your grid elements inside the <code class="language-css">.gl-container</code> to achieve this result.</p>
    <p><span class="italic">Note: By default the background color is white at 80% opacity and the grid containers have a padding of 2em. You can override this by adding different inline styling or color classes as needed.</span></p>

    <div class="demo-ct">
      <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/CHF-patient-with-cardiologist.jpg); background-position: top; margin-bottom:2em;">
        <div class="container__centered">
          <div class="gl-container">
            <div class="container__one-fourth bg--emerald">
              <h2>1/4</h2>
            </div>
            <div class="container__three-fourths">
              <h2>3/4</h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Start Gutterless Containers Code -->
    <pre><code class="language-html">&lt;!-- Start gutterless containers example -->
&lt;div class="container">
  &lt;div class="container__centered">
    &lt;div class="gl-container">
      &lt;div class="container__one-fourth bg--emerald">1/4&lt;/div>
      &lt;div class="container__three-fourths">3/4&lt;/div>
    &lt;/div>
  &lt;/div>
&lt;/div>
&lt;!-- End gutterless containers example -->
</code></pre>
    <!-- End Gutterless Containers Code -->

    <h2>Additional Gutterless Grid Layout Options</h2>

    <div class="container center">
      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-fifth bg--light-gray">
            1/5
          </div>
          <div class="container__one-fifth bg--light-brown">
            1/5
          </div>
          <div class="container__one-fifth bg--light-gray">
            1/5
          </div>
          <div class="container__one-fifth bg--light-brown">
            1/5
          </div>
          <div class="container__one-fifth bg--light-gray">
            1/5
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-fourth bg--light-gray">
            1/4
          </div>
          <div class="container__one-fourth bg--light-brown">
            1/4
          </div>
          <div class="container__one-fourth bg--light-gray">
            1/4
          </div>
          <div class="container__one-fourth bg--light-brown">
            1/4
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-third bg--light-gray">
            1/3
          </div>
          <div class="container__one-third bg--light-brown">
            1/3
          </div>
          <div class="container__one-third bg--light-gray">
            1/3
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-half bg--light-gray">
            1/2
          </div>
          <div class="container__one-half bg--light-brown">
            1/2
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-fifth bg--light-gray">
            1/5
          </div>
          <div class="container__four-fifths bg--light-brown">
            4/5
          </div>
        </div>
      </div>

      <div class="container__centered" style="margin-bottom:2em;">
        <div class="gl-container">
          <div class="container__one-fourth bg--light-gray">
            1/4
          </div>
          <div class="container__three-fourths bg--light-brown">
            3/4
          </div>
        </div>
      </div>

      <div class="container__centered">
        <div class="gl-container">
          <div class="container__one-third bg--light-gray">
            1/3
          </div>
          <div class="container__two-thirds bg--light-brown">
            2/3
          </div>
        </div>
      </div>

    </div>

    <h2>Full Width Gutterless Grid</h2>
    <p>For a full width Gutterless Grid, we have to change the class to <code class="language-css">.fw-gl-container</code>.</p>

    <div class="demo-ct">
      <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blurred-hospital-lobby.jpg); margin-bottom:2em;">
        <div class="container__centered">
          <div class="fw-gl-container">
            <div class="container bg--emerald">
              <div class="center">
                <h2>Full Width</h2>
              </div>
            </div>
            <div class="container center">
              <h2>Full Width</h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Start Full Width Gutterless Containers Code -->
    <pre><code class="language-html">&lt;!-- Start full width gutterless containers example -->
&lt;div class="container">
  &lt;div class="container__centered">
    &lt;div class="fw-gl-container">
      &lt;div class="container bg--emerald">Full Width&lt;/div>
      &lt;div class="container">Full Width&lt;/div>
    &lt;/div>
  &lt;/div>
&lt;/div>
&lt;!-- End full width gutterless containers example -->
</code></pre>
    <!-- End Full Width Gutterless Containers Code -->

  </div>

  <!-- SIDEBAR -->
  <aside class="container__one-third">

    <div class="sidebar__nav panel">
      <?php
        $styleBlock = module_invoke('menu', 'block_view', 'menu-style-guide-side-nav');
        print render($styleBlock['content']); 
        ?>
    </div>

  </aside>
  <!-- END SIDEBAR -->

</section>

<!-- END style-guide--node-2791.php Gutterless Grid -->
