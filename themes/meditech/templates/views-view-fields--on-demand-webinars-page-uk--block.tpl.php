<!-- start views-view-fields--on-demand-webinars-page-uk--block.tpl.php template -->
<?php // This template is for each row of the Views block: ON-DEMAND WEBINARS PAGE - UK ....................... ?>

<?php
// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

// get_webinar_image function is in the template.php file
?>
<figure class="container no-pad">
  <div class="container__one-third">
    <?php $webinar_image = get_webinar_image($node); ?>
    <div class="square-img-cropper <?php print $webinar_image['crop']; ?>">
      <?php
      if( empty($fields['field_text_1']->content) || $fields['field_text_1']->content == '' ){
        $page_link = $fields['path']->content;
      } 
      else{
        $page_link = $fields['field_text_1']->content;
      }
      ?>
      <a class="webinars_link_gae" href="<?php print $page_link; ?>"><img src="<?php print $webinar_image['url']; ?>" alt="webinar thumbnail"></a>
    </div>
  </div>
  <figcaption class="container__two-thirds">
    <h3 class="header-four no-margin"><a class="webinar_on_demand_link_gae" href="<?php print $page_link; ?>"><?php print $fields['title']->content; ?></a></h3>
    <p><?php print $fields['field_summary']->content; ?></p>
    <?php 
    if( !empty($fields['field_duration']->content) ){ 
      print '<p>'.$fields['field_duration']->content.'</p>'; 
    }
    ?>
  </figcaption>
</figure>

<?php 
if( user_is_logged_in() ){ 
  print '<p style="text-align:right; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
}
?>
<hr>
<!-- end views-view-fields--on-demand-webinars-page-uk--block.tpl.php template -->