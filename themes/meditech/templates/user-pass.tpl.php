<!-- start user-pass.tpl.php template -->
  <section class="container__centered">
	<div class="container__two-thirds">
      <div>
        <h1>Request a new password</h1>
        <?php print drupal_render_children($form) ?>
      </div>
    </div>
  </section>
<!-- end user-pass.tpl.php template -->
