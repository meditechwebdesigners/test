<?php // This template is for each row of the Views block:  UPCOMING EVENTS ....................... 

// get node ID...
$nid = $fields['nid']->content;
$node = node_load($nid);

if($fields['field_do_not_allow_clicking']->content){
  $not_clickable = field_view_field('node', $node, 'field_do_not_allow_clicking'); 
  if($not_clickable["#items"][0]["value"] === 1){
    $link_html_1 = '';
    $link_html_2 = '';
  }
}
else{
  $link_html_1 = '<a class="events_main_link_gae" href="'.$fields['path']->content.'">';
  $link_html_2 = '</a>';
}
?>
<!-- start views-view-fields--events-upcoming-events--block.tpl.php template -->
<section class="article--card">
    <figure class="article--img">
        <?php print $link_html_1.$fields['field_event_image']->content.$link_html_2; ?>
    </figure>
    <div class="article--info">
        <h3 class="header-four"><?php print $link_html_1.$fields['title']->content.$link_html_2; ?></h3>
        <h5 class="no-margin--bottom"><?php print $fields['field_event_date']->content; ?></h5>
        <h5 class="no-margin--top">
            <?php 
            print $fields['field_location_city']->content; 
            if($fields['field_location_state']->content){
              $stateTerms = field_view_field('node', $node, 'field_location_state'); 
              if(!empty($stateTerms)){
            foreach($stateTerms["#items"] as $sTerm){
              $eventState = ', '.$sTerm["taxonomy_term"]->description;
            }
              }
              else{
            $eventState = '';
              }
              print $eventState; 
            }
            if($fields['field_location_country']->content){
              if($fields['field_location_country']->content != 'United States' && $fields['field_location_country']->content != 'Canada'){
            print ', '.$fields['field_location_country']->content;
              }
            }
            ?>
        </h5>
        <div class="line-clamp"><?php print $fields['field_summary']->content; ?></div>
    </div>

    <?php 
    if( user_is_logged_in() ){ 
      print '<p style="text-align:center; font-size:12px;"><a href="https://ehr.meditech.com/node/'.$nid.'/edit">Edit this content</a></p>';
    }
    ?>
</section>
<!-- end views-view-fields--events-upcoming-events--block.tpl.php template -->
