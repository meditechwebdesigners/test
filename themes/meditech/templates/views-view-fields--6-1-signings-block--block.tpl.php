<?php // This template is for each row of the Views block: 6.1 SIGNINGS - BLOCK \\\\\\\\\\\\\\\\\ ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start views-view-fields--6-1-signings-block--block.tpl.php template -->
      <div class="container__one-fourth">
        <div class="transparent-overlay">
          <div class="center" style="color: #6AC4A9; padding-bottom: .5em;"><i class="fas fa-certificate fa-4x" style="font-size:2em;"></i></div>
          <div class="text--white center">
            <a href="<?php print $url; ?><?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a>
          </div>
        </div>
      </div>
<!-- end views-view-fields--6-1-signings-block--block.tpl.php template -->