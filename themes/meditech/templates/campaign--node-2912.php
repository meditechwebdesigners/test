<!-- START campaign--node-2912.php BCA CAMPAIGN -->

<?php
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

    <div class="js__seo-tool__body-content">

        <!-- Hero -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/fuel-informed-decisions-with-your-data.jpg);">
            <div class="container__centered">

                <div class="container__three-fourths transparent-overlay text--white">
                    <h1 class="js__seo-tool__title">Fuel more informed decisions with your data</h1>
                    <p>Managing big data is critical to the health of your organization and your patients. But these initiatives can be a drain on time and resources. Now, Business &amp; Clinical Analytics (BCA) does the heavy lifting for you.</p>

                    <p>BCA is a web-based data visualization solution that helps organizations increase efficiency, measure progress, and improve performance. Easy to implement, easy to use, and fully integrated with MEDITECH's EHR, BCA puts your clinical, financial, and operational data to work and points your organization in the right direction.</p>
                    
                    <div class="center" style="margin-top:2em;">
                      <?php hubspot_button($cta_code, "Download The Value-Based Care eBook"); ?>
                    </div>
                </div>

            </div>
        </div>
        <!-- End of Hero -->

       
        <!-- Block 2 -->
        <div class="content__callout">
            <div class="content__callout__media">
                <div class="content__callout__image-wrapper">
                    <div class="video js__video" data-video-id="336802953">
                        <figure class="video__overlay">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Dr-Dailey-BCA-Data.jpg" alt="Dr. Dailey video thumbnail">
                        </figure>
                        <a class="video__play-btn video_gae" href="https://vimeo.com/336802953"></a>
                        <div class="video__container">
                        </div>
                    </div>
                </div>
            </div>
            <div class="content__callout__content">
                <div class="content__callout__body">
                   <h2>Same Data, New Insight at Golden Valley</h2>
                   <p>CMIO William Dailey, MD, explains how Golden Valley Memorial Hospital (Clinton, MO) is gaining insight from their data, with help from MEDITECH's Business and Clinical Analytics solution.</p>
                </div>
            </div>
        </div>
        <!-- End Block 2 -->
        

        <!-- Block 3 -->
        <div class="container bg--blue-gradient">

            <div class="container__centered text--white auto-margins">
                <h2>Start fast with a wealth of Standard Content...</h2>
                <p>Working with as much information as you do requires a lot of care, feeding, and maintenance. That's why BCA is loaded with standard dashboards to map out your organization's data and get you up and running on your big data initiatives.</p>
                <p>Our extensive library of standard dashboards covers:</p>
                <div class="container__one-third">
                    <ul>
                        <li>Meaningful Use/Quality</li>
                        <li>Census</li>
                        <li>Ambulatory</li>
                    </ul>
                </div>
                <div class="container__one-third">
                    <ul>
                        <li>General Ledger and Payroll</li>
                        <li>Revenue Cycle</li>
                        <li>Supply Chain</li>
                    </ul>
                </div>
                <div class="container__one-third">
                    <ul>
                        <li>Service Line</li>
                        <li>ED and Surgery</li>
                        <li>Population Health</li>
                    </ul>
                </div>
            </div>

            <div class="container__centered auto-margins text--white">
                <div>
                    <p>MEDITECH refreshes your dashboard library at regular intervals. Plus, we'll continue to add new dashboards and analyses, with a variety of selectors to filter by time periods, providers, payers, patient classes, clinical conditions, and more.</p>
                </div>
            </div>

            <br>

            <div class="container__centered">
                <figure style="text-align:center;">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg" alt="AMB Regulatory Graphical Clinical Quality Analysis">
                </figure>
            </div>

        </div>
        <!-- End Block 3 -->
       
        
        <!-- Video 2 -->
        <div class="content__callout">
            <div class="content__callout__media">
                <div class="content__callout__image-wrapper">
                    <div class="video js__video" data-video-id="301204161">
                        <figure class="video__overlay">
                            <img src="<?php print $url; ?>/sites/default/files/images/video/video-overlay--BCA.jpg" alt="Palo Pinto video thumbnail">
                        </figure>
                        <a class="video__play-btn video_gae" href="https://vimeo.com/301204161"></a>
                        <div class="video__container">
                        </div>
                    </div>
                </div>
            </div>
            <div class="content__callout__content">
                <div class="content__callout__body">
                   <h2>Palo Pinto Puts Their Data to Work with MEDITECH's BCA Solution</h2>
                   <p>IT Director Chasity Wilcox, Palo Pinto General Hospital (Mineral Wells, TX), explains how MEDITECH's BCA Solution makes big data more meaningful — by  displaying relevant information in easy-to-comprehend dashboards.
                   Watch the video to learn more about our Business and Clinical Analytics tool.</p>
                </div>
            </div>
        </div>
        <!-- End Video 2 -->
        

        <!-- Block 4 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/self-service-business-analytics.jpg);">
            <div class="container__centered text--white auto-margins">
                <h2>...and unleash the power of Self-Service Analytics</h2>
                <p>Creating personalized dashboards is a cinch with intuitive, interactive tools that draw from MEDITECH's pre-built datasets. User-defined dashboards are easy to assemble and even easier to digest. No programming expertise required — simply drag and drop metrics into colorful charts and graphs to display your data in meaningful ways.  Customize our standard dashboards or build unique dashboards from scratch.</p>
            </div>
            <br>
            <div class="container__centered">
                <figure style="text-align:center;">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg" alt="Ambulatory CPT Revenue Analysis">
                </figure>
            </div>
        </div>
        <!-- End Block 4 -->

        <!-- Block 5 -->
        <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-pattern.png);">
            <div class="container__centered text--white">
                <div class="container__one-third">
                    <h2>Business and Clinical Analytics is backed by the best</h2>
                    <p>BCA is powered by MicroStrategy, a MEDITECH third-party vendor. MicroStrategy stood out among a field of 19 vendors in a recently published Gartner Special Report <a href="https://www.microstrategy.com/us/resources/library/reports/2019-gartner-critical-capabilities-for-analytics-and-business-intelligence-platforms?CID=7012R000001ANCAQA4&mkt_tok=eyJpIjoiT0RWbE5EZ3daRFZsWXpreSIsInQiOiJLeXp0bjhjMDZxUnVSbGl2VnFcLzB1N2ljcTh2RlFnVEVQbUxGaEtrbG1KRWt1cHVvb2VPZjZsNTgrSG9LT3ppbTgyWng3Tk4rNm4yXC9PczRFUjQ0Z0gwRExueHl3bVowK1hpS1Z1OXJXdkhYN2RHWDdBZ1oyTGVUTlR5Q2VOOGxVIn0%3D" target="_blank">on critical capabilities for business intelligence and analytics platforms.</a></p>
                </div>
                <div class="container__two-thirds transparent-overlay">
                    <div class="container__centered">
                       <h3>Strong Integrated Product for all Use Cases</h3>
                        <div class="quote__content__text" style="padding-top:1em;">
                            "MicroStrategy has among the highest product scores of any vendor in this Critical Capabilities [category], both overall and for all the evaluated use cases. It gained excellent or outstanding scores for 12 out of 15 critical capabilities evaluated for this research. The nine capabilities that scored outstanding include administration, architecture and security; data source connectivity; scalability and model complexity; data storage and loading options; metadata management; advanced analytics for CDSs; interactive visualization; mobile; and embedded BI."</div>
                        <p style="margin-left:.5em;"> - Gartner Special Report, 2019"</p>
                    </div>
                </div>

            </div>
        </div>
        <!-- End Block 5 -->

        <!-- Block 6 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-Executive-Financial-Dashboards.jpg);">
            <div class="container__centered text--white">
                <div class="container__one-half transparent-overlay">
                    <h2>Nurture financial success and sustainability</h2>
                    <p>Identify trends and drill down for an illuminating view of your organization's fiscal workings and outlook. Determine the links between clinical and financial performance to avoid costly penalties and capture fuller reimbursement. Gauge your advancement toward organizational goals with dashboards that display targets and assess progress. When the right information is at your fingertips, <a href="https://ehr.meditech.com/ehr-solutions/ehr-value-and-sustainability">maximizing revenues and controlling costs</a> have never been more manageable.</p>
                </div>
            </div>
        </div>
        <!-- End Block 6 -->

        <!-- Block 7 -->
        <div class="container bg--green-gradient">
            <div class="container__centered text--white" style="margin-bottom:2em;">

                <div class="page__title--center auto-margins" style="margin-bottom:2em;">
                    <h2>Deliver safer, higher quality care, with enlightening analytics</h2>
                    <p>Detect costly and unnecessary variations in care with Quality Dashboards that assess performance at the individual, practice, and group level, and support adherence to best practices.</p>
                </div>

                <div class="container__one-third">
                    <div class="transparent-overlay">
                        <h2 style="text-align:center;">CQO</h2>
                        <div class="text--white">
                            <p>Identify root causes of deficiencies among your CQMs to improve patient outcomes, decrease readmission rates, and reduce penalties. Gain a better understanding of your patient populations as well.</p>
                        </div>
                    </div>
                </div>

                <div class="container__one-third">
                    <div class="transparent-overlay">
                        <h2 style="text-align:center;">CMO</h2>
                        <div class="text--white">
                            <p>Safeguard quality care, review benchmarks, and ensure that medical staff and administration are united in pursuing organizational goals. BCA's specialized dashboards track readmissions, HACs, Level of Service analysis, variations in care, and more.</p>
                        </div>
                    </div>
                </div>

                <div class="container__one-third">
                    <div class="transparent-overlay">
                        <h2 style="text-align:center;">CNO</h2>
                        <div class="text--white">
                            <p>Track and manage readmissions, HACs, turnaround times, surgical throughput, time to prepare room, average outpatient visits, and much more to keep your department running like a well-oiled machine.</p>
                        </div>
                    </div>
                </div>

            </div>

            <div class="container__centered">
                <figure style="text-align:center">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg" alt="Quality Readmissions Summary">
                </figure>
            </div>
        </div>
        <!-- End Block 7 -->

        <!-- Block 6 -->
        <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-pattern.png);">
            <div class="container__centered text--white auto-margins">
                <h2>Elevate the patient and consumer experience</h2>
                <p>See what patients and consumers think about your organization. Explore opportunities for improvement and growth with dashboards that help you to deliver more focused, streamlined care:</p>
                <ul>
                    <li>Track patient satisfaction scores, portal enrollment, and patient education.</li>
                    <li>Perform real-time assessment of ED wait times to reduce bottlenecks.</li>
                    <li>Fine-tune location management to maximize patient convenience.</li>
                </ul>
                <p>Get a perspective on patient populations in multiple areas with Geographic Analysis Dashboards. Pinpoint where patients are coming from, so you can tailor your <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">population health strategies</a> and optimize resource allocation. Identify new service line opportunities and potential locations for care.</p>
            </div>
            <br>
            <div class="container__centered">
                <figure style="text-align:center;">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/BCA-ED--Geographic-Analysis.jpg" alt="ED Geographic Analysis">
                </figure>
            </div>
        </div>
        <!-- End Block 6 -->

    </div>
    <!-- end js__seo-tool__body-content -->

    <!-- Block 7 - CTA Block -->
    <div class="container bg--white">
        <div class="container__centered auto-margins" style="text-align: center;">

            <?php cta_text($cta); ?>

            <div class="center" style="margin-top:2em;">
              <?php hubspot_button($cta_code, "Download The Value-Based Care eBook"); ?>
            </div>
            
            <div style="margin-top:1em;">
              <?php print $share_link_buttons; ?>
            </div>

        </div>
    </div>
    <!-- End Block 7 -->

    <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

    <!-- END campaign--node-2021.php -->
