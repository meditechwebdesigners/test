<!-- START campaign--node-2664.php -->
<?php // This template is set up to control the display of the campaign population health content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
$cta = field_collection_data($node, 'field_fc_cta_block');
?>


<style>
    /* Risk graph table */

    .pop-health__risk-spectrum-container {
        background-position: bottom;
        background-repeat: repeat-x;
        padding-bottom: 10.5em;
    }

    .pop-health__ambulatory-container {
        background-position: top;
        background-repeat: repeat-x;
    }

    /* Risk arrow graphic */

    .pop-health__risk-paragraph {
        margin-top: 1.5em;
    }

    .pop-health__risk-graph__triangle {
        position: absolute;
        top: 0;
        right: -30px;
        width: 0;
        height: 0;
        border-top: 15px solid transparent;
        border-left: 30px solid #ff813d;
        border-bottom: 15px solid transparent;
    }

    .pop-health__risk-graph {
        position: relative;
        color: $white;
        text-align: center;
        margin: 3em 40px 0 0;
        font-weight: 700;
        height: 30px;
        line-height: 30px;
        /* Gradient for the graph
  // Tool Used: http://www.cssmatic.com/gradient-generator#'\-moz\-linear\-gradient\%28left\%2C\%20rgba\%2816\%2C147\%2C114\%2C1\%29\%200\%25\%2C\%20rgba\%28255\%2C184\%2C61\%2C1\%29\%2048\%25\%2C\%20rgba\%28255\%2C129\%2C61\%2C1\%29\%20100\%25\%29\%3B' */
        background: rgba(16, 147, 114, 1);
        background: -moz-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -webkit-gradient(left top, right top, color-stop(0%, rgba(16, 147, 114, 1)), color-stop(48%, rgba(255, 184, 61, 1)), color-stop(100%, rgba(255, 129, 61, 1)));
        background: -webkit-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -o-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -ms-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: linear-gradient(to right, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#109372', endColorstr='#ff813d', GradientType=1);
    }

    /* Risk graph table */

    .pop-health__table {
        margin: 0 0 2em 0;
        border: none;
    }

    .pop-health__table th {
        border-left: none;
    }

    .pop-health__table--mobile {
        display: none;
    }

    .pop-health__table--mobile thead tr th:first-child,
    .pop-health__table--mobile tbody tr td:first-child {
        width: 3em;
    }

    /* Rotate Text */

    .pop-health__table--mobile .rotate {
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        filter: progid: DXImageTransform.Microsoft.BasicImage(rotation=3);
        text-align: center;
        white-space: nowrap;
        height: 10em;
        width: 9em;
        font-weight: bold;
        padding-top: 0.7em;
    }

    @media all and (max-width: 37.5em) {

        .pop-health__table--desktop,
        .pop-health__risk-graph {
            display: none;
        }

        .pop-health__table--mobile {
            display: table;
        }
    }

    .transparent-overlay--white {
        padding: 1em;
        background-color: rgba(255, 255, 255, 0.8);
        min-height: 5em;
    }

    .content__callout__content {
        background-color: #E6E9EE;
        padding-left: 0;
        padding-right: 6%;
    }

    @media all and (max-width: 50em) {
        .content__callout__content {
            padding-left: 6%;
            color: #3e4545;
            padding-top: 0em;
        }

        .video {
            max-width: 100%;
        }
    }

    .video--shadow {
        border-radius: 6px;
        box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
    }

    .patients { 
    padding: 1em; margin: 1em 2% 0 0; width: 30%; float: left; min-height: 370px; background-position: top right; background-repeat: no-repeat; 
  }
  .patients .transparent-overlay { margin-top: 10em; }
  .patients-info { font-size:.78em; }
  .patients h3 { margin-top: 0; margin-bottom: 0; font-size: 1em; }
  
  @media (max-width: 900px){  
    .full-width--tablet { width: 100%; }
    .patients { width: 100%; }
  }
  
  @media (max-width: 800px){  
    .patients .transparent-overlay { margin-top: 17em; }


</style>

<!-- Block 1 Hero -->

<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/people-collage-light.jpg);background-color:#3e4545;">
  <div class="container__centered">
    <div class="container__two-thirds transparent-overlay--xp text--white text-shadow--black" style="background-color: rgba(0, 0, 0, 0.9);"> 
      <h1 style="font-size: 2em;">See population health through clear lens.</h1>
      <p>If there’s one thing care teams need when tackling population health, it’s clarity. With MEDITECH Expanse Population Health, you’ll have the tools you need to get a clear picture of your patient populations &mdash; who they are, where they’ve been, and where they’re going. And you’ll have the functionality to support individual patients and help them manage health risks at every stage of life &mdash; no matter where their care journey takes them.
	</p>
	<div class="btn-holder--content__callout center" style="margin-bottom:1.5em;">
        <?php 
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $cta->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
        <div class="button--hubspot">
          <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>">
              <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png" alt="button" /></a></span>
            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
            <script type="text/javascript">
              hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});

            </script>
          </span>
          <!-- end HubSpot Call-to-Action Code -->
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<!-- Close Block 1 -->

<!-- Block 2 Patient story slider -->

<div class="container background--cover border-none hide__bg-image--mobile" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/family-happy-together-gradient.jpg); background-position: top; background-color:#045748;">

  <div class="container__centered">
  	<div class="container__centered auto-margins" style="color:white">
  		<h2> It’s all about the patient.</h2>
  		<p> Population health is about more than trends and reporting; it’s really about meeting patients where they are. Let’s take a look at how an effective population health strategy could impact the lives of four very different people.</p>
  	</div>

    <div class="slider text--white" style="padding-bottom:2em;">

<!-- Slider1 -->

      <div>
        <div class="container__one-half">
          <h2 style="padding-bottom:15px; color:#FDE598;">David, 8 years old</h2>
          <h4 style="color:#FDE598;">Patient Profile:</h4>
          <ul>
            <li>Has asthma</li>
            <li>Loves to draw</li>
            <li>Goes to day care after school</li>
            <li>Gets frequent colds</li>
          </ul>
          <h4 style="color:#FDE598;">Patient Portal Use:</h4>
          <ul>
            <li>His mother uses his Patient Portal to schedule wellness appointments and virtual visits with his pediatrician.</li>
            <li>She also uses the portal to manage immunizations for his asthma, renew medications, and email clinicians with questions.</li>
          </ul>
        </div>
        <div class="container__one-half" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/david-smiling-in-rain.jpg); height: 600px; background-position: top right; background-size: 100%; background-repeat: no-repeat;">
          &nbsp;
        </div>
      </div>

<!-- Slider2 -->

      <div>
        <div class="container__one-half">
          <h2 style="padding-bottom:15px; color:#FDE598;">Sarah, 30 years old</h2>
          <h4 style="color:#FDE598;">Patient Profile:</h4>
          <ul>
            <li>Active</li>
              <li>Generally healthy</li>
              <li>Loves the outdoors</li>
              <li>Celiac disease</li>
              <li>Only sees physician when feeling sick</li>
          </ul>
          <h4 style="color:#FDE598;">Patient Portal Use:</h4>
          <ul>
            <li>Patient Portal reminds her to schedule flu shots and annual wellness visits, and includes convenient access to lab results.</li>
            <li>Uses Patient Generated Health Data with wearables to monitor gluten-free diet and fitness goals. This data is integrated into her EHR.</li>
          </ul>
        </div>
        <div class="container__one-half" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/sarah-watering-garden.jpg); height: 600px; background-position: top right; background-size: 100%; background-repeat: no-repeat;">
          &nbsp;
        </div>
      </div>

<!-- Slider3 -->

      <div>
        <div class="container__one-half">
          <h2 style="padding-bottom:15px; color:#FDE598;">Patricia, 58 years old</h2>
          <h4 style="color:#FDE598;">Patient Profile:</h4>
          <ul>
            <li>High blood pressure</li>
            <li>Mild arthritis</li>
            <li>Family history of breast cancer</li>
            <li>Exercises infrequently</li>
            <li>Active in the community</li>
          </ul>
          <h4 style="color:#FDE598;">Patient Portal Use:</h4>
          <ul>
            <li>Uses the Patient Portal to track her test results, appointments, and provider emails.</li>
            <li>Protocols and registries help care navigators to track and schedule wellness visits, including annual mammograms.</li>
            <li>Portal sends her information on yoga classes at the local hospital, to help with arthritis pain.</li>
          </ul>
        </div>
        <div class="container__one-half" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/patricia-leaning-on-window.jpg); height: 600px; background-position: top right; background-size: 100%; background-repeat: no-repeat;">
          &nbsp;
        </div>
      </div>

<!-- Slider 4-->

      <div>
        <div class="container__one-half">
          <h2 style="padding-bottom:15px; color:#FDE598;">Richard, 76 years old</h2>
          <h4 style="color:#FDE598;">Patient Profile:</h4>
          <ul>
            <li>Insulin-dependent diabetic</li>
            <li>Congestive heart failure</li>
            <li>Limited mobility</li>
            <li>Lives alone in rural setting</li>
          </ul>
          <h4 style="color:#FDE598;">Patient Portal Use:</h4>
          <ul>
            <li>Richard's daughter accesses his information through the Patient Portal, to help manage his ongoing care.</li>
            <li>Now using home care services, telehealth devices send vitals to his care team.</li>
            <li>Virtual visits enable convenient, regular provider communication.</li>
          </ul>
        </div>
        <div class="container__one-half" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/richard-wheel-chair.jpg); height: 600px; background-position: top right; background-size: 100%; background-repeat: no-repeat;">
          &nbsp;
        </div>
      </div> 

<!-- End Slider -->

    </div>
  </div> 
</div>

<link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick.css">
<link rel="stylesheet" href="<?php print $url; ?>/sites/all/themes/meditech/css/slick-theme.css">
<script src="<?php print $url; ?>/sites/all/themes/meditech/js/slick.min.js"></script>
<script>
  jQuery(document).ready(function($) {
    $('.slider').slick({
      dots: true,
      prevArrow: '<a class="slick-prev">&#10092;</a>',
      nextArrow: '<a class="slick-next">&#10093;</a>',
    });
  });

</script>
<!-- End of Block 2 -->

 <!-- Block 3 - Patient Registry Screenshot -->
<div class="container bg--emerald">
    <div class="container__centered">
        <div class="auto-margins center" style="padding-bottom: 15px;">
            <h2>Take action with Patient Registries.</h2>
            <p>Our actionable Patient Registries are now bolstered by Arcadia-supplied information, including claims and a broad range of disparate EHR data. Care teams can examine entire groups of patients, determine who they’re accountable for, and decide on the appropriate interventions — all from just one screen. By identifying at-risk patients, gaps in care, and overdue health maintenance, our Patient Registries can help your organization promote wellness and take a more proactive approach to care coordination and disease management.
            </p>
        </div>
        <div>
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/risk-management-screen-cap.jpg" alt="Patient Registry Screenshot">
            </figure>
        </div>
    </div>
</div>
<!-- End of Block 3 -->

<!-- New Block 4 -->
<div class="container background--cover hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/hands-together-6.jpg);background-color:#959d9c">
    <div class="container__centered">
        <div class="page__title--center">
            <div class="container no-pad">
                <h2 style="color:black;">
                    Address Social Determinants of Health.
                </h2>
                <p class="auto-margins" style="color:black;">
                    Deliver care that reaches far beyond the hospital and physician’s office. Connect patients with social services and community resources, and use Expanse to help them take better control over their own health.
                </p>
            </div>
            <div class="container no-pad--top left">
                <h4 class="center" style="color:black; margin-bottom: 1.5em;">
                    See how MEDITECH customers are addressing social determinants of health in their communities:
                </h4>
                <div class="container__one-fourth transparent-overlay text--white" style="background-color: rgba(0, 0, 0, 0.8);">
                    <h3 class="center">
                        <a href="https://blog.meditech.com/beyond-numbers-creative-ways-to-address-population-health">Frisbie Memorial Hospital</a>
                    </h3>
                    <p>
                        Found that food insecurity was the root of their super utilizer problem in the ED, as many patients presented only to receive a hot meal. Frisbie partnered with community organizations to host weekly potluck dinners that kept community members properly fed and out of the hospital. 
                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay text--white" style="background-color: rgba(0, 0, 0, 0.8);">
                    <h3 class="center">
                        <a href="https://blog.meditech.com/grocery-store-walkthroughs-one-step-toward-improving-health-literacy">Kalispell Regional Medical Center</a>
                    </h3>
                    <p>
                        Established a program in which community health workers take Congestive Heart Failure patients on walkthroughs of their local grocery store to combat health illiteracy and help them establish healthy diets.

                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay text--white" style="background-color: rgba(0, 0, 0, 0.8);">
                    <h3 class="center">
                        <a href="https://blog.meditech.com/how-providers-can-bridge-care-gaps-caused-by-social-determinants-of-health">Southwestern Vermont Medical Center</a>
                    </h3>
                    <p>
                        Designed a transitional care program aimed at addressing the gaps in care that occured due to the social determinants of health affecting their rural community.
                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay text--white" style="background-color: rgba(0, 0, 0, 0.8);">
                    <h3 class="center">
                        <a href="https://blog.meditech.com/the-importance-of-social-determinants-in-behavioral-health">Bristol Hospital</a>
                    </h3>
                    <p>
                        Used surveys to identify patients in need of additional resources or support, and deployed local nursing students to follow up with patients in need.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of New Block 4 -->



<!-- Block 5 - Video -->
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper" style="padding:3em !important;">
            <div class="video js__video video--shadow" data-video-id="288754788">
                <figure class="video__overlay">
                    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay--PPGH-Bridge-to-Health.jpg" alt="Palo Pinto Mobile Health Initiative Video Covershot">
                </figure>
                <a class="video__play-btn" href="http://vimeo.com/288754788?&autoplay=1"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>
                     Mobile health initiatives go on the road.
                </h2>
                <p>
                    All aboard Palo Pinto’s mobile health clinic! In this video, see how Palo Pinto General Hospital’s fully-equipped medical bus gives their community a “bridge to health,” and enables providers to truly meet their patients where they are.
                </p>
            </div>
        </div>
    </div>
</div>
<!-- End of Block 5 -->


<!-- Block 6 - Portal Screenshot -->
<div class="container bg--emerald">
    <div class="container__centered">
        <div class="auto-margins center" style="padding-bottom: 15px;">
        	<h2>Support your decisions with solid population analytics.</h2>
        	<p>Back your population health initiatives with data that gives you a complete view of outcomes and utilization. MEDITECH’s Business and Clinical Analytics solution lets strategic teams look for trends across patient populations, including readmission rates, chronic conditions, and gaps in care. Armed with this data, you can target programs that deliver the highest quality care at the lowest cost. With Arcada.io data embedded directly into workflows, care teams can fill in the patient story, for an accurate and meaningful patient encounter every time.
            </p>
        </div>
        <div>
            <figure>
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Utilization-screen-grab.jpg" alt="population analytics">
            </figure>
        </div>
    </div>
</div>


<!-- Block 7 - Patient centered image -->
<div class="container background--cover hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/happy-patient-with-dr3.jpg);">

    <div class="container__centered">
    	<div class="container__one-half">
                &nbsp;
            </div>
        <div class="container__one-half transparent-overlay text--white" style="background-color: rgba(102, 108, 119, 0.8);"> 
            <h2 class="no-margin--top"> Interoperable care is connected care.</h2>
            <p>True <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">interoperability</a> connects communities and enables better population health strategies by supporting the sharing of patient information across all systems. This means that no matter where a patient seeks care, their care team will have the information they need to deliver better care and outcomes.
            </p>

            <p>As a Contributor Member of the CommonWell Health Alliance, MEDITECH’s interoperability efforts extend beyond local and regional affiliations. The patient’s information goes where they go, so clinicians can always treat the person and not just the problem — a crucial component of value-based care.</p>
        </div>
    </div>
</div>
<!-- End of Block 7 -->

<!-- Block 8 -->

<div class="container background" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/financial-background.png);">
    <div class="container__centered">
        <div class="container__three-fourths transparent-overlay" style="background-color: rgba(7, 79, 65, 0.7);">
            <h2 class="text--white">Revenue Cycle Reimbursement.</h2>
            <p class="text--white">MEDITECH will also help you navigate the shift to value-based care while keeping your organization financially viable. Unite all care fronts and cover patient access, middle, and back office processes to boost financial performance. A truly centralized business office and consolidated revenue cycle reports allow you to monitor AR days, cash flow, and denials to ensure your organization maintains healthy margins. 
            </p>
        </div>

        <div class="container__one-fourth">
        	<img style="padding-top:20px; padding-left:40px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Financial-cycle-icon.png" alt="population analytics">
        </div>
    </div>
</div>
<!-- End of Block 8 -->

<!-- New Block 3 
<div class="container background--cover" style="background-image: url(../images/population-health-2018--background-1.jpg);">
    <div class="container__centered">
        <div class="container__two-thirds">
            &nbsp;
        </div>
        <div class="container__one-third transparent-overlay--white">
            <h2>
                Interoperability leads to seamless, coordinated care.
            </h2>
            <p>
                True interoperability connects communities and enables better population health strategies by allowing for the sharing of data across all systems. As a Contributor Member of the CommonWell Health Alliance, MEDITECH is committed to technology that will give you the full patient story - so you can always treat the person and not just the problem.
            </p>
        </div>
    </div>
</div>
End of New Block 3 -->

<!-- Block 9 -->
<div class="container background" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
    <div class="container__centered center text--white">
        <h2>
            Learn about the 5 essential EHR tools that will help your organization thrive as you transition to value-based care delivery.

        </h2>
        <div class="button--hubspot">
          <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>">
              <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png" alt="button" /></a></span>
            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
            <script type="text/javascript">
              hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});

            </script>
          </span>
          <!-- end HubSpot Call-to-Action Code -->
        </div>
    </div>
</div>
<!-- End of Block 9 -->

<!-- END campaign--node-2664.php --
