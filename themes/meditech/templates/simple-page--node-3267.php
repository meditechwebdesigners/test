<!-- start simple-page--node-3267.tpl.php template -- CCPA Data Request Form PAGE -->

<style>
  .webform-client-form.webform-client-form-3213 label { font-weight: bold; }
  .webform-client-form.webform-client-form-3213 .radio-button-field label { font-weight: normal; }
  .radio-button.webform-container-inline div, .radio-button.webform-container-inline div.form-item,
  .checkbox div { display: block; }
  .checkbox div { margin-left: 1em; }
  .webform-client-form.webform-client-form-3213 .checkbox div label { font-weight: normal; }
  .radio-button-field input, .checkbox-field input { display: inline; }
</style>
<section class="container__centered">
  <div class="container__two-thirds">

    <div class="container no-pad">

      <div class="container no-pad js__seo-tool__body-content">
        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>

        <?php
          $formContent = module_invoke('webform', 'block_view', 'client-block-3213');
          print render($formContent['content']);
        ?>
      </div>
      
    </div>
    
  </div>
</section>

<!-- end simple-page--node-3267.tpl.php template -->