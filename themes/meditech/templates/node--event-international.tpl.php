<?php // This template is set up to control the display of the 'Event' content type 

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$accordion_shortcode_fix = '<p style="display:none;">&nbsp;</p>'; // to prevent accordion's close link from breaking (shortcode issue)

// get date data...
if( !empty($content['field_date_and_time']) ){
  $eventDate = field_get_items('node', $node, 'field_date_and_time');
  // get first date data...
  $date1 = date_create($eventDate[0]['value']);
  $day1 = date_format($date1, 'l, F j, Y');
  $time1 = date_format($date1, 'g:i A');
  // get second date data...
  $date2 = date_create($eventDate[1]['value']);
  $day2 = date_format($date2, 'l, F j, Y');
  $time2 = date_format($date2, 'g:i A');
}

// get event location data...
$eventLocation = field_get_items('node', $node, 'field_location_name');
$eventAddress = field_get_items('node', $node, 'field_location_address');
$eventCity = field_get_items('node', $node, 'field_location_city');
$eventZip = field_get_items('node', $node, 'field_location_zip');

$countryTerms = field_view_field('node', $node, 'field_location_country'); 
if(!empty($countryTerms)){
  foreach($countryTerms["#items"] as $cTerm){
    $eventCountry = ', '.$cTerm["taxonomy_term"]->name;
    $eventCountryAbbr = ', '.strip_tags($cTerm["taxonomy_term"]->description);
  }
}
else{
  $eventCountry = '';
  $eventCountryAbbr = '';
}
?>

<!-- start node--event-international.tpl.php template -->
  <section class="container__centered">
   
    <div class="container__two-thirds js__seo-tool__body-content">
      <div class="container no-pad--top">
       
        <?php if( !empty($content['field_event_image']) ){ // if image exists, display it... ?>
          <figure class="news__article__img">
            <?php print render($content['field_event_image']); ?>
          </figure>
        <?php } ?>
       
        <h1 class="page__title js__seo-tool__title no-margin--bottom"><?php print $title; ?></h1>

        <h3 class="no-margin--bottom"><?php print $eventCity[0]['value']; ?><?php print $eventCountry; ?></h3>
        
        <?php
          $dateTBD = field_get_items('node', $node, 'field_date_tbd');
          // if Date TBD is not checked...
          if($dateTBD[0]['value'] != 1){
            print '<p><strong>'.$day1.' | '.$time1.'</strong></p>';
          }
          else{
            print "<p><strong>Date: TBD</strong></p>";
          }
        ?>
         
        <div style="margin-top:2em;"> 
         
          <?php print render($content['field_event_intro']); ?>

          <?php // AGENDA =====================
          if( !empty($content['field_event_agenda']) ){
          ?>
            <!-- ACCORDIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
            <div class="accordion">
              <ul class="accordion__list">
                <li class="accordion__list__item">
                  <a class="accordion__link event_accordion_agenda_gae" href="#">Agenda<div class="accordion__list__control"></div></a>
                  <div class="accordion__dropdown">
                    <div class="js__seo-tool__body-content">
                      <?php print render($content['field_event_agenda']); ?>
                      <?php print $accordion_shortcode_fix; ?>
                    </div><!-- End .js__seo-tool__body-content -->
                    <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Agenda</a>
                  </div>
                </li>
              </ul>
            </div>
          <?php
          }
          ?>
        </div>
        
        <?php
        if( !empty($content['field_form_email']) ){
          
          print '<h3 style="margin-top:2em;">Registrations or Inquiries</h3>';

          $event_form = module_invoke('webform', 'block_view', 'client-block-2476');
          print render($event_form['content']); 

        }
        ?>

        <?php // SEO tool for internal use...
          if(node_access('update',$node)){
            print '<!-- SEO Tool is added to this div -->';
            print '<div class="container no-pad--top js__seo-tool"></div>';
          } 
        ?>  

      </div><!-- END container -->
    </div><!-- END container__two-thirds -->

    
    <!-- SIDEBAR =============================================== -->
    <aside class="container__one-third panel" style="margin-right:0;">
     
      <div class="sidebar--event__title--first">Event Location</div>

      <div class="js__seo-tool__body-content">
        <p>
          <?php 
            if($eventLocation[0]['value'] != ''){
              print $eventLocation[0]['value'].'<br />';
            }
            if($eventAddress[0]['value'] != ''){
              print $eventAddress[0]['value'].'<br />';
            }
            print $eventCity[0]['value'];
            print '<br>';
            $country = str_replace(', ', '', $eventCountry);
            print $country;
            print '<br>';
            print $eventZip[0]['value'];
          ?>
        </p>
      </div>    

      <?php if( !empty($content['field_event_questions']) ){?>
        <div class="sidebar--event__title">Questions?</div>
        <div class="js__seo-tool__body-content">
          <?php print render($content['field_event_questions']); ?>
        </div>
      <?php } ?>

    </aside>
    <!-- END SIDEBAR -->

  </section><!-- END section -->
 
<!-- end node--event-international.tpl.php template -->