<?php include( "_includes/header.inc"); ?>

<style>
    /* Risk graph table */

    .pop-health__risk-spectrum-container {
        background-position: bottom;
        background-repeat: repeat-x;
        padding-bottom: 10.5em;
    }

    .pop-health__ambulatory-container {
        background-position: top;
        background-repeat: repeat-x;
    }

    /* Risk arrow graphic */

    .pop-health__risk-paragraph {
        margin-top: 1.5em;
    }

    .pop-health__risk-graph__triangle {
        position: absolute;
        top: 0;
        right: -30px;
        width: 0;
        height: 0;
        border-top: 15px solid transparent;
        border-left: 30px solid #ff813d;
        border-bottom: 15px solid transparent;
    }

    .pop-health__risk-graph {
        position: relative;
        color: $white;
        text-align: center;
        margin: 3em 40px 0 0;
        font-weight: 700;
        height: 30px;
        line-height: 30px;
        /* Gradient for the graph
  // Tool Used: http://www.cssmatic.com/gradient-generator#'\-moz\-linear\-gradient\%28left\%2C\%20rgba\%2816\%2C147\%2C114\%2C1\%29\%200\%25\%2C\%20rgba\%28255\%2C184\%2C61\%2C1\%29\%2048\%25\%2C\%20rgba\%28255\%2C129\%2C61\%2C1\%29\%20100\%25\%29\%3B' */
        background: rgba(16, 147, 114, 1);
        background: -moz-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -webkit-gradient(left top, right top, color-stop(0%, rgba(16, 147, 114, 1)), color-stop(48%, rgba(255, 184, 61, 1)), color-stop(100%, rgba(255, 129, 61, 1)));
        background: -webkit-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -o-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: -ms-linear-gradient(left, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        background: linear-gradient(to right, rgba(16, 147, 114, 1) 0%, rgba(255, 184, 61, 1) 48%, rgba(255, 129, 61, 1) 100%);
        filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#109372', endColorstr='#ff813d', GradientType=1);
    }

    /* Risk graph table */

    .pop-health__table {
        margin: 0 0 2em 0;
        border: none;
    }

    .pop-health__table th {
        border-left: none;
    }

    .pop-health__table--mobile {
        display: none;
    }

    .pop-health__table--mobile thead tr th:first-child,
    .pop-health__table--mobile tbody tr td:first-child {
        width: 3em;
    }

    /* Rotate Text */

    .pop-health__table--mobile .rotate {
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        filter: progid: DXImageTransform.Microsoft.BasicImage(rotation=3);
        text-align: center;
        white-space: nowrap;
        height: 10em;
        width: 9em;
        font-weight: bold;
        padding-top: 0.7em;
    }

    @media all and (max-width: 37.5em) {

        .pop-health__table--desktop,
        .pop-health__risk-graph {
            display: none;
        }

        .pop-health__table--mobile {
            display: table;
        }
    }

    .transparent-overlay--white {
        padding: 1em;
        background-color: rgba(255, 255, 255, 0.8);
        min-height: 5em;
    }

    .content__callout__content {
        background-color: #E6E9EE;
        padding-left: 0;
        padding-right: 6%;
    }

    @media all and (max-width: 50em) {
        .content__callout__content {
            padding-left: 6%;
            color: #3e4545;
            padding-top: 0em;
        }

        .video {
            max-width: 100%;
        }
    }

    .video--shadow {
        border-radius: 6px;
        box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3);
    }

    .patients { 
    padding: 1em; margin: 1em 2% 0 0; width: 30%; float: left; min-height: 370px; background-position: top right; background-repeat: no-repeat; 
  }
  .patients .transparent-overlay { margin-top: 10em; }
  .patients-info { font-size:.78em; }
  .patients h3 { margin-top: 0; margin-bottom: 0; font-size: 1em; }
  
  @media (max-width: 900px){  
    .full-width--tablet { width: 100%; }
    .patients { width: 100%; }
  }
  
  @media (max-width: 800px){  
    .patients .transparent-overlay { margin-top: 17em; }


</style>

<!-- Title section -->
<div class="container--page-title">
    <div class="container__centered">
        <h1 class="page__title--main">Population Health</h1>
    </div>
</div>

<!-- Block 1 Hero -->

<div class="container" style="background-color:white;">
        <div class="container__one-half">
            <figure>
                <img src="../images/Woman-with-glasses-on-laptop.jpg" alt="BCA Quality Screenshot">
            </figure>
        </div>
        <div class="container__one-half" style="background-color:white;">
            <h2> See population health through a clear lens.</h2>
            <p>If there’s one thing care teams need when tackling population health, it’s clarity. With MEDITECH Expanse Population Health, you’ll have the tools you need to get a clear picture of your patient populations - who they are, where they’ve been, and where they’re going. And you’ll have the functionality to support individual patients and help them manage health risks at every stage of life - no matter where their care journey takes them.
            </p>
        </div>
</div>
<!-- Close Block 1 -->

<!-- Block 2 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/abstract-x-background-beige.jpg);">
    <div class="container__centered">
      <h2 class="no-margin" style="font-size: 2.5em; line-height: 1.15em;">It's all about the patient.</h2>
      <p style="margin-top:1em;">Population health is about more than trends and reporting; it’s really about meeting patients where they are. Let’s take a look at how an effective pop health strategy could impact the lives of three very different people.
	</p>
    </div>

    <div class="container__centered text--white">

      <div style="float: left; width: 100%;">

        <div class="patients" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/events/Andy-Burchett-DO.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Andy Burchett, DO</h3>
            <p class="patients-info-info">Medical Information Officer
              <br>Avera Health
              <br>Sioux Falls, SD</p>
          </div>
        </div>
        
        <div class="patients" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/events/Joe-Farr-RN.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Joe Farr, RN</h3>
            <p class="patients-info">Clinical Applications Coordinator
              <br>King's Daughters Medical Center
              <br>North Brookhaven, MS</p>
          </div>
        </div>

        <div class="patients" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/events/Joe-Farr-RN.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Joe Farr, RN</h3>
            <p class="patients-info">Clinical Applications Coordinator
              <br>King's Daughters Medical Center
              <br>North Brookhaven, MS</p>
          </div>
        </div>
        <div class="patients" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/events/Joe-Farr-RN.jpg);">
          <div class="transparent-overlay text--white">
            <h3>Joe Farr, RN</h3>
            <p class="patients-info">Clinical Applications Coordinator
              <br>King's Daughters Medical Center
              <br>North Brookhaven, MS</p>
          </div>
        </div>

      </div>


    </div>

 </div>
<!-- End of Block 2 -->

<!-- Block 3 - Web Ambulatory Screenshot 
<div class="container bg--emerald">
    <div class="container__centered">
        <div class="container__one-third">
            <h2 class="text--white no-margin--top">Web Ambulatory is just what the doctor ordered.</h2>
            <p class="text--white">Accelerate care delivery with MEDITECH’s Web Ambulatory, a fully interoperable web chart that displays all patient records and relevant information from across the continuum on a single screen. Physicians have instant access to all the information they need for clinical decision-making, including health maintenance items to keep patients healthy, shared allergies and problems lists, and external medical summaries.
            </p>
        </div>
        <div class="container__two-thirds">
            <figure>
                <img src="../images/population-health-2017-screenshot--web-ambulatory.jpg" alt="Web Ambulatory Screenshot">
            </figure>
        </div>
    </div>
</div>
 End of Block 3 -->

 <!-- Block 4 - Patient Registry Screenshot -->
<?php $background="../images/population-health-2017--background-1.jpg" ;?>

<div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php echo $background; ?>);">
    <div class="container__centered">
        <div class="container__two-thirds">
            <figure>
                <img src="../images/population-health-2017-screenshot--wellness-registry.jpg" alt="Patient Registry Screenshot">
            </figure>
        </div>
        <div class="container__one-third">
            <h2 class="no-margin--top">Take action with Patient Registries.</h2>
            <p>Our actionable Patient Registries are now bolstered by Arcadia-supplied information, including claims and a broad range of disparate EHR data. Care teams can examine entire groups of patients, determine who they’re accountable for, and decide on the appropriate interventions — all from just one screen. By identifying at-risk patients, gaps in care, and overdue health maintenance, our Patient Registries can help your organization promote wellness and take a more proactive approach to care coordination and disease management.
            </p>
        </div>
    </div>
</div>
<!-- End of Block 4 -->

<!-- New Block 3 -->
<div class="container bg--emerald">
    <div class="container__centered">
        <div class="page__title--center">
            <div class="container no-pad">
                <h2>
                    Address Social Determinants of Health.
                </h2>
                <p class="auto-margins">
                    Deliver care that reaches far beyond the hospital and physician’s office. Connect patients with social services and community resources, and use Expanse to help them take better control over their own health.
                </p>
            </div>
            <div class="container no-pad--top left">
                <p class="center">
                    See how MEDITECH customers are addressing social determinants of health in their communities:
                </p>
                <div class="container__one-fourth transparent-overlay">
                    <h3 class="center">
                        <a href="https://blog.meditech.com/beyond-numbers-creative-ways-to-address-population-health">Frisbie Memorial Hospital</a>
                    </h3>
                    <p>
                        Found that food insecurity was the root of their super utilizer problem in the ED, as many patients presented only to receive a hot meal. Frisbie partnered with community organizations to host weekly potluck dinners that kept community members properly fed and out of the hospital. 
                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay">
                    <h3 class="center">
                        <a href="https://blog.meditech.com/grocery-store-walkthroughs-one-step-toward-improving-health-literacy">Kalispell Regional Medical Center</a>
                    </h3>
                    <p>
                        Established a program in which community health workers take CHF patients on walkthroughs of their local grocery store to combat health illiteracy and help them establish healthy diets.

                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay">
                    <h3 class="center">
                        <a href="https://blog.meditech.com/how-providers-can-bridge-care-gaps-caused-by-social-determinants-of-health">Southwestern Vermont Medical Center</a>
                    </h3>
                    <p>
                        Designed a transitional care program aimed at addressing the gaps in care that occured due to the social determinants of health affecting their rural community.
                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay">
                    <h3 class="center">
                        <a href="https://blog.meditech.com/the-importance-of-social-determinants-in-behavioral-health">Bristol Hospital</a>
                    </h3>
                    <p>
                        Used surveys to identify patients in need of additional resources or support, and deployed local nursing students to follow up with patients in need.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of New Block 3 -->



<!-- Block 5 - Video -->
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper" style="padding:3em !important;">
            <div class="video js__video video--shadow" data-video-id="288754788">
                <figure class="video__overlay">
                    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay--PPGH-Bridge-to-Health.jpg" alt="Palo Pinto Mobile Health Initiative Video Covershot">
                </figure>
                <a class="video__play-btn" href="http://vimeo.com/288754788?&autoplay=1"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>
                    Palo Pinto Mobile Health Initiative.
                </h2>
                <p>
                    All aboard Palo Pinto’s mobile health clinic! In this video, see how Palo Pinto General Hospital’s fully-equipped medical bus gives their community a “bridge to health,” and enables providers to truly meet their patients where they are.
                </p>
            </div>
        </div>
    </div>
</div>
<!-- End of Block 5 -->

<!-- New Block 2 
<div class="container bg--green-gradient">
    <div class="container__centered">
        <h2>
            Aggregated data to help you see the complete picture.
        </h2>
        <div class="container__two-thirds">
            <p>
                MEDITECH is collaborating with Arcadia Healthcare Solutions to add aggregated data into our Ambulatory and BCA solutions, for a more complete patient record and improved population health management. See and use information from across the continuum (including claims and other-vendor EHR data) to get a better understanding of patient risks and utilization.
            </p>
        </div>
        <div class="container__one-third center">
            <img src="../images/Arcadia--logo.png" alt="Arcadia Logo">
        </div>
    </div>
</div>
End of New Block 2 -->


<!-- Block 6 - Portal Screenshot -->

<?php $background="../images/population-health-2017--background-2.jpg" ;?>

<div class="container background--cover" style="background-image: url(<?php echo $background; ?>);">
    <div class="container__centered">
        <h2 class="text--white">Support your decisions with solid population analytics.</h2>
        <div class="container__two-thirds">
            <p class="text--white">Back your population health initiatives with data that gives you a complete view of outcomes and utilization. MEDITECH’s Business and Clinical Analytics solution lets strategic teams look for trends across patient populations, including readmission rates, chronic conditions, and gaps in care. Armed with this data, you can target programs that deliver the highest quality care at the lowest cost. With Arcada.io data embedded directly into workflows, care teams can fill in the patient story, for an accurate and meaningful patient encounter every time.
            </p>
            <figure>
                <img src="../images/population-health-2017-screenshot--portal.jpg" alt="MEDITECH Patient Portal Screenshot">
            </figure>
        </div>
        <div class="container__one-third">
            <div class="transparent-overlay">
                <h5 class="text--white">Patient and Consumer Health Portal features include:</h5>
                <ul class="text--white left">
                    <li>Video Visits: Allow for convenient, remote interaction</li>
                    <li>Direct Appointment Scheduling: Improves access to care with the most streamlined way to schedule an appointment</li>
                    <li>Questionnaire: Allows patients to complete paperwork prior to visits</li>
                    <li>PGHD (Fitbit data, Blood Pressure and Glucometer information)</li>
                    <li>Online Bill Pay</li>
                </ul>
            </div>
        </div>
    </div>
</div>


<!--	<?php $background="../images/population-health-2017--background-2.jpg" ;?>


			<div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php echo $background; ?>);">
				<div class="container__centered">
					<div class="container__one-third">
						<h2 class="text--white no-margin--top">Engage and empower your communities.</h2>
						<p class="text--white">Help patients to help themselves. The Patient and Consumer Health Portal encourages patients to participate in their care, with easy access to visit history information from across the health system. Patients can also schedule appointments, request prescription renewals, and conveniently communicate with their providers, from one centralized portal.
						</p>
						<h5 class="text--white">Patient and Consumer Health Portal features include:</h5>
						<ul class="text--white left">
							<li>Video Visits: Allow for convenient, remote interaction</li>
							<li>Direct Appointment Scheduling: Improves access to care with the most streamlined way to schedule an appointment</li>
							<li>Questionnaire: Allows patients to complete paperwork prior to visits</li>
							<li>PGHD (Fitbit data, Blood Pressure and Glucometer information)</li>
							<li>Online Bill Pay</li>
						</ul>
					</div>
					<div class="container__two-thirds">
						<figure>
							<img src="../images/population-health-2017-screenshot--portal.jpg" alt="Portal Screenshot">
						</figure>
					</div>
				</div>
			</div>
-->

<!-- End of Block 6 -->

<!-- Block 7 - BCA Quality Screenshot -->
<div class="container" style="background-image: url(../images/styleguide/Gray-Triangles.jpg);">
    <div class="container__centered">
        <div class="container__two-thirds">
            <figure>
                <img src="../images/population-health-2017-screenshot--bca-quality.jpg" alt="BCA Quality Screenshot">
            </figure>
        </div>
        <div class="container__one-third">
            <h2 class="no-margin--top"> Interoperability leads to seamless, coordinated care.</h2>
            <p>True interoperability connects communities and enables better population health strategies by allowing for the sharing of data across all systems. As a Contributor Member of the CommonWell Health Alliance, MEDITECH is committed to technology that will give you the full patient story - so you can always treat the person and not just the problem. MEDITECH understands that Interoperability is a crucial component of the shift to value-based care, and is able to give care teams the patient information needed to deliver optimized care that extends across continuum.
            </p>
        </div>
    </div>
</div>
<!-- End of Block 7 -->

<!-- Block 8 -->
<?php $background="../images/Woman-with-glasses-on-laptop.jpg" ;?>

<div class="container background--cover" style="background-image: url(<?php echo $background; ?>);">
    <div class="container__centered">
        <div class="container__two-thirds">
            <h2 class="text--white">Revenue Cycle Reimbursement.</h2>
            <p class="text--white">MEDITECH will help you navigate the shift to value-based care while keeping your organization financially viable. Unite all care fronts and cover patient access, middle, and back office processes to boost financial performance. A truly centralized business office and consolidated revenue cycle reports allow you to monitor AR days, cash flow, and denials to ensure your organization maintains healthy margins.
            </p>
        </div>
    </div>
</div>
<!-- End of Block 8 -->

<!-- New Block 3 
<div class="container background--cover" style="background-image: url(../images/population-health-2018--background-1.jpg);">
    <div class="container__centered">
        <div class="container__two-thirds">
            &nbsp;
        </div>
        <div class="container__one-third transparent-overlay--white">
            <h2>
                Interoperability leads to seamless, coordinated care.
            </h2>
            <p>
                True interoperability connects communities and enables better population health strategies by allowing for the sharing of data across all systems. As a Contributor Member of the CommonWell Health Alliance, MEDITECH is committed to technology that will give you the full patient story - so you can always treat the person and not just the problem.
            </p>
        </div>
    </div>
</div>
End of New Block 3 -->

<!-- Block 9 -->
<div class="container" style="background-image: url(../images/Gray-Triangles-2.jpg);">
    <div class="container__centered center">
        <h2>
            Learn how MEDITECH's Population Health solution can help your organization coordinate care and improve outcomes.
        </h2>
        <a href="https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=b141e1b2-e8da-4eef-9775-53991f48c915&placement_guid=46908580-0b16-410a-b0dc-25a2485ff75e&portal_id=2897117&canon=https%3A%2F%2Fehr.meditech.com%2Fehr-solutions%2Fmeditech-population-health&redirect_url=APefjpEJF2lJGk3xx45WYlLLwKT9o4DOTy5tmCOLToXu8MrwRsYgQM-cQO5pv8Pqf83KG2s71AiEnM0hn6iRWmox-Hm4ebvuPRnqCVEv6CPnsze8vBPljWESSlqIQ1eKtWC3AbvHdiOrovlj9p747gSW1fUdM3MEbeSgnhC0gVnEDAJ8gSzcb7KclroDRw23-Luwe7okZ4I8c2jiXeJNxSdtfdWAZDdUh2maPKJJYe2JklvECNpiG8g3j7NbZBLD7GIFkHLuk8b430NgCFgJloZQBzod-HLvPpjTsS6lRDl1fHyTe_vH8bs&click=9d789eac-28e9-4b96-b6f9-90aff48d5c9c&hsutk=a48be55e9d79b320d0e2ea1cb0d9a300&utm_referrer=https%3A%2F%2Fehr.meditech.com%2Fehr-solutions&__hstc=188859636.a48be55e9d79b320d0e2ea1cb0d9a300.1500408509300.1538499599339.1539193829246.114&__hssc=188859636.4.1539193829246&__hsfp=2413047092" class="btn--orange">Sign Up For The Population Health Webinar</a>
    </div>
</div>
<!-- End of Block 9 -->

<!-- FOOTER -->
<?php include( "_includes/footer.inc"); ?>
