<?php // This template is set up to control the display of the 'simple page' content type
$url = $GLOBALS['base_url']; // grabs the site url
?>

<!-- start node--simple-page.tpl.php template -->

<?php
// list of simple page nodes that have specific templates
// this list must be updated with any new templates created...
$node_id = $node->nid;
$special_templates = array(358, 1172, 1086, 1863, 3499, 2402, 3267, 3269);

// if current page's node id is in the above template, a specific template will be used..
if( in_array($node_id, $special_templates) === true ){
  include('simple-page--node-'.$node_id.'.php');
} 

else { // all other simple pages with or without minor additions...
?>

   
    <section class="container__centered">
     
      <div class="container__two-thirds">
        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
        <div class="js__seo-tool__body-content">
         
        
          <?php print render($content['field_body']); ?>
    
          
          <?php // ================= Page Updated Notice ================ 
          if($node->published_at){
            switch($node->nid){
              case 3355:
              case 3357:
                print '<p><em>This page was last updated on: <time datetime="'.date('Y-m-d', $node->revision_timestamp).'" itemprop="datePublished">'.date('F j, Y', $node->revision_timestamp).'</time></em></p>';
                break;
              default:
                print '';
            }
          }
          ?>
          
          
          
          <?php
          if($node->nid == 344){ // ================= PAGE NOT FOUND ======================
            // rather than displaying a 'Page Not Found' message, we redirect to search results page and try to provide helpful links...
            // get current URL info to determine which layout to render...
            $currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            // remove main domain portain of url...
            $page_not_found = str_replace($url.'/', '', $currentURL);
            // replace certain characters with plus signs for searching...
            $items_to_replace = array('-', '_', '/', '.');
            $replace_with_these = array('+', '+', '+', '+');
            $term_to_search = str_replace($items_to_replace, $replace_with_these, $page_not_found);
            // redirect to search results ['pnf' stands for 'page not found']...
            header('Location: https://ehr.meditech.com/search-results?as_q='.$term_to_search.'&action=pnf');
            exit();
          }
          ?>
        </div>
      </div>
      
      
      <?php // ================= Valley case study page sidebar (temp) ================
        if($node->nid == 3547){
          print '<!-- SIDEBAR -->
            <aside class="container__one-third">
              <div class="panel">
                <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/valley-hospital.jpg" alt="Valley Hospital building" width="528" height="382" style="height:auto;" />
                
                <h2>The Valley Hospital at a glance</h2>

                <p>Located 26 miles from New York City, The Valley Hospital (Ridgewood, NJ) serves approximately 440,000 people in the Bergen County area. </p>

                <p>About Valley: </p>

                <ul>
                  <li>Part of Valley Health System, which also includes Valley Medical Group and Valley Home Care.&nbsp;</li>
                  <li>Recent awards include:
                  <ul><li>Pinnacle of Excellence Award in Patient Experience by Press Ganey.</li>
                    <li>“A” grade in patient safety from The Leapfrog Group.</li>
                  </ul></li>
                  <li>The hospital is dedicated to community service, providing healthcare education, support groups, classes, and screenings to those in need.</li>
                </ul>    
              </div>
            </aside>
            <!-- END SIDEBAR -->';
        }
      ?>
      
      
      <?php // ================= COVID-19 related sidebar ================
        switch($node->nid){
          case 3252:
          case 3283:
          case 3291:
          case 3355:
          case 3357:
            print '<!-- SIDEBAR -->
              <aside class="container__one-third">
                <div class="panel">
                <div class="sidebar__nav news_sidebar_gae">';
            include('inc-covid19-sidebar.php');
            print '</div>
                </div>
              </aside>
              <!-- END SIDEBAR -->';
            break;
          default:
            print '';
        }
      ?>
     

  </section>

<?php } ?>

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    if($node->nid != 1863){ // don't show on case study page...
      print '<!-- SEO Tool is added to this div -->';
      print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
    }
  }
?>

<!-- end node--simple-page.tpl.php template -->