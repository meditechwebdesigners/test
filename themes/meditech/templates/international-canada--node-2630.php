<!-- START international-canada--node-2630.php Canadian Campaign -->

<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
?>

<style>
    .transparent-overlay--white a:link {
        color: #3e4545;
    }

    .transparent-overlay--white a:active {
        color: #3e4545;
    }

    .transparent-overlay--white a:visited {
        color: #3e4545;
    }

    .transparent-overlay--white a:hover {
        color: #00bbb3;
    }

    .image-container {
        position: relative;
    }

    .image-container img {
        border-radius: 0.5em;
        margin-top: 1em;
        max-width: 100%;
        z-index: 1;

    }

    .grid-item {
        background-color: rgba(0, 0, 0, 0.6);
    }

    .bg-overlay--white {
        background-color: none;
    }

    @media all and (max-width: 30em) {
        .grid-item {
            border: none;
        }
    }

    @media all and (max-width: 800px) {
        .bg-overlay--white {
            background-color: rgba(255, 255, 255, 0.8);
        }

        .transparent-overlay--white {
            background-color: rgba(255, 255, 255, 0.0);
        }
    }

</style>

<!-- Block 1 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/hikers-looking-over-lake-in-canadian-wilderness.jpg);">
    <div class="container__centered text--white">
        <div class="container__one-half transparent-overlay--xp">
            <!--      <h1>MEDITECH in Canada</h1>-->
            <h1>Connecting Canada with MEDITECH’s EHR</h1>
            <!--      <h2>Connecting Canada with MEDITECH’s EHR</h2>-->
            <p>Clinicians, hospitals, and health authorities from Toronto to Nunavut rely on MEDITECH to connect them to the information they need to deliver quality care. That’s why we’re the market leader, with a 35-year history of uniting communities.</p>
            <div class="btn-holder--content__callout">
                <?php 
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $cta->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
                <div class="button--hubspot">
                    <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>">
                            <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png" alt="button" /></a></span>
                        <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                        <script type="text/javascript">
                            hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});

                        </script>
                    </span>
                    <!-- end HubSpot Call-to-Action Code -->
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- End Block 1 -->

<!-- Block 2 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/toronto-city-skyline.jpg); background-position: top;">
    <div class="container__centered text--white">
        <h2 class="center">Canada’s market leader.</h2>
        <div class="grid-items-lines text-shadow--black" style="padding-top:2em;">
            <div class="grid-item" style="border-color:#fff;">
                <h3 class="grid-item--numbers">
                    35+
                </h3>
                <p>
                    years of Canadian partnerships
                </p>
            </div>
            <div class="grid-item" style="border-color:#fff;">
                <h3 class="grid-item--numbers">
                    47%
                </h3>
                <p>
                    of the Canadian market
                </p>
            </div>
            <div class="grid-item" style="border-right:none; border-color:#fff;">
                <h3 class="grid-item--numbers">
                    7 out of 10
                </h3>
                <p>
                    Provinces and 2 out of 3 Territories
                </p>
            </div>
            <div class="grid-item" style="border-bottom:none; border-color:#fff;">
                <h3 class="grid-item--numbers">
                    50+
                </h3>
                <p>
                    LIVE regional installations
                </p>
            </div>
            <div class="grid-item" style="border-bottom:none; border-color:#fff;">
                <h3 class="grid-item--numbers">
                    500+
                </h3>
                <p>
                    Inpatient facilities
                </p>
            </div>
            <div class="grid-item" style="border-bottom:none; border-right:none; border-color:#fff;">
                <h3 class="grid-item--numbers">
                    38%
                </h3>
                <p>
                    of Stage 6 &amp; 7 hospitals
                </p>
            </div>
        </div>
    </div>
</div>
<!-- End Block 2 -->

<!-- Start Block 3 -->
<div class="bg--blue-gradient">
    <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/faint-white-connected-lines.png); padding: 4em 0;">
        <div class="container__centered">
            <h2>Introducing our EHR Excellence Toolkits for Canada.</h2>
            <div class="container__two-thirds">
                <p>MEDITECH's evidence-based <a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-fall-risk-management-toolkit">Fall Risk Management</a> and <a href="https://ehr.meditech.com/news/meditechs-antimicrobial-stewardship-toolkit-now-available">Antimicrobial Stewardship Toolkits</a> are now available in Canada. Designed using Expanse technology, these step-by-step guides help your organization get the most from our EHR. They're built and maintained in collaboration with staff physicians, clinicians, and customers, to get you up and running fast.</p>
                <p>Please visit our <a href="https://customer.meditech.com/en/d/ehrexcellence/homepage.htm">EHR Excellence Toolkits</a> website for implementation guidance and an educational video.</p>
            </div>
            <div class="container__one-third">
                <div class="container__one-half center">
                    <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/antimicrobial--toolkit-icon.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/antimicrobial--toolkit-icon.png';this.onerror=null;" alt="Antimicrobial icon">
                    <h4><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-antimicrobial-stewardship-toolkit" rel="noreferrer noopener">Antimicrobial
                            <br>Stewardship</a></h4>
                </div>
                <div class="container__one-half center">
                    <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/fall-risk--toolkit-icon.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/fall-risk--toolkit-icon.png';this.onerror=null;" alt="person falling icon">
                    <h4><a href="https://ehr.meditech.com/ehr-solutions/ehr-toolkits/meditechs-fall-risk-management-toolkit" rel="noreferrer noopener">Fall Risk
                            <br>Management</a></h4>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Block 3 -->

<!-- Start Block 4 -->
<div class="container background--cover no-pad" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/group-of-various-hospital-staff-huddled-around-tablet.jpg);  background-position:right;">
    <div class="container__centered bg-overlay--white" style="padding: 4em 1em;">
        <div class="container__one-half" style="padding-right: 3em;">
            <h2 class="no-margin--bottom">CHAMP-ions of connected care in Eastern Ontario.</h2>
            <div class="page__title__ribbon"></div>
            <p style="margin-top:1em;"><a href="https://ehr.meditech.com/news/ottawa-based-health-network-champ-excited-to-move-forward-with-meditech-s-web-ehr">CHAMP</a>, the Champlain Association of MEDITECH Partners, consists of six local hospitals working cooperatively to deliver enhanced patient care with Expanse.</p>
        </div>
        <div class="container__one-half transparent-overlay--white">
            <ul>
                <li>
                    <h4><a href="https://www.arnpriorregionalhealth.ca/" style="border-bottom:0;" target="_blank">Arnprior Regional Health</a></h4>
                </li>

                <li>
                    <h4><a href="https://www.bruyere.org/" style="border-bottom:0;" target="_blank">Bruyère Continuing Care</a></h4>
                </li>

                <li>
                    <h4><a href="https://hopitalmontfort.com/" style="border-bottom:0;" target="_blank">Hôpital Montfort</a></h4>
                </li>

                <li>
                    <h4><a href="https://hgmh.on.ca/" style="border-bottom:0;" target="_blank">Hôpital Glengarry Memorial Hospital</a></h4>
                </li>

                <li>
                    <h4><a href="https://www.carletonplacehospital.ca/hospital" style="border-bottom:0;" target="_blank">Carleton Place &amp; District Memorial Hospital</a></h4>
                </li>

                <li>
                    <h4><a href="https://www.qch.on.ca/" style="border-bottom:0;" target="_blank">Queensway Carleton Hospital</a></h4>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- End Block 4 -->

<!-- Start Block 5 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/cambridge-bay-nunavut.jpg);">
    <div class="container__centered text--white auto-margins center" style="padding-top:2em; padding-bottom:7em;">
        <h2>Expanse brings remote regions closer together.</h2>
        <p>Nunavut’s Department of Health and Social Services is moving forward with MEDITECH Expanse to connect 32 isolated communities on a single health record. Transportation in this arctic territory is usually by air and sea, and in some instances, snowmobile.</p>
    </div>
</div>
<!-- End Block 5 -->

<!-- Start Block 6 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/isometric-grid-background.png);">
    <div class="container__centered">
        <div class="container__one-half">
            <div class="image-container">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/Humber-River-Command-Center-Image-SQ6_2302.jpg" alt="Humber River Command Centre">
            </div>
        </div>
        <div class="container__one-half">
            <h2><a href="https://ehr.meditech.com/news/humber-river-command-centre-featured-in-usa-today">Canada’s first digital command centre</a> helps to drive positive patient outcomes.</h2>
            <p class="quote__content__text italic">"MEDITECH was instrumental in working with our technology team to successfully integrate more than 15 directly connected interfaces, which enables patient data to be transmitted in real time. Today, we’re providing better coordinated care and enabling clinicians to use all of their resources to make more informed decisions."</p>
            <p class="text--large no-margin--bottom"><span class="bold">Kevin Fernandes, Chief Technical Officer</span></p>
            <p>Humber River Hospital</p>
        </div>
    </div>
</div>
<!-- End Block 6 -->

<!-- Start Block 7 -->
<div class="container center background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/international/canada/aerial-view-of-downtown-vancouver-at-sunset.jpg); background-position:top;">
    <div class="container__centered">
        <div class="auto-margins">
            <h2>Connecting patient medication histories in British Columbia.</h2>
            <p>Physicians, nurses, and pharmacists at <a href="https://www.interiorhealth.ca/FindUs/_layouts/FindUs/info.aspx?type=Location&loc=South%20Okanagan%20General%20Hospital&svc=Patient/Family%20Counselling&ploc=N/A" target="_blank">South Okanagan General Hospital</a> (Oliver, BC) electronically retrieve medication histories province-wide, thanks to the seamless integration of MEDITECH, <a href="https://ihintheloop.ca/new-technology-improves-patient-safety-starting-at-sogh/" target="_blank">Pharmnet</a>, and <a href="https://ehr.meditech.com/news/meditech-drfirst-team-up-to-integrate-medication-history-into-south-okanagans-ehr" target="_blank">MedHx, from DrFirst</a>. The electronic patient medication history service helps to:</p>
        </div>
        <div class="container__centered text--white">
            <div class="container__one-fourth transparent-overlay">
                <div class="text--meditech-green" style="padding-bottom: .5em;"><i class="fas fa-file-prescription fa-3x"></i></div>
                <h5>Provide comprehensive patient prescription and allergy histories </h5>
            </div>
            <div class="container__one-fourth transparent-overlay">
                <div class="text--meditech-green" style="padding-bottom: .5em;"><i class="fas fa-diagnoses fa-3x"></i></div>
                <h5>Reduce adverse drug reactions (ADRs)</h5>
            </div>
            <div class="container__one-fourth transparent-overlay">
                <div class="text--meditech-green" style="padding-bottom: .5em;"><i class="fas fa-prescription-bottle fa-3x"></i></div>
                <h5>Streamline medication reconciliation</h5>
            </div>
            <div class="container__one-fourth transparent-overlay">
                <div class="text--meditech-green" style="padding-bottom: .5em;"><i class="fas fa-file-signature fa-3x"></i></div>
                <h5>Eliminate manual data entry, thus reducing potential errors</h5>
            </div>
        </div>
    </div>
</div>
<!--End Block 7 -->

<!-- Start Block 8 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/isometric-grid-background.png);">
    <div class="container__centered">
        <div class="center auto-margins">
            <h2>Keeping Alberta’s communities healthier.</h2>
            <p>Alberta Health Services deployed MEDITECH’s solution across five public health zones in Edmonton, to consolidate the province. More than 1.3 million patient records and 11 million immunizations records were converted into electronic files as part of the project.</p>
        </div>
        <figure class="container__one-fourth center">
            <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="Quote bubble">
        </figure>
        <div class="container__three-fourths">
            <div class="quote__content__text text--large" style="margin-top:1em;">
                <p class="italic">"The partnership we have with MEDITECH was evident every day. We worked arm-in-arm and functioned as a single collaborative team with the clinical business. We’re proud of the success we share in this and continue to value the ongoing relationship between AHS and MEDITECH."</p>
            </div>
            <p class="text--large no-margin--bottom"><span class="bold">Michael Cleghorn, Executive Director of IT Rural Clinical Services</span></p>
            <p>Alberta Health Services</p>
        </div>
    </div>
</div>
<!-- End Block 8 -->

<!-- Block 9 -->
<div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered" style="text-align: center;">
        <h2 class="text--white">Learn how Expanse Point of Care puts medication administration, documentation, and chart review in the palm of your hand. Hear an early adopter's experience with this functionality.</h2>
        <div class="btn-holder--content__callout">
            <?php 
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $cta->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
            <div class="button--hubspot">
                <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>">
                        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png" alt="button" /></a></span>
                    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                    <script type="text/javascript">
                        hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});

                    </script>
                </span>
                <!-- end HubSpot Call-to-Action Code -->
            </div>
            <?php } ?>
        </div>
        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>
    </div>
</div>
<!-- End Block 9 -->

<!-- END international-canada--node-2630.php -->
