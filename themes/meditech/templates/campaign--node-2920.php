<!-- START campaign--node-2920.php -->

<?php // This template is set up to control the display of the News Test Page

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .flex-container {
    padding: 0;
    margin: 0;
    list-style: none;

    -ms-box-orient: horizontal;
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -moz-flex;
    display: -webkit-flex;
    display: flex;

    -webkit-justify-content: space-around;
    justify-content: space-around;
    -webkit-flex-flow: row wrap;
    flex-flow: row wrap;
    -webkit-align-items: stretch;
    align-items: stretch;
  }

  .flex-item {
    flex-grow: 1;
    flex-wrap: wrap;
    background: tomato;
    border: 3px solid rgba(0, 0, 0, .2);
    line-height: 100px;
    color: white;
    font-weight: bold;
    font-size: 2em;
    text-align: center;
  }

</style>

<div class="container">
  <div class="container__centered">
    <div class="flex-container">
      <div class="flex-item">1</div>
      <div class="flex-item">2</div>
      <div class="flex-item">3</div>
    </div>
  </div>
</div>




<hr>

<style>
  .cards {
    display: flex;
    flex-wrap: wrap;
    list-style: none;
    margin-bottom: 2em;
    padding: 0;
  }

  .cards__item {
    width: 33.3333%;
    display: flex;
    padding: 1.5%;
  }

  .cards li {
    margin-bottom: 0;
  }

  .card {
    background-color: #fff;
    border-radius: 6px;
    box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
    display: flex;
    flex-direction: column;
    overflow: hidden;
  }

  .article--wrap {
    display: flex;
    flex-flow: column wrap;
    align-content: space-between;
    height: 1825px;
    align-items: center;
    /* Wrapping container needs a fixed height, and it needs to be taller than the tallest column. */
  }

  .article--card {
    width: 31.33333%;
    background: #fff;
    border-radius: 6px;
    box-shadow: 0px 0px 20px 1px rgba(120, 120, 120, .2);
    position: relative;
    margin-bottom: 3%;
    box-sizing: border-box;
  }

  .article--info {
    padding: .5em 1em 1em 1em;
  }

  .article--img {
    position: relative;
  }

  .article--img img {
    border-radius: 6px 6px 0 0;
    max-height: 250px;
  }

  .article--date {
    padding: .5em 1em;
    background-color: #e5f8f0;
    position: absolute;
    border-radius: 0 6px 0 0;
    bottom: 9px;
  }

  .text--small a {
    border-bottom: none;
  }

  /* Re-order items into 3 rows */
  .article--card:nth-child(3n+1) {
    order: 1;
  }

  .article--card:nth-child(3n+2) {
    order: 2;
  }

  .article--card:nth-child(3n) {
    order: 3;
  }

  /* Force new columns */
  .article--wrap::before,
  .article--wrap::after {
    content: "";
    flex-basis: 100%;
    width: 0;
    order: 2;
  }

  .btn--orange-gradient {
    background-image: linear-gradient(45deg, #fe9418 0%, #f5634b 75%);
    border-radius: 25px;
    color: #fff;
    font-size: 16px;
    font-weight: 700;
    display: inline-block;
    text-align: center;
    /*    white-space: nowrap;*/
    padding: 0em 1.5em;
    line-height: 2.8;
    flex: 1 1 auto;
    transition: 0.5s;
    background-size: 200% auto;
    /*    box-shadow: 0 0 20px #eee;*/
  }

  .btn--orange-gradient:hover {
    color: #fff;
    background-position: right center;
  }


  @media (max-width: 50em) {
    .cards__item {
      width: 50%;
    }
  }

  @media all and (max-width: 31.250em) {
    .cards__item {
      width: 100%;
    }
  }

  @media all and (max-width: 50em) {
    .article--card {
      width: 48.5%;
    }

    .article--wrap {
      height: 2500px;
    }

    .article--date {
      bottom: 7px;
    }

    /* Re-order items into 2 rows */
    .article--card:nth-child(2n+1) {
      order: 1;
    }

    .article--card:nth-child(2n) {
      order: 2;
    }
  }

  @media all and (max-width: 31.250em) {
    .article--card {
      width: 100%;
    }

    .article--wrap {
      height: 3500px;
    }

    /* Re-order items into 2 rows */
    .article--card:nth-child(1n) {
      order: 1;
    }
  }

</style>

<!-- Start News Articles Block - Equal Height Flexbox -->
<div class="container">
  <div class="container__centered">
    <h1>News Article Blocks - Equal Height Flexbox</h1>


    <ul class="cards">

      <li class="cards__item">

        <div class="card">

          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/MEDITECH-maintains-Canadian-EHR-market--article.jpg?itok=Ka-2oJCP&timestamp=1569257612">
            <div class="article--date text--small">September 24, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">MEDITECH Maintains Momentum in Canadian EHR Market</a></h3>
            <p>MEDITECH continues growth in 2019 with additional Expanse implementations and the launch of two new toolkits.</p>
            <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>
          </div>

        </div>

      </li>
      <li class="cards__item">
        <div class="card">
          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Fall-Risk-Management-2019--ARTICLE.jpg?itok=lL1hkdk5&timestamp=1564066069">
            <div class="article--date text--small">September 23, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">Supporting Chronic Care at Newman Regional Health (Video)</a></h3>
            <p>Watch Cathy Pimple, Chief Quality &amp; Compliance Officer at Newman Regional Health, discuss how capabilities such as BCA and Patient Registries are...</p>
            <p class="text--small"><a href="#">C-Level</a>, <a href="#">Physician</a>, <a href="#">Videos</a></p>
          </div>
        </div>
      </li>
      <li class="cards__item">
        <div class="card">
          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Aga-Khan-University-selects-MEDITECH--article.jpg?itok=dhKWBew7&timestamp=1568897373">
            <div class="article--date text--small">September 19, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">Aga Khan University Selects MEDITECH for International EHR Partnership</a></h3>
            <p>MEDITECH has further extended its global presence by partnering with Aga Khan University to deploy Expanse.</p>
            <p class="text--small"><a href="#">Corporate</a>, <a href="#">Press Releases</a>, <a href="#">Signings</a>, <a href="#">South Africa</a></p>
          </div>
        </div>
      </li>
      <li class="cards__item">
        <div class="card">
          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/World-Sepsis-Day--ARTICLE_0.png?itok=I4EAWFE6&timestamp=1568142022">
            <div class="article--date text--small">September 13, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">Join MEDITECH in Signing the World Sepsis Declaration</a></h3>
            <p>Thousands of organizations and individuals have committed to “this common call to worldwide action.”</p>
            <p class="text--small"><a href="#">Corporate</a>, <a href="#">C-Level</a>, <a href="#">Nurse</a>, <a href="#">Physician</a></p>
          </div>
        </div>
      </li>
      <li class="cards__item">
        <div class="card">
          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/FMH-Reduces-Sepsis-Mortality-Rate-by-65-Percent-with-MEDITECH.jpg?itok=N35V4xgV&timestamp=1568123296">
            <div class="article--date text--small">September 10, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">Frederick Memorial's Sepsis Success Featured by PSQH</a></h3>
            <p>Authored by Debra O’Connell and Lauren Small, the PSQH article explains how FMH was able to decrease sepsis mortality rates by 65 percent and improve SEP-1 compliance rates by 80 percent.</p>
            <p class="text--small"><a href="#">MEDITECH in the Press</a>, <a href="#">Press Releases</a>, <a href="#">Success Stories</a></p>
          </div>
        </div>
      </li>
      <li class="cards__item">
        <div class="card">
          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Sepsis-Awareness-Month--ARTICLE.png?itok=UYRJAx6s&timestamp=1567773326">
            <div class="article--date text--small">September 6, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">Recognize Sepsis Awareness Month with MEDITECH</a></h3>
            <p>This September, take a few minutes to discover how MEDITECH’s EHR helps care teams to detect sepsis sooner and intervene earlier.</p>
            <p class="text--small"><a href="#">C-Level</a>, <a href="#">Nurse</a>, <a href="#">Quality Outcomes</a></p>
          </div>
        </div>
      </li>
      <li class="cards__item">
        <div class="card">
          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/MEDITECH-UK-Positioned-for-Continued-Growth-in-Region.jpg?itok=oCn_1bQi&timestamp=1566479372">
            <div class="article--date text--small">August 22, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">MEDITECH UK Positioned for Continued Growth in Region</a></h3>
            <p>MEDITECH UK has furthered its presence in the region and continues to deliver on its commitment to customers since it was established last year in partnership with MEDITECH.</p>
            <p class="text--small"><a href="#">Events</a>, <a href="#">Industry Awards</a>, <a href="#">Press Releases</a>, <a href="#">United Kingdom</a></p>
          </div>
        </div>
      </li>
      <li class="cards__item">
        <div class="card">
          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Home-Health-Agencies-Prepare-for-New-PDGM_0.jpg?itok=03J97rPQ&timestamp=1565118868">
            <div class="article--date text--small">August 9, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">Home Health Agencies Prepare for New Patient-Driven Groupings Model (Part Two)</a></h3>
            <p>The second article in our PDGM series explores how home health agencies can prepare for the new regulatory changes, including the Five Factors for Determining Payment.</p>
            <p class="text--small"><a href="#">Home Care</a>, <a href="#">Regulatory</a></p>
          </div>
        </div>
      </li>
      <li class="cards__item">
        <div class="card">
          <figure class="article--img">
            <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/MEDITECH-Accepted-As-Accredited-EPR-Supplier-By-NHSX-and-NHS-England.jpg?itok=Wje206zh&timestamp=1565279447">
            <div class="article--date text--small">August 8, 2019</div>
          </figure>
          <div class="article--info">
            <h3 class="header-four"><a class="news_main_link_gae" href="#">MEDITECH Accepted As Accredited EPR Supplier By NHSX and NHS England</a></h3>
            <p>NHSX and NHS England have accepted MEDITECH as an accredited electronic patient record supplier as part of their efforts to help health organisations and integrated care systems access cutting-edge and solutions.</p>
            <p class="text--small"><a href="#">Corporate</a>, <a href="#">Solutions</a>, <a href="#">United Kingdom</a></p>
          </div>
        </div>
      </li>
    </ul>
    <div class="center">
      <a href="#" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Load More</a>
    </div>
  </div>
</div>
<!-- End News Articles Block - Equal Height Flexbox -->

<hr>

<!-- Start News Articles Block - Masonry & Flexbox -->


<div class="container">
  <div class="container__centered">
    <h1>News Article Blocks - Masonry &amp; Flexbox</h1>
    <div class="article--wrap">

      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/MEDITECH-maintains-Canadian-EHR-market--article.jpg?itok=Ka-2oJCP&timestamp=1569257612">
          <div class="article--date text--small">September 24, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">MEDITECH Maintains Momentum in Canadian EHR Market</a></h3>
          <p>MEDITECH continues growth in 2019 with additional Expanse implementations and the launch of two new toolkits.</p>
          <p class="text--small"><a href="#">Canada</a>, <a href="#">Corporate</a>, <a href="#">Press Releases</a></p>
        </div>
      </div>
      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Fall-Risk-Management-2019--ARTICLE.jpg?itok=lL1hkdk5&timestamp=1564066069">
          <div class="article--date text--small">September 23, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">Supporting Chronic Care at Newman Regional Health (Video)</a></h3>
          <p>Watch Cathy Pimple, Chief Quality &amp; Compliance Officer at Newman Regional Health, discuss how capabilities such as BCA and Patient Registries are driving positive changes in healthcare outcomes and costs.</p>
          <p class="text--small"><a href="#">C-Level</a>, <a href="#">Physician</a>, <a href="#">Videos</a></p>
        </div>
      </div>
      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Aga-Khan-University-selects-MEDITECH--article.jpg?itok=dhKWBew7&timestamp=1568897373">
          <div class="article--date text--small">September 19, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">Aga Khan University Selects MEDITECH for International EHR Partnership</a></h3>
          <p>MEDITECH has further extended its global presence by partnering with Aga Khan University to deploy Expanse.</p>
          <p class="text--small"><a href="#">Corporate</a>, <a href="#">Press Releases</a>, <a href="#">Signings</a>, <a href="#">South Africa</a></p>
        </div>
      </div>
      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/World-Sepsis-Day--ARTICLE_0.png?itok=I4EAWFE6&timestamp=1568142022">
          <div class="article--date text--small">September 13, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">Join MEDITECH in Signing the World Sepsis Declaration</a></h3>
          <p>Thousands of organizations and individuals have committed to “this common call to worldwide action.”</p>
          <p class="text--small"><a href="#">Corporate</a>, <a href="#">C-Level</a>, <a href="#">Nurse</a>, <a href="#">Physician</a></p>
        </div>
      </div>
      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/FMH-Reduces-Sepsis-Mortality-Rate-by-65-Percent-with-MEDITECH.jpg?itok=N35V4xgV&timestamp=1568123296">
          <div class="article--date text--small">September 10, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">Frederick Memorial's Sepsis Success Featured by PSQH</a></h3>
          <p>Authored by Debra O’Connell and Lauren Small, the PSQH article explains how FMH was able to decrease sepsis mortality rates by 65 percent and improve SEP-1 compliance rates by 80 percent.</p>
          <p class="text--small"><a href="#">MEDITECH in the Press</a>, <a href="#">Press Releases</a>, <a href="#">Success Stories</a></p>
        </div>
      </div>
      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Sepsis-Awareness-Month--ARTICLE.png?itok=UYRJAx6s&timestamp=1567773326">
          <div class="article--date text--small">September 6, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">Recognize Sepsis Awareness Month with MEDITECH</a></h3>
          <p>This September, take a few minutes to discover how MEDITECH’s EHR helps care teams to detect sepsis sooner and intervene earlier.</p>
          <p class="text--small"><a href="#">C-Level</a>, <a href="#">Nurse</a>, <a href="#">Quality Outcomes</a></p>
        </div>
      </div>
      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/MEDITECH-UK-Positioned-for-Continued-Growth-in-Region.jpg?itok=oCn_1bQi&timestamp=1566479372">
          <div class="article--date text--small">August 22, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">MEDITECH UK Positioned for Continued Growth in Region</a></h3>
          <p>MEDITECH UK has furthered its presence in the region and continues to deliver on its commitment to customers since it was established last year in partnership with MEDITECH.</p>
          <p class="text--small"><a href="#">Events</a>, <a href="#">Industry Awards</a>, <a href="#">Press Releases</a>, <a href="#">United Kingdom</a></p>
        </div>
      </div>
      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/Home-Health-Agencies-Prepare-for-New-PDGM_0.jpg?itok=03J97rPQ&timestamp=1565118868">
          <div class="article--date text--small">August 9, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">Home Health Agencies Prepare for New Patient-Driven Groupings Model (Part Two)</a></h3>
          <p>The second article in our PDGM series explores how home health agencies can prepare for the new regulatory changes, including the Five Factors for Determining Payment.</p>
          <p class="text--small"><a href="#">Home Care</a>, <a href="#">Regulatory</a></p>
        </div>
      </div>
      <div class="article--card">
        <figure class="article--img">
          <img src="https://ehr.meditech.com/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_big-medium_2x/public/images/news/MEDITECH-Accepted-As-Accredited-EPR-Supplier-By-NHSX-and-NHS-England.jpg?itok=Wje206zh&timestamp=1565279447">
          <div class="article--date text--small">August 8, 2019</div>
        </figure>
        <div class="article--info">
          <h3 class="header-four"><a class="news_main_link_gae" href="#">MEDITECH Accepted As Accredited EPR Supplier By NHSX and NHS England</a></h3>
          <p>NHSX and NHS England have accepted MEDITECH as an accredited electronic patient record supplier as part of their efforts to help health organisations and integrated care systems access cutting-edge and solutions.</p>
          <p class="text--small"><a href="#">Corporate</a>, <a href="#">Solutions</a>, <a href="#">United Kingdom</a></p>
        </div>
      </div>

    </div>
    <!--
    <div class="center">
      <a href="#" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Load More</a>
    </div>
-->
  </div>
</div>
<!-- End News Articles Block - Masonry & Flexbox -->

<!-- END campaign--node-2920.php -->
