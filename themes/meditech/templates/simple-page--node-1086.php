<!-- start simple-page--node-1086.tpl.php template -- MEDIA COMMUNICATIONS PAGE -->

<!-- Hero -->
<div class="content__callout">
  <div class="content__callout__media content__callout__bg__img--green" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Communications-Team_hero.jpg);">
    <!-- Title -->
    <div class="content__callout__title-wrapper--green">
      <h1 class="content__callout__title js__seo-tool__title">MEDITECH Communications Team</h1>
    </div>
  </div>
  <div class="content__callout__content--green js__seo-tool__body-content">
    <p>You can find helpful background information in our <a href="https://home.meditech.com/en/d/mktcontent/otherfiles/meditechhighlights.pdf" title="corporate infographic">Corporate Infographic</a>, while our <a href="<?php print $url; ?>/news" title="MEDITECH News">News page</a> contains the latest updates on MEDITECH, our solutions, and customers.</p>
  </div>
</div>
<!-- Hero -->

<section class="container__centered">
  <div class="container__two-thirds">

    <div class="container no-pad--bottom">

      <div class="container no-pad js__seo-tool__body-content">
      
      <!-- Christina Noel -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/ChristinaNoel.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Christina Noel</h3>
            <p>Social Media Coordinator<br>
              <a href="https://twitter.com/Christina_Noel" target="_blank" title="@Christina_Noel">@Christina_Noel</a>
            </p>
          </figcaption>
        </figure>
      
      <!-- Amanda Tullos -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/AmandaTullos.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Amanda Tullos</h3>
            <p>Public and Media Relations Manager<br>
              <a href="https://twitter.com/MEDITECHAmanda" target="_blank" title="@MEDITECHAmanda">@MEDITECHAmanda</a>
            </p>
          </figcaption>
        </figure>
        
      <!-- Robin Montville -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/RobinMontville.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Robin Montville</h3>
            <p>Public and Media Relations Manager<br>
              <a href="https://twitter.com/MontvilleRobin" target="_blank" title="@MontvilleRobin">@MontvilleRobin</a>
            </p>
          </figcaption>
        </figure>

      </div><!-- END container -->

      <!-- CONTACT FORM -->

      <h3><i class="fas fa-envelope meditech-green"></i>&nbsp; Send us a message</h3>

      <?php
        $formContent = module_invoke('webform', 'block_view', 'client-block-1087');
        print render($formContent['content']);
      ?>
    </div>

  </div><!-- END container__two-thirds -->


  <!-- SIDEBAR -->
  <aside class="container__one-third panel communications_sidebar_gae">

    <div class="sidebar__nav js__seo-tool__body-content">
      <ul class="menu">
        <li class=""><a href="https://home.meditech.com/en/d/mktcontent/otherfiles/meditechhighlights.pdf" title="MEDITECH corporate infographic">Corporate Infographic</a></li>
        <li class=""><a href="<?php print $url; ?>/news-tags/press-releases">MEDITECH Press Releases</a></li>
        <li class=""><a href="<?php print $url; ?>/news">MEDITECH News</a></li>
      </ul>
      <br>
      <p class="bold">Boilerplate</p>
      <p>The next digital transformation of healthcare is underway, and once again, MEDITECH is leading the charge. We’ve been unwavering in preparing for the demands of this paradigm shift, and now we’re all-in with Expanse, the only full-scale EHR designed specifically for the post-Meaningful Use era. Our cutting-edge technology is helping organizations all over the world to see healthcare through a new lens, and navigate this virtual landscape with unparalleled vision and clarity. Whether your destination is clinical efficiency, analytical prowess, or financial success, MEDITECH’s bold innovation, passion, and expertise will get you where you want to go.</p>
      <p><em>*Intended for use by media only, unless given permission otherwise.</em></p>
    </div>

    <a class="twitter-timeline" data-lang="en" data-width="360" data-height="500" data-dnt="true" data-theme="light" data-link-color="#ff610a" href="https://twitter.com/MEDITECH">Tweets by MEDITECH</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

  </aside>

</section>

<!-- end simple-page--node-1086.tpl.php -->