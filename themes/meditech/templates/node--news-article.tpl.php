<!-- START node--news-article.php -->
<?php
// This template is set up to control the display of the 'news-article' content type
// as well as the 'news-tags' taxonomy dynamic pages.
// 
// SECTION 1 describes the structure of the content when it's being displayed as an individual news article page.
// 
// SECTION 2 describes the structure of the content when it's being displayed in the main News page 
// OR as a list on a taxonomy page.


// get current URL info to determine which layout to render...
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$currentURLarray = explode('/', $currentURL);
$urlArrayCountMinusThree = count($currentURLarray) -3;
$urlArrayCountMinusTwo = count($currentURLarray) -2;
$urlArrayCountMinusOne = count($currentURLarray) -1;
$lastURLsegment = $currentURLarray[$urlArrayCountMinusOne];
$secondToLastURLsegment = $currentURLarray[$urlArrayCountMinusTwo];
$thirdToLastURLsegment = $currentURLarray[$urlArrayCountMinusThree];

include('inc-share-buttons.php');


// [SECTION 1] if a NEWS ARTICLE node is being shown then render the following ==========================
// ex: ../news/article-name
if($secondToLastURLsegment == 'news' || $thirdToLastURLsegment == 'news' || $secondToLastURLsegment == "node" || $thirdToLastURLsegment == "node"){

  include('inc--news-article.php');

}
else{
  // [SECTION 2] if the NEWS-TAGS taxonomy page is being shown then render the following ================
  // ex: ../news-tags/physician
  
  include('inc--news-article--taxonomy.php');
  
}
?>
<!-- END node--news-article.php -->