<!-- START campaign--node-2323.php MEDITECH Expanse -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

  <style>
    .header-tagline {
      padding-bottom: 8em;
    }

    @media all and (max-width: 68.750em) {
      .header-tagline {
        padding-bottom: 4em;
      }
    }

  </style>

  <div class="js__seo-tool__body-content">

    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/starry-sky-house-on-winter-hills.jpg);">
      <div class="container__centered">
        <div class="container">
          <img style="padding-left:2em; padding-right:2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-expanse-logo--green-white.png" alt="MEDITECH Expanse Logo">
        </div>
        <div class="container header-tagline center no-pad--top text--white text-shadow--black">
          <h1 class="js__seo-tool__title">One EHR, no limits. Welcome to the new vision of healthcare.</h1>
        </div>
      </div>
    </div>
    <!-- End Block 1 -->

    <!-- Block 2 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/a-new-level-of-clarity.jpg);">
      <div class="container__centered">
        <div class="container__one-third">
          <div class="transparent-overlay text--white" style="padding:2em;">
            <h2>A new level of clarity.</h2>
            <p><a href="https://ehr.meditech.com/videos/meditech-expanse-experience-the-difference">See healthcare through a whole new lens</a> with tools that help you navigate the digital frontier with confidence. Expanse reaches across the care continuum and pulls in all the data you need to see the big picture and <a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">treat the whole patient</a>. And with customizable layouts and widgets at your fingertips, you’ll be looking at everything you want to see (and nothing that you don’t).</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 2 -->

    <!-- Block 3 -->
    <div class="content__callout">
      <div class="content__callout__media">

        <div class="content__callout__image-wrapper">
          <div class="video js__video" data-video-id="283056884">
            <figure class="video__overlay">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--doug-kanis-expanse.jpg" alt="Putting the Physician First Expanse Video Covershot">
            </figure>
            <a class="video__play-btn" href="https://vimeo.com/283056884"></a>
            <div class="video__container"></div>
          </div>
        </div>
      </div>
      <div class="content__callout__content">
        <div class="content__callout__body">
          <div class="content__callout__body__text">
            <h2>Putting <span class="italic">you</span> first.</h2>
            <p>The best technologies add value to your life. Expanse prioritizes user satisfaction like no other EHR, and delivers a comfortable, personalized experience to make your day easier, and your work more productive. That means more face-to-face time with your patients, and more time for you to recharge outside the clinical setting.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 3 -->

    <!-- Block 4 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/break-down-the-walls.jpg);">
      <div class="container__centered">
        <div class="container__one-third text--white">
          <div class="transparent-overlay" style="padding:2em;">
            <h2>Break down the walls.</h2>
            <p>Healthcare doesn’t just happen in one place — you need an EHR to connect the dots. With MEDITECH Expanse, a single, <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">fully interoperable</a> system navigates the twists and turns of every care setting — so you can <a href="https://ehr.meditech.com/ehr-solutions/bridging-the-gaps-in-care">bridge gaps in care</a> and effectively treat the whole patient, wherever they may be in their journey. And with Virtual Visits and our <a href="https://ehr.meditech.com/ehr-solutions/to-improve-patient-engagement-focus-on-the-patient">Patient and Consumer Health Portal</a>, patients are never out of reach.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 4 -->

    <!-- Block 5 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/an-untethered-care-experience.jpg); background-position:top;">
      <div class="container__centered">
        <div class="container__two-thirds">
          <p></p>
        </div>
        <div class="container__one-third">
          <div class="transparent-overlay text--white" style="padding:2em;">
            <h2>An untethered care experience.</h2>
            <p>Why shouldn't your EHR be as easy and convenient as your favorite apps? Expanse takes full advantage of <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility">mobile technology</a>, untethering you from your desktop so you can tap and swipe through your charts more efficiently.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 5 -->

    <!-- Block 6 -->
    <div class="container bg--blue-gradient">
      <div class="container__centered">
        <div class="container__one-third">
          <div class="text--white">
            <h2>A keen eye for quality.</h2>
            <p>Steer clear of problems before they happen with tools that give you an extra set of eyes. Expanse’s <a href="https://info.meditech.com/meditechs-surveillance-identifies-and-prevents-infections-at-valley-case-study">surveillance solutions</a> provide you with meaningful alerts, so you can monitor at-risk patients and guard against HACs and life-threatening conditions. Meanwhile, our <a href="https://www.meditech.com/productbriefs/flyers/Quality_Vantage_Dashboard_Flyer.pdf" target="_blank">Quality Vantage Dashboards</a> assess performance at the individual, practice, and group level to detect costly, unnecessary variations in care.</p>
          </div>
        </div>
        <div class="container__two-thirds">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-ambulatory-quality-vantage.jpg" alt="MEDITECH Ambulatory Quality Vantage Screenshot">
        </div>
      </div>
    </div>
    <!-- End Block 6 -->

    <!-- Block 7 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/prove-your-value.jpg);">
      <div class="container__centered">
        <div class="container__two-thirds transparent-overlay text--white">
          <h2>Prove your value.</h2>
          <p>As the healthcare paradigm continues its shift from volume-based to value-based care, you need an EHR to help you make the transition. Expanse provides you with a host of tools like patient registries to <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">proactively manage patient populations </a>and keep up with new payment models. In addition, <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics</a> enables you to visualize data, monitor trends, and make more informed decisions to keep both your patients and your bottom line healthy.</p>
        </div>
        <div class="container__one-third">
          <p></p>
        </div>
      </div>
    </div>
    <!-- End Block 7 -->

    <!-- Block 8 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/vast-expanse-night-sky.jpg);">
      <div class="container__centered">
        <figure class="container__one-fourth center">
          <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/joe-farr.png" alt="Joe Farr, RN, Clinical Applications Coordinator head shot">
        </figure>
        <div class="container__three-fourths text--white">
          <div class="quote__content__text text--large" style="margin-top:1em;">
            <p class="italic">"Expanse Point of Care technology has generated more excitement than any nursing product to date! We’re confident it will solve our current logistical challenges and introduce new efficiencies for bedside providers, while still maintaining the highest patient safety standards."</p>
          </div>
          <p class="text--large no-margin--bottom"><span class="bold">Joe Farr, RN, Clinical Applications Coordinator</span></p>
          <p>King’s Daughters Medical Center</p>
        </div>
      </div>
    </div>
    <!-- End Block 8 -->

  </div>
  <!-- end js__seo-tool__body-content -->

  <!-- Block 9 - CTA Block -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/white-x-texture.jpg);">
    <div class="container__centered center">
      <img style="padding: 2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/expanse-logo--sm.png" alt="MEDITECH Expanse Logo">
      
      <?php cta_text($cta); ?>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Sign Up For A Webinar"); ?>
      </div>

      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
      
    </div>
  </div>
  <!-- End Block 9 -->

  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
  ?>

<!-- END campaign--node-2323.php -->